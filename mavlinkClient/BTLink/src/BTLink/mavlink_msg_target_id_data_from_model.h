#pragma once
// MESSAGE TARGET_ID_DATA_FROM_MODEL PACKING

#define MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL 35


typedef struct __mavlink_target_id_data_from_model_t {
 uint32_t time; /*<  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.*/
 float azimuth; /*<  .*/
 float azimuth_error; /*<  .*/
 float distance; /*<  .*/
 float distance_error; /*<  .*/
 float course; /*<  .*/
 float course_error; /*<  .*/
 float speed; /*<  .*/
 float speed_error; /*<  .*/
 float erp; /*<  .*/
 float erp_error; /*<  .*/
 uint8_t id; /*<  Target id.*/
} mavlink_target_id_data_from_model_t;

#define MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN 45
#define MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN 45
#define MAVLINK_MSG_ID_35_LEN 45
#define MAVLINK_MSG_ID_35_MIN_LEN 45

#define MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC 21
#define MAVLINK_MSG_ID_35_CRC 21



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_TARGET_ID_DATA_FROM_MODEL { \
    35, \
    "TARGET_ID_DATA_FROM_MODEL", \
    12, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_target_id_data_from_model_t, time) }, \
         { "id", NULL, MAVLINK_TYPE_UINT8_T, 0, 44, offsetof(mavlink_target_id_data_from_model_t, id) }, \
         { "azimuth", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_target_id_data_from_model_t, azimuth) }, \
         { "azimuth_error", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_target_id_data_from_model_t, azimuth_error) }, \
         { "distance", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_target_id_data_from_model_t, distance) }, \
         { "distance_error", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_target_id_data_from_model_t, distance_error) }, \
         { "course", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_target_id_data_from_model_t, course) }, \
         { "course_error", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_target_id_data_from_model_t, course_error) }, \
         { "speed", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_target_id_data_from_model_t, speed) }, \
         { "speed_error", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_target_id_data_from_model_t, speed_error) }, \
         { "erp", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_target_id_data_from_model_t, erp) }, \
         { "erp_error", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_target_id_data_from_model_t, erp_error) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_TARGET_ID_DATA_FROM_MODEL { \
    "TARGET_ID_DATA_FROM_MODEL", \
    12, \
    {  { "time", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_target_id_data_from_model_t, time) }, \
         { "id", NULL, MAVLINK_TYPE_UINT8_T, 0, 44, offsetof(mavlink_target_id_data_from_model_t, id) }, \
         { "azimuth", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_target_id_data_from_model_t, azimuth) }, \
         { "azimuth_error", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_target_id_data_from_model_t, azimuth_error) }, \
         { "distance", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_target_id_data_from_model_t, distance) }, \
         { "distance_error", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_target_id_data_from_model_t, distance_error) }, \
         { "course", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_target_id_data_from_model_t, course) }, \
         { "course_error", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_target_id_data_from_model_t, course_error) }, \
         { "speed", NULL, MAVLINK_TYPE_FLOAT, 0, 28, offsetof(mavlink_target_id_data_from_model_t, speed) }, \
         { "speed_error", NULL, MAVLINK_TYPE_FLOAT, 0, 32, offsetof(mavlink_target_id_data_from_model_t, speed_error) }, \
         { "erp", NULL, MAVLINK_TYPE_FLOAT, 0, 36, offsetof(mavlink_target_id_data_from_model_t, erp) }, \
         { "erp_error", NULL, MAVLINK_TYPE_FLOAT, 0, 40, offsetof(mavlink_target_id_data_from_model_t, erp_error) }, \
         } \
}
#endif

/**
 * @brief Pack a target_id_data_from_model message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param id  Target id.
 * @param azimuth  .
 * @param azimuth_error  .
 * @param distance  .
 * @param distance_error  .
 * @param course  .
 * @param course_error  .
 * @param speed  .
 * @param speed_error  .
 * @param erp  .
 * @param erp_error  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_target_id_data_from_model_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t time, uint8_t id, float azimuth, float azimuth_error, float distance, float distance_error, float course, float course_error, float speed, float speed_error, float erp, float erp_error)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, azimuth_error);
    _mav_put_float(buf, 12, distance);
    _mav_put_float(buf, 16, distance_error);
    _mav_put_float(buf, 20, course);
    _mav_put_float(buf, 24, course_error);
    _mav_put_float(buf, 28, speed);
    _mav_put_float(buf, 32, speed_error);
    _mav_put_float(buf, 36, erp);
    _mav_put_float(buf, 40, erp_error);
    _mav_put_uint8_t(buf, 44, id);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN);
#else
    mavlink_target_id_data_from_model_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.azimuth_error = azimuth_error;
    packet.distance = distance;
    packet.distance_error = distance_error;
    packet.course = course;
    packet.course_error = course_error;
    packet.speed = speed;
    packet.speed_error = speed_error;
    packet.erp = erp;
    packet.erp_error = erp_error;
    packet.id = id;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
}

/**
 * @brief Pack a target_id_data_from_model message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param id  Target id.
 * @param azimuth  .
 * @param azimuth_error  .
 * @param distance  .
 * @param distance_error  .
 * @param course  .
 * @param course_error  .
 * @param speed  .
 * @param speed_error  .
 * @param erp  .
 * @param erp_error  .
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_target_id_data_from_model_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t time,uint8_t id,float azimuth,float azimuth_error,float distance,float distance_error,float course,float course_error,float speed,float speed_error,float erp,float erp_error)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, azimuth_error);
    _mav_put_float(buf, 12, distance);
    _mav_put_float(buf, 16, distance_error);
    _mav_put_float(buf, 20, course);
    _mav_put_float(buf, 24, course_error);
    _mav_put_float(buf, 28, speed);
    _mav_put_float(buf, 32, speed_error);
    _mav_put_float(buf, 36, erp);
    _mav_put_float(buf, 40, erp_error);
    _mav_put_uint8_t(buf, 44, id);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN);
#else
    mavlink_target_id_data_from_model_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.azimuth_error = azimuth_error;
    packet.distance = distance;
    packet.distance_error = distance_error;
    packet.course = course;
    packet.course_error = course_error;
    packet.speed = speed;
    packet.speed_error = speed_error;
    packet.erp = erp;
    packet.erp_error = erp_error;
    packet.id = id;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
}

/**
 * @brief Encode a target_id_data_from_model struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param target_id_data_from_model C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_target_id_data_from_model_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_target_id_data_from_model_t* target_id_data_from_model)
{
    return mavlink_msg_target_id_data_from_model_pack(system_id, component_id, msg, target_id_data_from_model->time, target_id_data_from_model->id, target_id_data_from_model->azimuth, target_id_data_from_model->azimuth_error, target_id_data_from_model->distance, target_id_data_from_model->distance_error, target_id_data_from_model->course, target_id_data_from_model->course_error, target_id_data_from_model->speed, target_id_data_from_model->speed_error, target_id_data_from_model->erp, target_id_data_from_model->erp_error);
}

/**
 * @brief Encode a target_id_data_from_model struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param target_id_data_from_model C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_target_id_data_from_model_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_target_id_data_from_model_t* target_id_data_from_model)
{
    return mavlink_msg_target_id_data_from_model_pack_chan(system_id, component_id, chan, msg, target_id_data_from_model->time, target_id_data_from_model->id, target_id_data_from_model->azimuth, target_id_data_from_model->azimuth_error, target_id_data_from_model->distance, target_id_data_from_model->distance_error, target_id_data_from_model->course, target_id_data_from_model->course_error, target_id_data_from_model->speed, target_id_data_from_model->speed_error, target_id_data_from_model->erp, target_id_data_from_model->erp_error);
}

/**
 * @brief Send a target_id_data_from_model message
 * @param chan MAVLink channel to send the message
 *
 * @param time  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 * @param id  Target id.
 * @param azimuth  .
 * @param azimuth_error  .
 * @param distance  .
 * @param distance_error  .
 * @param course  .
 * @param course_error  .
 * @param speed  .
 * @param speed_error  .
 * @param erp  .
 * @param erp_error  .
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_target_id_data_from_model_send(mavlink_channel_t chan, uint32_t time, uint8_t id, float azimuth, float azimuth_error, float distance, float distance_error, float course, float course_error, float speed, float speed_error, float erp, float erp_error)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN];
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, azimuth_error);
    _mav_put_float(buf, 12, distance);
    _mav_put_float(buf, 16, distance_error);
    _mav_put_float(buf, 20, course);
    _mav_put_float(buf, 24, course_error);
    _mav_put_float(buf, 28, speed);
    _mav_put_float(buf, 32, speed_error);
    _mav_put_float(buf, 36, erp);
    _mav_put_float(buf, 40, erp_error);
    _mav_put_uint8_t(buf, 44, id);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL, buf, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
#else
    mavlink_target_id_data_from_model_t packet;
    packet.time = time;
    packet.azimuth = azimuth;
    packet.azimuth_error = azimuth_error;
    packet.distance = distance;
    packet.distance_error = distance_error;
    packet.course = course;
    packet.course_error = course_error;
    packet.speed = speed;
    packet.speed_error = speed_error;
    packet.erp = erp;
    packet.erp_error = erp_error;
    packet.id = id;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL, (const char *)&packet, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
#endif
}

/**
 * @brief Send a target_id_data_from_model message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_target_id_data_from_model_send_struct(mavlink_channel_t chan, const mavlink_target_id_data_from_model_t* target_id_data_from_model)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_target_id_data_from_model_send(chan, target_id_data_from_model->time, target_id_data_from_model->id, target_id_data_from_model->azimuth, target_id_data_from_model->azimuth_error, target_id_data_from_model->distance, target_id_data_from_model->distance_error, target_id_data_from_model->course, target_id_data_from_model->course_error, target_id_data_from_model->speed, target_id_data_from_model->speed_error, target_id_data_from_model->erp, target_id_data_from_model->erp_error);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL, (const char *)target_id_data_from_model, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
#endif
}

#if MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_target_id_data_from_model_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t time, uint8_t id, float azimuth, float azimuth_error, float distance, float distance_error, float course, float course_error, float speed, float speed_error, float erp, float erp_error)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, time);
    _mav_put_float(buf, 4, azimuth);
    _mav_put_float(buf, 8, azimuth_error);
    _mav_put_float(buf, 12, distance);
    _mav_put_float(buf, 16, distance_error);
    _mav_put_float(buf, 20, course);
    _mav_put_float(buf, 24, course_error);
    _mav_put_float(buf, 28, speed);
    _mav_put_float(buf, 32, speed_error);
    _mav_put_float(buf, 36, erp);
    _mav_put_float(buf, 40, erp_error);
    _mav_put_uint8_t(buf, 44, id);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL, buf, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
#else
    mavlink_target_id_data_from_model_t *packet = (mavlink_target_id_data_from_model_t *)msgbuf;
    packet->time = time;
    packet->azimuth = azimuth;
    packet->azimuth_error = azimuth_error;
    packet->distance = distance;
    packet->distance_error = distance_error;
    packet->course = course;
    packet->course_error = course_error;
    packet->speed = speed;
    packet->speed_error = speed_error;
    packet->erp = erp;
    packet->erp_error = erp_error;
    packet->id = id;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL, (const char *)packet, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_MIN_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_CRC);
#endif
}
#endif

#endif

// MESSAGE TARGET_ID_DATA_FROM_MODEL UNPACKING


/**
 * @brief Get field time from target_id_data_from_model message
 *
 * @return  Timestamp (UNIX Epoch time or time since system boot). The receiving end can infer timestamp format (since 1.1.1970 or since system boot) by checking for the magnitude of the number.
 */
static inline uint32_t mavlink_msg_target_id_data_from_model_get_time(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field id from target_id_data_from_model message
 *
 * @return  Target id.
 */
static inline uint8_t mavlink_msg_target_id_data_from_model_get_id(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  44);
}

/**
 * @brief Get field azimuth from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_azimuth(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field azimuth_error from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_azimuth_error(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field distance from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_distance(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field distance_error from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_distance_error(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field course from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_course(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  20);
}

/**
 * @brief Get field course_error from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_course_error(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Get field speed from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_speed(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  28);
}

/**
 * @brief Get field speed_error from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_speed_error(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  32);
}

/**
 * @brief Get field erp from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_erp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  36);
}

/**
 * @brief Get field erp_error from target_id_data_from_model message
 *
 * @return  .
 */
static inline float mavlink_msg_target_id_data_from_model_get_erp_error(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  40);
}

/**
 * @brief Decode a target_id_data_from_model message into a struct
 *
 * @param msg The message to decode
 * @param target_id_data_from_model C-struct to decode the message contents into
 */
static inline void mavlink_msg_target_id_data_from_model_decode(const mavlink_message_t* msg, mavlink_target_id_data_from_model_t* target_id_data_from_model)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    target_id_data_from_model->time = mavlink_msg_target_id_data_from_model_get_time(msg);
    target_id_data_from_model->azimuth = mavlink_msg_target_id_data_from_model_get_azimuth(msg);
    target_id_data_from_model->azimuth_error = mavlink_msg_target_id_data_from_model_get_azimuth_error(msg);
    target_id_data_from_model->distance = mavlink_msg_target_id_data_from_model_get_distance(msg);
    target_id_data_from_model->distance_error = mavlink_msg_target_id_data_from_model_get_distance_error(msg);
    target_id_data_from_model->course = mavlink_msg_target_id_data_from_model_get_course(msg);
    target_id_data_from_model->course_error = mavlink_msg_target_id_data_from_model_get_course_error(msg);
    target_id_data_from_model->speed = mavlink_msg_target_id_data_from_model_get_speed(msg);
    target_id_data_from_model->speed_error = mavlink_msg_target_id_data_from_model_get_speed_error(msg);
    target_id_data_from_model->erp = mavlink_msg_target_id_data_from_model_get_erp(msg);
    target_id_data_from_model->erp_error = mavlink_msg_target_id_data_from_model_get_erp_error(msg);
    target_id_data_from_model->id = mavlink_msg_target_id_data_from_model_get_id(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN? msg->len : MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN;
        memset(target_id_data_from_model, 0, MAVLINK_MSG_ID_TARGET_ID_DATA_FROM_MODEL_LEN);
    memcpy(target_id_data_from_model, _MAV_PAYLOAD(msg), len);
#endif
}

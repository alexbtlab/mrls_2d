
#ifndef __MRLS_UDP_H_
#define __MRLS_UDP_H_

	#include <stdio.h>
	#include "xgpiops.h"
	#include "xparameters.h"
	#include "assert.h"
	#include "netif/xadapter.h"
	#include "platform.h"
	#include "platform_config.h"
	#include "xil_printf.h"
	#include "lwip/tcp.h"
	#include "xil_cache.h"
	#include "lwip/dhcp.h"
	#include "lwip/udp.h"
	#include "xaxidma.h"
	#include "xstatus.h"
	#include "xil_io.h"
	#include "stdbool.h"
//	#include "HMC769.h"
	#include "xscugic.h"

#define FRAME_SIZE_DDR2PC 2048*4

int start_net();
int my_udp_server_init(void* buffer, size_t bufferlen);
void EthResetMIO7();
int transfer_data();
void UDPDebug(const char8 *str);
void UDPDebugNum(const char8 *str, u32 num);
void udp_recv_fn_callback(void *arg, struct udp_pcb *pcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);
void UDP_printf( char8 *format, ... );
void print_ip_settings(ip_addr_t *ip, ip_addr_t *mask, ip_addr_t *gw);

#endif //  __MRLS_UDP_H_




/***************************** Include Files *******************************/
#include "AD9650.h"

/************************** Function Definitions ***************************/

void AD9650_ADC_SetSwitch_AMP(uint32_t numAmpEnable){
                           Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR +4 , 0x1);
   if(numAmpEnable == 1)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0xA);
   if(numAmpEnable == 2)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0x30);
   if(numAmpEnable == 3)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0xF5);
   if(numAmpEnable == 4)   Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR, 0x3FF);
}

void AD9650_SetAdrData_andStartTransfer(u32 adr, u32 data, u8 stateRW){

	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR, adr | (data << 13));
	delay(1000);
	if(stateRW)
		Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 1);
	else
		Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 8 , 0);

	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 4 , 1);
	delay(10000);
	Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 4 , 0);
	delay(1000);

}
void AD9650_DIVIDE_CLOCK_10(){

	AD9650_SetAdrData_andStartTransfer		(AD9650_CHANNEL_INDEX_REG, 0b111, 	WRITE);
		AD9650_SetAdrData_andStartTransfer	(AD9650_CLOCK_DIVIDE_REG, 	7, 		WRITE);				// 1 - 40MHz 3 - 20MHz 7 - 10MHz
	AD9650_SetAdrData_andStartTransfer		(AD9650_DEVICE_UPDATE_REG, AD9650_SW_TRANSFER_BITREG, 	WRITE);
}


#include "main.h"

#define HMC769_ID_REG 0x0
#define HMC769_RST_Register_REG 0x1
#define HMC769_REFDIV_REG 0x2
#define HMC769_Frequency_Register_REG 0x3
#define HMC769_Frequency_Register_Fractional_Part_REG 0x4
#define HMC769_Seed_REG 0x5
#define HMC769_SD_CFG_REG 0x6
#define HMC769_Lock_Detect_REG 0x7
#define HMC769_Analog_EN_REG 0x8
#define HMC769_Charge_Pump_REG 0x9
#define HMC769_Modulation_Step_REG 0xA
#define HMC769_PD_REG 0xB
#define HMC769_ALTINT_REG 0xC
#define HMC769_ALTFRAC_REG 0xD
#define HMC769_SPI_TRIG_REG 0xE
#define HMC769_GPO_REG 0xF
#define HMC769_Reserve_REG 0x10
#define HMC769_Reserve2_REG 0x11
#define HMC769_GPO2_REG 0x12
#define HMC769_BIST_REG 0x13
#define HMC769_Lock_Detect_Timer_Status_REG 0x14

#define XPAR_HMC769_0_S00_AXI_BASEADDR XPAR_HIER_1_HMC769_0_S00_AXI_BASEADDR
#define SHIFT_FRONT_VAL(val) 	Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 28 , val);

#define DIS_SPI_CEN (0 << 2)
#define DIS_PAMP    (0 << 1)
#define DIS_ROW_PLL (0 << 0)
#define EN_SPI_CEN  (1 << 2)
#define EN_PAMP     (1 << 1)
#define EN_ROW_PLL  (1 << 0)

#define UARTLITE_DEVICE_ID	XPAR_UARTLITE_0_DEVICE_ID
#define TEST_BUFFER_SIZE 8
#define AD9650_DELAY_DCO_REG  0x17

#define IIC_COMPASS_SLAVE_ADDR		0x1E << 0
#define IIC_STM32_SLAVE_ADDR		9
#define IIC_SCLK_RATE		100000
#define IIC_DEVICE_ID		XPAR_XIICPS_0_DEVICE_ID

#define HMC5883l_Enable_A (0x10)
#define HMC5883l_Enable_B (0x00)
#define HMC5883l_MR (0x00)

#define SIZE_TRANSMIT_COMMAND_TO_ADPART 1

typedef enum configHMC{ config_HMC_MAIN120,
						config_HMC_MAIN240,
						config_HMC_MAIN_1MS_120,
						config_HMC_MAIN_1MS_240,
						config_HMC_SUB10
					   } tConfigHMC;

typedef enum{	MOTOR_SPEED_1_SEC,
				MOTOR_SPEED_1_5_SEC,
				MOTOR_SPEED_2_SEC
}tSpeedMotor;

char rxUartData[128];
int idx = 0;
char  str[128];


u8 regHMC5883 = 0x3;
u8   *  pregHMC5883 = &regHMC5883;
u8 regSTM32 = 0x2;
u8   *  pregSTM32 = &regSTM32;
u8 part_mem = 0;
uint8_t RegSettingA = HMC5883l_Enable_A;
uint8_t RegSettingB = HMC5883l_Enable_B;
uint8_t RegSettingMR = HMC5883l_MR;
uint32_t i = 0 ;
uint32_t g_delayDco = 0;
bool tmp;
size_t rxSize;

static bool uartRxDataDone = false;
static volatile size_t g_shiftFront = 1000;
static bool  g_stateTriger = false;

XScuGic INTCInst;
XAxiDma AxiDma;
XUartLite UartLite;
XUartPs UartPs;
XIicPs Iic;

ip_addr_t g_ipaddr, g_netmask, g_gw;

volatile int Error;
volatile int TxDone;
volatile int RxDone;

u8 SendBufferIIC[TEST_BUFFER_SIZE];	/* Buffer for Transmitting Data */
u8 RecvBufferIIC[TEST_BUFFER_SIZE];	/* Buffer for Receiving Data */
extern u8 SendBuffer[TEST_BUFFER_SIZE];	/* Buffer for Transmitting Data */
extern u8 RecvBuffer[TEST_BUFFER_SIZE];	/* Buffer for Receiving Data */

void AD9650_DELAY_DCO(bool direction, uint32_t step);
void trigerState( bool state );
u32  XUartPs_ReceiveBuffer(XUartPs *InstancePtr);
int i2c_init();
void compasInit();
void HMC769_2_write(u8 adrRegHMC, u32 dataSend);
u32 HMC769_2_read(u8 adrRegHMC);
int uartLiteInit();

// Param TX_atten_Param;
// Param HMC_useCable_Param;

void HMC769_2_init(){

    xil_printf("INFO: Start init AXI_HMC769--->\r\n");

    if (HMC769_2_read(HMC769_ID_REG) == 0x97370) // �������� �������������� ��
        xil_printf("INFO: init AXI_HMC769 completed \r\n");
    else
        xil_printf("ERROR: init AXI_HMC769 is fault\r\n");

    HMC769_2_write(HMC769_REFDIV_REG, 0x4); // �������� ������ ���������� SPI. ������ �������� 4 �������� ��������, ����� ������..

    if (HMC769_2_read(HMC769_REFDIV_REG) == 0x4) // �������� ����������� ��������
        xil_printf("INFO: compare value is completed\r\n");
    else
        xil_printf("ERROR: compare value is fault\r\n");
}
void HMC769_2_setRegAdr(u8 adr){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
    volatile unsigned int* slv_reg1 = hmcbaseaddr + 1;
    volatile unsigned int* slv_reg2 = hmcbaseaddr + 2;

    *slv_reg1 = adr;
    *slv_reg2 = adr;
}
void HMC769_2_startSend(){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
    (*hmcbaseaddr) = 0x1;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
void HMC769_2_startTrig(bool on){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
    (*hmcbaseaddr) = on ? 0x4 : 0;
}
void HMC769_2_setAtten(u8 val){

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
	volatile unsigned int* slv_reg4 = hmcbaseaddr + 4; // ����� 1-�� �������� ��� �������� ������ � IP ���� �� MB

	// TX_atten_Param = (Param){{val, PARAM_COUNT_TX_GROUP, 1, "TX_atten",  "TXGroup", PARAM_TYPE_UINT8}, setterATTEN,	getterATTEN};
	// addParam(&TX_atten_Param);

	*slv_reg4 = val;
}
void HMC769_2_waitReady(){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* ip2mb_reg0 = hmcbaseaddr + 8; // ����� 8-�� �������� ��� ������ ������ �� IP ���� � �� HMC769
    while ((*ip2mb_reg0) != 1) {
    }
}
u32 HMC769_2_getData(){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* ip2mb_reg1 = hmcbaseaddr + 9; // ����� 9-�� �������� ��� ������ ������ �� IP ���� � �� HMC769
    return *ip2mb_reg1;
}
void HMC769_2_setSendSata(u32 dataSend){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* slv_reg3 = hmcbaseaddr + 3; // ����� 3-�� �������� ��� �������� ������ � IP ���� �� MB
    *slv_reg3 = dataSend;
}
void HMC769_2_write(u8 adrRegHMC, u32 dataSend){

    HMC769_2_setRegAdr(adrRegHMC); // ��������� ������ �������� �� HMC769_2
    HMC769_2_setSendSata(dataSend); // ������������ ������ ��� �������� � �� HMC769
    HMC769_2_startSend(); // ������������ ������� ������ ������ ������ �� �� HMC769
    HMC769_2_waitReady(); // �������� ������� ready (��������� ������)
}
void HMC769_2_startReceive(){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR;
    (*hmcbaseaddr) = 0x2;
    for (u32 i = 0; i < 10000; i++) {
    }
    (*hmcbaseaddr) = 0x0;
}
u32 HMC769_2_read(u8 adrRegHMC){

	for(u64 i =0; i < 10000000; i++){}
    HMC769_2_setRegAdr(adrRegHMC);
    HMC769_2_startReceive();
    HMC769_2_waitReady();
    return HMC769_2_getData();
}
void HMC769_2_PWR_EN(uint8_t statePower){

    volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // ������� ����� �������� IP ���� �� HMC769
    volatile unsigned int* slv_reg5 = hmcbaseaddr + 5;
    *slv_reg5 = statePower;
}
void HMC769_2_setSweepVal2(u32 val){

	volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
	volatile unsigned int* slv_reg7 = hmcbaseaddr + 7; // Адрес 1-го регистра для передачи данных в IP ядро из MB

	*slv_reg7 = val; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
}
void HMC769_2_readBitMap(u8 adrRegHMC, void* rxReg){

//    u32 dataReg;
//    rxReg = &dataReg; // Указываем регистр dataReg принятых данных от МС, на необходимую структуру (void*)(созданную во время входа в функцию)
//                      // т.к. на каждый регистр принятых данных от МС есть своя структура
//
//    dataReg = HMC769_2_read(adrRegHMC); // Чтение данных по адресу u8 adrRegH из МС HMC769
//    xil_printf("\r\nHMC769 ADR_REG:%x DATA_REG:%x\r\n", adrRegHMC, *(u32*)rxReg);
//
//    switch (adrRegHMC) {
//    case HMC769_ID_REG:
//        xil_printf("chip_ID-%x\r\n", ((t_HMC769_ID_REG*)rxReg)->chip_ID);
//        break;
//    case HMC769_RST_Register_REG:
//        xil_printf("EnFromSPI-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnFromSPI);
//        xil_printf("EnKeepOns-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnKeepOns);
//        xil_printf("EnPinSel-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnPinSel);
//        xil_printf("EnSyncChpDis-%x\r\n", ((t_HMC769_RST_Register_REG*)rxReg)->EnSyncChpDis);
//        break;
//    case HMC769_REFDIV_REG:
//        xil_printf("rdiv:%x\r\n", ((t_HMC769_REFDIV_REG*)rxReg)->rdiv);
//        break;
//    case HMC769_Frequency_Register_REG:
//        xil_printf("intg-%x\r\n", ((t_HMC769_Frequency_Register_REG*)rxReg)->intg);
//        break;
//    case HMC769_Frequency_Register_Fractional_Part_REG:
//        xil_printf("frac-%x\r\n", ((t_HMC769_Frequency_Register_Fractional_Part_REG*)rxReg)->frac);
//        break;
//    case HMC769_Seed_REG:
//        xil_printf("SEED:%x\r\n", ((t_HMC769_Seed_REG*)rxReg)->SEED);
//        break;
//    case HMC769_SD_CFG_REG:
//        xil_printf("autoseed:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->autoseed);
//        xil_printf("BIST_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->BIST_Enable);
//        xil_printf("Disable_Reset:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Disable_Reset);
//        xil_printf("DSM_Clock_Source:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->DSM_Clock_Source);
//        xil_printf("External_Trigger_Enable:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->External_Trigger_Enable);
//        xil_printf("Force_DSM_Clock_n:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_DSM_Clock_n);
//        xil_printf("Force_RDIV_bypass:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Force_RDIV_bypass);
//        xil_printf("Invert_DSM_Clock:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Invert_DSM_Clock);
//        xil_printf("Modulator_Type:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Modulator_Type);
//        xil_printf("Number_of_Bist_Cycles:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Number_of_Bist_Cycles);
//        xil_printf("Reserved_0:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_0);
//        xil_printf("Reserved_1:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_1);
//        xil_printf("Reserved_7:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Reserved_7);
//        xil_printf("SD_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->SD_Mode);
//        xil_printf("Single_Step_Ramp_Mode:%x\r\n", ((t_HMC769_SD_CFG_REG*)rxReg)->Single_Step_Ramp_Mode);
//        break;
//    case HMC769_Lock_Detect_REG:
//        xil_printf("Cycle_Slip_Prevention_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Cycle_Slip_Prevention_Enable);
//        xil_printf("LKDCounts:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LKDCounts);
//        xil_printf("Lock_Detect_Timer_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Lock_Detect_Timer_Enable);
//        xil_printf("LockDetect_Counters_Enable:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->LockDetect_Counters_Enable);
//        xil_printf("Train_Lock_Detect_Timer:%x\r\n", ((t_HMC769_Lock_Detect_REG*)rxReg)->Train_Lock_Detect_Timer);
//        break;
//    case HMC769_Analog_EN_REG:
//        xil_printf("EnBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnBias);
//        xil_printf("EnCP:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnCP);
//        xil_printf("EnMcnt:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnMcnt);
//        xil_printf("EnOpAmp:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnOpAmp);
//        xil_printf("EnPFD:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPFD);
//        xil_printf("EnPS:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnPS);
//        xil_printf("EnVCO:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCO);
//        xil_printf("EnVCOBias:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnVCOBias);
//        xil_printf("EnXtal:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->EnXtal);
//        xil_printf("RFDiv2Sel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->RFDiv2Sel);
//        xil_printf("VCOBWSel:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOBWSel);
//        xil_printf("VCOOutBiasA:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasA);
//        xil_printf("VCOOutBiasB:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->VCOOutBiasB);
//        xil_printf("XtalDisSat:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalDisSat);
//        xil_printf("XtalLowGain:%x\r\n", ((t_HMC769_Analog_EN_REG*)rxReg)->XtalLowGain);
//        break;
//    case HMC769_Charge_Pump_REG:
//        xil_printf("CPHiK:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPHiK);
//        xil_printf("CPIdn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIdn);
//        xil_printf("CPIup:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPIup);
//        xil_printf("CPOffset:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPOffset);
//        xil_printf("CPSnkEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSnkEn);
//        xil_printf("CPSrcEn:%x\r\n", ((t_HMC769_Charge_Pump_REG*)rxReg)->CPSrcEn);
//        break;
//    case HMC769_Modulation_Step_REG:
//        xil_printf("MODSTEP:%x\r\n", ((t_HMC769_Modulation_Step_REG*)rxReg)->MODSTEP);
//        break;
//    case HMC769_PD_REG:
//        xil_printf("LKDProcTesttoCP:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->LKDProcTesttoCP);
//        xil_printf("McntClkGateSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->McntClkGateSel);
//        xil_printf("PFDDly:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDly);
//        xil_printf("PFDDnEN:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDDnEN);
//        xil_printf("PFDForceDn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceDn);
//        xil_printf("PFDForceMid:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceMid);
//        xil_printf("PFDForceUp:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDForceUp);
//        xil_printf("PFDInv:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDInv);
//        xil_printf("PFDShort:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDShort);
//        xil_printf("PFDUpEn:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PFDUpEn);
//        xil_printf("PSBiasSel:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->PSBiasSel);
//        xil_printf("VDIVExt:%x\r\n", ((t_HMC769_PD_REG*)rxReg)->VDIVExt);
//        break;
//    case HMC769_ALTINT_REG:
//        xil_printf("ALTINT:%x\r\n", ((t_HMC769_ALTINT_REG*)rxReg)->ALTINT);
//        break;
//    case HMC769_ALTFRAC_REG:
//        xil_printf("ALTFRAC:%x\r\n", ((t_HMC769_ALTFRAC_REG*)rxReg)->ALTFRAC);
//        break;
//    case HMC769_SPI_TRIG_REG:
//        xil_printf("SPITRIG:%x\r\n", ((t_HMC769_SPI_TRIG_REG*)rxReg)->SPITRIG);
//        break;
//    case HMC769_GPO_REG:
//        xil_printf("GPOAlways:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOAlways);
//        xil_printf("GPOOn:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOOn);
//        xil_printf("GPOPullDnDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullDnDis);
//        xil_printf("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
//        xil_printf("GPOPullUpDis:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOPullUpDis);
//        xil_printf("GPOTest:%x\r\n", ((t_HMC769_GPO_REG*)rxReg)->GPOTest);
//        break;
//    case HMC769_GPO2_REG:
//        xil_printf("GPO:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->GPO);
//        xil_printf("Lock_Detect:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Lock_Detect);
//        xil_printf("Ramp_Busy:%x\r\n", ((t_HMC769_GPO2_REG*)rxReg)->Ramp_Busy);
//        break;
//    case HMC769_BIST_REG:
//        xil_printf("BIST_Busy:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Busy);
//        xil_printf("BIST_Signature:%x\r\n", ((t_HMC769_BIST_REG*)rxReg)->BIST_Signature);
//        break;
//    case HMC769_Lock_Detect_Timer_Status_REG:
//        xil_printf("LkdSpeed:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdSpeed);
//        xil_printf("LkdTraining:%x\r\n", ((t_HMC769_Lock_Detect_Timer_Status_REG*)rxReg)->LkdTraining);
//        break;
//
//    default:
//        xil_printf("ERROR: no register with the given address u8 adrRegHMC\r\n");
//        break;
//    }
}
void HMC769_2_configIC(bool use_cable){

	if(use_cable)
		xil_printf("INFO: use_cable - ON  dF=240MHz \r\n");
	else
		xil_printf("INFO: use_cable - OFF dF=120MHz \r\n");


	// 970
    HMC769_2_write(0x01, 0x000002);
    HMC769_2_write(0x02, 0x000001);
    HMC769_2_write(0x03, 0x00001D);
    									HMC769_2_write(0x04, 1048576);
    HMC769_2_write(0x05, 0x000000);
    HMC769_2_write(0x07, 0x204865);
    HMC769_2_write(0x08, 0x036FFF);
    HMC769_2_write(0x09, 0x003264);

	                                    HMC769_2_write(0x0A, 81);
	HMC769_2_write(0x0B, 0x01E071);
	HMC769_2_write(0x0C, 0x00001D);
	                                    HMC769_2_write(0x0D, 7334176);

	HMC769_2_write(0x0E, 0x000000);
	HMC769_2_write(0x0F, 0x000001);
	HMC769_2_write(0x06, 0x001FBF);

//    HMC769_2_write(0x01, 0x000002);
//    HMC769_2_write(0x02, 0x000001);
//    HMC769_2_write(0x03, 0x00001D);
//    HMC769_2_write(0x04, 1048576);
//    HMC769_2_write(0x05, 0x000000);
//    HMC769_2_write(0x07, 0x204865);
//    HMC769_2_write(0x08, 0x036FFF);
//    HMC769_2_write(0x09, 0x003264);
//
//	if(use_cable){
//		HMC769_2_write(0x0A, 175);
//		HMC769_2_write(0x0B, 0x01E071);
//		HMC769_2_write(0x0C, 0x00001D);
//		HMC769_2_write(0x0D, 13648576);
//	}
//	else{
//		HMC769_2_write(0x0A, 70);
//		HMC769_2_write(0x0B, 0x01E071);
//		HMC769_2_write(0x0C, 0x00001D);
//		HMC769_2_write(0x0D, 7348576);
//    }
//	HMC769_2_write(0x0E, 0x000000);
//	HMC769_2_write(0x0F, 0x000001);
//	HMC769_2_write(0x06, 0x001FBF);
	//    T ramp 990
//	        HMC769_2_write(0x01, 0x000002);
//		    HMC769_2_write(0x02, 0x000001);
//		    HMC769_2_write(0x03, 0x00001D);
//		    									HMC769_2_write(0x04, 1048576);
//		    HMC769_2_write(0x05, 0x000000);
//		    HMC769_2_write(0x07, 0x204865);
//		    HMC769_2_write(0x08, 0x036FFF);
//		    HMC769_2_write(0x09, 0x003264);
//
//			                                    HMC769_2_write(0x0A, 79);
//			HMC769_2_write(0x0B, 0x01E071);
//			HMC769_2_write(0x0C, 0x00001D);
//			                                    HMC769_2_write(0x0D, 7305376);
//
//			HMC769_2_write(0x0E, 0x000000);
//			HMC769_2_write(0x0F, 0x000001);
//			HMC769_2_write(0x06, 0x001FBF);

	//    T ramp 980
//	    HMC769_2_write(0x01, 0x000002);
//	    HMC769_2_write(0x02, 0x000001);
//	    HMC769_2_write(0x03, 0x00001D);
//	    									HMC769_2_write(0x04, 1048576);
//	    HMC769_2_write(0x05, 0x000000);
//	    HMC769_2_write(0x07, 0x204865);
//	    HMC769_2_write(0x08, 0x036FFF);
//	    HMC769_2_write(0x09, 0x003264);
//
//		                                    HMC769_2_write(0x0A, 80);
//		HMC769_2_write(0x0B, 0x01E071);
//		HMC769_2_write(0x0C, 0x00001D);
//		                                    HMC769_2_write(0x0D, 7320576);
//
//		HMC769_2_write(0x0E, 0x000000);
//		HMC769_2_write(0x0F, 0x000001);
//		HMC769_2_write(0x06, 0x001FBF);



    xil_printf("INFO: HMC769 init completed \r\n");

    // HMC_useCable_Param   = (Param){{use_cable,  PARAM_COUNT_TX_GROUP, 3, "useCable", "TXGroup", PARAM_TYPE_BIT},		setterUseCable,	getterUseCable};
    // addParam(&HMC_useCable_Param);

    //HMC796_readAllReg();
}
void HMC769_2_configIC_sub10(){

	HMC769_2_init();


    								HMC769_2_write(0x01, 0x000002);
    								HMC769_2_write(0x02, 0x000001);
    HMC769_2_write(0x03, 0x00001D);
    HMC769_2_write(0x04, 524288);
    								HMC769_2_write(0x05, 0x000000);
									HMC769_2_write(0x07, 0x204865);
									HMC769_2_write(0x08, 0x036FFF);
									HMC769_2_write(0x09, 0x003264);
    HMC769_2_write(0x0A, 255);
    								HMC769_2_write(0x0B, 0x01E071);
	HMC769_2_write(0x0C, 0x00001D);
	HMC769_2_write(0x0D, 13124288);
									HMC769_2_write(0x0E, 0x000000);
									HMC769_2_write(0x0F, 0x000001);
									HMC769_2_write(0x06, 0x001FBF);

    xil_printf("INFO: HMC769 init completed \r\n");
}
void HMC769_2_setShiftFront(u16 val){

// volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
// volatile unsigned int* slv_reg6 = hmcbaseaddr + 6; // Адрес 1-го регистра для передачи данных в IP ядро из MB

// *slv_reg6 = val; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)

	g_shiftFront = val;

	int slv_reg7 = ( g_shiftFront  << 1 ) | g_stateTriger;
	Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 7*4, slv_reg7);

}
void HMC769_configIC_RT(tConfigHMC config){		// RealTimeConfig

	switch( config ){

	case config_HMC_MAIN120 :
//		trigerState( false );
			xil_printf("INFO_CRT: -------------> start configHMC..\r\n");
			HMC769_2_init();
			HMC769_2_configIC(false);
			xil_printf("INFO_CRT: -------------> configHMC done\r\n");
//		trigerState( true );

		break;
	case config_HMC_MAIN240 :
//		trigerState( false );
			xil_printf("INFO_CRT: -------------> start configHMC..\r\n");
			HMC769_2_init();
			HMC769_2_configIC(true);
			xil_printf("INFO_CRT: -------------> configHMC done\r\n");
//		trigerState( true );

		break;
	case config_HMC_MAIN_1MS_120: 			break;
	case config_HMC_MAIN_1MS_240: 			break;

	}

}
void cmdUartParse(char *str, size_t rxSize){


		     if(strncmp (rxUartData,"TRIG_EN\r\n",   rxSize) == 0)		trigerState( true );
		else if(strncmp (rxUartData,"TRIG_DIS\r\n",  rxSize) == 0)		trigerState( false );


		else if(strncmp (rxUartData,"SHINC\r\n",  rxSize) == 0)  	    {

			xil_printf("CMD: SHINC %d \r\n",     g_shiftFront);
			g_shiftFront+=10; HMC769_2_setShiftFront( g_shiftFront );
		}
		else if(strncmp (rxUartData,"SHDEC\r\n",  rxSize) == 0)  	    {

			xil_printf("CMD: SHDEC: %d \r\n",     g_shiftFront);
			g_shiftFront-=10; HMC769_2_setShiftFront( g_shiftFront );
		}

//		else if(strncmp (rxUartData,"sub10\r\n",  rxSize) == 0)				HMC769_2_configIC_sub10( 1 );
//		else if(strncmp (rxUartData,"HMC_MAIN240\r\n",  rxSize) == 0)		HMC769_configIC_RT( config_HMC_MAIN240 );

		else if(strncmp (rxUartData,"INCDCO\r\n",  rxSize) == 0)	    AD9650_DELAY_DCO( true, 1 );
		else if(strncmp (rxUartData,"DECDCO\r\n",  rxSize) == 0)		AD9650_DELAY_DCO( false, 1 );

//	else if(strncmp (rxUartData,"F0-91\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-91\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-92\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-93\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-94\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-95\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//
//	else if(strncmp (rxUartData,"SW-1\r\n",   rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"SW-15\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"SW-2\r\n",   rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//
//	else if(strncmp (rxUartData,"ST-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);
//	else if(strncmp (rxUartData,"F0-90\r\n",  rxSize) == 0)  	    xil_printf("cmdReceived: %s \r\n",     rxUartData);


	uartRxDataDone = false;

}
void HMC769_2_viewAllDataPLL(){

//    static t_HMC769_ID_REG* idReg;
//    static t_HMC769_RST_Register_REG* rstReg;
//    static t_HMC769_REFDIV_REG* refdivReg;
//    static t_HMC769_Frequency_Register_REG* freqReg;
//    static t_HMC769_Frequency_Register_Fractional_Part_REG* freqFracReg;
//    static t_HMC769_Seed_REG* seedReg;
//    static t_HMC769_SD_CFG_REG* sdCfgReg;
//    static t_HMC769_Lock_Detect_REG* lockDetReg;
//    static t_HMC769_Analog_EN_REG* AnEnReg;
//    static t_HMC769_Charge_Pump_REG* ChargePumpReg;
//    static t_HMC769_Modulation_Step_REG* modStepReg;
//    static t_HMC769_PD_REG* pdReg;
//    static t_HMC769_ALTINT_REG* altintReg;
//    static t_HMC769_ALTFRAC_REG* altFracReg;
//    static t_HMC769_SPI_TRIG_REG* spiTrigReg;
//    static t_HMC769_GPO_REG* gpoReg;
//    static t_HMC769_GPO2_REG* gpo2Reg;
//    static t_HMC769_BIST_REG* bistReg;
//    static t_HMC769_Lock_Detect_Timer_Status_REG* lockDetTimStatReg;
//
//    HMC769_readBitMap(HMC769_ID_REG, idReg);
//    HMC769_readBitMap(HMC769_RST_Register_REG, rstReg);
//    HMC769_readBitMap(HMC769_REFDIV_REG, refdivReg);
//    HMC769_readBitMap(HMC769_Frequency_Register_REG, freqReg);
//    HMC769_readBitMap(HMC769_Frequency_Register_Fractional_Part_REG, freqFracReg);
//    HMC769_readBitMap(HMC769_Seed_REG, seedReg);
//    HMC769_readBitMap(HMC769_SD_CFG_REG, sdCfgReg);
//    HMC769_readBitMap(HMC769_Lock_Detect_REG, lockDetReg);
//    HMC769_readBitMap(HMC769_Analog_EN_REG, AnEnReg);
//    HMC769_readBitMap(HMC769_Charge_Pump_REG, ChargePumpReg);
//    HMC769_readBitMap(HMC769_Modulation_Step_REG, modStepReg);
//    HMC769_readBitMap(HMC769_PD_REG, pdReg);
//    HMC769_readBitMap(HMC769_ALTINT_REG, altintReg);
//    HMC769_readBitMap(HMC769_ALTFRAC_REG, altFracReg);
//    HMC769_readBitMap(HMC769_SPI_TRIG_REG, spiTrigReg);
//    HMC769_readBitMap(HMC769_GPO_REG, gpoReg);
//    HMC769_readBitMap(HMC769_GPO2_REG, gpo2Reg);
//    HMC769_readBitMap(HMC769_BIST_REG, bistReg);
//    HMC769_readBitMap(HMC769_Lock_Detect_Timer_Status_REG, lockDetTimStatReg);
}
void trigerState( bool state ){

	g_stateTriger = state ? true : false;

	int slv_reg7 = ( g_shiftFront  << 1 ) | g_stateTriger;
	Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 7*4, slv_reg7);
}
void HMC769_2_setShiftFront_my(u16 val){

//	 volatile unsigned int* hmcbaseaddr = (unsigned int*)XPAR_HMC769_0_S00_AXI_BASEADDR; // Базовый адрес регистра IP ядра МС HMC769
//	 volatile unsigned int* slv_reg7 = hmcbaseaddr + 7; // Адрес 1-го регистра для передачи данных в IP ядро из MB

//	 *slv_reg7 = ( val << 1 ) | g_stateTriger; // Запись адреса регистра МС HMC769, в регистр IP ядра (slv_reg2 - адреса регистра МС для приема)
	g_shiftFront = val;
	int slv_reg7 = ( g_shiftFront << 1 ) | g_stateTriger;
    Xil_Out32(XPAR_HMC769_0_S00_AXI_BASEADDR + 28 , slv_reg7);
}
void delay(u32 delayVal){
	while(delayVal--){}
}
void amp_en(u8 num_amp_in_receiver){

	if(num_amp_in_receiver  == 2)		Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR + 8, 1);
	if(num_amp_in_receiver  == 3)		Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR + 8, 3);
	if(num_amp_in_receiver  == 4)		Xil_Out32(XPAR_SWITCHMICROWAVEPATH_0_S00_AXI_BASEADDR + 8, 7);
}
void AD9650_DELAY_DCO(bool direction, uint32_t step){

	if( direction )
		g_delayDco += 1*step;
	else
		g_delayDco -= 1*step;

	xil_printf("CMD: delay DCO: %d\r\n", g_delayDco);

	AD9650_SetAdrData_andStartTransfer		(AD9650_CHANNEL_INDEX_REG, 0b111, 	WRITE);
		AD9650_SetAdrData_andStartTransfer	(AD9650_DELAY_DCO_REG, 	   g_delayDco, 		WRITE);				// 1 - 40MHz 3 - 20MHz 7 - 10MHz
	AD9650_SetAdrData_andStartTransfer		(AD9650_DEVICE_UPDATE_REG, AD9650_SW_TRANSFER_BITREG, 	WRITE);
}
int ADPART_runMotor(){

	unsigned int SentCount;
	SendBuffer[0] = 1;

	SentCount = XUartLite_Send(&UartLite, SendBuffer, 1);
	if (SentCount != TEST_BUFFER_SIZE) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
int ADPART_stopMotor(){

	unsigned int SentCount;
	SendBuffer[0] = 0;
	SentCount = XUartLite_Send(&UartLite, SendBuffer, 1);
	if (SentCount != TEST_BUFFER_SIZE) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
int ADPART_setSpeedMotor( tSpeedMotor speed ){

	unsigned int SentCount;

	if ( speed == MOTOR_SPEED_1_SEC )    SendBuffer[0] = 6;
	if ( speed == MOTOR_SPEED_1_5_SEC )  SendBuffer[0] = 7;
	if ( speed == MOTOR_SPEED_2_SEC )    SendBuffer[0] = 8;

	SentCount = XUartLite_Send(&UartLite, SendBuffer, 1);
	if (SentCount != TEST_BUFFER_SIZE) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}
int uartLiteInit(){

	int Status;

//		unsigned int ReceivedCount = 0;
//		int Index;

		Status = XUartLite_Initialize(&UartLite, UARTLITE_DEVICE_ID);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		Status = XUartLite_SelfTest(&UartLite);
		if (Status != XST_SUCCESS) {
			return XST_FAILURE;
		}
		return XST_SUCCESS;
}
void Process_mrls(){

	SendRawDataToPC();
	xemacif_input(echo_netif);
//	transfer_data();

	if( uartRxDataDone ){

		cmdUartParse(rxUartData, rxSize);

	}
}
void UartPSRxIntrHandler(void *CallBackRef, u32 Event, unsigned int EventData)
{
	/* All of the data has been sent */
	rxSize = EventData;

//	int data;// = XUartPs_ReceiveBuffer(&UartPs);
	UartPs.ReceiveBuffer.NextBytePtr = RecvBuffer;
//	int data2 = RecvBuffer[0];

	uartRxDataDone = true;

	for(size_t i = 0; i < rxSize; i++)
		rxUartData[i] = RecvBuffer[i];

	rxUartData[rxSize] = 0;

	XUartPs_Recv(&UartPs, RecvBuffer, 128);




//	xil_printf("XUARTPS_EVENT_RECV_DATA%d\r\n", rxSize);

	/* All of the data has been received */
	if (Event == XUARTPS_EVENT_RECV_DATA) {




//		xil_printf("XUARTPS_EVENT_RECV_DATA%d\r\n", rxSize);



//		xil_printf("XUARTPS_EVENT_RECV_2DATA%d\r\n", data);
//		int data = RecvBuffer[0];


//		UartPs.ReceiveBuffer.NextBytePtr = BufferPtr;

//		if( RecvBuffer[0] != '\r'){
//			XUartPs_Recv(&UartPs, RecvBuffer, sizeof(char));
//
//			rxData[idx] = RecvBuffer[0];
//		}
//		else{
//			uartRxDataDone = true;
//			idx = 0;
//		}		idx++;




//		xil_printf("%d\r\n",  XUartPs_ReceiveBuffer(&UartPs));
//		xil_printf("%d\r\n",  EventData);
//
//		xil_printf("%d\r\n",  ( UartPs.ReceiveBuffer.RemainingBytes ));

//		u32 ReceivedCount = XUartPs_Recv(&UartPs, dfd, 64); //read the data available in UART receive buffer
////		UartPs->ReceiveBuffer
////		XUartPs_Recv(UartPs, RecvBuffer, TEST_BUFFER_SIZE);
////		u32 ReceivedCount = XUartPs_ReceiveBuffer(&UartPs);
//		xil_printf("%s\r\n", dfd);
	}

	/*
	 * Data was received, but not the expected number of bytes, a
	 * timeout just indicates the data stopped for 8 character times
	 */
	if (Event == XUARTPS_EVENT_RECV_TOUT) {
//		xil_printf("XUARTPS_EVENT_RECV_TOUT:%d\r\n", data);

//		rxSize = XUartPs_ReceiveBuffer(&UartPs);
//		RecvBuffer

	}

	/*
	 * Data was received with an error, keep the data but determine
	 * what kind of errors occurred
	 */
	if (Event == XUARTPS_EVENT_RECV_ERROR) {
		xil_printf("XUARTPS_EVENT_RECV_ERROR\r\n");
	}

	/*
	 * Data was received with an parity or frame or break error, keep the data
	 * but determine what kind of errors occurred. Specific to Zynq Ultrascale+
	 * MP.
	 */
	if (Event == XUARTPS_EVENT_PARE_FRAME_BRKE) {
		xil_printf("XUARTPS_EVENT_PARE_FRAME_BRKE\r\n");
	}

	/*
	 * Data was received with an overrun error, keep the data but determine
	 * what kind of errors occurred. Specific to Zynq Ultrascale+ MP.
	 */
	if (Event == XUARTPS_EVENT_RECV_ORERR) {
		xil_printf("XUARTPS_EVENT_RECV_ORERR\r\n");
	}
}
void DmaRxIntrHandler(void *Callback)
{
//	static uint64_t cnt_turnover = 0;
//	static uint32_t* mem_area_for_azimut[];


	u32 IrqStatus;
	int TimeOut;
	XAxiDma *AxiDmaInst = (XAxiDma *)Callback;
	/* Read pending interrupts */
	IrqStatus = XAxiDma_IntrGetIrq(AxiDmaInst, XAXIDMA_DEVICE_TO_DMA);
	/* Acknowledge pending interrupts */
	XAxiDma_IntrAckIrq(AxiDmaInst, IrqStatus, XAXIDMA_DEVICE_TO_DMA);
	/*
	 * If no interrupt is asserted, we do not do anything
	 */
	if (!(IrqStatus & XAXIDMA_IRQ_ALL_MASK)) {
		return;
	}
    if(mem_empty){
		if(part_mem == 0){
			part_mem = 1;
			praw = (int32_t *)RX_BUFFER_BASE4;			//MEMCPY(raw , (u32 *)0x80070000, FRAME_SIZE*4);
			XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) praw,	MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);
		}
		else{
			part_mem = 0;
			praw = (int32_t *)RX_BUFFER_BASE4;			//MEMCPY(raw ,  (u32 *)0x80080000, FRAME_SIZE*4);
			XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) praw,	MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);
		}
		mem_empty = false;
    }



	/*
	 * If error interrupt is asserted, raise error flag, reset the
	 * hardware to recover from the error, and return with no further
	 * processing.
	 */
	if ((IrqStatus & XAXIDMA_IRQ_ERROR_MASK)) {
		Error = 1;

		/* Reset could fail and hang
		 * NEED a way to handle this or do not call it??
		 */
		XAxiDma_Reset(AxiDmaInst);
		TimeOut = RESET_TIMEOUT_COUNTER;

		xil_printf( "ERR: DMA IrqStatus is not Done\r\n" );

		while (TimeOut) {
			if(XAxiDma_ResetIsDone(AxiDmaInst)) {
				break;
			}
			TimeOut -= 1;
		}
		return;
	}
	/*
	 * If completion interrupt is asserted, then set RxDone flag
	 */
	if ((IrqStatus & XAXIDMA_IRQ_IOC_MASK)) {
		RxDone = 1;
	}
}
int i2c_init(){

	int Status;

		xil_printf("IIC Master Polled Example Test \r\n");

			XIicPs_Config *Config;
//			int Index;

			Config = XIicPs_LookupConfig(IIC_DEVICE_ID);
			if (NULL == Config) {
				return XST_FAILURE;
			}

			Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			Status = XIicPs_SelfTest(&Iic);
			if (Status != XST_SUCCESS) {
				return XST_FAILURE;
			}

			XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);

//			for (Index = 0; Index < TEST_BUFFER_SIZE; Index++) {
//				SendBuffer[Index] = (Index % TEST_BUFFER_SIZE);
//				RecvBuffer[Index] = 0;
//			}
			SendBufferIIC[0] = 0x6;

			return XST_SUCCESS;
}
void HMC5883L_Initialize()
{
    // write CONFIG_A register

    uint8_t tmp = (HMC5883L_AVERAGING_8 << (HMC5883L_CRA_AVERAGE_BIT - HMC5883L_CRA_AVERAGE_LENGTH + 1))
            | (HMC5883L_RATE_15 << (HMC5883L_CRA_RATE_BIT - HMC5883L_CRA_RATE_LENGTH + 1))
            | (HMC5883L_BIAS_NORMAL << (HMC5883L_CRA_BIAS_BIT - HMC5883L_CRA_BIAS_LENGTH + 1));

    XIicPs_MasterSendPolled(&Iic, &tmp, 2, IIC_COMPASS_SLAVE_ADDR);


    XIicPs_MasterSendPolled(&Iic, &tmp, 2, IIC_COMPASS_SLAVE_ADDR);
//    XIicPs_MasterSendPolled(&Iic, &tmp, TEST_BUFFER_SIZE, IIC_SLAVE_ADDR);


//    HMC5883L_I2C_ByteWrite(HMC5883L_DEFAULT_ADDRESS, &tmp, HMC5883L_RA_CONFIG_A);

    // write CONFIG_B register
//    HMC5883L_SetGain(HMC5883L_GAIN_1090);

    // write MODE register
//    HMC5883L_SetMode(HMC5883L_MODE_SINGLE);
}
bool HMC5883L_GetReadyStatus()
{

//    HMC5883L_ReadBit(HMC5883L_DEFAULT_ADDRESS, HMC5883L_RA_STATUS, HMC5883L_STATUS_READY_BIT, &tmp);

	u8 tmpAdrReg = 9;

    XIicPs_MasterSendPolled(&Iic, &tmpAdrReg, 1, IIC_COMPASS_SLAVE_ADDR);
    while (XIicPs_BusIsBusy(&Iic)) {}
    XIicPs_MasterRecvPolled(&Iic, RecvBufferIIC,  1, IIC_COMPASS_SLAVE_ADDR);
    while (XIicPs_BusIsBusy(&Iic)) {}


//    for(uint32_t i =0 ; i < 20000; i++){}

    return tmp ==  ( RecvBufferIIC[0] & 0x1  ) ? TRUE : FALSE;
}
void HMC5883_readStatusReg(){

	u8 tmpAdrReg;
	tmpAdrReg = HMC5883L_RA_STATUS;
	XIicPs_MasterSendPolled(&Iic, &tmpAdrReg, 1, IIC_COMPASS_SLAVE_ADDR);
	while (XIicPs_BusIsBusy(&Iic)) {  }
	XIicPs_MasterRecvPolled(&Iic, RecvBufferIIC,  1, IIC_COMPASS_SLAVE_ADDR);

	xil_printf("reg9 RDY:%x\r\n",  ( RecvBufferIIC[0] & 0x1 ));
	xil_printf("reg9 LOCK:%x\r\n", ( RecvBufferIIC[0] & 0x2 ) >> 1);
	xil_printf("reg9 REN:%x\r\n",  ( RecvBufferIIC[0] & 0x4 ) >> 2);

}
int HMC5883_readID(){

	u8 tmpAdrReg;
	tmpAdrReg = HMC5883L_RA_ID_A;
	XIicPs_MasterSendPolled(&Iic, &tmpAdrReg, 1, IIC_COMPASS_SLAVE_ADDR);
	while (XIicPs_BusIsBusy(&Iic)) {  }
	XIicPs_MasterRecvPolled(&Iic, RecvBufferIIC,  3, IIC_COMPASS_SLAVE_ADDR);

	xil_printf("ID_1:%x\r\n", RecvBufferIIC[0]);
	xil_printf("ID_2:%x\r\n", RecvBufferIIC[1]);
	xil_printf("ID_3:%x\r\n", RecvBufferIIC[2]);


	return ( RecvBufferIIC[0] == 0x48 &&
		     RecvBufferIIC[1] == 0x34 &&
			 RecvBufferIIC[2] == 0x33 ) ? true : false;
}
void HMC_setRegA(){

	u8 dataTX[2];
	dataTX[0] = HMC5883L_RA_CONFIG_A;
	dataTX[1] = RegSettingA;

	    XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_COMPASS_SLAVE_ADDR);
	    while (XIicPs_BusIsBusy(&Iic)) {}
}
void HMC_setRegB(){

	u8 dataTX[2];
	dataTX[0] = HMC5883L_RA_CONFIG_B;
	dataTX[1] = RegSettingB;

		XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_COMPASS_SLAVE_ADDR);
		while (XIicPs_BusIsBusy(&Iic)) {}
}
void HMC_setRegMR(){

	u8 dataTX[2];
		dataTX[0] = HMC5883L_RA_MODE;
		dataTX[1] = RegSettingMR;

    XIicPs_MasterSendPolled(&Iic, dataTX, 2, IIC_COMPASS_SLAVE_ADDR);
    while (XIicPs_BusIsBusy(&Iic)) {}
}
void compasProcces(){


	double angelRadian;
	double angelDegree;

	regHMC5883 = 3;

		XIicPs_MasterSendPolled(&Iic, pregHMC5883, 1, IIC_COMPASS_SLAVE_ADDR);
		while (XIicPs_BusIsBusy(&Iic)) {  }


		XIicPs_MasterRecvPolled(&Iic, RecvBufferIIC,  6, IIC_COMPASS_SLAVE_ADDR);

		int16_t X = ( RecvBufferIIC[0] << 8 ) | ( RecvBufferIIC[1] );
		int16_t Y = ( RecvBufferIIC[4] << 8 ) | ( RecvBufferIIC[5] );
		int16_t Z = ( RecvBufferIIC[2] << 8 ) | ( RecvBufferIIC[3] );

		angelRadian = atan2(X, Y);
	    angelDegree = ( angelRadian * 180 ) / 3.14;

	//    if ( X > 0 )
	    	angelDegree += 180.0;

	    printf(" angelDegree:%f   \r\n",  angelDegree);
		xil_printf("X:%4d   Y:%4d    Z:%4d  \r\n", X, Y, Z);

		for(uint32_t i = 0 ; i < 50000000; i++){}
}
void compasInit(){

	HMC_setRegA();
	HMC_setRegB();
	HMC_setRegMR();
}
void proccesCommSTM32(){

	XIicPs_MasterSendPolled(&Iic, pregSTM32, 1,9);
		while (XIicPs_BusIsBusy(&Iic)) {  }
		i++;
		if ( i == 20)
			while(1);
		for(uint32_t i = 0 ; i < 5000; i++){}

}
void setSpeedMotor( tSpeedMotor speed ){

	if ( speed == MOTOR_SPEED_1_SEC )	regSTM32 = MOTOR_SPEED_1_SEC;
	if ( speed == MOTOR_SPEED_1_5_SEC )	regSTM32 = MOTOR_SPEED_1_5_SEC;
	if ( speed == MOTOR_SPEED_2_SEC )	regSTM32 = MOTOR_SPEED_2_SEC;

	XIicPs_MasterSendPolled(&Iic, pregSTM32, SIZE_TRANSMIT_COMMAND_TO_ADPART,  IIC_STM32_SLAVE_ADDR);
	while (XIicPs_BusIsBusy(&Iic)) {  }
}
void app(){

	xil_printf("mrls_2D_v1.2\r\n");
	UDP_printf("mrls_2D_v1.2\r\n");

//	i2c_init();
//	uartLiteInit();
//	compasInit();

			Xil_Out32(XPAR_AD9650_0_S00_AXI_BASEADDR + 12,  ( 284 ));	// долбим сюда размер фрейма для ФФТ и максимальное количество азимутов которое будет приходить на ПК
			Xil_Out32(XPAR_HIER_1_AVERAGEFFT_0_S00_AXI_BASEADDR, 8);  // на каком фрейме устанавливаем прерывание по фронту ноги

//			Xil_Out32(XPAR_GPIO_0_BASEADDR, 1);
//			Xil_Out32(XPAR_GPIO_1_BASEADDR, 3);

			HMC769_2_PWR_EN( EN_SPI_CEN |DIS_PAMP | EN_ROW_PLL );

				AD9650_SetAdrData_andStartTransfer(AD9650_CHANNEL_INDEX_REG, 	0b11, 	WRITE);
				AD9650_SetAdrData_andStartTransfer(AD9650_CLOCK_REG, 			AD9650_DUTY_CYCLE_STABILIZE_BITREG, 	WRITE);
				AD9650_SetAdrData_andStartTransfer(AD9650_DEVICE_UPDATE_REG,	AD9650_SW_TRANSFER_BITREG, 	WRITE);

				AD9650_SetAdrData_andStartTransfer(AD9650_CHANNEL_INDEX_REG, 	0b11, 	WRITE);
				AD9650_SetAdrData_andStartTransfer(AD9650_SYNC_CONTROL_REG,  	0b011, 	WRITE);
				AD9650_SetAdrData_andStartTransfer(AD9650_DEVICE_UPDATE_REG, 	AD9650_SW_TRANSFER_BITREG, 	WRITE);
				AD9650_DIVIDE_CLOCK_10();

//			HMC769_PWR_EN( EN_SPI_CEN |EN_PAMP | EN_ROW_PLL );
				HMC769_2_setAtten( ATTEN_VAL );
				AD9650_ADC_SetSwitch_AMP( NUM_AMP_RECIVER );     // 1, 2, 3, 4

				HMC769_2_init();

//				HMC769_2_configIC_sub10();
				HMC769_2_configIC(1);
//				HMC796_readAllReg();

		    HMC769_2_setShiftFront( g_shiftFront );
		    SWEEP_VAL( 9900 )

			amp_en(4);   // 2, 3 , 4

			AD9650_SetAdrData_andStartTransfer(0x100, 0b000, 	WRITE);
			AD9650_SetAdrData_andStartTransfer(0xFF, 0b1, 		WRITE);

			PWDN_RESET
}

int main(){

	EthResetMIO7();
	start_net();
	my_udp_server_init(praw, sizeof(raw));
	app();

	UDP_printf("IP_ADDR:  %d.%d.%d.%d ", ip4_addr1(&g_ipaddr),  ip4_addr2(&g_ipaddr),  ip4_addr3(&g_ipaddr),  ip4_addr4(&g_ipaddr));
	UDP_printf("NM_ADDR:  %d.%d.%d.%d ", ip4_addr1(&g_netmask), ip4_addr2(&g_netmask), ip4_addr3(&g_netmask), ip4_addr4(&g_netmask));
	UDP_printf("GW_ADDR:  %d.%d.%d.%d ", ip4_addr1(&g_gw),      ip4_addr2(&g_gw),      ip4_addr3(&g_gw),      ip4_addr4(&g_gw));

	UDP_printf("Auto Calibration Azimuth start...");

//	while( AUTO_CALIBRATION_WAIT )
//						xemacif_input(echo_netif);

	u32 maxAzimuth = Xil_In32(XPAR_AD9650_0_S00_AXI_BASEADDR + 24);

	UDP_printf("Auto Calibration Azimuth done maxAzimuth:%d ", maxAzimuth);

	if( maxAzimuth != 286)		UDP_printf("ERROR: Auto Calibration Err maxAzimuth != 286 ");
	else                 		UDP_printf("INFO:  Auto Calibration OK  maxAzimuth = 286 ");

//		XUartPs_Recv(&UartPs, RecvBuffer, 128);

	xil_printf("INFO: XAxiDma_SimpleTransfer. ProgrammExecuting...\r\n");
	XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) praw,	MAX_PKT_LEN, XAXIDMA_DEVICE_TO_DMA);
	trigerState( true );

	while(true){
       Process_mrls();
	}

	cleanup_platform();
	return 0;
}

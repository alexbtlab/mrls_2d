
#ifndef __MRLS_PARAMETERS_H_
#define __MRLS_PARAMETERS_H_

#include "lwip/udp.h"
#include "mavlink_parser/mavlink_parser.h"

#define MAX_NUM_PARAM 9
#define SYS_ID 55
#define COMPONENT_ID 0
#define VERSION 211

typedef void (*paramFuncSet)(void *);
typedef void (*paramFuncGet)(void *);

struct Param {
	mavlink_param_value_t param;
	paramFuncSet setter;
	paramFuncGet getter;
};
typedef enum{
	ATTEN,
	PAMP_state,
	ADC_MODE_PDn,
	ETH_REF_CLK_EN_and_RST,
	PLL_ROW_EN,
	RECIEVER_NUM_AMP
}param_t;
typedef enum{
	ON,
	OFF
}stateConfigReciever_t;

void setParam(mavlink_param_value_t *newParam) ;
void getParam(mavlink_param_value_t *Param, void* data);

#endif //  __MRLS_PARAMETERS_H_


// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Mon Nov 29 09:50:53 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_HMC769_0_0_sim_netlist.v
// Design      : design_1_HMC769_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v4_0
   (spi_pll_cen,
    ATTEN,
    PLL_POW_EN,
    PAMP_EN,
    s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    s00_axi_rdata,
    frame_even,
    pll_trig,
    start_adc_count,
    spi_pll_sck,
    s00_axi_rvalid,
    spi_pll_sen,
    spi_pll_mosi,
    s00_axi_bvalid,
    spi_pll_ld_sdo,
    clk_10MHz,
    s00_axi_aclk,
    clkDCO_10MHz,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_aresetn,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_bready,
    s00_axi_rready);
  output spi_pll_cen;
  output [5:0]ATTEN;
  output PLL_POW_EN;
  output PAMP_EN;
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output [31:0]s00_axi_rdata;
  output frame_even;
  output pll_trig;
  output start_adc_count;
  output spi_pll_sck;
  output s00_axi_rvalid;
  output spi_pll_sen;
  output spi_pll_mosi;
  output s00_axi_bvalid;
  input spi_pll_ld_sdo;
  input clk_10MHz;
  input s00_axi_aclk;
  input clkDCO_10MHz;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input s00_axi_aresetn;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_bready;
  input s00_axi_rready;

  wire [5:0]ATTEN;
  wire HMC769_v4_0_S00_AXI_inst_n_1;
  wire HMC769_v4_0_S00_AXI_inst_n_15;
  wire HMC769_v4_0_S00_AXI_inst_n_16;
  wire HMC769_v4_0_S00_AXI_inst_n_17;
  wire HMC769_v4_0_S00_AXI_inst_n_18;
  wire HMC769_v4_0_S00_AXI_inst_n_19;
  wire HMC769_v4_0_S00_AXI_inst_n_20;
  wire HMC769_v4_0_S00_AXI_inst_n_21;
  wire HMC769_v4_0_S00_AXI_inst_n_22;
  wire HMC769_v4_0_S00_AXI_inst_n_23;
  wire HMC769_v4_0_S00_AXI_inst_n_24;
  wire HMC769_v4_0_S00_AXI_inst_n_25;
  wire HMC769_v4_0_S00_AXI_inst_n_26;
  wire HMC769_v4_0_S00_AXI_inst_n_27;
  wire HMC769_v4_0_S00_AXI_inst_n_39;
  wire HMC769_v4_0_S00_AXI_inst_n_56;
  wire HMC769_v4_0_S00_AXI_inst_n_57;
  wire HMC769_v4_0_S00_AXI_inst_n_6;
  wire HMC769_v4_0_S00_AXI_inst_n_7;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire clkDCO_10MHz;
  wire clk_10MHz;
  wire frame_even;
  wire [23:1]ip2mb_reg1;
  wire pll_trig;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [3:0]sel0;
  wire [11:0]shift_front;
  wire [2:0]slv_reg0;
  wire [5:0]slv_reg4;
  wire [2:0]slv_reg5;
  wire [2:0]\spi_hmc_mode_RX_inst/cnt_reg ;
  wire [4:0]\spi_hmc_mode_TX_inst/cnt_reg ;
  wire spi_pll_cen;
  wire spi_pll_ld_sdo;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_adc_count;
  wire [15:0]sweep_val;
  wire top_PLL_control_i_n_30;
  wire [15:0]trig_tsweep_counter_reg;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v4_0_S00_AXI HMC769_v4_0_S00_AXI_inst
       (.O({HMC769_v4_0_S00_AXI_inst_n_56,HMC769_v4_0_S00_AXI_inst_n_57}),
        .Q({shift_front[11:9],shift_front[5:3],shift_front[0]}),
        .S({HMC769_v4_0_S00_AXI_inst_n_6,HMC769_v4_0_S00_AXI_inst_n_7}),
        .\axi_araddr_reg[4]_0 (HMC769_v4_0_S00_AXI_inst_n_39),
        .\axi_araddr_reg[5]_0 ({sel0[3],sel0[0]}),
        .axi_arready_reg_0(s00_axi_arready),
        .axi_awready_reg_0(s00_axi_awready),
        .\axi_rdata_reg[0]_0 (top_PLL_control_i_n_30),
        .axi_wready_reg_0(s00_axi_wready),
        .\cnt_reg[2] (HMC769_v4_0_S00_AXI_inst_n_23),
        .\cnt_reg[2]_0 (HMC769_v4_0_S00_AXI_inst_n_24),
        .\cnt_reg[2]_1 (HMC769_v4_0_S00_AXI_inst_n_25),
        .ip2mb_reg1(ip2mb_reg1),
        .mosi_r_reg(\spi_hmc_mode_RX_inst/cnt_reg ),
        .mosi_r_reg_0({\spi_hmc_mode_TX_inst/cnt_reg [4],\spi_hmc_mode_TX_inst/cnt_reg [2:0]}),
        .out(trig_tsweep_counter_reg),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_aresetn_0(HMC769_v4_0_S00_AXI_inst_n_1),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .slv_reg0(slv_reg0),
        .\slv_reg2_reg[4]_0 (HMC769_v4_0_S00_AXI_inst_n_22),
        .\slv_reg3_reg[0]_0 (HMC769_v4_0_S00_AXI_inst_n_27),
        .\slv_reg3_reg[4]_0 (HMC769_v4_0_S00_AXI_inst_n_26),
        .\slv_reg4_reg[5]_0 (slv_reg4),
        .\slv_reg5_reg[2]_0 (slv_reg5),
        .\slv_reg6_reg[15]_0 (sweep_val),
        .\slv_reg7_reg[8]_0 ({HMC769_v4_0_S00_AXI_inst_n_20,HMC769_v4_0_S00_AXI_inst_n_21}),
        .\trig_tsweep_counter_reg[11] ({HMC769_v4_0_S00_AXI_inst_n_15,HMC769_v4_0_S00_AXI_inst_n_16,HMC769_v4_0_S00_AXI_inst_n_17}),
        .\trig_tsweep_counter_reg[15] ({HMC769_v4_0_S00_AXI_inst_n_18,HMC769_v4_0_S00_AXI_inst_n_19}));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control top_PLL_control_i
       (.ATTEN(ATTEN),
        .D(sweep_val),
        .O({HMC769_v4_0_S00_AXI_inst_n_56,HMC769_v4_0_S00_AXI_inst_n_57}),
        .PAMP_EN(PAMP_EN),
        .PLL_POW_EN(PLL_POW_EN),
        .Q({shift_front[11:9],shift_front[5:3],shift_front[0]}),
        .S({HMC769_v4_0_S00_AXI_inst_n_6,HMC769_v4_0_S00_AXI_inst_n_7}),
        .SS(HMC769_v4_0_S00_AXI_inst_n_1),
        .\atten_reg_reg[5]_0 (slv_reg4),
        .\axi_rdata_reg[0] ({sel0[3],sel0[0]}),
        .\axi_rdata_reg[0]_0 (HMC769_v4_0_S00_AXI_inst_n_39),
        .clkDCO_10MHz(clkDCO_10MHz),
        .clk_10MHz(clk_10MHz),
        .\cnt_reg[4] ({\spi_hmc_mode_TX_inst/cnt_reg [4],\spi_hmc_mode_TX_inst/cnt_reg [2:0]}),
        .\data_r_reg[0] (top_PLL_control_i_n_30),
        .\data_r_reg[23] (ip2mb_reg1),
        .frame_even(frame_even),
        .mosi_r_reg(HMC769_v4_0_S00_AXI_inst_n_23),
        .mosi_r_reg_0(HMC769_v4_0_S00_AXI_inst_n_25),
        .mosi_r_reg_1(HMC769_v4_0_S00_AXI_inst_n_26),
        .mosi_r_reg_2(HMC769_v4_0_S00_AXI_inst_n_27),
        .mosi_r_reg_3(HMC769_v4_0_S00_AXI_inst_n_22),
        .mosi_r_reg_4(HMC769_v4_0_S00_AXI_inst_n_24),
        .out(\spi_hmc_mode_RX_inst/cnt_reg ),
        .pll_cen_reg_reg_0(slv_reg5),
        .pll_trig(pll_trig),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_aresetn(s00_axi_aresetn),
        .slv_reg0(slv_reg0),
        .spi_pll_cen(spi_pll_cen),
        .spi_pll_ld_sdo(spi_pll_ld_sdo),
        .spi_pll_mosi(spi_pll_mosi),
        .spi_pll_sck(spi_pll_sck),
        .spi_pll_sen(spi_pll_sen),
        .start_adc_count(start_adc_count),
        .\start_adc_count_r1_inferred__2/i__carry__0_0 ({HMC769_v4_0_S00_AXI_inst_n_15,HMC769_v4_0_S00_AXI_inst_n_16,HMC769_v4_0_S00_AXI_inst_n_17}),
        .\start_adc_count_r1_inferred__3/i__carry__0_0 ({HMC769_v4_0_S00_AXI_inst_n_20,HMC769_v4_0_S00_AXI_inst_n_21}),
        .start_adc_count_r_reg_0({HMC769_v4_0_S00_AXI_inst_n_18,HMC769_v4_0_S00_AXI_inst_n_19}),
        .\trig_tsweep_counter_reg[15]_0 (trig_tsweep_counter_reg));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v4_0_S00_AXI
   (axi_wready_reg_0,
    s00_axi_aresetn_0,
    axi_awready_reg_0,
    axi_arready_reg_0,
    s00_axi_bvalid,
    s00_axi_rvalid,
    S,
    Q,
    \trig_tsweep_counter_reg[11] ,
    \trig_tsweep_counter_reg[15] ,
    \slv_reg7_reg[8]_0 ,
    \slv_reg2_reg[4]_0 ,
    \cnt_reg[2] ,
    \cnt_reg[2]_0 ,
    \cnt_reg[2]_1 ,
    \slv_reg3_reg[4]_0 ,
    \slv_reg3_reg[0]_0 ,
    \axi_araddr_reg[5]_0 ,
    \slv_reg5_reg[2]_0 ,
    \slv_reg4_reg[5]_0 ,
    \axi_araddr_reg[4]_0 ,
    \slv_reg6_reg[15]_0 ,
    O,
    s00_axi_rdata,
    slv_reg0,
    s00_axi_aclk,
    out,
    s00_axi_aresetn,
    mosi_r_reg,
    mosi_r_reg_0,
    \axi_rdata_reg[0]_0 ,
    ip2mb_reg1,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_bready,
    s00_axi_arvalid,
    s00_axi_rready,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb);
  output axi_wready_reg_0;
  output s00_axi_aresetn_0;
  output axi_awready_reg_0;
  output axi_arready_reg_0;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output [1:0]S;
  output [6:0]Q;
  output [2:0]\trig_tsweep_counter_reg[11] ;
  output [1:0]\trig_tsweep_counter_reg[15] ;
  output [1:0]\slv_reg7_reg[8]_0 ;
  output \slv_reg2_reg[4]_0 ;
  output \cnt_reg[2] ;
  output \cnt_reg[2]_0 ;
  output \cnt_reg[2]_1 ;
  output \slv_reg3_reg[4]_0 ;
  output \slv_reg3_reg[0]_0 ;
  output [1:0]\axi_araddr_reg[5]_0 ;
  output [2:0]\slv_reg5_reg[2]_0 ;
  output [5:0]\slv_reg4_reg[5]_0 ;
  output \axi_araddr_reg[4]_0 ;
  output [15:0]\slv_reg6_reg[15]_0 ;
  output [1:0]O;
  output [31:0]s00_axi_rdata;
  output [2:0]slv_reg0;
  input s00_axi_aclk;
  input [15:0]out;
  input s00_axi_aresetn;
  input [2:0]mosi_r_reg;
  input [3:0]mosi_r_reg_0;
  input \axi_rdata_reg[0]_0 ;
  input [22:0]ip2mb_reg1;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_bready;
  input s00_axi_arvalid;
  input s00_axi_rready;
  input [3:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [3:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;

  wire [1:0]O;
  wire [6:0]Q;
  wire [1:0]S;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire \axi_araddr_reg[4]_0 ;
  wire [1:0]\axi_araddr_reg[5]_0 ;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire axi_awready0;
  wire axi_awready_reg_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[10]_i_3_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[11]_i_3_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[12]_i_3_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[13]_i_3_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[14]_i_3_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[15]_i_3_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[16]_i_3_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[17]_i_3_n_0 ;
  wire \axi_rdata[18]_i_3_n_0 ;
  wire \axi_rdata[18]_i_4_n_0 ;
  wire \axi_rdata[19]_i_3_n_0 ;
  wire \axi_rdata[19]_i_4_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_3_n_0 ;
  wire \axi_rdata[20]_i_4_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[21]_i_3_n_0 ;
  wire \axi_rdata[21]_i_4_n_0 ;
  wire \axi_rdata[22]_i_3_n_0 ;
  wire \axi_rdata[22]_i_4_n_0 ;
  wire \axi_rdata[23]_i_3_n_0 ;
  wire \axi_rdata[23]_i_4_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[24]_i_3_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[25]_i_3_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[26]_i_3_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[27]_i_3_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[28]_i_3_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[29]_i_3_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[30]_i_3_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[8]_i_3_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire \axi_rdata[9]_i_3_n_0 ;
  wire \axi_rdata_reg[0]_0 ;
  wire \axi_rdata_reg[18]_i_2_n_0 ;
  wire \axi_rdata_reg[19]_i_2_n_0 ;
  wire \axi_rdata_reg[1]_i_2_n_0 ;
  wire \axi_rdata_reg[20]_i_2_n_0 ;
  wire \axi_rdata_reg[22]_i_2_n_0 ;
  wire \axi_rdata_reg[23]_i_2_n_0 ;
  wire \axi_rdata_reg[2]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire \cnt_reg[2] ;
  wire \cnt_reg[2]_0 ;
  wire \cnt_reg[2]_1 ;
  wire i__carry__0_i_3__2_n_0;
  wire i__carry__0_i_3__2_n_2;
  wire i__carry__0_i_3__2_n_3;
  wire i__carry__0_i_3__2_n_5;
  wire i__carry__0_i_3__2_n_6;
  wire i__carry__0_i_3__2_n_7;
  wire i__carry_i_5__2_n_0;
  wire i__carry_i_5__2_n_1;
  wire i__carry_i_5__2_n_2;
  wire i__carry_i_5__2_n_3;
  wire i__carry_i_5__2_n_4;
  wire i__carry_i_5__2_n_5;
  wire i__carry_i_5__2_n_6;
  wire i__carry_i_5__2_n_7;
  wire i__carry_i_6__1_n_0;
  wire i__carry_i_6__1_n_1;
  wire i__carry_i_6__1_n_2;
  wire i__carry_i_6__1_n_3;
  wire i__carry_i_6__1_n_4;
  wire i__carry_i_6__1_n_5;
  wire i__carry_i_6__1_n_6;
  wire i__carry_i_6__1_n_7;
  wire i__carry_i_7__1_n_0;
  wire i__carry_i_7__1_n_1;
  wire i__carry_i_7__1_n_2;
  wire i__carry_i_7__1_n_3;
  wire i__carry_i_7__1_n_4;
  wire i__carry_i_7__1_n_5;
  wire [22:0]ip2mb_reg1;
  wire mosi_r_i_18_n_0;
  wire mosi_r_i_19_n_0;
  wire mosi_r_i_21_n_0;
  wire mosi_r_i_7__0_n_0;
  wire mosi_r_i_7_n_0;
  wire mosi_r_i_8__0_n_0;
  wire [2:0]mosi_r_reg;
  wire [3:0]mosi_r_reg_0;
  wire [15:0]out;
  wire [3:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [3:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_aresetn_0;
  wire s00_axi_arvalid;
  wire [3:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:1]sel0;
  wire [31:1]shift_front;
  wire [2:0]slv_reg0;
  wire \slv_reg0[0]_i_1_n_0 ;
  wire \slv_reg0[1]_i_1_n_0 ;
  wire \slv_reg0[2]_i_1_n_0 ;
  wire \slv_reg0[2]_i_2_n_0 ;
  wire [31:0]slv_reg1;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire \slv_reg2_reg[4]_0 ;
  wire [31:0]slv_reg3;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire \slv_reg3_reg[0]_0 ;
  wire \slv_reg3_reg[4]_0 ;
  wire [31:6]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire [5:0]\slv_reg4_reg[5]_0 ;
  wire [31:3]slv_reg5;
  wire \slv_reg5[15]_i_1_n_0 ;
  wire \slv_reg5[23]_i_1_n_0 ;
  wire \slv_reg5[31]_i_1_n_0 ;
  wire \slv_reg5[7]_i_1_n_0 ;
  wire [2:0]\slv_reg5_reg[2]_0 ;
  wire \slv_reg6[15]_i_1_n_0 ;
  wire \slv_reg6[23]_i_1_n_0 ;
  wire \slv_reg6[31]_i_1_n_0 ;
  wire \slv_reg6[7]_i_1_n_0 ;
  wire [15:0]\slv_reg6_reg[15]_0 ;
  wire \slv_reg6_reg_n_0_[16] ;
  wire \slv_reg6_reg_n_0_[17] ;
  wire \slv_reg6_reg_n_0_[18] ;
  wire \slv_reg6_reg_n_0_[19] ;
  wire \slv_reg6_reg_n_0_[20] ;
  wire \slv_reg6_reg_n_0_[21] ;
  wire \slv_reg6_reg_n_0_[22] ;
  wire \slv_reg6_reg_n_0_[23] ;
  wire \slv_reg6_reg_n_0_[24] ;
  wire \slv_reg6_reg_n_0_[25] ;
  wire \slv_reg6_reg_n_0_[26] ;
  wire \slv_reg6_reg_n_0_[27] ;
  wire \slv_reg6_reg_n_0_[28] ;
  wire \slv_reg6_reg_n_0_[29] ;
  wire \slv_reg6_reg_n_0_[30] ;
  wire \slv_reg6_reg_n_0_[31] ;
  wire \slv_reg7[15]_i_1_n_0 ;
  wire \slv_reg7[23]_i_1_n_0 ;
  wire \slv_reg7[31]_i_1_n_0 ;
  wire \slv_reg7[7]_i_1_n_0 ;
  wire [1:0]\slv_reg7_reg[8]_0 ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;
  wire [2:0]\trig_tsweep_counter_reg[11] ;
  wire [1:0]\trig_tsweep_counter_reg[15] ;
  wire [2:2]NLW_i__carry__0_i_3__2_CO_UNCONNECTED;
  wire [3:3]NLW_i__carry__0_i_3__2_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(aw_en_reg_n_0),
        .I2(axi_awready_reg_0),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(s00_axi_aresetn_0));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(\axi_araddr_reg[5]_0 [0]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_araddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[3]),
        .Q(\axi_araddr_reg[5]_0 [1]),
        .R(s00_axi_aresetn_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(s00_axi_aresetn_0));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_awaddr_reg[5] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[3]),
        .Q(p_0_in[3]),
        .R(s00_axi_aresetn_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(s00_axi_aresetn_0));
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(axi_awready_reg_0),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(axi_awready_reg_0),
        .I3(axi_wready_reg_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(s00_axi_aresetn_0));
  LUT5 #(
    .INIT(32'hFFFF4540)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[0]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[0]_i_3_n_0 ),
        .I4(\axi_rdata_reg[0]_0 ),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(Q[0]),
        .I1(\slv_reg6_reg[15]_0 [0]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[2]_0 [0]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[0]_i_3 
       (.I0(slv_reg3[0]),
        .I1(slv_reg2[0]),
        .I2(sel0[1]),
        .I3(slv_reg1[0]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \axi_rdata[0]_i_5 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .O(\axi_araddr_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[9]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[10]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[10]_i_3_n_0 ),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(Q[5]),
        .I1(\slv_reg6_reg[15]_0 [10]),
        .I2(sel0[1]),
        .I3(slv_reg5[10]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[10]_i_3 
       (.I0(slv_reg3[10]),
        .I1(slv_reg2[10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[10]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[11]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[11]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[10]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(Q[6]),
        .I1(\slv_reg6_reg[15]_0 [11]),
        .I2(sel0[1]),
        .I3(slv_reg5[11]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[11]_i_3 
       (.I0(slv_reg3[11]),
        .I1(slv_reg2[11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[11]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[11]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[12]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[12]_i_3_n_0 ),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(shift_front[12]),
        .I1(\slv_reg6_reg[15]_0 [12]),
        .I2(sel0[1]),
        .I3(slv_reg5[12]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[12]_i_3 
       (.I0(slv_reg3[12]),
        .I1(slv_reg2[12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[12]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[13]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[13]_i_3_n_0 ),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(shift_front[13]),
        .I1(\slv_reg6_reg[15]_0 [13]),
        .I2(sel0[1]),
        .I3(slv_reg5[13]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[13]_i_3 
       (.I0(slv_reg3[13]),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[14]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[14]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[13]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(shift_front[14]),
        .I1(\slv_reg6_reg[15]_0 [14]),
        .I2(sel0[1]),
        .I3(slv_reg5[14]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[14]_i_3 
       (.I0(slv_reg3[14]),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[14]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[15]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[15]_i_3_n_0 ),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(shift_front[15]),
        .I1(\slv_reg6_reg[15]_0 [15]),
        .I2(sel0[1]),
        .I3(slv_reg5[15]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[15]_i_3 
       (.I0(slv_reg3[15]),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[15]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[15]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[16]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[16]_i_3_n_0 ),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(shift_front[16]),
        .I1(\slv_reg6_reg_n_0_[16] ),
        .I2(sel0[1]),
        .I3(slv_reg5[16]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[16]_i_3 
       (.I0(slv_reg3[16]),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[16]),
        .O(\axi_rdata[16]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[16]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[17]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[17]_i_3_n_0 ),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(shift_front[17]),
        .I1(\slv_reg6_reg_n_0_[17] ),
        .I2(sel0[1]),
        .I3(slv_reg5[17]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[17]_i_3 
       (.I0(slv_reg3[17]),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[17]),
        .O(\axi_rdata[17]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[18]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[17]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[18]_i_2_n_0 ),
        .O(reg_data_out[18]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[18]_i_3 
       (.I0(slv_reg3[18]),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[18]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_4 
       (.I0(shift_front[18]),
        .I1(\slv_reg6_reg_n_0_[18] ),
        .I2(sel0[1]),
        .I3(slv_reg5[18]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[18]),
        .O(\axi_rdata[18]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[19]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[18]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[19]_i_2_n_0 ),
        .O(reg_data_out[19]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[19]_i_3 
       (.I0(slv_reg3[19]),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[19]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_4 
       (.I0(shift_front[19]),
        .I1(\slv_reg6_reg_n_0_[19] ),
        .I2(sel0[1]),
        .I3(slv_reg5[19]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[19]),
        .O(\axi_rdata[19]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAEAFFFFAAEA0000)) 
    \axi_rdata[1]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[0]),
        .I3(sel0[2]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[1]_i_2_n_0 ),
        .O(reg_data_out[1]));
  LUT5 #(
    .INIT(32'hAFA0C0C0)) 
    \axi_rdata[1]_i_3 
       (.I0(slv_reg3[1]),
        .I1(slv_reg2[1]),
        .I2(sel0[1]),
        .I3(slv_reg1[1]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_4 
       (.I0(shift_front[1]),
        .I1(\slv_reg6_reg[15]_0 [1]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[2]_0 [1]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [1]),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[20]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[19]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[20]_i_2_n_0 ),
        .O(reg_data_out[20]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[20]_i_3 
       (.I0(slv_reg3[20]),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[20]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_4 
       (.I0(shift_front[20]),
        .I1(\slv_reg6_reg_n_0_[20] ),
        .I2(sel0[1]),
        .I3(slv_reg5[20]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[20]),
        .O(\axi_rdata[20]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[21]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[21]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[20]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(shift_front[21]),
        .I1(\slv_reg6_reg_n_0_[21] ),
        .I2(sel0[1]),
        .I3(slv_reg5[21]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[21]_i_3 
       (.I0(slv_reg3[21]),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[21]),
        .O(\axi_rdata[21]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \axi_rdata[21]_i_4 
       (.I0(\axi_araddr_reg[5]_0 [0]),
        .I1(sel0[1]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(sel0[2]),
        .O(\axi_rdata[21]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[22]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[21]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[22]_i_2_n_0 ),
        .O(reg_data_out[22]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[22]_i_3 
       (.I0(slv_reg3[22]),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[22]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_4 
       (.I0(shift_front[22]),
        .I1(\slv_reg6_reg_n_0_[22] ),
        .I2(sel0[1]),
        .I3(slv_reg5[22]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[22]),
        .O(\axi_rdata[22]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFEAFFFFFFEA0000)) 
    \axi_rdata[23]_i_1 
       (.I0(sel0[2]),
        .I1(\axi_araddr_reg[5]_0 [0]),
        .I2(ip2mb_reg1[22]),
        .I3(sel0[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[23]_i_2_n_0 ),
        .O(reg_data_out[23]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[23]_i_3 
       (.I0(slv_reg3[23]),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[23]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_4 
       (.I0(shift_front[23]),
        .I1(\slv_reg6_reg_n_0_[23] ),
        .I2(sel0[1]),
        .I3(slv_reg5[23]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[23]),
        .O(\axi_rdata[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[24]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[24]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(slv_reg3[24]),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_3 
       (.I0(shift_front[24]),
        .I1(\slv_reg6_reg_n_0_[24] ),
        .I2(sel0[1]),
        .I3(slv_reg5[24]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[24]),
        .O(\axi_rdata[24]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[25]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[25]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(slv_reg3[25]),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_3 
       (.I0(shift_front[25]),
        .I1(\slv_reg6_reg_n_0_[25] ),
        .I2(sel0[1]),
        .I3(slv_reg5[25]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[25]),
        .O(\axi_rdata[25]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[26]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[26]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[26]_i_3_n_0 ),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(shift_front[26]),
        .I1(\slv_reg6_reg_n_0_[26] ),
        .I2(sel0[1]),
        .I3(slv_reg5[26]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[26]_i_3 
       (.I0(slv_reg3[26]),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[26]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[27]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[27]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[27]_i_3_n_0 ),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(shift_front[27]),
        .I1(\slv_reg6_reg_n_0_[27] ),
        .I2(sel0[1]),
        .I3(slv_reg5[27]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[27]_i_3 
       (.I0(slv_reg3[27]),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[27]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[28]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[28]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(slv_reg3[28]),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_3 
       (.I0(shift_front[28]),
        .I1(\slv_reg6_reg_n_0_[28] ),
        .I2(sel0[1]),
        .I3(slv_reg5[28]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[28]),
        .O(\axi_rdata[28]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[29]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[29]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[29]_i_3_n_0 ),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(shift_front[29]),
        .I1(\slv_reg6_reg_n_0_[29] ),
        .I2(sel0[1]),
        .I3(slv_reg5[29]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[29]_i_3 
       (.I0(slv_reg3[29]),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[29]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hBAAAFFFFBAAA0000)) 
    \axi_rdata[2]_i_1 
       (.I0(sel0[2]),
        .I1(sel0[1]),
        .I2(\axi_araddr_reg[5]_0 [0]),
        .I3(ip2mb_reg1[1]),
        .I4(\axi_araddr_reg[5]_0 [1]),
        .I5(\axi_rdata_reg[2]_i_2_n_0 ),
        .O(reg_data_out[2]));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[2]_i_3 
       (.I0(slv_reg3[2]),
        .I1(slv_reg2[2]),
        .I2(sel0[1]),
        .I3(slv_reg1[2]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_4 
       (.I0(shift_front[2]),
        .I1(\slv_reg6_reg[15]_0 [2]),
        .I2(sel0[1]),
        .I3(\slv_reg5_reg[2]_0 [2]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[2]),
        .I2(\axi_rdata[30]_i_3_n_0 ),
        .I3(\axi_araddr_reg[5]_0 [1]),
        .O(reg_data_out[30]));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(slv_reg3[30]),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_3 
       (.I0(shift_front[30]),
        .I1(\slv_reg6_reg_n_0_[30] ),
        .I2(sel0[1]),
        .I3(slv_reg5[30]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[30]),
        .O(\axi_rdata[30]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFCBBFC88)) 
    \axi_rdata[31]_i_1 
       (.I0(sel0[1]),
        .I1(\axi_araddr_reg[5]_0 [1]),
        .I2(\axi_rdata[31]_i_2_n_0 ),
        .I3(sel0[2]),
        .I4(\axi_rdata[31]_i_3_n_0 ),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(shift_front[31]),
        .I1(\slv_reg6_reg_n_0_[31] ),
        .I2(sel0[1]),
        .I3(slv_reg5[31]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[31]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[31]_i_3 
       (.I0(slv_reg3[31]),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[3]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[3]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[2]),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(Q[1]),
        .I1(\slv_reg6_reg[15]_0 [3]),
        .I2(sel0[1]),
        .I3(slv_reg5[3]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[3]_i_3 
       (.I0(slv_reg3[3]),
        .I1(slv_reg2[3]),
        .I2(sel0[1]),
        .I3(slv_reg1[3]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[3]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[4]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[4]_i_3_n_0 ),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(Q[2]),
        .I1(\slv_reg6_reg[15]_0 [4]),
        .I2(sel0[1]),
        .I3(slv_reg5[4]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0C0C0)) 
    \axi_rdata[4]_i_3 
       (.I0(slv_reg3[4]),
        .I1(slv_reg2[4]),
        .I2(sel0[1]),
        .I3(slv_reg1[4]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[4]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[5]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[5]_i_3_n_0 ),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(Q[3]),
        .I1(\slv_reg6_reg[15]_0 [5]),
        .I2(sel0[1]),
        .I3(slv_reg5[5]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(\slv_reg4_reg[5]_0 [5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[5]_i_3 
       (.I0(slv_reg3[5]),
        .I1(slv_reg2[5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[5]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[6]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[6]_i_3_n_0 ),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(shift_front[6]),
        .I1(\slv_reg6_reg[15]_0 [6]),
        .I2(sel0[1]),
        .I3(slv_reg5[6]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[6]_i_3 
       (.I0(slv_reg3[6]),
        .I1(slv_reg2[6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4540FFFF45404540)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_araddr_reg[5]_0 [1]),
        .I1(\axi_rdata[7]_i_2_n_0 ),
        .I2(sel0[2]),
        .I3(\axi_rdata[7]_i_3_n_0 ),
        .I4(\axi_rdata[21]_i_4_n_0 ),
        .I5(ip2mb_reg1[6]),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(shift_front[7]),
        .I1(\slv_reg6_reg[15]_0 [7]),
        .I2(sel0[1]),
        .I3(slv_reg5[7]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg3[7]),
        .I1(slv_reg2[7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[7]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[8]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[8]_i_3_n_0 ),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(shift_front[8]),
        .I1(\slv_reg6_reg[15]_0 [8]),
        .I2(sel0[1]),
        .I3(slv_reg5[8]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFC0A0C0)) 
    \axi_rdata[8]_i_3 
       (.I0(slv_reg3[8]),
        .I1(slv_reg2[8]),
        .I2(sel0[1]),
        .I3(\axi_araddr_reg[5]_0 [0]),
        .I4(slv_reg1[8]),
        .O(\axi_rdata[8]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F4F4F444444)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[21]_i_4_n_0 ),
        .I1(ip2mb_reg1[8]),
        .I2(\axi_araddr_reg[5]_0 [1]),
        .I3(\axi_rdata[9]_i_2_n_0 ),
        .I4(sel0[2]),
        .I5(\axi_rdata[9]_i_3_n_0 ),
        .O(reg_data_out[9]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(Q[4]),
        .I1(\slv_reg6_reg[15]_0 [9]),
        .I2(sel0[1]),
        .I3(slv_reg5[9]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .I5(slv_reg4[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAFA0CFCF)) 
    \axi_rdata[9]_i_3 
       (.I0(slv_reg3[9]),
        .I1(slv_reg2[9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(\axi_araddr_reg[5]_0 [0]),
        .O(\axi_rdata[9]_i_3_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[18]_i_2 
       (.I0(\axi_rdata[18]_i_3_n_0 ),
        .I1(\axi_rdata[18]_i_4_n_0 ),
        .O(\axi_rdata_reg[18]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[19]_i_2 
       (.I0(\axi_rdata[19]_i_3_n_0 ),
        .I1(\axi_rdata[19]_i_4_n_0 ),
        .O(\axi_rdata_reg[19]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[1]_i_2 
       (.I0(\axi_rdata[1]_i_3_n_0 ),
        .I1(\axi_rdata[1]_i_4_n_0 ),
        .O(\axi_rdata_reg[1]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[20]_i_2 
       (.I0(\axi_rdata[20]_i_3_n_0 ),
        .I1(\axi_rdata[20]_i_4_n_0 ),
        .O(\axi_rdata_reg[20]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[22]_i_2 
       (.I0(\axi_rdata[22]_i_3_n_0 ),
        .I1(\axi_rdata[22]_i_4_n_0 ),
        .O(\axi_rdata_reg[22]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[23]_i_2 
       (.I0(\axi_rdata[23]_i_3_n_0 ),
        .I1(\axi_rdata[23]_i_4_n_0 ),
        .O(\axi_rdata_reg[23]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(s00_axi_aresetn_0));
  MUXF7 \axi_rdata_reg[2]_i_2 
       (.I0(\axi_rdata[2]_i_3_n_0 ),
        .I1(\axi_rdata[2]_i_4_n_0 ),
        .O(\axi_rdata_reg[2]_i_2_n_0 ),
        .S(sel0[2]));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(s00_axi_aresetn_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(s00_axi_aresetn_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(axi_arready_reg_0),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(s00_axi_aresetn_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(axi_wready_reg_0),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(s00_axi_aresetn_0));
  LUT3 #(
    .INIT(8'h41)) 
    i__carry__0_i_1__0
       (.I0(i__carry__0_i_3__2_n_0),
        .I1(i__carry__0_i_3__2_n_5),
        .I2(out[15]),
        .O(\trig_tsweep_counter_reg[15] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    i__carry__0_i_1__3
       (.I0(shift_front[15]),
        .I1(out[15]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_2__2
       (.I0(i__carry__0_i_3__2_n_7),
        .I1(out[13]),
        .I2(out[14]),
        .I3(i__carry__0_i_3__2_n_6),
        .I4(out[12]),
        .I5(i__carry_i_5__2_n_4),
        .O(\trig_tsweep_counter_reg[15] [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_2__3
       (.I0(shift_front[14]),
        .I1(out[14]),
        .I2(out[12]),
        .I3(shift_front[12]),
        .I4(out[13]),
        .I5(shift_front[13]),
        .O(S[0]));
  CARRY4 i__carry__0_i_3__2
       (.CI(i__carry_i_5__2_n_0),
        .CO({i__carry__0_i_3__2_n_0,NLW_i__carry__0_i_3__2_CO_UNCONNECTED[2],i__carry__0_i_3__2_n_2,i__carry__0_i_3__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_i__carry__0_i_3__2_O_UNCONNECTED[3],i__carry__0_i_3__2_n_5,i__carry__0_i_3__2_n_6,i__carry__0_i_3__2_n_7}),
        .S({1'b1,shift_front[15:13]}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1__1
       (.I0(i__carry_i_5__2_n_5),
        .I1(out[11]),
        .I2(out[9]),
        .I3(i__carry_i_5__2_n_7),
        .I4(out[10]),
        .I5(i__carry_i_5__2_n_6),
        .O(\trig_tsweep_counter_reg[11] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2__1
       (.I0(i__carry_i_6__1_n_5),
        .I1(out[7]),
        .I2(out[8]),
        .I3(i__carry_i_6__1_n_4),
        .I4(out[6]),
        .I5(i__carry_i_6__1_n_6),
        .O(\trig_tsweep_counter_reg[11] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2__2
       (.I0(shift_front[8]),
        .I1(out[8]),
        .I2(out[6]),
        .I3(shift_front[6]),
        .I4(out[7]),
        .I5(shift_front[7]),
        .O(\slv_reg7_reg[8]_0 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3__1
       (.I0(i__carry_i_7__1_n_4),
        .I1(out[4]),
        .I2(out[5]),
        .I3(i__carry_i_6__1_n_7),
        .I4(out[3]),
        .I5(i__carry_i_7__1_n_5),
        .O(\trig_tsweep_counter_reg[11] [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_4__2
       (.I0(Q[0]),
        .I1(out[0]),
        .I2(out[1]),
        .I3(shift_front[1]),
        .I4(out[2]),
        .I5(shift_front[2]),
        .O(\slv_reg7_reg[8]_0 [0]));
  CARRY4 i__carry_i_5__2
       (.CI(i__carry_i_6__1_n_0),
        .CO({i__carry_i_5__2_n_0,i__carry_i_5__2_n_1,i__carry_i_5__2_n_2,i__carry_i_5__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({i__carry_i_5__2_n_4,i__carry_i_5__2_n_5,i__carry_i_5__2_n_6,i__carry_i_5__2_n_7}),
        .S({shift_front[12],Q[6:4]}));
  CARRY4 i__carry_i_6__1
       (.CI(i__carry_i_7__1_n_0),
        .CO({i__carry_i_6__1_n_0,i__carry_i_6__1_n_1,i__carry_i_6__1_n_2,i__carry_i_6__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({i__carry_i_6__1_n_4,i__carry_i_6__1_n_5,i__carry_i_6__1_n_6,i__carry_i_6__1_n_7}),
        .S({shift_front[8:6],Q[3]}));
  CARRY4 i__carry_i_7__1
       (.CI(1'b0),
        .CO({i__carry_i_7__1_n_0,i__carry_i_7__1_n_1,i__carry_i_7__1_n_2,i__carry_i_7__1_n_3}),
        .CYINIT(Q[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({i__carry_i_7__1_n_4,i__carry_i_7__1_n_5,O}),
        .S({Q[2:1],shift_front[2:1]}));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_10__0
       (.I0(slv_reg3[4]),
        .I1(slv_reg3[5]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg3[6]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg3[7]),
        .O(\slv_reg3_reg[4]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_11__0
       (.I0(slv_reg3[0]),
        .I1(slv_reg3[1]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg3[2]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg3[3]),
        .O(\slv_reg3_reg[0]_0 ));
  LUT4 #(
    .INIT(16'h8BBB)) 
    mosi_r_i_14
       (.I0(mosi_r_i_21_n_0),
        .I1(mosi_r_reg_0[2]),
        .I2(mosi_r_reg_0[0]),
        .I3(slv_reg1[4]),
        .O(\cnt_reg[2] ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_18
       (.I0(slv_reg3[12]),
        .I1(slv_reg3[13]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg3[14]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg3[15]),
        .O(mosi_r_i_18_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_19
       (.I0(slv_reg3[8]),
        .I1(slv_reg3[9]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg3[10]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg3[11]),
        .O(mosi_r_i_19_n_0));
  LUT6 #(
    .INIT(64'h5555300055553F00)) 
    mosi_r_i_2
       (.I0(mosi_r_i_7_n_0),
        .I1(slv_reg2[4]),
        .I2(mosi_r_reg[0]),
        .I3(mosi_r_reg[1]),
        .I4(mosi_r_reg[2]),
        .I5(slv_reg2[5]),
        .O(\slv_reg2_reg[4]_0 ));
  LUT6 #(
    .INIT(64'h505F3030505F3F3F)) 
    mosi_r_i_21
       (.I0(slv_reg1[0]),
        .I1(slv_reg1[1]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg1[2]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg1[3]),
        .O(mosi_r_i_21_n_0));
  LUT4 #(
    .INIT(16'h00E2)) 
    mosi_r_i_2__0
       (.I0(mosi_r_i_7__0_n_0),
        .I1(mosi_r_reg_0[2]),
        .I2(mosi_r_i_8__0_n_0),
        .I3(mosi_r_reg_0[3]),
        .O(\cnt_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_7
       (.I0(slv_reg2[0]),
        .I1(slv_reg2[1]),
        .I2(mosi_r_reg[1]),
        .I3(slv_reg2[2]),
        .I4(mosi_r_reg[0]),
        .I5(slv_reg2[3]),
        .O(mosi_r_i_7_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_7__0
       (.I0(slv_reg3[20]),
        .I1(slv_reg3[21]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg3[22]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg3[23]),
        .O(mosi_r_i_7__0_n_0));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    mosi_r_i_8__0
       (.I0(slv_reg3[16]),
        .I1(slv_reg3[17]),
        .I2(mosi_r_reg_0[1]),
        .I3(slv_reg3[18]),
        .I4(mosi_r_reg_0[0]),
        .I5(slv_reg3[19]),
        .O(mosi_r_i_8__0_n_0));
  MUXF7 mosi_r_reg_i_9
       (.I0(mosi_r_i_18_n_0),
        .I1(mosi_r_i_19_n_0),
        .O(\cnt_reg[2]_1 ),
        .S(mosi_r_reg_0[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[0]_i_1 
       (.I0(s00_axi_wdata[0]),
        .I1(\slv_reg0[2]_i_2_n_0 ),
        .I2(slv_reg0[0]),
        .O(\slv_reg0[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[1]_i_1 
       (.I0(s00_axi_wdata[1]),
        .I1(\slv_reg0[2]_i_2_n_0 ),
        .I2(slv_reg0[1]),
        .O(\slv_reg0[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \slv_reg0[2]_i_1 
       (.I0(s00_axi_wdata[2]),
        .I1(\slv_reg0[2]_i_2_n_0 ),
        .I2(slv_reg0[2]),
        .O(\slv_reg0[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \slv_reg0[2]_i_2 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(s00_axi_wstrb[0]),
        .O(\slv_reg0[2]_i_2_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[0]_i_1_n_0 ),
        .Q(slv_reg0[0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[1]_i_1_n_0 ),
        .Q(slv_reg0[1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\slv_reg0[2]_i_1_n_0 ),
        .Q(slv_reg0[2]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg1[31]_i_2 
       (.I0(axi_wready_reg_0),
        .I1(axi_awready_reg_0),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[2]),
        .I5(p_0_in[1]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .I4(p_0_in[2]),
        .I5(p_0_in[3]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg3[0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg3[10]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg3[11]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg3[12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg3[13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg3[14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg3[15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg3[16]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg3[17]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg3[18]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg3[19]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg3[1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg3[20]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg3[21]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg3[22]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg3[23]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg3[24]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg3[25]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg3[26]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg3[27]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg3[28]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg3[29]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg3[2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg3[30]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg3[31]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg3[3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg3[4]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg3[5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg3[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg3[7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg3[8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg3[9]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[3]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[2]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg4_reg[5]_0 [0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg4_reg[5]_0 [1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg4_reg[5]_0 [2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg4_reg[5]_0 [3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg4_reg[5]_0 [4]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg4_reg[5]_0 [5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg5[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg5[7]_i_1_n_0 ));
  FDRE \slv_reg5_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg5_reg[2]_0 [0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg5[10]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg5[11]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg5[12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg5[13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg5[14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg5[15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg5[16]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg5[17]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg5[18]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg5[19]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg5_reg[2]_0 [1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg5[20]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg5[21]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg5[22]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg5[23]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg5[24]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg5[25]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg5[26]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg5[27]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg5[28]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg5[29]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg5_reg[2]_0 [2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg5[30]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg5[31]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg5[3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg5[4]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg5[5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg5[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg5[7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg5[8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg5_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg5[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg5[9]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[1]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[2]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[3]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    \slv_reg6[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(s00_axi_wstrb[0]),
        .I2(p_0_in[2]),
        .I3(p_0_in[1]),
        .I4(p_0_in[0]),
        .I5(p_0_in[3]),
        .O(\slv_reg6[7]_i_1_n_0 ));
  FDRE \slv_reg6_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg6_reg[15]_0 [0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg6_reg[15]_0 [10]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg6_reg[15]_0 [11]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg6_reg[15]_0 [12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg6_reg[15]_0 [13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg6_reg[15]_0 [14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg6_reg[15]_0 [15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg6_reg_n_0_[16] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg6_reg_n_0_[17] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg6_reg_n_0_[18] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg6_reg_n_0_[19] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg6_reg[15]_0 [1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg6_reg_n_0_[20] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg6_reg_n_0_[21] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg6_reg_n_0_[22] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg6_reg_n_0_[23] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg6_reg_n_0_[24] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg6_reg_n_0_[25] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg6_reg_n_0_[26] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg6_reg_n_0_[27] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg6_reg_n_0_[28] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg6_reg_n_0_[29] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg6_reg[15]_0 [2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg6_reg_n_0_[30] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg6_reg_n_0_[31] ),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg6_reg[15]_0 [3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg6_reg[15]_0 [4]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg6_reg[15]_0 [5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg6_reg[15]_0 [6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg6_reg[15]_0 [7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg6_reg[15]_0 [8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg6_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg6[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg6_reg[15]_0 [9]),
        .R(s00_axi_aresetn_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[1]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[2]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[3]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    \slv_reg7[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[2]),
        .I2(s00_axi_wstrb[0]),
        .I3(p_0_in[0]),
        .I4(p_0_in[1]),
        .I5(p_0_in[3]),
        .O(\slv_reg7[7]_i_1_n_0 ));
  FDRE \slv_reg7_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(Q[0]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(Q[5]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(Q[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(shift_front[12]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(shift_front[13]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(shift_front[14]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(shift_front[15]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(shift_front[16]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(shift_front[17]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(shift_front[18]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(shift_front[19]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(shift_front[1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(shift_front[20]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(shift_front[21]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(shift_front[22]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(shift_front[23]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(shift_front[24]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(shift_front[25]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(shift_front[26]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(shift_front[27]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(shift_front[28]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(shift_front[29]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(shift_front[2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(shift_front[30]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(shift_front[31]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(Q[1]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(Q[2]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(Q[3]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(shift_front[6]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(shift_front[7]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(shift_front[8]),
        .R(s00_axi_aresetn_0));
  FDRE \slv_reg7_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg7[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(Q[4]),
        .R(s00_axi_aresetn_0));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden__0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_HMC769_0_0,HMC769_v4_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "HMC769_v4_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_10MHz,
    clkDCO_10MHz,
    clkDCO_100MHz,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    spi_pll_sen,
    spi_pll_sck,
    spi_pll_mosi,
    spi_pll_cen,
    spi_pll_ld_sdo,
    pll_trig,
    ATTEN,
    PLL_POW_EN,
    PAMP_EN,
    start_adc_count,
    frame_even,
    s00_axi_aclk,
    s00_axi_aresetn);
  input clk_10MHz;
  input clkDCO_10MHz;
  input clkDCO_100MHz;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL sen" *) output spi_pll_sen;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL sck" *) output spi_pll_sck;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL mosi" *) output spi_pll_mosi;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL cen" *) output spi_pll_cen;
  (* X_INTERFACE_INFO = "user.org:interface:spi_pll:1.0 SPI_PLL ld_sdo" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME SPI_PLL, SV_INTERFACE true" *) input spi_pll_ld_sdo;
  output pll_trig;
  output [5:0]ATTEN;
  output PLL_POW_EN;
  output PAMP_EN;
  output start_adc_count;
  output frame_even;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s00_axi_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aclk, ASSOCIATED_BUSIF S00_AXI:SPI_PLL, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [5:0]ATTEN;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire clkDCO_10MHz;
  wire clk_10MHz;
  wire frame_even;
  wire pll_trig;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire spi_pll_cen;
  wire spi_pll_ld_sdo;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_adc_count;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_HMC769_v4_0 inst
       (.ATTEN(ATTEN),
        .PAMP_EN(PAMP_EN),
        .PLL_POW_EN(PLL_POW_EN),
        .clkDCO_10MHz(clkDCO_10MHz),
        .clk_10MHz(clk_10MHz),
        .frame_even(frame_even),
        .pll_trig(pll_trig),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[5:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[5:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .spi_pll_cen(spi_pll_cen),
        .spi_pll_ld_sdo(spi_pll_ld_sdo),
        .spi_pll_mosi(spi_pll_mosi),
        .spi_pll_sck(spi_pll_sck),
        .spi_pll_sen(spi_pll_sen),
        .start_adc_count(start_adc_count));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX
   (enable_sck,
    pll_mosi_read,
    pll_sen_read,
    out,
    \data_r_reg[0]_0 ,
    \data_r_reg[23]_0 ,
    clk_10MHz,
    START_receive,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    pll_data_ready_TX,
    mosi_r_reg_0,
    spi_pll_ld_sdo);
  output enable_sck;
  output pll_mosi_read;
  output pll_sen_read;
  output [2:0]out;
  output \data_r_reg[0]_0 ;
  output [22:0]\data_r_reg[23]_0 ;
  input clk_10MHz;
  input START_receive;
  input [1:0]\axi_rdata_reg[0] ;
  input \axi_rdata_reg[0]_0 ;
  input pll_data_ready_TX;
  input mosi_r_reg_0;
  input spi_pll_ld_sdo;

  wire START_receive;
  wire [1:0]\axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire clk_10MHz;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[0]_i_3_n_0 ;
  wire \cnt[0]_i_4__0_n_0 ;
  wire \cnt[0]_i_5_n_0 ;
  wire \cnt[0]_i_6__0_n_0 ;
  wire [15:3]cnt_reg;
  wire \cnt_reg[0]_i_2_n_0 ;
  wire \cnt_reg[0]_i_2_n_1 ;
  wire \cnt_reg[0]_i_2_n_2 ;
  wire \cnt_reg[0]_i_2_n_3 ;
  wire \cnt_reg[0]_i_2_n_4 ;
  wire \cnt_reg[0]_i_2_n_5 ;
  wire \cnt_reg[0]_i_2_n_6 ;
  wire \cnt_reg[0]_i_2_n_7 ;
  wire \cnt_reg[12]_i_1_n_1 ;
  wire \cnt_reg[12]_i_1_n_2 ;
  wire \cnt_reg[12]_i_1_n_3 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_1 ;
  wire \cnt_reg[4]_i_1_n_2 ;
  wire \cnt_reg[4]_i_1_n_3 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_1 ;
  wire \cnt_reg[8]_i_1_n_2 ;
  wire \cnt_reg[8]_i_1_n_3 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire cs_r_i_1_n_0;
  wire [15:1]data_r1;
  wire \data_r1_inferred__0/i__carry__0_n_0 ;
  wire \data_r1_inferred__0/i__carry__0_n_1 ;
  wire \data_r1_inferred__0/i__carry__0_n_2 ;
  wire \data_r1_inferred__0/i__carry__0_n_3 ;
  wire \data_r1_inferred__0/i__carry__1_n_0 ;
  wire \data_r1_inferred__0/i__carry__1_n_1 ;
  wire \data_r1_inferred__0/i__carry__1_n_2 ;
  wire \data_r1_inferred__0/i__carry__1_n_3 ;
  wire \data_r1_inferred__0/i__carry__2_n_0 ;
  wire \data_r1_inferred__0/i__carry__2_n_2 ;
  wire \data_r1_inferred__0/i__carry__2_n_3 ;
  wire \data_r1_inferred__0/i__carry_n_0 ;
  wire \data_r1_inferred__0/i__carry_n_1 ;
  wire \data_r1_inferred__0/i__carry_n_2 ;
  wire \data_r1_inferred__0/i__carry_n_3 ;
  wire \data_r[0]_i_1_n_0 ;
  wire \data_r[10]_i_1_n_0 ;
  wire \data_r[11]_i_1_n_0 ;
  wire \data_r[12]_i_1_n_0 ;
  wire \data_r[13]_i_1_n_0 ;
  wire \data_r[14]_i_1_n_0 ;
  wire \data_r[15]_i_1_n_0 ;
  wire \data_r[15]_i_2_n_0 ;
  wire \data_r[16]_i_1_n_0 ;
  wire \data_r[17]_i_1_n_0 ;
  wire \data_r[18]_i_1_n_0 ;
  wire \data_r[19]_i_1_n_0 ;
  wire \data_r[1]_i_1_n_0 ;
  wire \data_r[20]_i_1_n_0 ;
  wire \data_r[21]_i_1_n_0 ;
  wire \data_r[22]_i_1_n_0 ;
  wire \data_r[23]_i_1_n_0 ;
  wire \data_r[23]_i_2_n_0 ;
  wire \data_r[23]_i_3_n_0 ;
  wire \data_r[23]_i_4_n_0 ;
  wire \data_r[23]_i_5_n_0 ;
  wire \data_r[23]_i_6_n_0 ;
  wire \data_r[23]_i_7_n_0 ;
  wire \data_r[23]_i_8_n_0 ;
  wire \data_r[2]_i_1_n_0 ;
  wire \data_r[3]_i_1_n_0 ;
  wire \data_r[4]_i_1_n_0 ;
  wire \data_r[5]_i_1_n_0 ;
  wire \data_r[6]_i_1_n_0 ;
  wire \data_r[7]_i_1_n_0 ;
  wire \data_r[7]_i_2_n_0 ;
  wire \data_r[8]_i_1_n_0 ;
  wire \data_r[9]_i_1_n_0 ;
  wire \data_r_reg[0]_0 ;
  wire [22:0]\data_r_reg[23]_0 ;
  wire enable_cnt;
  wire enable_sck;
  wire enable_sck0;
  wire enable_sck_i_2__0_n_0;
  wire enable_sck_i_3_n_0;
  wire i__carry__0_i_1__1_n_0;
  wire i__carry__0_i_2_n_0;
  wire i__carry__0_i_3__1_n_0;
  wire i__carry__0_i_4__1_n_0;
  wire i__carry__1_i_1_n_0;
  wire i__carry__1_i_2_n_0;
  wire i__carry__1_i_3_n_0;
  wire i__carry__1_i_4_n_0;
  wire i__carry__2_i_1_n_0;
  wire i__carry__2_i_2_n_0;
  wire i__carry__2_i_3_n_0;
  wire i__carry_i_1__3_n_0;
  wire i__carry_i_2__3_n_0;
  wire i__carry_i_3__3_n_0;
  wire i__carry_i_4__3_n_0;
  wire i__carry_i_5__0_n_0;
  wire [0:0]ip2mb_reg1;
  wire mosi_r_i_10_n_0;
  wire mosi_r_i_11_n_0;
  wire mosi_r_i_12_n_0;
  wire mosi_r_i_1_n_0;
  wire mosi_r_i_3_n_0;
  wire mosi_r_i_4_n_0;
  wire mosi_r_i_6_n_0;
  wire mosi_r_i_8_n_0;
  wire mosi_r_i_9_n_0;
  wire mosi_r_reg_0;
  wire [2:0]out;
  wire pll_data_ready_RX;
  wire pll_data_ready_TX;
  wire pll_mosi_read;
  wire pll_sen_read;
  wire ready_r0;
  wire ready_r_i_1_n_0;
  wire ready_r_i_3_n_0;
  wire ready_r_i_4_n_0;
  wire spi_pll_ld_sdo;
  wire start_prev;
  wire start_prev0;
  wire start_prev_i_1_n_0;
  wire start_prev_i_3__0_n_0;
  wire start_prev_i_4_n_0;
  wire start_prev_i_5_n_0;
  wire [3:3]\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED ;
  wire [2:2]\NLW_data_r1_inferred__0/i__carry__2_CO_UNCONNECTED ;
  wire [3:3]\NLW_data_r1_inferred__0/i__carry__2_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h8C8C8C8CC0C0C000)) 
    \axi_rdata[0]_i_4 
       (.I0(ip2mb_reg1),
        .I1(\axi_rdata_reg[0] [1]),
        .I2(\axi_rdata_reg[0]_0 ),
        .I3(pll_data_ready_TX),
        .I4(pll_data_ready_RX),
        .I5(\axi_rdata_reg[0] [0]),
        .O(\data_r_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h55555555555D5555)) 
    \cnt[0]_i_1 
       (.I0(enable_cnt),
        .I1(\cnt[0]_i_3_n_0 ),
        .I2(\cnt[0]_i_4__0_n_0 ),
        .I3(cnt_reg[12]),
        .I4(out[2]),
        .I5(\cnt[0]_i_5_n_0 ),
        .O(\cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \cnt[0]_i_3 
       (.I0(cnt_reg[10]),
        .I1(cnt_reg[11]),
        .I2(cnt_reg[9]),
        .I3(cnt_reg[14]),
        .I4(cnt_reg[13]),
        .O(\cnt[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \cnt[0]_i_4__0 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .I2(cnt_reg[4]),
        .I3(cnt_reg[5]),
        .I4(cnt_reg[8]),
        .O(\cnt[0]_i_4__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cnt[0]_i_5 
       (.I0(out[0]),
        .I1(out[1]),
        .I2(cnt_reg[3]),
        .I3(cnt_reg[15]),
        .O(\cnt[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_6__0 
       (.I0(out[0]),
        .O(\cnt[0]_i_6__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_7 ),
        .Q(out[0]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2_n_0 ,\cnt_reg[0]_i_2_n_1 ,\cnt_reg[0]_i_2_n_2 ,\cnt_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2_n_4 ,\cnt_reg[0]_i_2_n_5 ,\cnt_reg[0]_i_2_n_6 ,\cnt_reg[0]_i_2_n_7 }),
        .S({cnt_reg[3],out[2:1],\cnt[0]_i_6__0_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_reg[12]_i_1_n_1 ,\cnt_reg[12]_i_1_n_2 ,\cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_6 ),
        .Q(out[1]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_5 ),
        .Q(out[2]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_4 ),
        .Q(cnt_reg[3]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_2_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\cnt_reg[4]_i_1_n_1 ,\cnt_reg[4]_i_1_n_2 ,\cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S(cnt_reg[7:4]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\cnt_reg[8]_i_1_n_1 ,\cnt_reg[8]_i_1_n_2 ,\cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]),
        .R(\cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h00AE)) 
    cs_r_i_1
       (.I0(pll_sen_read),
        .I1(START_receive),
        .I2(enable_cnt),
        .I3(mosi_r_i_3_n_0),
        .O(cs_r_i_1_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    cs_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(cs_r_i_1_n_0),
        .Q(pll_sen_read),
        .R(1'b0));
  CARRY4 \data_r1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\data_r1_inferred__0/i__carry_n_0 ,\data_r1_inferred__0/i__carry_n_1 ,\data_r1_inferred__0/i__carry_n_2 ,\data_r1_inferred__0/i__carry_n_3 }),
        .CYINIT(i__carry_i_1__3_n_0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data_r1[4:1]),
        .S({i__carry_i_2__3_n_0,i__carry_i_3__3_n_0,i__carry_i_4__3_n_0,i__carry_i_5__0_n_0}));
  CARRY4 \data_r1_inferred__0/i__carry__0 
       (.CI(\data_r1_inferred__0/i__carry_n_0 ),
        .CO({\data_r1_inferred__0/i__carry__0_n_0 ,\data_r1_inferred__0/i__carry__0_n_1 ,\data_r1_inferred__0/i__carry__0_n_2 ,\data_r1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,i__carry__0_i_1__1_n_0}),
        .O(data_r1[8:5]),
        .S({i__carry__0_i_2_n_0,i__carry__0_i_3__1_n_0,i__carry__0_i_4__1_n_0,cnt_reg[5]}));
  CARRY4 \data_r1_inferred__0/i__carry__1 
       (.CI(\data_r1_inferred__0/i__carry__0_n_0 ),
        .CO({\data_r1_inferred__0/i__carry__1_n_0 ,\data_r1_inferred__0/i__carry__1_n_1 ,\data_r1_inferred__0/i__carry__1_n_2 ,\data_r1_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data_r1[12:9]),
        .S({i__carry__1_i_1_n_0,i__carry__1_i_2_n_0,i__carry__1_i_3_n_0,i__carry__1_i_4_n_0}));
  CARRY4 \data_r1_inferred__0/i__carry__2 
       (.CI(\data_r1_inferred__0/i__carry__1_n_0 ),
        .CO({\data_r1_inferred__0/i__carry__2_n_0 ,\NLW_data_r1_inferred__0/i__carry__2_CO_UNCONNECTED [2],\data_r1_inferred__0/i__carry__2_n_2 ,\data_r1_inferred__0/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_data_r1_inferred__0/i__carry__2_O_UNCONNECTED [3],data_r1[15:13]}),
        .S({1'b1,i__carry__2_i_1_n_0,i__carry__2_i_2_n_0,i__carry__2_i_3_n_0}));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000002)) 
    \data_r[0]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(ip2mb_reg1),
        .O(\data_r[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[10]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [9]),
        .O(\data_r[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \data_r[11]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [10]),
        .O(\data_r[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \data_r[12]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [11]),
        .O(\data_r[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[13]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [12]),
        .O(\data_r[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[14]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [13]),
        .O(\data_r[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \data_r[15]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [14]),
        .O(\data_r[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \data_r[15]_i_2 
       (.I0(\data_r[23]_i_3_n_0 ),
        .I1(data_r1[3]),
        .I2(data_r1[4]),
        .I3(\data_r[23]_i_6_n_0 ),
        .I4(\data_r[23]_i_5_n_0 ),
        .I5(\data_r[23]_i_4_n_0 ),
        .O(\data_r[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFB00000008)) 
    \data_r[16]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [15]),
        .O(\data_r[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFBFF00000800)) 
    \data_r[17]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(data_r1[1]),
        .I3(out[0]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [16]),
        .O(\data_r[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFBFF00000800)) 
    \data_r[18]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [17]),
        .O(\data_r[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \data_r[19]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [18]),
        .O(\data_r[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[1]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [0]),
        .O(\data_r[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFBFFFF00080000)) 
    \data_r[20]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [19]),
        .O(\data_r[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \data_r[21]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(data_r1[1]),
        .I3(out[0]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [20]),
        .O(\data_r[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \data_r[22]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [21]),
        .O(\data_r[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hBFFFFFFF80000000)) 
    \data_r[23]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(\data_r[23]_i_2_n_0 ),
        .I2(out[0]),
        .I3(data_r1[1]),
        .I4(data_r1[2]),
        .I5(\data_r_reg[23]_0 [22]),
        .O(\data_r[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \data_r[23]_i_2 
       (.I0(\data_r[23]_i_3_n_0 ),
        .I1(data_r1[3]),
        .I2(data_r1[4]),
        .I3(\data_r[23]_i_4_n_0 ),
        .I4(\data_r[23]_i_5_n_0 ),
        .I5(\data_r[23]_i_6_n_0 ),
        .O(\data_r[23]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFF1D)) 
    \data_r[23]_i_3 
       (.I0(mosi_r_i_11_n_0),
        .I1(cnt_reg[5]),
        .I2(\data_r[23]_i_7_n_0 ),
        .I3(enable_sck_i_3_n_0),
        .I4(\data_r[23]_i_8_n_0 ),
        .I5(cnt_reg[8]),
        .O(\data_r[23]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \data_r[23]_i_4 
       (.I0(data_r1[5]),
        .I1(data_r1[14]),
        .I2(data_r1[12]),
        .I3(data_r1[15]),
        .O(\data_r[23]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \data_r[23]_i_5 
       (.I0(data_r1[9]),
        .I1(data_r1[11]),
        .I2(data_r1[6]),
        .I3(data_r1[10]),
        .O(\data_r[23]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \data_r[23]_i_6 
       (.I0(\data_r1_inferred__0/i__carry__2_n_0 ),
        .I1(data_r1[8]),
        .I2(data_r1[7]),
        .I3(data_r1[13]),
        .O(\data_r[23]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \data_r[23]_i_7 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[4]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(out[2]),
        .O(\data_r[23]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \data_r[23]_i_8 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .O(\data_r[23]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[2]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [1]),
        .O(\data_r[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFBF00000080)) 
    \data_r[3]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [2]),
        .O(\data_r[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEFF00000200)) 
    \data_r[4]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [3]),
        .O(\data_r[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[5]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [4]),
        .O(\data_r[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \data_r[6]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [5]),
        .O(\data_r[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBFFF00008000)) 
    \data_r[7]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[7]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [6]),
        .O(\data_r[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \data_r[7]_i_2 
       (.I0(\data_r[23]_i_3_n_0 ),
        .I1(data_r1[4]),
        .I2(\data_r[23]_i_6_n_0 ),
        .I3(\data_r[23]_i_5_n_0 ),
        .I4(\data_r[23]_i_4_n_0 ),
        .I5(data_r1[3]),
        .O(\data_r[7]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000002)) 
    \data_r[8]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(out[0]),
        .I2(data_r1[1]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [7]),
        .O(\data_r[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFEF00000020)) 
    \data_r[9]_i_1 
       (.I0(spi_pll_ld_sdo),
        .I1(data_r1[1]),
        .I2(out[0]),
        .I3(data_r1[2]),
        .I4(\data_r[15]_i_2_n_0 ),
        .I5(\data_r_reg[23]_0 [8]),
        .O(\data_r[9]_i_1_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[0]_i_1_n_0 ),
        .Q(ip2mb_reg1),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[10]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [9]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[11]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [10]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[12]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [11]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[13]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [12]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[14]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [13]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[15]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [14]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[16] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[16]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [15]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[17] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[17]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [16]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[18] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[18]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [17]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[19] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[19]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [18]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[1]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[20] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[20]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [19]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[21] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[21]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [20]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[22] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[22]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [21]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[23] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[23]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [22]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[2]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [1]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[3]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [2]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[4]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [3]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[5]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [4]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[6]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [5]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[7]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [6]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[8]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [7]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \data_r_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\data_r[9]_i_1_n_0 ),
        .Q(\data_r_reg[23]_0 [8]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA8)) 
    enable_sck_i_1
       (.I0(enable_sck_i_2__0_n_0),
        .I1(out[2]),
        .I2(out[0]),
        .I3(out[1]),
        .I4(cnt_reg[4]),
        .I5(cnt_reg[3]),
        .O(enable_sck0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    enable_sck_i_2__0
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[8]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[7]),
        .I4(enable_sck_i_3_n_0),
        .O(enable_sck_i_2__0_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFD)) 
    enable_sck_i_3
       (.I0(start_prev_i_3__0_n_0),
        .I1(cnt_reg[12]),
        .I2(cnt_reg[13]),
        .I3(cnt_reg[14]),
        .I4(cnt_reg[15]),
        .O(enable_sck_i_3_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    enable_sck_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(enable_sck0),
        .Q(enable_sck),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_1__1
       (.I0(cnt_reg[5]),
        .O(i__carry__0_i_1__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_2
       (.I0(cnt_reg[8]),
        .O(i__carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_3__1
       (.I0(cnt_reg[7]),
        .O(i__carry__0_i_3__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_4__1
       (.I0(cnt_reg[6]),
        .O(i__carry__0_i_4__1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_1
       (.I0(cnt_reg[12]),
        .O(i__carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_2
       (.I0(cnt_reg[11]),
        .O(i__carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_3
       (.I0(cnt_reg[10]),
        .O(i__carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__1_i_4
       (.I0(cnt_reg[9]),
        .O(i__carry__1_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_1
       (.I0(cnt_reg[15]),
        .O(i__carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_2
       (.I0(cnt_reg[14]),
        .O(i__carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__2_i_3
       (.I0(cnt_reg[13]),
        .O(i__carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_1__3
       (.I0(out[0]),
        .O(i__carry_i_1__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_2__3
       (.I0(cnt_reg[4]),
        .O(i__carry_i_2__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_3__3
       (.I0(cnt_reg[3]),
        .O(i__carry_i_3__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_4__3
       (.I0(out[2]),
        .O(i__carry_i_4__3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_5__0
       (.I0(out[1]),
        .O(i__carry_i_5__0_n_0));
  LUT6 #(
    .INIT(64'h1010101F10101010)) 
    mosi_r_i_1
       (.I0(mosi_r_reg_0),
        .I1(mosi_r_i_3_n_0),
        .I2(mosi_r_i_4_n_0),
        .I3(start_prev0),
        .I4(mosi_r_i_6_n_0),
        .I5(pll_mosi_read),
        .O(mosi_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    mosi_r_i_10
       (.I0(enable_sck_i_3_n_0),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[8]),
        .I4(cnt_reg[5]),
        .I5(mosi_r_i_11_n_0),
        .O(mosi_r_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'hE)) 
    mosi_r_i_11
       (.I0(cnt_reg[4]),
        .I1(cnt_reg[3]),
        .O(mosi_r_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    mosi_r_i_12
       (.I0(enable_sck_i_3_n_0),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[8]),
        .O(mosi_r_i_12_n_0));
  LUT6 #(
    .INIT(64'h0000000000000004)) 
    mosi_r_i_3
       (.I0(mosi_r_i_8_n_0),
        .I1(start_prev_i_3__0_n_0),
        .I2(cnt_reg[12]),
        .I3(cnt_reg[14]),
        .I4(cnt_reg[13]),
        .I5(mosi_r_i_9_n_0),
        .O(mosi_r_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hAAA8)) 
    mosi_r_i_4
       (.I0(mosi_r_i_10_n_0),
        .I1(out[1]),
        .I2(out[0]),
        .I3(out[2]),
        .O(mosi_r_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    mosi_r_i_5
       (.I0(START_receive),
        .I1(enable_cnt),
        .O(start_prev0));
  LUT6 #(
    .INIT(64'h0000000050525252)) 
    mosi_r_i_6
       (.I0(cnt_reg[5]),
        .I1(out[2]),
        .I2(mosi_r_i_11_n_0),
        .I3(out[1]),
        .I4(out[0]),
        .I5(mosi_r_i_12_n_0),
        .O(mosi_r_i_6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    mosi_r_i_8
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[15]),
        .I2(cnt_reg[4]),
        .I3(cnt_reg[8]),
        .I4(cnt_reg[7]),
        .I5(cnt_reg[6]),
        .O(mosi_r_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFBFF)) 
    mosi_r_i_9
       (.I0(out[1]),
        .I1(out[0]),
        .I2(out[2]),
        .I3(cnt_reg[5]),
        .O(mosi_r_i_9_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    mosi_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(mosi_r_i_1_n_0),
        .Q(pll_mosi_read),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h0000EECE)) 
    ready_r_i_1
       (.I0(pll_data_ready_RX),
        .I1(ready_r0),
        .I2(START_receive),
        .I3(enable_cnt),
        .I4(start_prev),
        .O(ready_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h2020202000000020)) 
    ready_r_i_2
       (.I0(ready_r_i_3_n_0),
        .I1(enable_sck_i_3_n_0),
        .I2(cnt_reg[8]),
        .I3(cnt_reg[3]),
        .I4(out[2]),
        .I5(\cnt[0]_i_4__0_n_0 ),
        .O(ready_r0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF800000)) 
    ready_r_i_3
       (.I0(ready_r_i_4_n_0),
        .I1(cnt_reg[3]),
        .I2(out[2]),
        .I3(cnt_reg[4]),
        .I4(cnt_reg[5]),
        .I5(\data_r[23]_i_8_n_0 ),
        .O(ready_r_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'hE)) 
    ready_r_i_4
       (.I0(out[1]),
        .I1(out[0]),
        .O(ready_r_i_4_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    ready_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(ready_r_i_1_n_0),
        .Q(pll_data_ready_RX),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    start_prev_i_1
       (.I0(enable_cnt),
        .I1(START_receive),
        .I2(start_prev),
        .O(start_prev_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    start_prev_i_2
       (.I0(cnt_reg[13]),
        .I1(cnt_reg[14]),
        .I2(start_prev_i_3__0_n_0),
        .I3(start_prev_i_4_n_0),
        .I4(\cnt[0]_i_5_n_0 ),
        .I5(start_prev_i_5_n_0),
        .O(start_prev));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h01)) 
    start_prev_i_3__0
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[11]),
        .I2(cnt_reg[10]),
        .O(start_prev_i_3__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    start_prev_i_4
       (.I0(cnt_reg[5]),
        .I1(cnt_reg[4]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[7]),
        .O(start_prev_i_4_n_0));
  LUT5 #(
    .INIT(32'hFF4FFFFF)) 
    start_prev_i_5
       (.I0(cnt_reg[13]),
        .I1(cnt_reg[12]),
        .I2(out[2]),
        .I3(cnt_reg[14]),
        .I4(cnt_reg[8]),
        .O(start_prev_i_5_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(start_prev_i_1_n_0),
        .Q(enable_cnt),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX
   (pll_data_ready_TX,
    out,
    spi_pll_sck,
    spi_pll_sen,
    spi_pll_mosi,
    clk_10MHz,
    START_send,
    mosi_r_reg_0,
    enable_sck,
    mosi_r_reg_1,
    mosi_r_reg_2,
    mosi_r_reg_3,
    pll_sen_read,
    pll_mosi_read,
    mosi_r_reg_4);
  output pll_data_ready_TX;
  output [3:0]out;
  output spi_pll_sck;
  output spi_pll_sen;
  output spi_pll_mosi;
  input clk_10MHz;
  input START_send;
  input mosi_r_reg_0;
  input enable_sck;
  input mosi_r_reg_1;
  input mosi_r_reg_2;
  input mosi_r_reg_3;
  input pll_sen_read;
  input pll_mosi_read;
  input mosi_r_reg_4;

  wire START_send;
  wire clk_10MHz;
  wire \cnt[0]_i_1__0_n_0 ;
  wire \cnt[0]_i_3__0_n_0 ;
  wire \cnt[0]_i_4_n_0 ;
  wire \cnt[0]_i_5__0_n_0 ;
  wire \cnt[0]_i_6_n_0 ;
  wire \cnt[0]_i_7_n_0 ;
  wire [15:3]cnt_reg;
  wire \cnt_reg[0]_i_2__0_n_0 ;
  wire \cnt_reg[0]_i_2__0_n_1 ;
  wire \cnt_reg[0]_i_2__0_n_2 ;
  wire \cnt_reg[0]_i_2__0_n_3 ;
  wire \cnt_reg[0]_i_2__0_n_4 ;
  wire \cnt_reg[0]_i_2__0_n_5 ;
  wire \cnt_reg[0]_i_2__0_n_6 ;
  wire \cnt_reg[0]_i_2__0_n_7 ;
  wire \cnt_reg[12]_i_1__0_n_1 ;
  wire \cnt_reg[12]_i_1__0_n_2 ;
  wire \cnt_reg[12]_i_1__0_n_3 ;
  wire \cnt_reg[12]_i_1__0_n_4 ;
  wire \cnt_reg[12]_i_1__0_n_5 ;
  wire \cnt_reg[12]_i_1__0_n_6 ;
  wire \cnt_reg[12]_i_1__0_n_7 ;
  wire \cnt_reg[4]_i_1__0_n_0 ;
  wire \cnt_reg[4]_i_1__0_n_1 ;
  wire \cnt_reg[4]_i_1__0_n_2 ;
  wire \cnt_reg[4]_i_1__0_n_3 ;
  wire \cnt_reg[4]_i_1__0_n_4 ;
  wire \cnt_reg[4]_i_1__0_n_5 ;
  wire \cnt_reg[4]_i_1__0_n_6 ;
  wire \cnt_reg[4]_i_1__0_n_7 ;
  wire \cnt_reg[8]_i_1__0_n_0 ;
  wire \cnt_reg[8]_i_1__0_n_1 ;
  wire \cnt_reg[8]_i_1__0_n_2 ;
  wire \cnt_reg[8]_i_1__0_n_3 ;
  wire \cnt_reg[8]_i_1__0_n_4 ;
  wire \cnt_reg[8]_i_1__0_n_5 ;
  wire \cnt_reg[8]_i_1__0_n_6 ;
  wire \cnt_reg[8]_i_1__0_n_7 ;
  wire cs_r_i_1__0_n_0;
  wire cs_r_i_2_n_0;
  wire cs_r_i_3_n_0;
  wire cs_r_i_4_n_0;
  wire enable_cnt;
  wire enable_sck;
  wire enable_sck_0;
  wire enable_sck_i_1__0_n_0;
  wire enable_sck_i_2_n_0;
  wire enable_sck_i_3__0_n_0;
  wire mosi_r_i_12__0_n_0;
  wire mosi_r_i_13_n_0;
  wire mosi_r_i_15_n_0;
  wire mosi_r_i_16_n_0;
  wire mosi_r_i_17_n_0;
  wire mosi_r_i_1__0_n_0;
  wire mosi_r_i_20_n_0;
  wire mosi_r_i_22_n_0;
  wire mosi_r_i_3__0_n_0;
  wire mosi_r_i_4__0_n_0;
  wire mosi_r_i_5__0_n_0;
  wire mosi_r_i_6__0_n_0;
  wire mosi_r_reg_0;
  wire mosi_r_reg_1;
  wire mosi_r_reg_2;
  wire mosi_r_reg_3;
  wire mosi_r_reg_4;
  wire [3:0]out;
  wire pll_data_ready_TX;
  wire pll_mosi_read;
  wire pll_mosi_write;
  wire pll_sen_read;
  wire pll_sen_write;
  wire ready_tx_r;
  wire ready_tx_r08_out;
  wire ready_tx_r_i_1_n_0;
  wire ready_tx_r_i_3_n_0;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_prev_i_1__0_n_0;
  wire start_prev_i_3_n_0;
  wire [3:3]\NLW_cnt_reg[12]_i_1__0_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h5555555555555755)) 
    \cnt[0]_i_1__0 
       (.I0(enable_cnt),
        .I1(\cnt[0]_i_3__0_n_0 ),
        .I2(cnt_reg[3]),
        .I3(out[2]),
        .I4(\cnt[0]_i_4_n_0 ),
        .I5(\cnt[0]_i_5__0_n_0 ),
        .O(\cnt[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \cnt[0]_i_3__0 
       (.I0(cnt_reg[10]),
        .I1(cnt_reg[11]),
        .I2(cnt_reg[9]),
        .I3(cnt_reg[12]),
        .I4(cnt_reg[15]),
        .I5(\cnt[0]_i_7_n_0 ),
        .O(\cnt[0]_i_3__0_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \cnt[0]_i_4 
       (.I0(out[1]),
        .I1(out[0]),
        .O(\cnt[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \cnt[0]_i_5__0 
       (.I0(cnt_reg[5]),
        .I1(out[3]),
        .I2(cnt_reg[8]),
        .I3(cnt_reg[7]),
        .I4(cnt_reg[6]),
        .O(\cnt[0]_i_5__0_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_6 
       (.I0(out[0]),
        .O(\cnt[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \cnt[0]_i_7 
       (.I0(cnt_reg[13]),
        .I1(cnt_reg[14]),
        .O(\cnt[0]_i_7_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_7 ),
        .Q(out[0]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[0]_i_2__0 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2__0_n_0 ,\cnt_reg[0]_i_2__0_n_1 ,\cnt_reg[0]_i_2__0_n_2 ,\cnt_reg[0]_i_2__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2__0_n_4 ,\cnt_reg[0]_i_2__0_n_5 ,\cnt_reg[0]_i_2__0_n_6 ,\cnt_reg[0]_i_2__0_n_7 }),
        .S({cnt_reg[3],out[2:1],\cnt[0]_i_6_n_0 }));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_5 ),
        .Q(cnt_reg[10]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_4 ),
        .Q(cnt_reg[11]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_7 ),
        .Q(cnt_reg[12]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[12]_i_1__0 
       (.CI(\cnt_reg[8]_i_1__0_n_0 ),
        .CO({\NLW_cnt_reg[12]_i_1__0_CO_UNCONNECTED [3],\cnt_reg[12]_i_1__0_n_1 ,\cnt_reg[12]_i_1__0_n_2 ,\cnt_reg[12]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1__0_n_4 ,\cnt_reg[12]_i_1__0_n_5 ,\cnt_reg[12]_i_1__0_n_6 ,\cnt_reg[12]_i_1__0_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_6 ),
        .Q(cnt_reg[13]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_5 ),
        .Q(cnt_reg[14]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1__0_n_4 ),
        .Q(cnt_reg[15]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_6 ),
        .Q(out[1]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_5 ),
        .Q(out[2]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2__0_n_4 ),
        .Q(cnt_reg[3]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_7 ),
        .Q(out[3]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[4]_i_1__0 
       (.CI(\cnt_reg[0]_i_2__0_n_0 ),
        .CO({\cnt_reg[4]_i_1__0_n_0 ,\cnt_reg[4]_i_1__0_n_1 ,\cnt_reg[4]_i_1__0_n_2 ,\cnt_reg[4]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1__0_n_4 ,\cnt_reg[4]_i_1__0_n_5 ,\cnt_reg[4]_i_1__0_n_6 ,\cnt_reg[4]_i_1__0_n_7 }),
        .S({cnt_reg[7:5],out[3]}));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_6 ),
        .Q(cnt_reg[5]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_5 ),
        .Q(cnt_reg[6]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1__0_n_4 ),
        .Q(cnt_reg[7]),
        .R(\cnt[0]_i_1__0_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_7 ),
        .Q(cnt_reg[8]),
        .R(\cnt[0]_i_1__0_n_0 ));
  CARRY4 \cnt_reg[8]_i_1__0 
       (.CI(\cnt_reg[4]_i_1__0_n_0 ),
        .CO({\cnt_reg[8]_i_1__0_n_0 ,\cnt_reg[8]_i_1__0_n_1 ,\cnt_reg[8]_i_1__0_n_2 ,\cnt_reg[8]_i_1__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1__0_n_4 ,\cnt_reg[8]_i_1__0_n_5 ,\cnt_reg[8]_i_1__0_n_6 ,\cnt_reg[8]_i_1__0_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1__0_n_6 ),
        .Q(cnt_reg[9]),
        .R(\cnt[0]_i_1__0_n_0 ));
  LUT6 #(
    .INIT(64'hAEAEAE00AEAEAEAE)) 
    cs_r_i_1__0
       (.I0(pll_sen_write),
        .I1(START_send),
        .I2(enable_cnt),
        .I3(cs_r_i_2_n_0),
        .I4(cs_r_i_3_n_0),
        .I5(cs_r_i_4_n_0),
        .O(cs_r_i_1__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    cs_r_i_2
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[15]),
        .I2(out[1]),
        .I3(out[0]),
        .I4(cnt_reg[13]),
        .I5(cnt_reg[14]),
        .O(cs_r_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFEFF)) 
    cs_r_i_3
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .I2(cnt_reg[8]),
        .I3(cnt_reg[5]),
        .I4(out[2]),
        .I5(out[3]),
        .O(cs_r_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000001010001)) 
    cs_r_i_4
       (.I0(cnt_reg[10]),
        .I1(cnt_reg[11]),
        .I2(cnt_reg[9]),
        .I3(cnt_reg[12]),
        .I4(cnt_reg[13]),
        .I5(cnt_reg[14]),
        .O(cs_r_i_4_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    cs_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(cs_r_i_1__0_n_0),
        .Q(pll_sen_write),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h1110)) 
    enable_sck_i_1__0
       (.I0(enable_sck_i_2_n_0),
        .I1(cnt_reg[5]),
        .I2(enable_sck_i_3__0_n_0),
        .I3(out[0]),
        .O(enable_sck_i_1__0_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    enable_sck_i_2
       (.I0(\cnt[0]_i_3__0_n_0 ),
        .I1(cnt_reg[7]),
        .I2(cnt_reg[6]),
        .I3(cnt_reg[8]),
        .O(enable_sck_i_2_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    enable_sck_i_3__0
       (.I0(out[2]),
        .I1(out[1]),
        .I2(cnt_reg[3]),
        .I3(out[3]),
        .O(enable_sck_i_3__0_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    enable_sck_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(enable_sck_i_1__0_n_0),
        .Q(enable_sck_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    mosi_r_i_12__0
       (.I0(mosi_r_i_20_n_0),
        .I1(cnt_reg[5]),
        .I2(out[3]),
        .I3(cnt_reg[3]),
        .I4(\cnt[0]_i_3__0_n_0 ),
        .I5(cnt_reg[8]),
        .O(mosi_r_i_12__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'hE)) 
    mosi_r_i_13
       (.I0(out[1]),
        .I1(out[2]),
        .O(mosi_r_i_13_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFEC00000000)) 
    mosi_r_i_15
       (.I0(out[0]),
        .I1(out[2]),
        .I2(out[1]),
        .I3(cnt_reg[3]),
        .I4(out[3]),
        .I5(cnt_reg[5]),
        .O(mosi_r_i_15_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    mosi_r_i_16
       (.I0(out[2]),
        .I1(out[1]),
        .I2(cnt_reg[5]),
        .I3(out[0]),
        .I4(out[3]),
        .I5(cnt_reg[3]),
        .O(mosi_r_i_16_n_0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h0000000E)) 
    mosi_r_i_17
       (.I0(out[2]),
        .I1(out[1]),
        .I2(cnt_reg[8]),
        .I3(\cnt[0]_i_3__0_n_0 ),
        .I4(mosi_r_i_22_n_0),
        .O(mosi_r_i_17_n_0));
  LUT6 #(
    .INIT(64'hFF0F0E0E000F0E0E)) 
    mosi_r_i_1__0
       (.I0(mosi_r_reg_4),
        .I1(mosi_r_i_3__0_n_0),
        .I2(mosi_r_i_4__0_n_0),
        .I3(mosi_r_i_5__0_n_0),
        .I4(mosi_r_i_6__0_n_0),
        .I5(pll_mosi_write),
        .O(mosi_r_i_1__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'hE)) 
    mosi_r_i_20
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .O(mosi_r_i_20_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    mosi_r_i_22
       (.I0(cnt_reg[3]),
        .I1(out[3]),
        .I2(cnt_reg[5]),
        .I3(cnt_reg[7]),
        .I4(cnt_reg[6]),
        .O(mosi_r_i_22_n_0));
  LUT6 #(
    .INIT(64'hA8A8A8080808A808)) 
    mosi_r_i_3__0
       (.I0(out[3]),
        .I1(mosi_r_reg_1),
        .I2(cnt_reg[3]),
        .I3(mosi_r_reg_2),
        .I4(out[2]),
        .I5(mosi_r_reg_3),
        .O(mosi_r_i_3__0_n_0));
  LUT6 #(
    .INIT(64'hFBFBFB51FBFBFFFF)) 
    mosi_r_i_4__0
       (.I0(mosi_r_i_12__0_n_0),
        .I1(mosi_r_i_13_n_0),
        .I2(mosi_r_reg_0),
        .I3(mosi_r_i_15_n_0),
        .I4(enable_sck_i_2_n_0),
        .I5(mosi_r_i_16_n_0),
        .O(mosi_r_i_4__0_n_0));
  LUT6 #(
    .INIT(64'h5545554500005545)) 
    mosi_r_i_5__0
       (.I0(mosi_r_i_17_n_0),
        .I1(enable_sck_i_3__0_n_0),
        .I2(out[0]),
        .I3(enable_sck_i_2_n_0),
        .I4(START_send),
        .I5(enable_cnt),
        .O(mosi_r_i_5__0_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    mosi_r_i_6__0
       (.I0(mosi_r_i_15_n_0),
        .I1(enable_sck_i_2_n_0),
        .I2(mosi_r_i_12__0_n_0),
        .O(mosi_r_i_6__0_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    mosi_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(mosi_r_i_1__0_n_0),
        .Q(pll_mosi_write),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h0000EECE)) 
    ready_tx_r_i_1
       (.I0(pll_data_ready_TX),
        .I1(ready_tx_r08_out),
        .I2(START_send),
        .I3(enable_cnt),
        .I4(ready_tx_r),
        .O(ready_tx_r_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000AA0200000000)) 
    ready_tx_r_i_2
       (.I0(ready_tx_r_i_3_n_0),
        .I1(cnt_reg[3]),
        .I2(out[2]),
        .I3(\cnt[0]_i_5__0_n_0 ),
        .I4(\cnt[0]_i_3__0_n_0 ),
        .I5(cnt_reg[8]),
        .O(ready_tx_r08_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFF800000)) 
    ready_tx_r_i_3
       (.I0(cnt_reg[3]),
        .I1(out[2]),
        .I2(\cnt[0]_i_4_n_0 ),
        .I3(out[3]),
        .I4(cnt_reg[5]),
        .I5(mosi_r_i_20_n_0),
        .O(ready_tx_r_i_3_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    ready_tx_r_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(ready_tx_r_i_1_n_0),
        .Q(pll_data_ready_TX),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hE)) 
    spi_pll_mosi_INST_0
       (.I0(pll_mosi_write),
        .I1(pll_mosi_read),
        .O(spi_pll_mosi));
  LUT3 #(
    .INIT(8'hA8)) 
    spi_pll_sck_INST_0
       (.I0(clk_10MHz),
        .I1(enable_sck_0),
        .I2(enable_sck),
        .O(spi_pll_sck));
  LUT2 #(
    .INIT(4'hE)) 
    spi_pll_sen_INST_0
       (.I0(pll_sen_write),
        .I1(pll_sen_read),
        .O(spi_pll_sen));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    start_prev_i_1__0
       (.I0(enable_cnt),
        .I1(START_send),
        .I2(ready_tx_r),
        .O(start_prev_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h0000000000020000)) 
    start_prev_i_2__0
       (.I0(out[2]),
        .I1(cnt_reg[15]),
        .I2(cnt_reg[3]),
        .I3(start_prev_i_3_n_0),
        .I4(cs_r_i_4_n_0),
        .I5(\cnt[0]_i_5__0_n_0 ),
        .O(ready_tx_r));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    start_prev_i_3
       (.I0(out[0]),
        .I1(out[1]),
        .I2(cnt_reg[14]),
        .I3(cnt_reg[13]),
        .O(start_prev_i_3_n_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    start_prev_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(start_prev_i_1__0_n_0),
        .Q(enable_cnt),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top_PLL_control
   (spi_pll_cen,
    PLL_POW_EN,
    PAMP_EN,
    frame_even,
    out,
    pll_trig,
    start_adc_count,
    \trig_tsweep_counter_reg[15]_0 ,
    \cnt_reg[4] ,
    spi_pll_sck,
    \data_r_reg[0] ,
    \data_r_reg[23] ,
    spi_pll_sen,
    spi_pll_mosi,
    ATTEN,
    clk_10MHz,
    slv_reg0,
    s00_axi_aclk,
    clkDCO_10MHz,
    pll_cen_reg_reg_0,
    \start_adc_count_r1_inferred__2/i__carry__0_0 ,
    start_adc_count_r_reg_0,
    \start_adc_count_r1_inferred__3/i__carry__0_0 ,
    S,
    mosi_r_reg,
    s00_axi_aresetn,
    O,
    Q,
    mosi_r_reg_0,
    mosi_r_reg_1,
    mosi_r_reg_2,
    \axi_rdata_reg[0] ,
    \axi_rdata_reg[0]_0 ,
    mosi_r_reg_3,
    mosi_r_reg_4,
    spi_pll_ld_sdo,
    SS,
    D,
    \atten_reg_reg[5]_0 );
  output spi_pll_cen;
  output PLL_POW_EN;
  output PAMP_EN;
  output frame_even;
  output [2:0]out;
  output pll_trig;
  output start_adc_count;
  output [15:0]\trig_tsweep_counter_reg[15]_0 ;
  output [3:0]\cnt_reg[4] ;
  output spi_pll_sck;
  output \data_r_reg[0] ;
  output [22:0]\data_r_reg[23] ;
  output spi_pll_sen;
  output spi_pll_mosi;
  output [5:0]ATTEN;
  input clk_10MHz;
  input [2:0]slv_reg0;
  input s00_axi_aclk;
  input clkDCO_10MHz;
  input [2:0]pll_cen_reg_reg_0;
  input [2:0]\start_adc_count_r1_inferred__2/i__carry__0_0 ;
  input [1:0]start_adc_count_r_reg_0;
  input [1:0]\start_adc_count_r1_inferred__3/i__carry__0_0 ;
  input [1:0]S;
  input mosi_r_reg;
  input s00_axi_aresetn;
  input [1:0]O;
  input [6:0]Q;
  input mosi_r_reg_0;
  input mosi_r_reg_1;
  input mosi_r_reg_2;
  input [1:0]\axi_rdata_reg[0] ;
  input \axi_rdata_reg[0]_0 ;
  input mosi_r_reg_3;
  input mosi_r_reg_4;
  input spi_pll_ld_sdo;
  input [0:0]SS;
  input [15:0]D;
  input [5:0]\atten_reg_reg[5]_0 ;

  wire [5:0]ATTEN;
  wire [15:0]D;
  wire [1:0]O;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire [6:0]Q;
  wire [1:0]S;
  wire [0:0]SS;
  wire START_receive;
  wire START_send;
  wire START_trig;
  wire [5:0]\atten_reg_reg[5]_0 ;
  wire [1:0]\axi_rdata_reg[0] ;
  wire \axi_rdata_reg[0]_0 ;
  wire clkDCO_10MHz;
  wire clk_10MHz;
  wire [3:0]\cnt_reg[4] ;
  wire \data_r_reg[0] ;
  wire [22:0]\data_r_reg[23] ;
  wire enable_sck;
  wire frame_count;
  wire \frame_count[0]_i_10_n_0 ;
  wire \frame_count[0]_i_11_n_0 ;
  wire \frame_count[0]_i_12_n_0 ;
  wire \frame_count[0]_i_13_n_0 ;
  wire \frame_count[0]_i_14_n_0 ;
  wire \frame_count[0]_i_15_n_0 ;
  wire \frame_count[0]_i_1_n_0 ;
  wire \frame_count[0]_i_4_n_0 ;
  wire \frame_count[0]_i_5_n_0 ;
  wire \frame_count[0]_i_6_n_0 ;
  wire \frame_count[0]_i_7_n_0 ;
  wire \frame_count[0]_i_8_n_0 ;
  wire \frame_count[0]_i_9_n_0 ;
  wire [31:1]frame_count_reg;
  wire \frame_count_reg[0]_i_3_n_0 ;
  wire \frame_count_reg[0]_i_3_n_1 ;
  wire \frame_count_reg[0]_i_3_n_2 ;
  wire \frame_count_reg[0]_i_3_n_3 ;
  wire \frame_count_reg[0]_i_3_n_4 ;
  wire \frame_count_reg[0]_i_3_n_5 ;
  wire \frame_count_reg[0]_i_3_n_6 ;
  wire \frame_count_reg[0]_i_3_n_7 ;
  wire \frame_count_reg[12]_i_1_n_0 ;
  wire \frame_count_reg[12]_i_1_n_1 ;
  wire \frame_count_reg[12]_i_1_n_2 ;
  wire \frame_count_reg[12]_i_1_n_3 ;
  wire \frame_count_reg[12]_i_1_n_4 ;
  wire \frame_count_reg[12]_i_1_n_5 ;
  wire \frame_count_reg[12]_i_1_n_6 ;
  wire \frame_count_reg[12]_i_1_n_7 ;
  wire \frame_count_reg[16]_i_1_n_0 ;
  wire \frame_count_reg[16]_i_1_n_1 ;
  wire \frame_count_reg[16]_i_1_n_2 ;
  wire \frame_count_reg[16]_i_1_n_3 ;
  wire \frame_count_reg[16]_i_1_n_4 ;
  wire \frame_count_reg[16]_i_1_n_5 ;
  wire \frame_count_reg[16]_i_1_n_6 ;
  wire \frame_count_reg[16]_i_1_n_7 ;
  wire \frame_count_reg[20]_i_1_n_0 ;
  wire \frame_count_reg[20]_i_1_n_1 ;
  wire \frame_count_reg[20]_i_1_n_2 ;
  wire \frame_count_reg[20]_i_1_n_3 ;
  wire \frame_count_reg[20]_i_1_n_4 ;
  wire \frame_count_reg[20]_i_1_n_5 ;
  wire \frame_count_reg[20]_i_1_n_6 ;
  wire \frame_count_reg[20]_i_1_n_7 ;
  wire \frame_count_reg[24]_i_1_n_0 ;
  wire \frame_count_reg[24]_i_1_n_1 ;
  wire \frame_count_reg[24]_i_1_n_2 ;
  wire \frame_count_reg[24]_i_1_n_3 ;
  wire \frame_count_reg[24]_i_1_n_4 ;
  wire \frame_count_reg[24]_i_1_n_5 ;
  wire \frame_count_reg[24]_i_1_n_6 ;
  wire \frame_count_reg[24]_i_1_n_7 ;
  wire \frame_count_reg[28]_i_1_n_1 ;
  wire \frame_count_reg[28]_i_1_n_2 ;
  wire \frame_count_reg[28]_i_1_n_3 ;
  wire \frame_count_reg[28]_i_1_n_4 ;
  wire \frame_count_reg[28]_i_1_n_5 ;
  wire \frame_count_reg[28]_i_1_n_6 ;
  wire \frame_count_reg[28]_i_1_n_7 ;
  wire \frame_count_reg[4]_i_1_n_0 ;
  wire \frame_count_reg[4]_i_1_n_1 ;
  wire \frame_count_reg[4]_i_1_n_2 ;
  wire \frame_count_reg[4]_i_1_n_3 ;
  wire \frame_count_reg[4]_i_1_n_4 ;
  wire \frame_count_reg[4]_i_1_n_5 ;
  wire \frame_count_reg[4]_i_1_n_6 ;
  wire \frame_count_reg[4]_i_1_n_7 ;
  wire \frame_count_reg[8]_i_1_n_0 ;
  wire \frame_count_reg[8]_i_1_n_1 ;
  wire \frame_count_reg[8]_i_1_n_2 ;
  wire \frame_count_reg[8]_i_1_n_3 ;
  wire \frame_count_reg[8]_i_1_n_4 ;
  wire \frame_count_reg[8]_i_1_n_5 ;
  wire \frame_count_reg[8]_i_1_n_6 ;
  wire \frame_count_reg[8]_i_1_n_7 ;
  wire frame_even;
  wire i__carry__0_i_1__2_n_0;
  wire i__carry__0_i_1__2_n_2;
  wire i__carry__0_i_1__2_n_3;
  wire i__carry__0_i_1_n_0;
  wire i__carry__0_i_2__0_n_0;
  wire i__carry__0_i_2__1_n_0;
  wire i__carry__0_i_3__0_n_0;
  wire i__carry__0_i_3_n_0;
  wire i__carry__0_i_4__0_n_0;
  wire i__carry__0_i_4_n_0;
  wire i__carry__0_i_5__0_n_0;
  wire i__carry__0_i_5_n_0;
  wire i__carry__0_i_6__0_n_0;
  wire i__carry__0_i_6_n_0;
  wire i__carry__0_i_7_n_0;
  wire i__carry__0_i_8_n_0;
  wire i__carry_i_10_n_0;
  wire i__carry_i_11_n_0;
  wire i__carry_i_12_n_0;
  wire i__carry_i_13_n_0;
  wire i__carry_i_14_n_0;
  wire i__carry_i_15_n_0;
  wire i__carry_i_16_n_0;
  wire i__carry_i_17_n_0;
  wire i__carry_i_18_n_0;
  wire i__carry_i_19_n_0;
  wire i__carry_i_1__0_n_0;
  wire i__carry_i_1__2_n_0;
  wire i__carry_i_1_n_0;
  wire i__carry_i_2__0_n_0;
  wire i__carry_i_2_n_0;
  wire i__carry_i_3__0_n_0;
  wire i__carry_i_3__2_n_0;
  wire i__carry_i_3_n_0;
  wire i__carry_i_4__0_n_0;
  wire i__carry_i_4__1_n_0;
  wire i__carry_i_4_n_0;
  wire i__carry_i_5__1_n_0;
  wire i__carry_i_5__1_n_1;
  wire i__carry_i_5__1_n_2;
  wire i__carry_i_5__1_n_3;
  wire i__carry_i_5_n_0;
  wire i__carry_i_6__0_n_0;
  wire i__carry_i_6__0_n_1;
  wire i__carry_i_6__0_n_2;
  wire i__carry_i_6__0_n_3;
  wire i__carry_i_6_n_0;
  wire i__carry_i_7__0_n_0;
  wire i__carry_i_7__0_n_1;
  wire i__carry_i_7__0_n_2;
  wire i__carry_i_7__0_n_3;
  wire i__carry_i_7_n_0;
  wire i__carry_i_8__0_n_0;
  wire i__carry_i_8_n_0;
  wire i__carry_i_9_n_0;
  wire mosi_r_reg;
  wire mosi_r_reg_0;
  wire mosi_r_reg_1;
  wire mosi_r_reg_2;
  wire mosi_r_reg_3;
  wire mosi_r_reg_4;
  wire [2:0]out;
  wire [15:1]p_2_in;
  wire [2:0]pll_cen_reg_reg_0;
  wire pll_data_ready_TX;
  wire pll_mosi_read;
  wire pll_sen_read;
  wire pll_trig;
  wire pll_trig_reg_i_1_n_0;
  wire pll_trig_reg_i_2_n_0;
  wire pll_trig_reg_i_3_n_0;
  wire pll_trig_reg_i_4_n_0;
  wire pll_trig_reg_i_5_n_0;
  wire s00_axi_aclk;
  wire s00_axi_aresetn;
  wire [2:0]slv_reg0;
  wire spi_pll_cen;
  wire spi_pll_ld_sdo;
  wire spi_pll_mosi;
  wire spi_pll_sck;
  wire spi_pll_sen;
  wire start_adc_count;
  wire start_adc_count_r11_in;
  wire start_adc_count_r11_out;
  wire start_adc_count_r1_carry__0_i_1_n_3;
  wire start_adc_count_r1_carry__0_i_2_n_0;
  wire start_adc_count_r1_carry__0_i_3_n_0;
  wire start_adc_count_r1_carry__0_i_4_n_0;
  wire start_adc_count_r1_carry__0_i_4_n_1;
  wire start_adc_count_r1_carry__0_i_4_n_2;
  wire start_adc_count_r1_carry__0_i_4_n_3;
  wire start_adc_count_r1_carry__0_i_5_n_0;
  wire start_adc_count_r1_carry__0_i_6_n_0;
  wire start_adc_count_r1_carry__0_i_7_n_0;
  wire start_adc_count_r1_carry__0_i_8_n_0;
  wire start_adc_count_r1_carry__0_n_0;
  wire start_adc_count_r1_carry__0_n_1;
  wire start_adc_count_r1_carry__0_n_2;
  wire start_adc_count_r1_carry__0_n_3;
  wire start_adc_count_r1_carry__1_n_1;
  wire start_adc_count_r1_carry__1_n_2;
  wire start_adc_count_r1_carry__1_n_3;
  wire start_adc_count_r1_carry_i_10_n_0;
  wire start_adc_count_r1_carry_i_11_n_0;
  wire start_adc_count_r1_carry_i_12_n_0;
  wire start_adc_count_r1_carry_i_13_n_0;
  wire start_adc_count_r1_carry_i_14_n_0;
  wire start_adc_count_r1_carry_i_15_n_0;
  wire start_adc_count_r1_carry_i_16_n_0;
  wire start_adc_count_r1_carry_i_17_n_0;
  wire start_adc_count_r1_carry_i_18_n_0;
  wire start_adc_count_r1_carry_i_1_n_0;
  wire start_adc_count_r1_carry_i_2_n_0;
  wire start_adc_count_r1_carry_i_3_n_0;
  wire start_adc_count_r1_carry_i_4_n_0;
  wire start_adc_count_r1_carry_i_5_n_0;
  wire start_adc_count_r1_carry_i_5_n_1;
  wire start_adc_count_r1_carry_i_5_n_2;
  wire start_adc_count_r1_carry_i_5_n_3;
  wire start_adc_count_r1_carry_i_6_n_0;
  wire start_adc_count_r1_carry_i_6_n_1;
  wire start_adc_count_r1_carry_i_6_n_2;
  wire start_adc_count_r1_carry_i_6_n_3;
  wire start_adc_count_r1_carry_i_7_n_0;
  wire start_adc_count_r1_carry_i_7_n_1;
  wire start_adc_count_r1_carry_i_7_n_2;
  wire start_adc_count_r1_carry_i_7_n_3;
  wire start_adc_count_r1_carry_i_8_n_0;
  wire start_adc_count_r1_carry_i_9_n_0;
  wire start_adc_count_r1_carry_n_0;
  wire start_adc_count_r1_carry_n_1;
  wire start_adc_count_r1_carry_n_2;
  wire start_adc_count_r1_carry_n_3;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_0 ;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_1 ;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_2 ;
  wire \start_adc_count_r1_inferred__0/i__carry__0_n_3 ;
  wire \start_adc_count_r1_inferred__0/i__carry__1_n_2 ;
  wire \start_adc_count_r1_inferred__0/i__carry__1_n_3 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_0 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_1 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_2 ;
  wire \start_adc_count_r1_inferred__0/i__carry_n_3 ;
  wire [2:0]\start_adc_count_r1_inferred__2/i__carry__0_0 ;
  wire \start_adc_count_r1_inferred__2/i__carry__0_n_3 ;
  wire \start_adc_count_r1_inferred__2/i__carry_n_0 ;
  wire \start_adc_count_r1_inferred__2/i__carry_n_1 ;
  wire \start_adc_count_r1_inferred__2/i__carry_n_2 ;
  wire \start_adc_count_r1_inferred__2/i__carry_n_3 ;
  wire [1:0]\start_adc_count_r1_inferred__3/i__carry__0_0 ;
  wire \start_adc_count_r1_inferred__3/i__carry__0_n_2 ;
  wire \start_adc_count_r1_inferred__3/i__carry__0_n_3 ;
  wire \start_adc_count_r1_inferred__3/i__carry_n_0 ;
  wire \start_adc_count_r1_inferred__3/i__carry_n_1 ;
  wire \start_adc_count_r1_inferred__3/i__carry_n_2 ;
  wire \start_adc_count_r1_inferred__3/i__carry_n_3 ;
  wire [15:0]start_adc_count_r2;
  wire [15:1]start_adc_count_r20_in;
  wire start_adc_count_r_i_1_n_0;
  wire start_adc_count_r_i_2_n_0;
  wire [1:0]start_adc_count_r_reg_0;
  wire [15:0]sweep_val;
  wire trig_tguard_counter;
  wire [5:5]trig_tguard_counter0_in;
  wire \trig_tguard_counter[0]_i_1_n_0 ;
  wire \trig_tguard_counter[15]_i_10_n_0 ;
  wire \trig_tguard_counter[15]_i_11_n_0 ;
  wire \trig_tguard_counter[15]_i_12_n_0 ;
  wire \trig_tguard_counter[15]_i_4_n_0 ;
  wire \trig_tguard_counter[15]_i_5_n_0 ;
  wire \trig_tguard_counter[15]_i_6_n_0 ;
  wire \trig_tguard_counter[15]_i_7_n_0 ;
  wire \trig_tguard_counter[15]_i_8_n_0 ;
  wire \trig_tguard_counter[15]_i_9_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[12]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[15]_i_3_n_2 ;
  wire \trig_tguard_counter_reg[15]_i_3_n_3 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[4]_i_1_n_3 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_0 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_1 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_2 ;
  wire \trig_tguard_counter_reg[8]_i_1_n_3 ;
  wire \trig_tguard_counter_reg_n_0_[0] ;
  wire \trig_tguard_counter_reg_n_0_[10] ;
  wire \trig_tguard_counter_reg_n_0_[11] ;
  wire \trig_tguard_counter_reg_n_0_[12] ;
  wire \trig_tguard_counter_reg_n_0_[13] ;
  wire \trig_tguard_counter_reg_n_0_[14] ;
  wire \trig_tguard_counter_reg_n_0_[15] ;
  wire \trig_tguard_counter_reg_n_0_[1] ;
  wire \trig_tguard_counter_reg_n_0_[2] ;
  wire \trig_tguard_counter_reg_n_0_[3] ;
  wire \trig_tguard_counter_reg_n_0_[4] ;
  wire \trig_tguard_counter_reg_n_0_[5] ;
  wire \trig_tguard_counter_reg_n_0_[6] ;
  wire \trig_tguard_counter_reg_n_0_[7] ;
  wire \trig_tguard_counter_reg_n_0_[8] ;
  wire \trig_tguard_counter_reg_n_0_[9] ;
  wire trig_tsweep_counter114_in;
  wire \trig_tsweep_counter1_inferred__0/i__carry__0_n_1 ;
  wire \trig_tsweep_counter1_inferred__0/i__carry__0_n_2 ;
  wire \trig_tsweep_counter1_inferred__0/i__carry__0_n_3 ;
  wire \trig_tsweep_counter1_inferred__0/i__carry_n_0 ;
  wire \trig_tsweep_counter1_inferred__0/i__carry_n_1 ;
  wire \trig_tsweep_counter1_inferred__0/i__carry_n_2 ;
  wire \trig_tsweep_counter1_inferred__0/i__carry_n_3 ;
  wire trig_tsweep_counter2;
  wire trig_tsweep_counter2_carry__0_i_1_n_0;
  wire trig_tsweep_counter2_carry__0_i_2_n_0;
  wire trig_tsweep_counter2_carry__0_n_3;
  wire trig_tsweep_counter2_carry_i_1_n_0;
  wire trig_tsweep_counter2_carry_i_2_n_0;
  wire trig_tsweep_counter2_carry_i_3_n_0;
  wire trig_tsweep_counter2_carry_i_4_n_0;
  wire trig_tsweep_counter2_carry_n_0;
  wire trig_tsweep_counter2_carry_n_1;
  wire trig_tsweep_counter2_carry_n_2;
  wire trig_tsweep_counter2_carry_n_3;
  wire \trig_tsweep_counter[0]_i_1_n_0 ;
  wire \trig_tsweep_counter[0]_i_2_n_0 ;
  wire \trig_tsweep_counter[0]_i_4_n_0 ;
  wire \trig_tsweep_counter[0]_i_5_n_0 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_0 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_1 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_2 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_3 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_4 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_5 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_6 ;
  wire \trig_tsweep_counter_reg[0]_i_3_n_7 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[12]_i_1_n_7 ;
  wire [15:0]\trig_tsweep_counter_reg[15]_0 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[4]_i_1_n_7 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_0 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_1 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_2 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_3 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_4 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_5 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_6 ;
  wire \trig_tsweep_counter_reg[8]_i_1_n_7 ;
  wire [3:3]\NLW_frame_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [2:2]NLW_i__carry__0_i_1__2_CO_UNCONNECTED;
  wire [3:3]NLW_i__carry__0_i_1__2_O_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r1_carry_O_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r1_carry__0_O_UNCONNECTED;
  wire [3:1]NLW_start_adc_count_r1_carry__0_i_1_CO_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r1_carry__0_i_1_O_UNCONNECTED;
  wire [3:3]NLW_start_adc_count_r1_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_start_adc_count_r1_carry__1_O_UNCONNECTED;
  wire [3:0]\NLW_start_adc_count_r1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:3]\NLW_start_adc_count_r1_inferred__0/i__carry__1_CO_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__0/i__carry__1_O_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__2/i__carry_O_UNCONNECTED ;
  wire [3:2]\NLW_start_adc_count_r1_inferred__2/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__2/i__carry__0_O_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__3/i__carry_O_UNCONNECTED ;
  wire [3:2]\NLW_start_adc_count_r1_inferred__3/i__carry__0_CO_UNCONNECTED ;
  wire [3:0]\NLW_start_adc_count_r1_inferred__3/i__carry__0_O_UNCONNECTED ;
  wire [3:2]\NLW_trig_tguard_counter_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_trig_tguard_counter_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_trig_tsweep_counter1_inferred__0/i__carry_O_UNCONNECTED ;
  wire [3:0]\NLW_trig_tsweep_counter1_inferred__0/i__carry__0_O_UNCONNECTED ;
  wire [3:0]NLW_trig_tsweep_counter2_carry_O_UNCONNECTED;
  wire [3:2]NLW_trig_tsweep_counter2_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_trig_tsweep_counter2_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_trig_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED ;

  FDRE PAMP_EN_reg_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pll_cen_reg_reg_0[1]),
        .Q(PAMP_EN),
        .R(1'b0));
  FDRE PLL_POW_EN_reg_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pll_cen_reg_reg_0[0]),
        .Q(PLL_POW_EN),
        .R(1'b0));
  FDRE START_receive_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(slv_reg0[1]),
        .Q(START_receive),
        .R(1'b0));
  FDRE START_send_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(slv_reg0[0]),
        .Q(START_send),
        .R(1'b0));
  FDRE START_trig_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(slv_reg0[2]),
        .Q(START_trig),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [0]),
        .Q(ATTEN[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [1]),
        .Q(ATTEN[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [2]),
        .Q(ATTEN[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [3]),
        .Q(ATTEN[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [4]),
        .Q(ATTEN[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1)) 
    \atten_reg_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\atten_reg_reg[5]_0 [5]),
        .Q(ATTEN[5]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h005D)) 
    \frame_count[0]_i_1 
       (.I0(s00_axi_aresetn),
        .I1(\frame_count[0]_i_4_n_0 ),
        .I2(\frame_count[0]_i_5_n_0 ),
        .I3(frame_count),
        .O(\frame_count[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \frame_count[0]_i_10 
       (.I0(frame_even),
        .I1(frame_count_reg[30]),
        .I2(frame_count_reg[12]),
        .I3(frame_count_reg[25]),
        .I4(\frame_count[0]_i_14_n_0 ),
        .O(\frame_count[0]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \frame_count[0]_i_11 
       (.I0(frame_count_reg[1]),
        .I1(frame_count_reg[14]),
        .I2(frame_count_reg[20]),
        .I3(frame_count_reg[8]),
        .O(\frame_count[0]_i_11_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \frame_count[0]_i_12 
       (.I0(frame_count_reg[6]),
        .I1(frame_count_reg[24]),
        .I2(frame_count_reg[7]),
        .I3(frame_count_reg[26]),
        .I4(\frame_count[0]_i_15_n_0 ),
        .O(\frame_count[0]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \frame_count[0]_i_13 
       (.I0(\trig_tguard_counter_reg_n_0_[15] ),
        .I1(\trig_tguard_counter_reg_n_0_[14] ),
        .I2(\trig_tguard_counter_reg_n_0_[12] ),
        .I3(\trig_tguard_counter_reg_n_0_[13] ),
        .O(\frame_count[0]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \frame_count[0]_i_14 
       (.I0(frame_count_reg[27]),
        .I1(frame_count_reg[2]),
        .I2(frame_count_reg[16]),
        .I3(frame_count_reg[5]),
        .O(\frame_count[0]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \frame_count[0]_i_15 
       (.I0(frame_count_reg[13]),
        .I1(frame_count_reg[9]),
        .I2(frame_count_reg[11]),
        .I3(frame_count_reg[3]),
        .O(\frame_count[0]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \frame_count[0]_i_2 
       (.I0(\frame_count[0]_i_6_n_0 ),
        .I1(\frame_count[0]_i_7_n_0 ),
        .I2(\trig_tguard_counter_reg_n_0_[2] ),
        .I3(\trig_tguard_counter_reg_n_0_[3] ),
        .I4(\trig_tguard_counter_reg_n_0_[5] ),
        .I5(\trig_tguard_counter_reg_n_0_[6] ),
        .O(frame_count));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \frame_count[0]_i_4 
       (.I0(\frame_count[0]_i_9_n_0 ),
        .I1(frame_count_reg[29]),
        .I2(frame_count_reg[28]),
        .I3(frame_count_reg[23]),
        .I4(frame_count_reg[17]),
        .I5(\frame_count[0]_i_10_n_0 ),
        .O(\frame_count[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \frame_count[0]_i_5 
       (.I0(\frame_count[0]_i_11_n_0 ),
        .I1(frame_count_reg[22]),
        .I2(frame_count_reg[18]),
        .I3(frame_count_reg[15]),
        .I4(frame_count_reg[10]),
        .I5(\frame_count[0]_i_12_n_0 ),
        .O(\frame_count[0]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \frame_count[0]_i_6 
       (.I0(\trig_tguard_counter_reg_n_0_[4] ),
        .I1(\trig_tguard_counter_reg_n_0_[1] ),
        .I2(\trig_tguard_counter_reg_n_0_[0] ),
        .O(\frame_count[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \frame_count[0]_i_7 
       (.I0(\trig_tguard_counter_reg_n_0_[7] ),
        .I1(\frame_count[0]_i_13_n_0 ),
        .I2(\trig_tguard_counter_reg_n_0_[9] ),
        .I3(\trig_tguard_counter_reg_n_0_[8] ),
        .I4(\trig_tguard_counter_reg_n_0_[10] ),
        .I5(\trig_tguard_counter_reg_n_0_[11] ),
        .O(\frame_count[0]_i_7_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \frame_count[0]_i_8 
       (.I0(frame_even),
        .O(\frame_count[0]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \frame_count[0]_i_9 
       (.I0(frame_count_reg[4]),
        .I1(frame_count_reg[31]),
        .I2(frame_count_reg[21]),
        .I3(frame_count_reg[19]),
        .O(\frame_count[0]_i_9_n_0 ));
  FDRE \frame_count_reg[0] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[0]_i_3_n_7 ),
        .Q(frame_even),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\frame_count_reg[0]_i_3_n_0 ,\frame_count_reg[0]_i_3_n_1 ,\frame_count_reg[0]_i_3_n_2 ,\frame_count_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\frame_count_reg[0]_i_3_n_4 ,\frame_count_reg[0]_i_3_n_5 ,\frame_count_reg[0]_i_3_n_6 ,\frame_count_reg[0]_i_3_n_7 }),
        .S({frame_count_reg[3:1],\frame_count[0]_i_8_n_0 }));
  FDRE \frame_count_reg[10] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[8]_i_1_n_5 ),
        .Q(frame_count_reg[10]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[11] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[8]_i_1_n_4 ),
        .Q(frame_count_reg[11]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[12] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[12]_i_1_n_7 ),
        .Q(frame_count_reg[12]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[12]_i_1 
       (.CI(\frame_count_reg[8]_i_1_n_0 ),
        .CO({\frame_count_reg[12]_i_1_n_0 ,\frame_count_reg[12]_i_1_n_1 ,\frame_count_reg[12]_i_1_n_2 ,\frame_count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[12]_i_1_n_4 ,\frame_count_reg[12]_i_1_n_5 ,\frame_count_reg[12]_i_1_n_6 ,\frame_count_reg[12]_i_1_n_7 }),
        .S(frame_count_reg[15:12]));
  FDRE \frame_count_reg[13] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[12]_i_1_n_6 ),
        .Q(frame_count_reg[13]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[14] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[12]_i_1_n_5 ),
        .Q(frame_count_reg[14]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[15] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[12]_i_1_n_4 ),
        .Q(frame_count_reg[15]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[16] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[16]_i_1_n_7 ),
        .Q(frame_count_reg[16]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[16]_i_1 
       (.CI(\frame_count_reg[12]_i_1_n_0 ),
        .CO({\frame_count_reg[16]_i_1_n_0 ,\frame_count_reg[16]_i_1_n_1 ,\frame_count_reg[16]_i_1_n_2 ,\frame_count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[16]_i_1_n_4 ,\frame_count_reg[16]_i_1_n_5 ,\frame_count_reg[16]_i_1_n_6 ,\frame_count_reg[16]_i_1_n_7 }),
        .S(frame_count_reg[19:16]));
  FDRE \frame_count_reg[17] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[16]_i_1_n_6 ),
        .Q(frame_count_reg[17]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[18] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[16]_i_1_n_5 ),
        .Q(frame_count_reg[18]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[19] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[16]_i_1_n_4 ),
        .Q(frame_count_reg[19]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[1] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[0]_i_3_n_6 ),
        .Q(frame_count_reg[1]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[20] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[20]_i_1_n_7 ),
        .Q(frame_count_reg[20]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[20]_i_1 
       (.CI(\frame_count_reg[16]_i_1_n_0 ),
        .CO({\frame_count_reg[20]_i_1_n_0 ,\frame_count_reg[20]_i_1_n_1 ,\frame_count_reg[20]_i_1_n_2 ,\frame_count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[20]_i_1_n_4 ,\frame_count_reg[20]_i_1_n_5 ,\frame_count_reg[20]_i_1_n_6 ,\frame_count_reg[20]_i_1_n_7 }),
        .S(frame_count_reg[23:20]));
  FDRE \frame_count_reg[21] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[20]_i_1_n_6 ),
        .Q(frame_count_reg[21]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[22] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[20]_i_1_n_5 ),
        .Q(frame_count_reg[22]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[23] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[20]_i_1_n_4 ),
        .Q(frame_count_reg[23]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[24] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[24]_i_1_n_7 ),
        .Q(frame_count_reg[24]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[24]_i_1 
       (.CI(\frame_count_reg[20]_i_1_n_0 ),
        .CO({\frame_count_reg[24]_i_1_n_0 ,\frame_count_reg[24]_i_1_n_1 ,\frame_count_reg[24]_i_1_n_2 ,\frame_count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[24]_i_1_n_4 ,\frame_count_reg[24]_i_1_n_5 ,\frame_count_reg[24]_i_1_n_6 ,\frame_count_reg[24]_i_1_n_7 }),
        .S(frame_count_reg[27:24]));
  FDRE \frame_count_reg[25] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[24]_i_1_n_6 ),
        .Q(frame_count_reg[25]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[26] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[24]_i_1_n_5 ),
        .Q(frame_count_reg[26]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[27] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[24]_i_1_n_4 ),
        .Q(frame_count_reg[27]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[28] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[28]_i_1_n_7 ),
        .Q(frame_count_reg[28]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[28]_i_1 
       (.CI(\frame_count_reg[24]_i_1_n_0 ),
        .CO({\NLW_frame_count_reg[28]_i_1_CO_UNCONNECTED [3],\frame_count_reg[28]_i_1_n_1 ,\frame_count_reg[28]_i_1_n_2 ,\frame_count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[28]_i_1_n_4 ,\frame_count_reg[28]_i_1_n_5 ,\frame_count_reg[28]_i_1_n_6 ,\frame_count_reg[28]_i_1_n_7 }),
        .S(frame_count_reg[31:28]));
  FDRE \frame_count_reg[29] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[28]_i_1_n_6 ),
        .Q(frame_count_reg[29]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[2] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[0]_i_3_n_5 ),
        .Q(frame_count_reg[2]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[30] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[28]_i_1_n_5 ),
        .Q(frame_count_reg[30]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[31] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[28]_i_1_n_4 ),
        .Q(frame_count_reg[31]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[3] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[0]_i_3_n_4 ),
        .Q(frame_count_reg[3]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[4] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[4]_i_1_n_7 ),
        .Q(frame_count_reg[4]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[4]_i_1 
       (.CI(\frame_count_reg[0]_i_3_n_0 ),
        .CO({\frame_count_reg[4]_i_1_n_0 ,\frame_count_reg[4]_i_1_n_1 ,\frame_count_reg[4]_i_1_n_2 ,\frame_count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[4]_i_1_n_4 ,\frame_count_reg[4]_i_1_n_5 ,\frame_count_reg[4]_i_1_n_6 ,\frame_count_reg[4]_i_1_n_7 }),
        .S(frame_count_reg[7:4]));
  FDRE \frame_count_reg[5] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[4]_i_1_n_6 ),
        .Q(frame_count_reg[5]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[6] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[4]_i_1_n_5 ),
        .Q(frame_count_reg[6]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[7] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[4]_i_1_n_4 ),
        .Q(frame_count_reg[7]),
        .R(\frame_count[0]_i_1_n_0 ));
  FDRE \frame_count_reg[8] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[8]_i_1_n_7 ),
        .Q(frame_count_reg[8]),
        .R(\frame_count[0]_i_1_n_0 ));
  CARRY4 \frame_count_reg[8]_i_1 
       (.CI(\frame_count_reg[4]_i_1_n_0 ),
        .CO({\frame_count_reg[8]_i_1_n_0 ,\frame_count_reg[8]_i_1_n_1 ,\frame_count_reg[8]_i_1_n_2 ,\frame_count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\frame_count_reg[8]_i_1_n_4 ,\frame_count_reg[8]_i_1_n_5 ,\frame_count_reg[8]_i_1_n_6 ,\frame_count_reg[8]_i_1_n_7 }),
        .S(frame_count_reg[11:8]));
  FDRE \frame_count_reg[9] 
       (.C(clkDCO_10MHz),
        .CE(frame_count),
        .D(\frame_count_reg[8]_i_1_n_6 ),
        .Q(frame_count_reg[9]),
        .R(\frame_count[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2B22)) 
    i__carry__0_i_1
       (.I0(sweep_val[15]),
        .I1(\trig_tsweep_counter_reg[15]_0 [15]),
        .I2(\trig_tsweep_counter_reg[15]_0 [14]),
        .I3(sweep_val[14]),
        .O(i__carry__0_i_1_n_0));
  CARRY4 i__carry__0_i_1__2
       (.CI(i__carry_i_5__1_n_0),
        .CO({i__carry__0_i_1__2_n_0,NLW_i__carry__0_i_1__2_CO_UNCONNECTED[2],i__carry__0_i_1__2_n_2,i__carry__0_i_1__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,sweep_val[15:13]}),
        .O({NLW_i__carry__0_i_1__2_O_UNCONNECTED[3],start_adc_count_r20_in[15:13]}),
        .S({1'b1,i__carry__0_i_4__0_n_0,i__carry__0_i_5__0_n_0,i__carry__0_i_6__0_n_0}));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_2__0
       (.I0(sweep_val[13]),
        .I1(\trig_tsweep_counter_reg[15]_0 [13]),
        .I2(sweep_val[12]),
        .I3(\trig_tsweep_counter_reg[15]_0 [12]),
        .O(i__carry__0_i_2__0_n_0));
  LUT3 #(
    .INIT(8'h82)) 
    i__carry__0_i_2__1
       (.I0(i__carry__0_i_1__2_n_0),
        .I1(start_adc_count_r20_in[15]),
        .I2(\trig_tsweep_counter_reg[15]_0 [15]),
        .O(i__carry__0_i_2__1_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_3
       (.I0(sweep_val[11]),
        .I1(\trig_tsweep_counter_reg[15]_0 [11]),
        .I2(sweep_val[10]),
        .I3(\trig_tsweep_counter_reg[15]_0 [10]),
        .O(i__carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry__0_i_3__0
       (.I0(start_adc_count_r20_in[13]),
        .I1(\trig_tsweep_counter_reg[15]_0 [13]),
        .I2(\trig_tsweep_counter_reg[15]_0 [14]),
        .I3(start_adc_count_r20_in[14]),
        .I4(\trig_tsweep_counter_reg[15]_0 [12]),
        .I5(start_adc_count_r20_in[12]),
        .O(i__carry__0_i_3__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry__0_i_4
       (.I0(sweep_val[9]),
        .I1(\trig_tsweep_counter_reg[15]_0 [9]),
        .I2(sweep_val[8]),
        .I3(\trig_tsweep_counter_reg[15]_0 [8]),
        .O(i__carry__0_i_4_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_4__0
       (.I0(sweep_val[15]),
        .O(i__carry__0_i_4__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_5
       (.I0(\trig_tsweep_counter_reg[15]_0 [15]),
        .I1(sweep_val[15]),
        .I2(\trig_tsweep_counter_reg[15]_0 [14]),
        .I3(sweep_val[14]),
        .O(i__carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_5__0
       (.I0(sweep_val[14]),
        .O(i__carry__0_i_5__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_6
       (.I0(\trig_tsweep_counter_reg[15]_0 [13]),
        .I1(sweep_val[13]),
        .I2(\trig_tsweep_counter_reg[15]_0 [12]),
        .I3(sweep_val[12]),
        .O(i__carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry__0_i_6__0
       (.I0(sweep_val[13]),
        .O(i__carry__0_i_6__0_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_7
       (.I0(\trig_tsweep_counter_reg[15]_0 [11]),
        .I1(sweep_val[11]),
        .I2(\trig_tsweep_counter_reg[15]_0 [10]),
        .I3(sweep_val[10]),
        .O(i__carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry__0_i_8
       (.I0(\trig_tsweep_counter_reg[15]_0 [9]),
        .I1(sweep_val[9]),
        .I2(\trig_tsweep_counter_reg[15]_0 [8]),
        .I3(sweep_val[8]),
        .O(i__carry__0_i_8_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_1
       (.I0(sweep_val[7]),
        .I1(\trig_tsweep_counter_reg[15]_0 [7]),
        .I2(sweep_val[6]),
        .I3(\trig_tsweep_counter_reg[15]_0 [6]),
        .O(i__carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_10
       (.I0(sweep_val[10]),
        .O(i__carry_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_11
       (.I0(sweep_val[9]),
        .O(i__carry_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_12
       (.I0(sweep_val[8]),
        .O(i__carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_13
       (.I0(sweep_val[7]),
        .O(i__carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_14
       (.I0(sweep_val[6]),
        .O(i__carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_15
       (.I0(sweep_val[5]),
        .O(i__carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_16
       (.I0(sweep_val[4]),
        .O(i__carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_17
       (.I0(sweep_val[3]),
        .O(i__carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_18
       (.I0(sweep_val[2]),
        .O(i__carry_i_18_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_19
       (.I0(sweep_val[1]),
        .O(i__carry_i_19_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1__0
       (.I0(start_adc_count_r20_in[11]),
        .I1(\trig_tsweep_counter_reg[15]_0 [11]),
        .I2(\trig_tsweep_counter_reg[15]_0 [9]),
        .I3(start_adc_count_r20_in[9]),
        .I4(\trig_tsweep_counter_reg[15]_0 [10]),
        .I5(start_adc_count_r20_in[10]),
        .O(i__carry_i_1__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_1__2
       (.I0(\trig_tsweep_counter_reg[15]_0 [11]),
        .I1(Q[6]),
        .I2(\trig_tsweep_counter_reg[15]_0 [9]),
        .I3(Q[4]),
        .I4(Q[5]),
        .I5(\trig_tsweep_counter_reg[15]_0 [10]),
        .O(i__carry_i_1__2_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_2
       (.I0(sweep_val[5]),
        .I1(\trig_tsweep_counter_reg[15]_0 [5]),
        .I2(sweep_val[4]),
        .I3(\trig_tsweep_counter_reg[15]_0 [4]),
        .O(i__carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_2__0
       (.I0(start_adc_count_r20_in[8]),
        .I1(\trig_tsweep_counter_reg[15]_0 [8]),
        .I2(\trig_tsweep_counter_reg[15]_0 [6]),
        .I3(start_adc_count_r20_in[6]),
        .I4(\trig_tsweep_counter_reg[15]_0 [7]),
        .I5(start_adc_count_r20_in[7]),
        .O(i__carry_i_2__0_n_0));
  LUT4 #(
    .INIT(16'h22B2)) 
    i__carry_i_3
       (.I0(sweep_val[3]),
        .I1(\trig_tsweep_counter_reg[15]_0 [3]),
        .I2(sweep_val[2]),
        .I3(\trig_tsweep_counter_reg[15]_0 [2]),
        .O(i__carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3__0
       (.I0(start_adc_count_r20_in[4]),
        .I1(\trig_tsweep_counter_reg[15]_0 [4]),
        .I2(\trig_tsweep_counter_reg[15]_0 [5]),
        .I3(start_adc_count_r20_in[5]),
        .I4(\trig_tsweep_counter_reg[15]_0 [3]),
        .I5(start_adc_count_r20_in[3]),
        .O(i__carry_i_3__0_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    i__carry_i_3__2
       (.I0(\trig_tsweep_counter_reg[15]_0 [4]),
        .I1(Q[2]),
        .I2(\trig_tsweep_counter_reg[15]_0 [3]),
        .I3(Q[1]),
        .I4(Q[3]),
        .I5(\trig_tsweep_counter_reg[15]_0 [5]),
        .O(i__carry_i_3__2_n_0));
  LUT4 #(
    .INIT(16'h2B22)) 
    i__carry_i_4
       (.I0(sweep_val[1]),
        .I1(\trig_tsweep_counter_reg[15]_0 [1]),
        .I2(\trig_tsweep_counter_reg[15]_0 [0]),
        .I3(sweep_val[0]),
        .O(i__carry_i_4_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    i__carry_i_4__0
       (.I0(\trig_tsweep_counter_reg[15]_0 [0]),
        .I1(sweep_val[0]),
        .I2(\trig_tsweep_counter_reg[15]_0 [2]),
        .I3(start_adc_count_r20_in[2]),
        .I4(\trig_tsweep_counter_reg[15]_0 [1]),
        .I5(start_adc_count_r20_in[1]),
        .O(i__carry_i_4__0_n_0));
  LUT6 #(
    .INIT(64'h0000900990090000)) 
    i__carry_i_4__1
       (.I0(\trig_tsweep_counter_reg[15]_0 [1]),
        .I1(O[0]),
        .I2(\trig_tsweep_counter_reg[15]_0 [2]),
        .I3(O[1]),
        .I4(Q[0]),
        .I5(\trig_tsweep_counter_reg[15]_0 [0]),
        .O(i__carry_i_4__1_n_0));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_5
       (.I0(\trig_tsweep_counter_reg[15]_0 [7]),
        .I1(sweep_val[7]),
        .I2(\trig_tsweep_counter_reg[15]_0 [6]),
        .I3(sweep_val[6]),
        .O(i__carry_i_5_n_0));
  CARRY4 i__carry_i_5__1
       (.CI(i__carry_i_6__0_n_0),
        .CO({i__carry_i_5__1_n_0,i__carry_i_5__1_n_1,i__carry_i_5__1_n_2,i__carry_i_5__1_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[12:9]),
        .O(start_adc_count_r20_in[12:9]),
        .S({i__carry_i_8__0_n_0,i__carry_i_9_n_0,i__carry_i_10_n_0,i__carry_i_11_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_6
       (.I0(\trig_tsweep_counter_reg[15]_0 [5]),
        .I1(sweep_val[5]),
        .I2(\trig_tsweep_counter_reg[15]_0 [4]),
        .I3(sweep_val[4]),
        .O(i__carry_i_6_n_0));
  CARRY4 i__carry_i_6__0
       (.CI(i__carry_i_7__0_n_0),
        .CO({i__carry_i_6__0_n_0,i__carry_i_6__0_n_1,i__carry_i_6__0_n_2,i__carry_i_6__0_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[8:5]),
        .O(start_adc_count_r20_in[8:5]),
        .S({i__carry_i_12_n_0,i__carry_i_13_n_0,i__carry_i_14_n_0,i__carry_i_15_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_7
       (.I0(\trig_tsweep_counter_reg[15]_0 [3]),
        .I1(sweep_val[3]),
        .I2(\trig_tsweep_counter_reg[15]_0 [2]),
        .I3(sweep_val[2]),
        .O(i__carry_i_7_n_0));
  CARRY4 i__carry_i_7__0
       (.CI(1'b0),
        .CO({i__carry_i_7__0_n_0,i__carry_i_7__0_n_1,i__carry_i_7__0_n_2,i__carry_i_7__0_n_3}),
        .CYINIT(sweep_val[0]),
        .DI(sweep_val[4:1]),
        .O(start_adc_count_r20_in[4:1]),
        .S({i__carry_i_16_n_0,i__carry_i_17_n_0,i__carry_i_18_n_0,i__carry_i_19_n_0}));
  LUT4 #(
    .INIT(16'h9009)) 
    i__carry_i_8
       (.I0(\trig_tsweep_counter_reg[15]_0 [1]),
        .I1(sweep_val[1]),
        .I2(\trig_tsweep_counter_reg[15]_0 [0]),
        .I3(sweep_val[0]),
        .O(i__carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_8__0
       (.I0(sweep_val[12]),
        .O(i__carry_i_8__0_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    i__carry_i_9
       (.I0(sweep_val[11]),
        .O(i__carry_i_9_n_0));
  FDRE pll_cen_reg_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(pll_cen_reg_reg_0[2]),
        .Q(spi_pll_cen),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h00000000EEEEE2EA)) 
    pll_trig_reg_i_1
       (.I0(pll_trig),
        .I1(START_trig),
        .I2(pll_trig_reg_i_2_n_0),
        .I3(pll_trig_reg_i_3_n_0),
        .I4(pll_trig_reg_i_4_n_0),
        .I5(\trig_tsweep_counter[0]_i_2_n_0 ),
        .O(pll_trig_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    pll_trig_reg_i_2
       (.I0(trig_tsweep_counter2),
        .I1(\trig_tguard_counter_reg_n_0_[3] ),
        .I2(\trig_tguard_counter_reg_n_0_[2] ),
        .I3(\trig_tguard_counter_reg_n_0_[6] ),
        .I4(\trig_tguard_counter_reg_n_0_[5] ),
        .I5(pll_trig_reg_i_5_n_0),
        .O(pll_trig_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h01)) 
    pll_trig_reg_i_3
       (.I0(\frame_count[0]_i_7_n_0 ),
        .I1(\trig_tguard_counter[15]_i_6_n_0 ),
        .I2(\trig_tguard_counter[15]_i_5_n_0 ),
        .O(pll_trig_reg_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    pll_trig_reg_i_4
       (.I0(frame_count),
        .I1(\trig_tguard_counter[15]_i_5_n_0 ),
        .O(pll_trig_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    pll_trig_reg_i_5
       (.I0(\frame_count[0]_i_7_n_0 ),
        .I1(\trig_tguard_counter_reg_n_0_[0] ),
        .I2(\trig_tguard_counter_reg_n_0_[1] ),
        .I3(\trig_tguard_counter_reg_n_0_[4] ),
        .O(pll_trig_reg_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    pll_trig_reg_reg
       (.C(clkDCO_10MHz),
        .CE(1'b1),
        .D(pll_trig_reg_i_1_n_0),
        .Q(pll_trig),
        .R(1'b0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_RX spi_hmc_mode_RX_inst
       (.START_receive(START_receive),
        .\axi_rdata_reg[0] (\axi_rdata_reg[0] ),
        .\axi_rdata_reg[0]_0 (\axi_rdata_reg[0]_0 ),
        .clk_10MHz(clk_10MHz),
        .\data_r_reg[0]_0 (\data_r_reg[0] ),
        .\data_r_reg[23]_0 (\data_r_reg[23] ),
        .enable_sck(enable_sck),
        .mosi_r_reg_0(mosi_r_reg_3),
        .out(out),
        .pll_data_ready_TX(pll_data_ready_TX),
        .pll_mosi_read(pll_mosi_read),
        .pll_sen_read(pll_sen_read),
        .spi_pll_ld_sdo(spi_pll_ld_sdo));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_spi_hmc_mode_TX spi_hmc_mode_TX_inst
       (.START_send(START_send),
        .clk_10MHz(clk_10MHz),
        .enable_sck(enable_sck),
        .mosi_r_reg_0(mosi_r_reg),
        .mosi_r_reg_1(mosi_r_reg_0),
        .mosi_r_reg_2(mosi_r_reg_1),
        .mosi_r_reg_3(mosi_r_reg_2),
        .mosi_r_reg_4(mosi_r_reg_4),
        .out(\cnt_reg[4] ),
        .pll_data_ready_TX(pll_data_ready_TX),
        .pll_mosi_read(pll_mosi_read),
        .pll_sen_read(pll_sen_read),
        .spi_pll_mosi(spi_pll_mosi),
        .spi_pll_sck(spi_pll_sck),
        .spi_pll_sen(spi_pll_sen));
  CARRY4 start_adc_count_r1_carry
       (.CI(1'b0),
        .CO({start_adc_count_r1_carry_n_0,start_adc_count_r1_carry_n_1,start_adc_count_r1_carry_n_2,start_adc_count_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_adc_count_r1_carry_O_UNCONNECTED[3:0]),
        .S({start_adc_count_r1_carry_i_1_n_0,start_adc_count_r1_carry_i_2_n_0,start_adc_count_r1_carry_i_3_n_0,start_adc_count_r1_carry_i_4_n_0}));
  CARRY4 start_adc_count_r1_carry__0
       (.CI(start_adc_count_r1_carry_n_0),
        .CO({start_adc_count_r1_carry__0_n_0,start_adc_count_r1_carry__0_n_1,start_adc_count_r1_carry__0_n_2,start_adc_count_r1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_adc_count_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({start_adc_count_r1_carry__0_i_1_n_3,start_adc_count_r1_carry__0_i_1_n_3,start_adc_count_r1_carry__0_i_2_n_0,start_adc_count_r1_carry__0_i_3_n_0}));
  CARRY4 start_adc_count_r1_carry__0_i_1
       (.CI(start_adc_count_r1_carry__0_i_4_n_0),
        .CO({NLW_start_adc_count_r1_carry__0_i_1_CO_UNCONNECTED[3:1],start_adc_count_r1_carry__0_i_1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_adc_count_r1_carry__0_i_1_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  LUT3 #(
    .INIT(8'h82)) 
    start_adc_count_r1_carry__0_i_2
       (.I0(start_adc_count_r1_carry__0_i_1_n_3),
        .I1(start_adc_count_r2[15]),
        .I2(\trig_tsweep_counter_reg[15]_0 [15]),
        .O(start_adc_count_r1_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r1_carry__0_i_3
       (.I0(start_adc_count_r2[14]),
        .I1(\trig_tsweep_counter_reg[15]_0 [14]),
        .I2(\trig_tsweep_counter_reg[15]_0 [13]),
        .I3(start_adc_count_r2[13]),
        .I4(\trig_tsweep_counter_reg[15]_0 [12]),
        .I5(start_adc_count_r2[12]),
        .O(start_adc_count_r1_carry__0_i_3_n_0));
  CARRY4 start_adc_count_r1_carry__0_i_4
       (.CI(start_adc_count_r1_carry_i_5_n_0),
        .CO({start_adc_count_r1_carry__0_i_4_n_0,start_adc_count_r1_carry__0_i_4_n_1,start_adc_count_r1_carry__0_i_4_n_2,start_adc_count_r1_carry__0_i_4_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[15:12]),
        .O(start_adc_count_r2[15:12]),
        .S({start_adc_count_r1_carry__0_i_5_n_0,start_adc_count_r1_carry__0_i_6_n_0,start_adc_count_r1_carry__0_i_7_n_0,start_adc_count_r1_carry__0_i_8_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry__0_i_5
       (.I0(sweep_val[15]),
        .O(start_adc_count_r1_carry__0_i_5_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry__0_i_6
       (.I0(sweep_val[14]),
        .O(start_adc_count_r1_carry__0_i_6_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry__0_i_7
       (.I0(sweep_val[13]),
        .O(start_adc_count_r1_carry__0_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry__0_i_8
       (.I0(sweep_val[12]),
        .O(start_adc_count_r1_carry__0_i_8_n_0));
  CARRY4 start_adc_count_r1_carry__1
       (.CI(start_adc_count_r1_carry__0_n_0),
        .CO({NLW_start_adc_count_r1_carry__1_CO_UNCONNECTED[3],start_adc_count_r1_carry__1_n_1,start_adc_count_r1_carry__1_n_2,start_adc_count_r1_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_adc_count_r1_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,start_adc_count_r1_carry__0_i_1_n_3,start_adc_count_r1_carry__0_i_1_n_3,start_adc_count_r1_carry__0_i_1_n_3}));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r1_carry_i_1
       (.I0(start_adc_count_r2[11]),
        .I1(\trig_tsweep_counter_reg[15]_0 [11]),
        .I2(\trig_tsweep_counter_reg[15]_0 [10]),
        .I3(start_adc_count_r2[10]),
        .I4(\trig_tsweep_counter_reg[15]_0 [9]),
        .I5(start_adc_count_r2[9]),
        .O(start_adc_count_r1_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_10
       (.I0(sweep_val[9]),
        .O(start_adc_count_r1_carry_i_10_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_11
       (.I0(sweep_val[8]),
        .O(start_adc_count_r1_carry_i_11_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_12
       (.I0(sweep_val[7]),
        .O(start_adc_count_r1_carry_i_12_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_13
       (.I0(sweep_val[6]),
        .O(start_adc_count_r1_carry_i_13_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_14
       (.I0(sweep_val[5]),
        .O(start_adc_count_r1_carry_i_14_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_15
       (.I0(sweep_val[4]),
        .O(start_adc_count_r1_carry_i_15_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_16
       (.I0(sweep_val[3]),
        .O(start_adc_count_r1_carry_i_16_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_17
       (.I0(sweep_val[2]),
        .O(start_adc_count_r1_carry_i_17_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_18
       (.I0(sweep_val[1]),
        .O(start_adc_count_r1_carry_i_18_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r1_carry_i_2
       (.I0(start_adc_count_r2[7]),
        .I1(\trig_tsweep_counter_reg[15]_0 [7]),
        .I2(\trig_tsweep_counter_reg[15]_0 [8]),
        .I3(start_adc_count_r2[8]),
        .I4(\trig_tsweep_counter_reg[15]_0 [6]),
        .I5(start_adc_count_r2[6]),
        .O(start_adc_count_r1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r1_carry_i_3
       (.I0(start_adc_count_r2[4]),
        .I1(\trig_tsweep_counter_reg[15]_0 [4]),
        .I2(\trig_tsweep_counter_reg[15]_0 [5]),
        .I3(start_adc_count_r2[5]),
        .I4(\trig_tsweep_counter_reg[15]_0 [3]),
        .I5(start_adc_count_r2[3]),
        .O(start_adc_count_r1_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    start_adc_count_r1_carry_i_4
       (.I0(\trig_tsweep_counter_reg[15]_0 [2]),
        .I1(start_adc_count_r2[2]),
        .I2(\trig_tsweep_counter_reg[15]_0 [0]),
        .I3(start_adc_count_r2[0]),
        .I4(start_adc_count_r2[1]),
        .I5(\trig_tsweep_counter_reg[15]_0 [1]),
        .O(start_adc_count_r1_carry_i_4_n_0));
  CARRY4 start_adc_count_r1_carry_i_5
       (.CI(start_adc_count_r1_carry_i_6_n_0),
        .CO({start_adc_count_r1_carry_i_5_n_0,start_adc_count_r1_carry_i_5_n_1,start_adc_count_r1_carry_i_5_n_2,start_adc_count_r1_carry_i_5_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[11:8]),
        .O(start_adc_count_r2[11:8]),
        .S({start_adc_count_r1_carry_i_8_n_0,start_adc_count_r1_carry_i_9_n_0,start_adc_count_r1_carry_i_10_n_0,start_adc_count_r1_carry_i_11_n_0}));
  CARRY4 start_adc_count_r1_carry_i_6
       (.CI(start_adc_count_r1_carry_i_7_n_0),
        .CO({start_adc_count_r1_carry_i_6_n_0,start_adc_count_r1_carry_i_6_n_1,start_adc_count_r1_carry_i_6_n_2,start_adc_count_r1_carry_i_6_n_3}),
        .CYINIT(1'b0),
        .DI(sweep_val[7:4]),
        .O(start_adc_count_r2[7:4]),
        .S({start_adc_count_r1_carry_i_12_n_0,start_adc_count_r1_carry_i_13_n_0,start_adc_count_r1_carry_i_14_n_0,start_adc_count_r1_carry_i_15_n_0}));
  CARRY4 start_adc_count_r1_carry_i_7
       (.CI(1'b0),
        .CO({start_adc_count_r1_carry_i_7_n_0,start_adc_count_r1_carry_i_7_n_1,start_adc_count_r1_carry_i_7_n_2,start_adc_count_r1_carry_i_7_n_3}),
        .CYINIT(1'b0),
        .DI({sweep_val[3:1],1'b0}),
        .O(start_adc_count_r2[3:0]),
        .S({start_adc_count_r1_carry_i_16_n_0,start_adc_count_r1_carry_i_17_n_0,start_adc_count_r1_carry_i_18_n_0,sweep_val[0]}));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_8
       (.I0(sweep_val[11]),
        .O(start_adc_count_r1_carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    start_adc_count_r1_carry_i_9
       (.I0(sweep_val[10]),
        .O(start_adc_count_r1_carry_i_9_n_0));
  CARRY4 \start_adc_count_r1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\start_adc_count_r1_inferred__0/i__carry_n_0 ,\start_adc_count_r1_inferred__0/i__carry_n_1 ,\start_adc_count_r1_inferred__0/i__carry_n_2 ,\start_adc_count_r1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1__0_n_0,i__carry_i_2__0_n_0,i__carry_i_3__0_n_0,i__carry_i_4__0_n_0}));
  CARRY4 \start_adc_count_r1_inferred__0/i__carry__0 
       (.CI(\start_adc_count_r1_inferred__0/i__carry_n_0 ),
        .CO({\start_adc_count_r1_inferred__0/i__carry__0_n_0 ,\start_adc_count_r1_inferred__0/i__carry__0_n_1 ,\start_adc_count_r1_inferred__0/i__carry__0_n_2 ,\start_adc_count_r1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_1__2_n_0,i__carry__0_i_1__2_n_0,i__carry__0_i_2__1_n_0,i__carry__0_i_3__0_n_0}));
  CARRY4 \start_adc_count_r1_inferred__0/i__carry__1 
       (.CI(\start_adc_count_r1_inferred__0/i__carry__0_n_0 ),
        .CO({\NLW_start_adc_count_r1_inferred__0/i__carry__1_CO_UNCONNECTED [3],start_adc_count_r11_out,\start_adc_count_r1_inferred__0/i__carry__1_n_2 ,\start_adc_count_r1_inferred__0/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__0/i__carry__1_O_UNCONNECTED [3:0]),
        .S({1'b0,i__carry__0_i_1__2_n_0,i__carry__0_i_1__2_n_0,i__carry__0_i_1__2_n_0}));
  CARRY4 \start_adc_count_r1_inferred__2/i__carry 
       (.CI(1'b0),
        .CO({\start_adc_count_r1_inferred__2/i__carry_n_0 ,\start_adc_count_r1_inferred__2/i__carry_n_1 ,\start_adc_count_r1_inferred__2/i__carry_n_2 ,\start_adc_count_r1_inferred__2/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__2/i__carry_O_UNCONNECTED [3:0]),
        .S({\start_adc_count_r1_inferred__2/i__carry__0_0 ,i__carry_i_4__1_n_0}));
  CARRY4 \start_adc_count_r1_inferred__2/i__carry__0 
       (.CI(\start_adc_count_r1_inferred__2/i__carry_n_0 ),
        .CO({\NLW_start_adc_count_r1_inferred__2/i__carry__0_CO_UNCONNECTED [3:2],start_adc_count_r11_in,\start_adc_count_r1_inferred__2/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__2/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,start_adc_count_r_reg_0}));
  CARRY4 \start_adc_count_r1_inferred__3/i__carry 
       (.CI(1'b0),
        .CO({\start_adc_count_r1_inferred__3/i__carry_n_0 ,\start_adc_count_r1_inferred__3/i__carry_n_1 ,\start_adc_count_r1_inferred__3/i__carry_n_2 ,\start_adc_count_r1_inferred__3/i__carry_n_3 }),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__3/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_1__2_n_0,\start_adc_count_r1_inferred__3/i__carry__0_0 [1],i__carry_i_3__2_n_0,\start_adc_count_r1_inferred__3/i__carry__0_0 [0]}));
  CARRY4 \start_adc_count_r1_inferred__3/i__carry__0 
       (.CI(\start_adc_count_r1_inferred__3/i__carry_n_0 ),
        .CO({\NLW_start_adc_count_r1_inferred__3/i__carry__0_CO_UNCONNECTED [3:2],\start_adc_count_r1_inferred__3/i__carry__0_n_2 ,\start_adc_count_r1_inferred__3/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_start_adc_count_r1_inferred__3/i__carry__0_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,S}));
  LUT6 #(
    .INIT(64'h000000FE00000000)) 
    start_adc_count_r_i_1
       (.I0(start_adc_count),
        .I1(start_adc_count_r11_in),
        .I2(\start_adc_count_r1_inferred__3/i__carry__0_n_2 ),
        .I3(start_adc_count_r1_carry__1_n_1),
        .I4(start_adc_count_r11_out),
        .I5(start_adc_count_r_i_2_n_0),
        .O(start_adc_count_r_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    start_adc_count_r_i_2
       (.I0(START_trig),
        .I1(s00_axi_aresetn),
        .O(start_adc_count_r_i_2_n_0));
  FDRE start_adc_count_r_reg
       (.C(clkDCO_10MHz),
        .CE(1'b1),
        .D(start_adc_count_r_i_1_n_0),
        .Q(start_adc_count),
        .R(1'b0));
  FDRE \sweep_val_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[0]),
        .Q(sweep_val[0]),
        .R(SS));
  FDSE \sweep_val_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[10]),
        .Q(sweep_val[10]),
        .S(SS));
  FDRE \sweep_val_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[11]),
        .Q(sweep_val[11]),
        .R(SS));
  FDRE \sweep_val_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[12]),
        .Q(sweep_val[12]),
        .R(SS));
  FDSE \sweep_val_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[13]),
        .Q(sweep_val[13]),
        .S(SS));
  FDRE \sweep_val_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[14]),
        .Q(sweep_val[14]),
        .R(SS));
  FDRE \sweep_val_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[15]),
        .Q(sweep_val[15]),
        .R(SS));
  FDRE \sweep_val_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[1]),
        .Q(sweep_val[1]),
        .R(SS));
  FDSE \sweep_val_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[2]),
        .Q(sweep_val[2]),
        .S(SS));
  FDSE \sweep_val_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[3]),
        .Q(sweep_val[3]),
        .S(SS));
  FDRE \sweep_val_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[4]),
        .Q(sweep_val[4]),
        .R(SS));
  FDSE \sweep_val_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[5]),
        .Q(sweep_val[5]),
        .S(SS));
  FDRE \sweep_val_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[6]),
        .Q(sweep_val[6]),
        .R(SS));
  FDSE \sweep_val_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[7]),
        .Q(sweep_val[7]),
        .S(SS));
  FDRE \sweep_val_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[8]),
        .Q(sweep_val[8]),
        .R(SS));
  FDSE \sweep_val_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(D[9]),
        .Q(sweep_val[9]),
        .S(SS));
  LUT5 #(
    .INIT(32'h5AFF5A12)) 
    \trig_tguard_counter[0]_i_1 
       (.I0(\trig_tguard_counter_reg_n_0_[0] ),
        .I1(pll_trig_reg_i_4_n_0),
        .I2(trig_tguard_counter),
        .I3(\trig_tguard_counter[15]_i_4_n_0 ),
        .I4(pll_trig_reg_i_2_n_0),
        .O(\trig_tguard_counter[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h32)) 
    \trig_tguard_counter[15]_i_1 
       (.I0(pll_trig_reg_i_2_n_0),
        .I1(\trig_tguard_counter[15]_i_4_n_0 ),
        .I2(pll_trig_reg_i_4_n_0),
        .O(trig_tguard_counter0_in));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \trig_tguard_counter[15]_i_10 
       (.I0(\trig_tguard_counter_reg_n_0_[2] ),
        .I1(\trig_tguard_counter_reg_n_0_[3] ),
        .O(\trig_tguard_counter[15]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[15]_i_11 
       (.I0(\trig_tguard_counter_reg_n_0_[3] ),
        .I1(\trig_tguard_counter_reg_n_0_[2] ),
        .I2(\trig_tguard_counter_reg_n_0_[6] ),
        .I3(\trig_tguard_counter_reg_n_0_[5] ),
        .O(\trig_tguard_counter[15]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[15]_i_12 
       (.I0(\trig_tsweep_counter_reg[15]_0 [8]),
        .I1(\trig_tsweep_counter_reg[15]_0 [3]),
        .I2(\trig_tsweep_counter_reg[15]_0 [4]),
        .I3(\trig_tsweep_counter_reg[15]_0 [1]),
        .O(\trig_tguard_counter[15]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'h0100010000000100)) 
    \trig_tguard_counter[15]_i_2 
       (.I0(\trig_tguard_counter[15]_i_5_n_0 ),
        .I1(\trig_tguard_counter[15]_i_6_n_0 ),
        .I2(\frame_count[0]_i_7_n_0 ),
        .I3(START_trig),
        .I4(trig_tsweep_counter114_in),
        .I5(\trig_tguard_counter[15]_i_7_n_0 ),
        .O(trig_tguard_counter));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h4F)) 
    \trig_tguard_counter[15]_i_4 
       (.I0(\trig_tguard_counter[15]_i_7_n_0 ),
        .I1(trig_tsweep_counter114_in),
        .I2(START_trig),
        .O(\trig_tguard_counter[15]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \trig_tguard_counter[15]_i_5 
       (.I0(\trig_tguard_counter[15]_i_8_n_0 ),
        .I1(\trig_tsweep_counter_reg[15]_0 [15]),
        .I2(\trig_tsweep_counter_reg[15]_0 [5]),
        .I3(\trig_tsweep_counter_reg[15]_0 [14]),
        .I4(\trig_tsweep_counter_reg[15]_0 [12]),
        .I5(\trig_tguard_counter[15]_i_9_n_0 ),
        .O(\trig_tguard_counter[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFF0000000000001)) 
    \trig_tguard_counter[15]_i_6 
       (.I0(\trig_tguard_counter_reg_n_0_[1] ),
        .I1(\trig_tguard_counter_reg_n_0_[0] ),
        .I2(\trig_tguard_counter_reg_n_0_[4] ),
        .I3(\trig_tguard_counter[15]_i_10_n_0 ),
        .I4(\trig_tguard_counter_reg_n_0_[5] ),
        .I5(\trig_tguard_counter_reg_n_0_[6] ),
        .O(\trig_tguard_counter[15]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \trig_tguard_counter[15]_i_7 
       (.I0(\trig_tguard_counter_reg_n_0_[4] ),
        .I1(\trig_tguard_counter_reg_n_0_[1] ),
        .I2(\trig_tguard_counter_reg_n_0_[0] ),
        .I3(\frame_count[0]_i_7_n_0 ),
        .I4(\trig_tguard_counter[15]_i_11_n_0 ),
        .O(\trig_tguard_counter[15]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \trig_tguard_counter[15]_i_8 
       (.I0(\trig_tsweep_counter_reg[15]_0 [2]),
        .I1(\trig_tsweep_counter_reg[15]_0 [0]),
        .I2(\trig_tsweep_counter_reg[15]_0 [11]),
        .I3(\trig_tsweep_counter_reg[15]_0 [6]),
        .O(\trig_tguard_counter[15]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \trig_tguard_counter[15]_i_9 
       (.I0(\trig_tsweep_counter_reg[15]_0 [7]),
        .I1(\trig_tsweep_counter_reg[15]_0 [9]),
        .I2(\trig_tsweep_counter_reg[15]_0 [10]),
        .I3(\trig_tsweep_counter_reg[15]_0 [13]),
        .I4(\trig_tguard_counter[15]_i_12_n_0 ),
        .O(\trig_tguard_counter[15]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[0] 
       (.C(clkDCO_10MHz),
        .CE(1'b1),
        .D(\trig_tguard_counter[0]_i_1_n_0 ),
        .Q(\trig_tguard_counter_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[10] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[10]),
        .Q(\trig_tguard_counter_reg_n_0_[10] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[11] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[11]),
        .Q(\trig_tguard_counter_reg_n_0_[11] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[12] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[12]),
        .Q(\trig_tguard_counter_reg_n_0_[12] ),
        .R(trig_tguard_counter0_in));
  CARRY4 \trig_tguard_counter_reg[12]_i_1 
       (.CI(\trig_tguard_counter_reg[8]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[12]_i_1_n_0 ,\trig_tguard_counter_reg[12]_i_1_n_1 ,\trig_tguard_counter_reg[12]_i_1_n_2 ,\trig_tguard_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[12:9]),
        .S({\trig_tguard_counter_reg_n_0_[12] ,\trig_tguard_counter_reg_n_0_[11] ,\trig_tguard_counter_reg_n_0_[10] ,\trig_tguard_counter_reg_n_0_[9] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[13] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[13]),
        .Q(\trig_tguard_counter_reg_n_0_[13] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[14] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[14]),
        .Q(\trig_tguard_counter_reg_n_0_[14] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[15] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[15]),
        .Q(\trig_tguard_counter_reg_n_0_[15] ),
        .R(trig_tguard_counter0_in));
  CARRY4 \trig_tguard_counter_reg[15]_i_3 
       (.CI(\trig_tguard_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_trig_tguard_counter_reg[15]_i_3_CO_UNCONNECTED [3:2],\trig_tguard_counter_reg[15]_i_3_n_2 ,\trig_tguard_counter_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_trig_tguard_counter_reg[15]_i_3_O_UNCONNECTED [3],p_2_in[15:13]}),
        .S({1'b0,\trig_tguard_counter_reg_n_0_[15] ,\trig_tguard_counter_reg_n_0_[14] ,\trig_tguard_counter_reg_n_0_[13] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[1] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[1]),
        .Q(\trig_tguard_counter_reg_n_0_[1] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[2] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[2]),
        .Q(\trig_tguard_counter_reg_n_0_[2] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[3] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[3]),
        .Q(\trig_tguard_counter_reg_n_0_[3] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[4] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[4]),
        .Q(\trig_tguard_counter_reg_n_0_[4] ),
        .R(trig_tguard_counter0_in));
  CARRY4 \trig_tguard_counter_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\trig_tguard_counter_reg[4]_i_1_n_0 ,\trig_tguard_counter_reg[4]_i_1_n_1 ,\trig_tguard_counter_reg[4]_i_1_n_2 ,\trig_tguard_counter_reg[4]_i_1_n_3 }),
        .CYINIT(\trig_tguard_counter_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[4:1]),
        .S({\trig_tguard_counter_reg_n_0_[4] ,\trig_tguard_counter_reg_n_0_[3] ,\trig_tguard_counter_reg_n_0_[2] ,\trig_tguard_counter_reg_n_0_[1] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[5] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[5]),
        .Q(\trig_tguard_counter_reg_n_0_[5] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[6] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[6]),
        .Q(\trig_tguard_counter_reg_n_0_[6] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[7] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[7]),
        .Q(\trig_tguard_counter_reg_n_0_[7] ),
        .R(trig_tguard_counter0_in));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[8] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[8]),
        .Q(\trig_tguard_counter_reg_n_0_[8] ),
        .R(trig_tguard_counter0_in));
  CARRY4 \trig_tguard_counter_reg[8]_i_1 
       (.CI(\trig_tguard_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tguard_counter_reg[8]_i_1_n_0 ,\trig_tguard_counter_reg[8]_i_1_n_1 ,\trig_tguard_counter_reg[8]_i_1_n_2 ,\trig_tguard_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(p_2_in[8:5]),
        .S({\trig_tguard_counter_reg_n_0_[8] ,\trig_tguard_counter_reg_n_0_[7] ,\trig_tguard_counter_reg_n_0_[6] ,\trig_tguard_counter_reg_n_0_[5] }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tguard_counter_reg[9] 
       (.C(clkDCO_10MHz),
        .CE(trig_tguard_counter),
        .D(p_2_in[9]),
        .Q(\trig_tguard_counter_reg_n_0_[9] ),
        .R(trig_tguard_counter0_in));
  CARRY4 \trig_tsweep_counter1_inferred__0/i__carry 
       (.CI(1'b0),
        .CO({\trig_tsweep_counter1_inferred__0/i__carry_n_0 ,\trig_tsweep_counter1_inferred__0/i__carry_n_1 ,\trig_tsweep_counter1_inferred__0/i__carry_n_2 ,\trig_tsweep_counter1_inferred__0/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry_i_1_n_0,i__carry_i_2_n_0,i__carry_i_3_n_0,i__carry_i_4_n_0}),
        .O(\NLW_trig_tsweep_counter1_inferred__0/i__carry_O_UNCONNECTED [3:0]),
        .S({i__carry_i_5_n_0,i__carry_i_6_n_0,i__carry_i_7_n_0,i__carry_i_8_n_0}));
  CARRY4 \trig_tsweep_counter1_inferred__0/i__carry__0 
       (.CI(\trig_tsweep_counter1_inferred__0/i__carry_n_0 ),
        .CO({trig_tsweep_counter114_in,\trig_tsweep_counter1_inferred__0/i__carry__0_n_1 ,\trig_tsweep_counter1_inferred__0/i__carry__0_n_2 ,\trig_tsweep_counter1_inferred__0/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({i__carry__0_i_1_n_0,i__carry__0_i_2__0_n_0,i__carry__0_i_3_n_0,i__carry__0_i_4_n_0}),
        .O(\NLW_trig_tsweep_counter1_inferred__0/i__carry__0_O_UNCONNECTED [3:0]),
        .S({i__carry__0_i_5_n_0,i__carry__0_i_6_n_0,i__carry__0_i_7_n_0,i__carry__0_i_8_n_0}));
  CARRY4 trig_tsweep_counter2_carry
       (.CI(1'b0),
        .CO({trig_tsweep_counter2_carry_n_0,trig_tsweep_counter2_carry_n_1,trig_tsweep_counter2_carry_n_2,trig_tsweep_counter2_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_trig_tsweep_counter2_carry_O_UNCONNECTED[3:0]),
        .S({trig_tsweep_counter2_carry_i_1_n_0,trig_tsweep_counter2_carry_i_2_n_0,trig_tsweep_counter2_carry_i_3_n_0,trig_tsweep_counter2_carry_i_4_n_0}));
  CARRY4 trig_tsweep_counter2_carry__0
       (.CI(trig_tsweep_counter2_carry_n_0),
        .CO({NLW_trig_tsweep_counter2_carry__0_CO_UNCONNECTED[3:2],trig_tsweep_counter2,trig_tsweep_counter2_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_trig_tsweep_counter2_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,trig_tsweep_counter2_carry__0_i_1_n_0,trig_tsweep_counter2_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    trig_tsweep_counter2_carry__0_i_1
       (.I0(\trig_tsweep_counter_reg[15]_0 [15]),
        .I1(sweep_val[15]),
        .O(trig_tsweep_counter2_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    trig_tsweep_counter2_carry__0_i_2
       (.I0(sweep_val[12]),
        .I1(\trig_tsweep_counter_reg[15]_0 [12]),
        .I2(sweep_val[13]),
        .I3(\trig_tsweep_counter_reg[15]_0 [13]),
        .I4(\trig_tsweep_counter_reg[15]_0 [14]),
        .I5(sweep_val[14]),
        .O(trig_tsweep_counter2_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    trig_tsweep_counter2_carry_i_1
       (.I0(sweep_val[10]),
        .I1(\trig_tsweep_counter_reg[15]_0 [10]),
        .I2(sweep_val[11]),
        .I3(\trig_tsweep_counter_reg[15]_0 [11]),
        .I4(\trig_tsweep_counter_reg[15]_0 [9]),
        .I5(sweep_val[9]),
        .O(trig_tsweep_counter2_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    trig_tsweep_counter2_carry_i_2
       (.I0(sweep_val[6]),
        .I1(\trig_tsweep_counter_reg[15]_0 [6]),
        .I2(sweep_val[7]),
        .I3(\trig_tsweep_counter_reg[15]_0 [7]),
        .I4(\trig_tsweep_counter_reg[15]_0 [8]),
        .I5(sweep_val[8]),
        .O(trig_tsweep_counter2_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    trig_tsweep_counter2_carry_i_3
       (.I0(sweep_val[4]),
        .I1(\trig_tsweep_counter_reg[15]_0 [4]),
        .I2(sweep_val[5]),
        .I3(\trig_tsweep_counter_reg[15]_0 [5]),
        .I4(\trig_tsweep_counter_reg[15]_0 [3]),
        .I5(sweep_val[3]),
        .O(trig_tsweep_counter2_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    trig_tsweep_counter2_carry_i_4
       (.I0(sweep_val[0]),
        .I1(\trig_tsweep_counter_reg[15]_0 [0]),
        .I2(sweep_val[1]),
        .I3(\trig_tsweep_counter_reg[15]_0 [1]),
        .I4(\trig_tsweep_counter_reg[15]_0 [2]),
        .I5(sweep_val[2]),
        .O(trig_tsweep_counter2_carry_i_4_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    \trig_tsweep_counter[0]_i_1 
       (.I0(\trig_tsweep_counter[0]_i_4_n_0 ),
        .I1(\trig_tguard_counter[15]_i_4_n_0 ),
        .O(\trig_tsweep_counter[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h40)) 
    \trig_tsweep_counter[0]_i_2 
       (.I0(\trig_tguard_counter[15]_i_7_n_0 ),
        .I1(trig_tsweep_counter114_in),
        .I2(START_trig),
        .O(\trig_tsweep_counter[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h4444FFFF4444444F)) 
    \trig_tsweep_counter[0]_i_4 
       (.I0(\trig_tguard_counter[15]_i_7_n_0 ),
        .I1(trig_tsweep_counter2),
        .I2(\frame_count[0]_i_7_n_0 ),
        .I3(\trig_tguard_counter[15]_i_6_n_0 ),
        .I4(\trig_tguard_counter[15]_i_5_n_0 ),
        .I5(frame_count),
        .O(\trig_tsweep_counter[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \trig_tsweep_counter[0]_i_5 
       (.I0(\trig_tsweep_counter_reg[15]_0 [0]),
        .O(\trig_tsweep_counter[0]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[0] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_7 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [0]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\trig_tsweep_counter_reg[0]_i_3_n_0 ,\trig_tsweep_counter_reg[0]_i_3_n_1 ,\trig_tsweep_counter_reg[0]_i_3_n_2 ,\trig_tsweep_counter_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\trig_tsweep_counter_reg[0]_i_3_n_4 ,\trig_tsweep_counter_reg[0]_i_3_n_5 ,\trig_tsweep_counter_reg[0]_i_3_n_6 ,\trig_tsweep_counter_reg[0]_i_3_n_7 }),
        .S({\trig_tsweep_counter_reg[15]_0 [3:1],\trig_tsweep_counter[0]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[10] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_5 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [10]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[11] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_4 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [11]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[12] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_7 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [12]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[12]_i_1 
       (.CI(\trig_tsweep_counter_reg[8]_i_1_n_0 ),
        .CO({\NLW_trig_tsweep_counter_reg[12]_i_1_CO_UNCONNECTED [3],\trig_tsweep_counter_reg[12]_i_1_n_1 ,\trig_tsweep_counter_reg[12]_i_1_n_2 ,\trig_tsweep_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[12]_i_1_n_4 ,\trig_tsweep_counter_reg[12]_i_1_n_5 ,\trig_tsweep_counter_reg[12]_i_1_n_6 ,\trig_tsweep_counter_reg[12]_i_1_n_7 }),
        .S(\trig_tsweep_counter_reg[15]_0 [15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[13] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_6 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [13]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[14] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_5 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [14]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[15] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[12]_i_1_n_4 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [15]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[1] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_6 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [1]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[2] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_5 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [2]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[3] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[0]_i_3_n_4 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [3]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[4] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_7 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [4]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[4]_i_1 
       (.CI(\trig_tsweep_counter_reg[0]_i_3_n_0 ),
        .CO({\trig_tsweep_counter_reg[4]_i_1_n_0 ,\trig_tsweep_counter_reg[4]_i_1_n_1 ,\trig_tsweep_counter_reg[4]_i_1_n_2 ,\trig_tsweep_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[4]_i_1_n_4 ,\trig_tsweep_counter_reg[4]_i_1_n_5 ,\trig_tsweep_counter_reg[4]_i_1_n_6 ,\trig_tsweep_counter_reg[4]_i_1_n_7 }),
        .S(\trig_tsweep_counter_reg[15]_0 [7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[5] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_6 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [5]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[6] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_5 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [6]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[7] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[4]_i_1_n_4 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [7]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[8] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_7 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [8]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
  CARRY4 \trig_tsweep_counter_reg[8]_i_1 
       (.CI(\trig_tsweep_counter_reg[4]_i_1_n_0 ),
        .CO({\trig_tsweep_counter_reg[8]_i_1_n_0 ,\trig_tsweep_counter_reg[8]_i_1_n_1 ,\trig_tsweep_counter_reg[8]_i_1_n_2 ,\trig_tsweep_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\trig_tsweep_counter_reg[8]_i_1_n_4 ,\trig_tsweep_counter_reg[8]_i_1_n_5 ,\trig_tsweep_counter_reg[8]_i_1_n_6 ,\trig_tsweep_counter_reg[8]_i_1_n_7 }),
        .S(\trig_tsweep_counter_reg[15]_0 [11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \trig_tsweep_counter_reg[9] 
       (.C(clkDCO_10MHz),
        .CE(\trig_tsweep_counter[0]_i_2_n_0 ),
        .D(\trig_tsweep_counter_reg[8]_i_1_n_6 ),
        .Q(\trig_tsweep_counter_reg[15]_0 [9]),
        .R(\trig_tsweep_counter[0]_i_1_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

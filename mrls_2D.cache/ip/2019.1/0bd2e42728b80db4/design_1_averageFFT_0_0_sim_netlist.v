// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Thu Dec 16 12:32:11 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.v
// Design      : design_1_averageFFT_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v3_0
   (azimut8,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    S_AXI_RDATA,
    m00_axis_tdata,
    S_AXI_RVALID,
    m00_axis_tlast,
    m00_axis_tvalid,
    S_AXI_BVALID,
    s00_axis_tvalid,
    m00_axis_aresetn,
    S_AXI_ACLK,
    S_AXI_ARADDR,
    S_AXI_ARVALID,
    S_AXI_AWADDR,
    S_AXI_WVALID,
    S_AXI_AWVALID,
    S_AXI_WDATA,
    clk_10MHz,
    m00_axis_aclk,
    S_AXI_WSTRB,
    azimut_0,
    allowed_clk,
    s00_axis_tlast,
    S_AXI_ARESETN,
    S_AXI_BREADY,
    S_AXI_RREADY);
  output [15:0]azimut8;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]S_AXI_RDATA;
  output [16:0]m00_axis_tdata;
  output S_AXI_RVALID;
  output m00_axis_tlast;
  output m00_axis_tvalid;
  output S_AXI_BVALID;
  input s00_axis_tvalid;
  input m00_axis_aresetn;
  input S_AXI_ACLK;
  input [1:0]S_AXI_ARADDR;
  input S_AXI_ARVALID;
  input [1:0]S_AXI_AWADDR;
  input S_AXI_WVALID;
  input S_AXI_AWVALID;
  input [31:0]S_AXI_WDATA;
  input clk_10MHz;
  input m00_axis_aclk;
  input [3:0]S_AXI_WSTRB;
  input azimut_0;
  input allowed_clk;
  input s00_axis_tlast;
  input S_AXI_ARESETN;
  input S_AXI_BREADY;
  input S_AXI_RREADY;

  wire S_AXI_ACLK;
  wire [1:0]S_AXI_ARADDR;
  wire S_AXI_ARESETN;
  wire S_AXI_ARREADY;
  wire S_AXI_ARVALID;
  wire [1:0]S_AXI_AWADDR;
  wire S_AXI_AWREADY;
  wire S_AXI_AWVALID;
  wire S_AXI_BREADY;
  wire S_AXI_BVALID;
  wire [31:0]S_AXI_RDATA;
  wire S_AXI_RREADY;
  wire S_AXI_RVALID;
  wire [31:0]S_AXI_WDATA;
  wire S_AXI_WREADY;
  wire [3:0]S_AXI_WSTRB;
  wire S_AXI_WVALID;
  wire [15:0]adr;
  wire \adr[15]_i_1_n_0 ;
  wire \adr[15]_i_4_n_0 ;
  wire \adr[15]_i_5_n_0 ;
  wire \adr[15]_i_6_n_0 ;
  wire \adr[15]_i_7_n_0 ;
  wire \adr_reg[12]_i_2_n_0 ;
  wire \adr_reg[12]_i_2_n_1 ;
  wire \adr_reg[12]_i_2_n_2 ;
  wire \adr_reg[12]_i_2_n_3 ;
  wire \adr_reg[12]_i_2_n_4 ;
  wire \adr_reg[12]_i_2_n_5 ;
  wire \adr_reg[12]_i_2_n_6 ;
  wire \adr_reg[12]_i_2_n_7 ;
  wire \adr_reg[15]_i_3_n_2 ;
  wire \adr_reg[15]_i_3_n_3 ;
  wire \adr_reg[15]_i_3_n_5 ;
  wire \adr_reg[15]_i_3_n_6 ;
  wire \adr_reg[15]_i_3_n_7 ;
  wire \adr_reg[4]_i_2_n_0 ;
  wire \adr_reg[4]_i_2_n_1 ;
  wire \adr_reg[4]_i_2_n_2 ;
  wire \adr_reg[4]_i_2_n_3 ;
  wire \adr_reg[4]_i_2_n_4 ;
  wire \adr_reg[4]_i_2_n_5 ;
  wire \adr_reg[4]_i_2_n_6 ;
  wire \adr_reg[4]_i_2_n_7 ;
  wire \adr_reg[8]_i_2_n_0 ;
  wire \adr_reg[8]_i_2_n_1 ;
  wire \adr_reg[8]_i_2_n_2 ;
  wire \adr_reg[8]_i_2_n_3 ;
  wire \adr_reg[8]_i_2_n_4 ;
  wire \adr_reg[8]_i_2_n_5 ;
  wire \adr_reg[8]_i_2_n_6 ;
  wire \adr_reg[8]_i_2_n_7 ;
  wire allowed_clk;
  wire [15:0]azimut8;
  wire azimut_0;
  wire azimut_0_prev;
  wire clk_10MHz;
  wire \cnt_high_allowed_clk[0]_i_1_n_0 ;
  wire \cnt_high_allowed_clk[0]_i_3_n_0 ;
  wire [15:0]cnt_high_allowed_clk_reg;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_0 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_1 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_2 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_3 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_4 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_5 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_6 ;
  wire \cnt_high_allowed_clk_reg[0]_i_2_n_7 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_1 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_2 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_3 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_4 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_5 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_6 ;
  wire \cnt_high_allowed_clk_reg[12]_i_1_n_7 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_0 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_1 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_2 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_3 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_4 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_5 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_6 ;
  wire \cnt_high_allowed_clk_reg[4]_i_1_n_7 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_0 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_1 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_2 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_3 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_4 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_5 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_6 ;
  wire \cnt_high_allowed_clk_reg[8]_i_1_n_7 ;
  wire fft_azimut8_r;
  wire \fft_azimut8_r[15]_i_1_n_0 ;
  wire \fft_azimut8_r[15]_i_4_n_0 ;
  wire \fft_azimut8_r[15]_i_5_n_0 ;
  wire \fft_azimut8_r[15]_i_6_n_0 ;
  wire \fft_azimut8_r[3]_i_2_n_0 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_0 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_1 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_2 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_3 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_4 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_5 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_6 ;
  wire \fft_azimut8_r_reg[11]_i_1_n_7 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_1 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_2 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_3 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_4 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_5 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_6 ;
  wire \fft_azimut8_r_reg[15]_i_3_n_7 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_0 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_1 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_2 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_3 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_4 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_5 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_6 ;
  wire \fft_azimut8_r_reg[3]_i_1_n_7 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_0 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_1 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_2 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_3 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_4 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_5 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_6 ;
  wire \fft_azimut8_r_reg[7]_i_1_n_7 ;
  wire fft_azimut_r;
  wire \fft_azimut_r[0]_i_1_n_0 ;
  wire \fft_azimut_r[1]_i_1_n_0 ;
  wire \fft_azimut_r[2]_i_1_n_0 ;
  wire \fft_azimut_r[3]_i_2_n_0 ;
  wire \fft_azimut_r[3]_i_3_n_0 ;
  wire \fft_azimut_r_reg_n_0_[0] ;
  wire \fft_azimut_r_reg_n_0_[1] ;
  wire \fft_azimut_r_reg_n_0_[2] ;
  wire \fft_azimut_r_reg_n_0_[3] ;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [16:0]m00_axis_tdata;
  wire [31:11]m00_axis_tdata_r;
  wire \m00_axis_tdata_r[16]_i_1_n_0 ;
  wire \m00_axis_tdata_r[31]_i_2_n_0 ;
  wire \m00_axis_tdata_r[31]_i_4_n_0 ;
  wire \m00_axis_tdata_r[31]_i_5_n_0 ;
  wire \m00_axis_tdata_r[31]_i_6_n_0 ;
  wire \m00_axis_tdata_r[31]_i_7_n_0 ;
  wire \m00_axis_tdata_r[31]_i_8_n_0 ;
  wire m00_axis_tdata_r_0;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;
  wire s00_axis_tlast;
  wire s00_axis_tvalid;
  wire [15:0]sel0;
  wire [3:2]\NLW_adr_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_adr_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:3]\NLW_cnt_high_allowed_clk_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_fft_azimut8_r_reg[15]_i_3_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \adr[0]_i_1 
       (.I0(sel0[0]),
        .O(adr[0]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[10]_i_1 
       (.I0(\adr_reg[12]_i_2_n_6 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[10]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[11]_i_1 
       (.I0(\adr_reg[12]_i_2_n_5 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[11]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[12]_i_1 
       (.I0(\adr_reg[12]_i_2_n_4 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[12]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[13]_i_1 
       (.I0(\adr_reg[15]_i_3_n_7 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[13]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[14]_i_1 
       (.I0(\adr_reg[15]_i_3_n_6 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[14]));
  LUT2 #(
    .INIT(4'h7)) 
    \adr[15]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(m00_axis_aresetn),
        .O(\adr[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \adr[15]_i_2 
       (.I0(\adr_reg[15]_i_3_n_5 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[15]));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \adr[15]_i_4 
       (.I0(\m00_axis_tdata_r[31]_i_6_n_0 ),
        .I1(sel0[12]),
        .I2(sel0[5]),
        .I3(\adr[15]_i_5_n_0 ),
        .I4(\adr[15]_i_6_n_0 ),
        .I5(\adr[15]_i_7_n_0 ),
        .O(\adr[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \adr[15]_i_5 
       (.I0(sel0[9]),
        .I1(sel0[10]),
        .I2(sel0[11]),
        .I3(sel0[4]),
        .O(\adr[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFF7F)) 
    \adr[15]_i_6 
       (.I0(sel0[6]),
        .I1(sel0[1]),
        .I2(sel0[7]),
        .I3(sel0[15]),
        .O(\adr[15]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \adr[15]_i_7 
       (.I0(sel0[3]),
        .I1(sel0[0]),
        .I2(sel0[8]),
        .I3(sel0[2]),
        .O(\adr[15]_i_7_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[1]_i_1 
       (.I0(\adr_reg[4]_i_2_n_7 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[1]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[2]_i_1 
       (.I0(\adr_reg[4]_i_2_n_6 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[2]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[3]_i_1 
       (.I0(\adr_reg[4]_i_2_n_5 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[3]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[4]_i_1 
       (.I0(\adr_reg[4]_i_2_n_4 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[4]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[5]_i_1 
       (.I0(\adr_reg[8]_i_2_n_7 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[5]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[6]_i_1 
       (.I0(\adr_reg[8]_i_2_n_6 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[6]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[7]_i_1 
       (.I0(\adr_reg[8]_i_2_n_5 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[7]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[8]_i_1 
       (.I0(\adr_reg[8]_i_2_n_4 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[8]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \adr[9]_i_1 
       (.I0(\adr_reg[12]_i_2_n_7 ),
        .I1(\adr[15]_i_4_n_0 ),
        .O(adr[9]));
  FDRE \adr_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[0]),
        .Q(sel0[0]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[10]),
        .Q(sel0[10]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[11]),
        .Q(sel0[11]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[12]),
        .Q(sel0[12]),
        .R(\adr[15]_i_1_n_0 ));
  CARRY4 \adr_reg[12]_i_2 
       (.CI(\adr_reg[8]_i_2_n_0 ),
        .CO({\adr_reg[12]_i_2_n_0 ,\adr_reg[12]_i_2_n_1 ,\adr_reg[12]_i_2_n_2 ,\adr_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\adr_reg[12]_i_2_n_4 ,\adr_reg[12]_i_2_n_5 ,\adr_reg[12]_i_2_n_6 ,\adr_reg[12]_i_2_n_7 }),
        .S(sel0[12:9]));
  FDRE \adr_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[13]),
        .Q(sel0[13]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[14]),
        .Q(sel0[14]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[15]),
        .Q(sel0[15]),
        .R(\adr[15]_i_1_n_0 ));
  CARRY4 \adr_reg[15]_i_3 
       (.CI(\adr_reg[12]_i_2_n_0 ),
        .CO({\NLW_adr_reg[15]_i_3_CO_UNCONNECTED [3:2],\adr_reg[15]_i_3_n_2 ,\adr_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_adr_reg[15]_i_3_O_UNCONNECTED [3],\adr_reg[15]_i_3_n_5 ,\adr_reg[15]_i_3_n_6 ,\adr_reg[15]_i_3_n_7 }),
        .S({1'b0,sel0[15:13]}));
  FDRE \adr_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[1]),
        .Q(sel0[1]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[2]),
        .Q(sel0[2]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[3]),
        .Q(sel0[3]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[4]),
        .Q(sel0[4]),
        .R(\adr[15]_i_1_n_0 ));
  CARRY4 \adr_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\adr_reg[4]_i_2_n_0 ,\adr_reg[4]_i_2_n_1 ,\adr_reg[4]_i_2_n_2 ,\adr_reg[4]_i_2_n_3 }),
        .CYINIT(sel0[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\adr_reg[4]_i_2_n_4 ,\adr_reg[4]_i_2_n_5 ,\adr_reg[4]_i_2_n_6 ,\adr_reg[4]_i_2_n_7 }),
        .S(sel0[4:1]));
  FDRE \adr_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[5]),
        .Q(sel0[5]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[6]),
        .Q(sel0[6]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[7]),
        .Q(sel0[7]),
        .R(\adr[15]_i_1_n_0 ));
  FDRE \adr_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[8]),
        .Q(sel0[8]),
        .R(\adr[15]_i_1_n_0 ));
  CARRY4 \adr_reg[8]_i_2 
       (.CI(\adr_reg[4]_i_2_n_0 ),
        .CO({\adr_reg[8]_i_2_n_0 ,\adr_reg[8]_i_2_n_1 ,\adr_reg[8]_i_2_n_2 ,\adr_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\adr_reg[8]_i_2_n_4 ,\adr_reg[8]_i_2_n_5 ,\adr_reg[8]_i_2_n_6 ,\adr_reg[8]_i_2_n_7 }),
        .S(sel0[8:5]));
  FDRE \adr_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(adr[9]),
        .Q(sel0[9]),
        .R(\adr[15]_i_1_n_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v3_0_S00_AXI averageFFT_v3_0_S00_AXI
       (.S_AXI_ACLK(S_AXI_ACLK),
        .S_AXI_ARADDR(S_AXI_ARADDR),
        .S_AXI_ARESETN(S_AXI_ARESETN),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_ARVALID(S_AXI_ARVALID),
        .S_AXI_AWADDR(S_AXI_AWADDR),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_AWVALID(S_AXI_AWVALID),
        .S_AXI_BREADY(S_AXI_BREADY),
        .S_AXI_BVALID(S_AXI_BVALID),
        .S_AXI_RDATA(S_AXI_RDATA),
        .S_AXI_RREADY(S_AXI_RREADY),
        .S_AXI_RVALID(S_AXI_RVALID),
        .S_AXI_WDATA(S_AXI_WDATA),
        .S_AXI_WREADY(S_AXI_WREADY),
        .S_AXI_WSTRB(S_AXI_WSTRB),
        .S_AXI_WVALID(S_AXI_WVALID));
  FDRE #(
    .INIT(1'b0)) 
    azimut_0_prev_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(azimut_0),
        .Q(azimut_0_prev),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h5DFF)) 
    \cnt_high_allowed_clk[0]_i_1 
       (.I0(m00_axis_aresetn),
        .I1(azimut_0),
        .I2(azimut_0_prev),
        .I3(allowed_clk),
        .O(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_high_allowed_clk[0]_i_3 
       (.I0(cnt_high_allowed_clk_reg[0]),
        .O(\cnt_high_allowed_clk[0]_i_3_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_7 ),
        .Q(cnt_high_allowed_clk_reg[0]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_high_allowed_clk_reg[0]_i_2_n_0 ,\cnt_high_allowed_clk_reg[0]_i_2_n_1 ,\cnt_high_allowed_clk_reg[0]_i_2_n_2 ,\cnt_high_allowed_clk_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_high_allowed_clk_reg[0]_i_2_n_4 ,\cnt_high_allowed_clk_reg[0]_i_2_n_5 ,\cnt_high_allowed_clk_reg[0]_i_2_n_6 ,\cnt_high_allowed_clk_reg[0]_i_2_n_7 }),
        .S({cnt_high_allowed_clk_reg[3:1],\cnt_high_allowed_clk[0]_i_3_n_0 }));
  FDRE \cnt_high_allowed_clk_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_5 ),
        .Q(cnt_high_allowed_clk_reg[10]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_4 ),
        .Q(cnt_high_allowed_clk_reg[11]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_7 ),
        .Q(cnt_high_allowed_clk_reg[12]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[12]_i_1 
       (.CI(\cnt_high_allowed_clk_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_high_allowed_clk_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_high_allowed_clk_reg[12]_i_1_n_1 ,\cnt_high_allowed_clk_reg[12]_i_1_n_2 ,\cnt_high_allowed_clk_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_high_allowed_clk_reg[12]_i_1_n_4 ,\cnt_high_allowed_clk_reg[12]_i_1_n_5 ,\cnt_high_allowed_clk_reg[12]_i_1_n_6 ,\cnt_high_allowed_clk_reg[12]_i_1_n_7 }),
        .S(cnt_high_allowed_clk_reg[15:12]));
  FDRE \cnt_high_allowed_clk_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_6 ),
        .Q(cnt_high_allowed_clk_reg[13]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_5 ),
        .Q(cnt_high_allowed_clk_reg[14]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[12]_i_1_n_4 ),
        .Q(cnt_high_allowed_clk_reg[15]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_6 ),
        .Q(cnt_high_allowed_clk_reg[1]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_5 ),
        .Q(cnt_high_allowed_clk_reg[2]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[0]_i_2_n_4 ),
        .Q(cnt_high_allowed_clk_reg[3]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_7 ),
        .Q(cnt_high_allowed_clk_reg[4]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[4]_i_1 
       (.CI(\cnt_high_allowed_clk_reg[0]_i_2_n_0 ),
        .CO({\cnt_high_allowed_clk_reg[4]_i_1_n_0 ,\cnt_high_allowed_clk_reg[4]_i_1_n_1 ,\cnt_high_allowed_clk_reg[4]_i_1_n_2 ,\cnt_high_allowed_clk_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_high_allowed_clk_reg[4]_i_1_n_4 ,\cnt_high_allowed_clk_reg[4]_i_1_n_5 ,\cnt_high_allowed_clk_reg[4]_i_1_n_6 ,\cnt_high_allowed_clk_reg[4]_i_1_n_7 }),
        .S(cnt_high_allowed_clk_reg[7:4]));
  FDRE \cnt_high_allowed_clk_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_6 ),
        .Q(cnt_high_allowed_clk_reg[5]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_5 ),
        .Q(cnt_high_allowed_clk_reg[6]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[4]_i_1_n_4 ),
        .Q(cnt_high_allowed_clk_reg[7]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_high_allowed_clk_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_7 ),
        .Q(cnt_high_allowed_clk_reg[8]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_high_allowed_clk_reg[8]_i_1 
       (.CI(\cnt_high_allowed_clk_reg[4]_i_1_n_0 ),
        .CO({\cnt_high_allowed_clk_reg[8]_i_1_n_0 ,\cnt_high_allowed_clk_reg[8]_i_1_n_1 ,\cnt_high_allowed_clk_reg[8]_i_1_n_2 ,\cnt_high_allowed_clk_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_high_allowed_clk_reg[8]_i_1_n_4 ,\cnt_high_allowed_clk_reg[8]_i_1_n_5 ,\cnt_high_allowed_clk_reg[8]_i_1_n_6 ,\cnt_high_allowed_clk_reg[8]_i_1_n_7 }),
        .S(cnt_high_allowed_clk_reg[11:8]));
  FDRE \cnt_high_allowed_clk_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_high_allowed_clk_reg[8]_i_1_n_6 ),
        .Q(cnt_high_allowed_clk_reg[9]),
        .R(\cnt_high_allowed_clk[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h4F)) 
    \fft_azimut8_r[15]_i_1 
       (.I0(azimut_0_prev),
        .I1(azimut_0),
        .I2(m00_axis_aresetn),
        .O(\fft_azimut8_r[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000040)) 
    \fft_azimut8_r[15]_i_2 
       (.I0(\fft_azimut8_r[15]_i_4_n_0 ),
        .I1(\fft_azimut8_r[15]_i_5_n_0 ),
        .I2(\fft_azimut_r_reg_n_0_[3] ),
        .I3(\fft_azimut_r_reg_n_0_[0] ),
        .I4(\fft_azimut_r_reg_n_0_[2] ),
        .I5(\fft_azimut_r_reg_n_0_[1] ),
        .O(fft_azimut8_r));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \fft_azimut8_r[15]_i_4 
       (.I0(cnt_high_allowed_clk_reg[11]),
        .I1(cnt_high_allowed_clk_reg[13]),
        .I2(cnt_high_allowed_clk_reg[0]),
        .I3(cnt_high_allowed_clk_reg[2]),
        .I4(\fft_azimut8_r[15]_i_6_n_0 ),
        .O(\fft_azimut8_r[15]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00004000)) 
    \fft_azimut8_r[15]_i_5 
       (.I0(cnt_high_allowed_clk_reg[12]),
        .I1(cnt_high_allowed_clk_reg[9]),
        .I2(cnt_high_allowed_clk_reg[5]),
        .I3(cnt_high_allowed_clk_reg[6]),
        .I4(\fft_azimut_r[3]_i_3_n_0 ),
        .O(\fft_azimut8_r[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hEFFF)) 
    \fft_azimut8_r[15]_i_6 
       (.I0(cnt_high_allowed_clk_reg[15]),
        .I1(cnt_high_allowed_clk_reg[1]),
        .I2(cnt_high_allowed_clk_reg[8]),
        .I3(cnt_high_allowed_clk_reg[7]),
        .O(\fft_azimut8_r[15]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \fft_azimut8_r[3]_i_2 
       (.I0(azimut8[0]),
        .O(\fft_azimut8_r[3]_i_2_n_0 ));
  FDRE \fft_azimut8_r_reg[0] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_7 ),
        .Q(azimut8[0]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[10] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_5 ),
        .Q(azimut8[10]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[11] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_4 ),
        .Q(azimut8[11]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[11]_i_1 
       (.CI(\fft_azimut8_r_reg[7]_i_1_n_0 ),
        .CO({\fft_azimut8_r_reg[11]_i_1_n_0 ,\fft_azimut8_r_reg[11]_i_1_n_1 ,\fft_azimut8_r_reg[11]_i_1_n_2 ,\fft_azimut8_r_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\fft_azimut8_r_reg[11]_i_1_n_4 ,\fft_azimut8_r_reg[11]_i_1_n_5 ,\fft_azimut8_r_reg[11]_i_1_n_6 ,\fft_azimut8_r_reg[11]_i_1_n_7 }),
        .S(azimut8[11:8]));
  FDRE \fft_azimut8_r_reg[12] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_7 ),
        .Q(azimut8[12]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[13] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_6 ),
        .Q(azimut8[13]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[14] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_5 ),
        .Q(azimut8[14]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[15] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[15]_i_3_n_4 ),
        .Q(azimut8[15]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[15]_i_3 
       (.CI(\fft_azimut8_r_reg[11]_i_1_n_0 ),
        .CO({\NLW_fft_azimut8_r_reg[15]_i_3_CO_UNCONNECTED [3],\fft_azimut8_r_reg[15]_i_3_n_1 ,\fft_azimut8_r_reg[15]_i_3_n_2 ,\fft_azimut8_r_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\fft_azimut8_r_reg[15]_i_3_n_4 ,\fft_azimut8_r_reg[15]_i_3_n_5 ,\fft_azimut8_r_reg[15]_i_3_n_6 ,\fft_azimut8_r_reg[15]_i_3_n_7 }),
        .S(azimut8[15:12]));
  FDRE \fft_azimut8_r_reg[1] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_6 ),
        .Q(azimut8[1]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[2] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_5 ),
        .Q(azimut8[2]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[3] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[3]_i_1_n_4 ),
        .Q(azimut8[3]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\fft_azimut8_r_reg[3]_i_1_n_0 ,\fft_azimut8_r_reg[3]_i_1_n_1 ,\fft_azimut8_r_reg[3]_i_1_n_2 ,\fft_azimut8_r_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\fft_azimut8_r_reg[3]_i_1_n_4 ,\fft_azimut8_r_reg[3]_i_1_n_5 ,\fft_azimut8_r_reg[3]_i_1_n_6 ,\fft_azimut8_r_reg[3]_i_1_n_7 }),
        .S({azimut8[3:1],\fft_azimut8_r[3]_i_2_n_0 }));
  FDRE \fft_azimut8_r_reg[4] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_7 ),
        .Q(azimut8[4]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[5] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_6 ),
        .Q(azimut8[5]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[6] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_5 ),
        .Q(azimut8[6]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[7] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[7]_i_1_n_4 ),
        .Q(azimut8[7]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  CARRY4 \fft_azimut8_r_reg[7]_i_1 
       (.CI(\fft_azimut8_r_reg[3]_i_1_n_0 ),
        .CO({\fft_azimut8_r_reg[7]_i_1_n_0 ,\fft_azimut8_r_reg[7]_i_1_n_1 ,\fft_azimut8_r_reg[7]_i_1_n_2 ,\fft_azimut8_r_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\fft_azimut8_r_reg[7]_i_1_n_4 ,\fft_azimut8_r_reg[7]_i_1_n_5 ,\fft_azimut8_r_reg[7]_i_1_n_6 ,\fft_azimut8_r_reg[7]_i_1_n_7 }),
        .S(azimut8[7:4]));
  FDRE \fft_azimut8_r_reg[8] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_7 ),
        .Q(azimut8[8]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut8_r_reg[9] 
       (.C(clk_10MHz),
        .CE(fft_azimut8_r),
        .D(\fft_azimut8_r_reg[11]_i_1_n_6 ),
        .Q(azimut8[9]),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h00FD)) 
    \fft_azimut_r[0]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[3] ),
        .I1(\fft_azimut_r_reg_n_0_[2] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .I3(\fft_azimut_r_reg_n_0_[0] ),
        .O(\fft_azimut_r[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \fft_azimut_r[1]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[1] ),
        .I1(\fft_azimut_r_reg_n_0_[0] ),
        .O(\fft_azimut_r[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \fft_azimut_r[2]_i_1 
       (.I0(\fft_azimut_r_reg_n_0_[2] ),
        .I1(\fft_azimut_r_reg_n_0_[0] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .O(\fft_azimut_r[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000004000)) 
    \fft_azimut_r[3]_i_1 
       (.I0(\fft_azimut_r[3]_i_3_n_0 ),
        .I1(cnt_high_allowed_clk_reg[6]),
        .I2(cnt_high_allowed_clk_reg[5]),
        .I3(cnt_high_allowed_clk_reg[9]),
        .I4(cnt_high_allowed_clk_reg[12]),
        .I5(\fft_azimut8_r[15]_i_4_n_0 ),
        .O(fft_azimut_r));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h7E80)) 
    \fft_azimut_r[3]_i_2 
       (.I0(\fft_azimut_r_reg_n_0_[2] ),
        .I1(\fft_azimut_r_reg_n_0_[0] ),
        .I2(\fft_azimut_r_reg_n_0_[1] ),
        .I3(\fft_azimut_r_reg_n_0_[3] ),
        .O(\fft_azimut_r[3]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \fft_azimut_r[3]_i_3 
       (.I0(cnt_high_allowed_clk_reg[10]),
        .I1(cnt_high_allowed_clk_reg[4]),
        .I2(cnt_high_allowed_clk_reg[3]),
        .I3(cnt_high_allowed_clk_reg[14]),
        .O(\fft_azimut_r[3]_i_3_n_0 ));
  FDRE \fft_azimut_r_reg[0] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[0]_i_1_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[0] ),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut_r_reg[1] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[1]_i_1_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[1] ),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut_r_reg[2] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[2]_i_1_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[2] ),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  FDRE \fft_azimut_r_reg[3] 
       (.C(clk_10MHz),
        .CE(fft_azimut_r),
        .D(\fft_azimut_r[3]_i_2_n_0 ),
        .Q(\fft_azimut_r_reg_n_0_[3] ),
        .R(\fft_azimut8_r[15]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_axis_tdata_r[11]_i_1 
       (.I0(m00_axis_aresetn),
        .I1(s00_axis_tvalid),
        .O(m00_axis_tdata_r[11]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[16]_i_1 
       (.I0(s00_axis_tvalid),
        .I1(m00_axis_aresetn),
        .I2(azimut8[0]),
        .O(\m00_axis_tdata_r[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[17]_i_1 
       (.I0(azimut8[1]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[17]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[18]_i_1 
       (.I0(azimut8[2]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[18]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[19]_i_1 
       (.I0(azimut8[3]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[19]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[20]_i_1 
       (.I0(azimut8[4]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[20]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[21]_i_1 
       (.I0(azimut8[5]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[21]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[22]_i_1 
       (.I0(azimut8[6]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[22]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[23]_i_1 
       (.I0(azimut8[7]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[23]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[24]_i_1 
       (.I0(azimut8[8]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[24]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[25]_i_1 
       (.I0(azimut8[9]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[25]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[26]_i_1 
       (.I0(azimut8[10]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[26]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[27]_i_1 
       (.I0(azimut8[11]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[27]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[28]_i_1 
       (.I0(azimut8[12]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[28]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[29]_i_1 
       (.I0(azimut8[13]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[29]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[30]_i_1 
       (.I0(azimut8[14]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[30]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAA8A)) 
    \m00_axis_tdata_r[31]_i_1 
       (.I0(m00_axis_tdata_r[11]),
        .I1(\m00_axis_tdata_r[31]_i_4_n_0 ),
        .I2(\m00_axis_tdata_r[31]_i_5_n_0 ),
        .I3(sel0[1]),
        .I4(sel0[7]),
        .I5(\m00_axis_tdata_r[31]_i_6_n_0 ),
        .O(m00_axis_tdata_r_0));
  LUT2 #(
    .INIT(4'hB)) 
    \m00_axis_tdata_r[31]_i_2 
       (.I0(s00_axis_tvalid),
        .I1(m00_axis_aresetn),
        .O(\m00_axis_tdata_r[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h80)) 
    \m00_axis_tdata_r[31]_i_3 
       (.I0(azimut8[15]),
        .I1(s00_axis_tvalid),
        .I2(m00_axis_aresetn),
        .O(m00_axis_tdata_r[31]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    \m00_axis_tdata_r[31]_i_4 
       (.I0(\m00_axis_tdata_r[31]_i_7_n_0 ),
        .I1(\m00_axis_tdata_r[31]_i_8_n_0 ),
        .I2(sel0[10]),
        .I3(sel0[4]),
        .I4(sel0[0]),
        .I5(sel0[3]),
        .O(\m00_axis_tdata_r[31]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \m00_axis_tdata_r[31]_i_5 
       (.I0(sel0[11]),
        .I1(sel0[8]),
        .I2(sel0[5]),
        .I3(sel0[2]),
        .O(\m00_axis_tdata_r[31]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \m00_axis_tdata_r[31]_i_6 
       (.I0(sel0[14]),
        .I1(sel0[13]),
        .O(\m00_axis_tdata_r[31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFEFF)) 
    \m00_axis_tdata_r[31]_i_7 
       (.I0(\fft_azimut_r_reg_n_0_[1] ),
        .I1(\fft_azimut_r_reg_n_0_[2] ),
        .I2(\fft_azimut_r_reg_n_0_[0] ),
        .I3(\fft_azimut_r_reg_n_0_[3] ),
        .O(\m00_axis_tdata_r[31]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \m00_axis_tdata_r[31]_i_8 
       (.I0(sel0[12]),
        .I1(sel0[15]),
        .I2(sel0[9]),
        .I3(sel0[6]),
        .O(\m00_axis_tdata_r[31]_i_8_n_0 ));
  FDRE \m00_axis_tdata_r_reg[11] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[11]),
        .Q(m00_axis_tdata[0]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[16] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(\m00_axis_tdata_r[16]_i_1_n_0 ),
        .Q(m00_axis_tdata[1]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[17] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[17]),
        .Q(m00_axis_tdata[2]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[18] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[18]),
        .Q(m00_axis_tdata[3]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[19] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[19]),
        .Q(m00_axis_tdata[4]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[20] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[20]),
        .Q(m00_axis_tdata[5]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[21] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[21]),
        .Q(m00_axis_tdata[6]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[22] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[22]),
        .Q(m00_axis_tdata[7]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[23] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[23]),
        .Q(m00_axis_tdata[8]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[24] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[24]),
        .Q(m00_axis_tdata[9]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[25] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[25]),
        .Q(m00_axis_tdata[10]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[26] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[26]),
        .Q(m00_axis_tdata[11]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[27] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[27]),
        .Q(m00_axis_tdata[12]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[28] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[28]),
        .Q(m00_axis_tdata[13]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[29] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[29]),
        .Q(m00_axis_tdata[14]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[30] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[30]),
        .Q(m00_axis_tdata[15]),
        .R(m00_axis_tdata_r_0));
  FDRE \m00_axis_tdata_r_reg[31] 
       (.C(m00_axis_aclk),
        .CE(\m00_axis_tdata_r[31]_i_2_n_0 ),
        .D(m00_axis_tdata_r[31]),
        .Q(m00_axis_tdata[16]),
        .R(m00_axis_tdata_r_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    m00_axis_tlast_INST_0
       (.I0(s00_axis_tlast),
        .I1(\fft_azimut_r_reg_n_0_[3] ),
        .I2(\fft_azimut_r_reg_n_0_[0] ),
        .I3(\fft_azimut_r_reg_n_0_[2] ),
        .I4(\fft_azimut_r_reg_n_0_[1] ),
        .O(m00_axis_tlast));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    m00_axis_tvalid_INST_0
       (.I0(s00_axis_tvalid),
        .I1(\fft_azimut_r_reg_n_0_[3] ),
        .I2(\fft_azimut_r_reg_n_0_[0] ),
        .I3(\fft_azimut_r_reg_n_0_[2] ),
        .I4(\fft_azimut_r_reg_n_0_[1] ),
        .O(m00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v3_0_S00_AXI
   (S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    S_AXI_BVALID,
    S_AXI_RVALID,
    S_AXI_RDATA,
    S_AXI_ACLK,
    S_AXI_ARESETN,
    S_AXI_AWVALID,
    S_AXI_WVALID,
    S_AXI_BREADY,
    S_AXI_ARVALID,
    S_AXI_RREADY,
    S_AXI_ARADDR,
    S_AXI_AWADDR,
    S_AXI_WDATA,
    S_AXI_WSTRB);
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output S_AXI_BVALID;
  output S_AXI_RVALID;
  output [31:0]S_AXI_RDATA;
  input S_AXI_ACLK;
  input S_AXI_ARESETN;
  input S_AXI_AWVALID;
  input S_AXI_WVALID;
  input S_AXI_BREADY;
  input S_AXI_ARVALID;
  input S_AXI_RREADY;
  input [1:0]S_AXI_ARADDR;
  input [1:0]S_AXI_AWADDR;
  input [31:0]S_AXI_WDATA;
  input [3:0]S_AXI_WSTRB;

  wire S_AXI_ACLK;
  wire [1:0]S_AXI_ARADDR;
  wire S_AXI_ARESETN;
  wire S_AXI_ARREADY;
  wire S_AXI_ARVALID;
  wire [1:0]S_AXI_AWADDR;
  wire S_AXI_AWREADY;
  wire S_AXI_AWVALID;
  wire S_AXI_BREADY;
  wire S_AXI_BVALID;
  wire [31:0]S_AXI_RDATA;
  wire S_AXI_RREADY;
  wire S_AXI_RVALID;
  wire [31:0]S_AXI_WDATA;
  wire S_AXI_WREADY;
  wire [3:0]S_AXI_WSTRB;
  wire S_AXI_WVALID;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire [3:2]axi_araddr;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire axi_arready0;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [1:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire [31:0]slv_reg0;
  wire \slv_reg0[15]_i_1_n_0 ;
  wire \slv_reg0[23]_i_1_n_0 ;
  wire \slv_reg0[31]_i_1_n_0 ;
  wire \slv_reg0[7]_i_1_n_0 ;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire [31:0]slv_reg3;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;

  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(S_AXI_AWVALID),
        .I1(aw_en_reg_n_0),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WVALID),
        .I4(S_AXI_BREADY),
        .I5(S_AXI_BVALID),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(S_AXI_ARADDR[0]),
        .I1(S_AXI_ARVALID),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[2]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(S_AXI_ARADDR[1]),
        .I1(S_AXI_ARVALID),
        .I2(S_AXI_ARREADY),
        .I3(axi_araddr[3]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(axi_araddr[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(axi_araddr[3]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(S_AXI_ARVALID),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[2]_i_1 
       (.I0(S_AXI_AWADDR[0]),
        .I1(S_AXI_WVALID),
        .I2(S_AXI_AWREADY),
        .I3(aw_en_reg_n_0),
        .I4(S_AXI_AWVALID),
        .I5(p_0_in[0]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[3]_i_1 
       (.I0(S_AXI_AWADDR[1]),
        .I1(S_AXI_WVALID),
        .I2(S_AXI_AWREADY),
        .I3(aw_en_reg_n_0),
        .I4(S_AXI_AWVALID),
        .I5(p_0_in[1]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(S_AXI_ARESETN),
        .O(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_2
       (.I0(S_AXI_WVALID),
        .I1(S_AXI_AWREADY),
        .I2(aw_en_reg_n_0),
        .I3(S_AXI_AWVALID),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(S_AXI_AWVALID),
        .I1(S_AXI_WVALID),
        .I2(S_AXI_AWREADY),
        .I3(S_AXI_WREADY),
        .I4(S_AXI_BREADY),
        .I5(S_AXI_BVALID),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(S_AXI_BVALID),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[0]_i_1 
       (.I0(slv_reg1[0]),
        .I1(slv_reg0[0]),
        .I2(slv_reg3[0]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[0]),
        .O(reg_data_out[0]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg1[10]),
        .I1(slv_reg0[10]),
        .I2(slv_reg3[10]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[10]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg1[11]),
        .I1(slv_reg0[11]),
        .I2(slv_reg3[11]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[11]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg1[12]),
        .I1(slv_reg0[12]),
        .I2(slv_reg3[12]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[12]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg1[13]),
        .I1(slv_reg0[13]),
        .I2(slv_reg3[13]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[13]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg1[14]),
        .I1(slv_reg0[14]),
        .I2(slv_reg3[14]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[14]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg1[15]),
        .I1(slv_reg0[15]),
        .I2(slv_reg3[15]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[15]),
        .O(reg_data_out[15]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[16]_i_1 
       (.I0(slv_reg1[16]),
        .I1(slv_reg0[16]),
        .I2(slv_reg3[16]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[16]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[17]_i_1 
       (.I0(slv_reg1[17]),
        .I1(slv_reg0[17]),
        .I2(slv_reg3[17]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[17]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[18]_i_1 
       (.I0(slv_reg1[18]),
        .I1(slv_reg0[18]),
        .I2(slv_reg3[18]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[18]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[19]_i_1 
       (.I0(slv_reg1[19]),
        .I1(slv_reg0[19]),
        .I2(slv_reg3[19]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[19]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[1]_i_1 
       (.I0(slv_reg1[1]),
        .I1(slv_reg0[1]),
        .I2(slv_reg3[1]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[1]),
        .O(reg_data_out[1]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[20]_i_1 
       (.I0(slv_reg1[20]),
        .I1(slv_reg0[20]),
        .I2(slv_reg3[20]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[20]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[21]_i_1 
       (.I0(slv_reg1[21]),
        .I1(slv_reg0[21]),
        .I2(slv_reg3[21]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[21]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[22]_i_1 
       (.I0(slv_reg1[22]),
        .I1(slv_reg0[22]),
        .I2(slv_reg3[22]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[22]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[23]_i_1 
       (.I0(slv_reg1[23]),
        .I1(slv_reg0[23]),
        .I2(slv_reg3[23]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[23]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[24]_i_1 
       (.I0(slv_reg1[24]),
        .I1(slv_reg0[24]),
        .I2(slv_reg3[24]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[24]),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[25]_i_1 
       (.I0(slv_reg1[25]),
        .I1(slv_reg0[25]),
        .I2(slv_reg3[25]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[25]),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[26]_i_1 
       (.I0(slv_reg1[26]),
        .I1(slv_reg0[26]),
        .I2(slv_reg3[26]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[26]),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[27]_i_1 
       (.I0(slv_reg1[27]),
        .I1(slv_reg0[27]),
        .I2(slv_reg3[27]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[27]),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[28]_i_1 
       (.I0(slv_reg1[28]),
        .I1(slv_reg0[28]),
        .I2(slv_reg3[28]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[28]),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[29]_i_1 
       (.I0(slv_reg1[29]),
        .I1(slv_reg0[29]),
        .I2(slv_reg3[29]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[29]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[2]_i_1 
       (.I0(slv_reg1[2]),
        .I1(slv_reg0[2]),
        .I2(slv_reg3[2]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[2]),
        .O(reg_data_out[2]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[30]_i_1 
       (.I0(slv_reg1[30]),
        .I1(slv_reg0[30]),
        .I2(slv_reg3[30]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[30]),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[31]_i_1 
       (.I0(slv_reg1[31]),
        .I1(slv_reg0[31]),
        .I2(slv_reg3[31]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[31]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[3]_i_1 
       (.I0(slv_reg1[3]),
        .I1(slv_reg0[3]),
        .I2(slv_reg3[3]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[3]),
        .O(reg_data_out[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[4]_i_1 
       (.I0(slv_reg1[4]),
        .I1(slv_reg0[4]),
        .I2(slv_reg3[4]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[4]),
        .O(reg_data_out[4]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[5]_i_1 
       (.I0(slv_reg1[5]),
        .I1(slv_reg0[5]),
        .I2(slv_reg3[5]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[5]),
        .O(reg_data_out[5]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[6]_i_1 
       (.I0(slv_reg1[6]),
        .I1(slv_reg0[6]),
        .I2(slv_reg3[6]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[6]),
        .O(reg_data_out[6]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[7]_i_1 
       (.I0(slv_reg1[7]),
        .I1(slv_reg0[7]),
        .I2(slv_reg3[7]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[7]),
        .O(reg_data_out[7]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[8]_i_1 
       (.I0(slv_reg1[8]),
        .I1(slv_reg0[8]),
        .I2(slv_reg3[8]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[8]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg1[9]),
        .I1(slv_reg0[9]),
        .I2(slv_reg3[9]),
        .I3(axi_araddr[3]),
        .I4(axi_araddr[2]),
        .I5(slv_reg2[9]),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[0]),
        .Q(S_AXI_RDATA[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[10]),
        .Q(S_AXI_RDATA[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[11]),
        .Q(S_AXI_RDATA[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[12]),
        .Q(S_AXI_RDATA[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[13]),
        .Q(S_AXI_RDATA[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[14]),
        .Q(S_AXI_RDATA[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[15]),
        .Q(S_AXI_RDATA[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[16]),
        .Q(S_AXI_RDATA[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[17]),
        .Q(S_AXI_RDATA[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[18]),
        .Q(S_AXI_RDATA[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[19]),
        .Q(S_AXI_RDATA[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[1]),
        .Q(S_AXI_RDATA[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[20]),
        .Q(S_AXI_RDATA[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[21]),
        .Q(S_AXI_RDATA[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[22]),
        .Q(S_AXI_RDATA[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[23]),
        .Q(S_AXI_RDATA[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[24]),
        .Q(S_AXI_RDATA[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[25]),
        .Q(S_AXI_RDATA[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[26]),
        .Q(S_AXI_RDATA[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[27]),
        .Q(S_AXI_RDATA[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[28]),
        .Q(S_AXI_RDATA[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[29]),
        .Q(S_AXI_RDATA[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[2]),
        .Q(S_AXI_RDATA[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[30]),
        .Q(S_AXI_RDATA[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[31]),
        .Q(S_AXI_RDATA[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[3]),
        .Q(S_AXI_RDATA[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[4]),
        .Q(S_AXI_RDATA[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[5]),
        .Q(S_AXI_RDATA[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[6]),
        .Q(S_AXI_RDATA[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[7]),
        .Q(S_AXI_RDATA[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[8]),
        .Q(S_AXI_RDATA[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(S_AXI_ACLK),
        .CE(slv_reg_rden__0),
        .D(reg_data_out[9]),
        .Q(S_AXI_RDATA[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(S_AXI_ARREADY),
        .I1(S_AXI_ARVALID),
        .I2(S_AXI_RVALID),
        .I3(S_AXI_RREADY),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(S_AXI_RVALID),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(S_AXI_AWVALID),
        .I1(S_AXI_WVALID),
        .I2(S_AXI_WREADY),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(S_AXI_ACLK),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(S_AXI_WSTRB[1]),
        .O(\slv_reg0[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(S_AXI_WSTRB[2]),
        .O(\slv_reg0[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(S_AXI_WSTRB[3]),
        .O(\slv_reg0[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0200)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(S_AXI_WSTRB[0]),
        .O(\slv_reg0[7]_i_1_n_0 ));
  FDRE \slv_reg0_reg[0] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[0]),
        .Q(slv_reg0[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[10] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[10]),
        .Q(slv_reg0[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[11] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[11]),
        .Q(slv_reg0[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[12] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[12]),
        .Q(slv_reg0[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[13] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[13]),
        .Q(slv_reg0[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[14] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[14]),
        .Q(slv_reg0[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[15] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[15]),
        .Q(slv_reg0[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[16] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[16]),
        .Q(slv_reg0[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[17] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[17]),
        .Q(slv_reg0[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[18] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[18]),
        .Q(slv_reg0[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[19] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[19]),
        .Q(slv_reg0[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[1] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[1]),
        .Q(slv_reg0[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[20] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[20]),
        .Q(slv_reg0[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[21] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[21]),
        .Q(slv_reg0[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[22] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[22]),
        .Q(slv_reg0[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[23] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[23]),
        .Q(slv_reg0[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[24] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[24]),
        .Q(slv_reg0[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[25] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[25]),
        .Q(slv_reg0[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[26] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[26]),
        .Q(slv_reg0[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[27] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[27]),
        .Q(slv_reg0[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[28] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[28]),
        .Q(slv_reg0[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[29] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[29]),
        .Q(slv_reg0[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[2]),
        .Q(slv_reg0[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[30] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[30]),
        .Q(slv_reg0[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[31] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[31]),
        .Q(slv_reg0[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[3]),
        .Q(slv_reg0[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[4] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[4]),
        .Q(slv_reg0[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[5] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[5]),
        .Q(slv_reg0[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[6] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[6]),
        .Q(slv_reg0[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[7] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[7]),
        .Q(slv_reg0[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[8] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[8]),
        .Q(slv_reg0[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[9] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg0[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[9]),
        .Q(slv_reg0[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[1]),
        .Q(slv_reg1[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[1]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[2]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[3]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0080)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(p_0_in[1]),
        .I2(S_AXI_WSTRB[0]),
        .I3(p_0_in[0]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(S_AXI_WDATA[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(S_AXI_WDATA[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(S_AXI_WDATA[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(S_AXI_ACLK),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(S_AXI_WDATA[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(S_AXI_WSTRB[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[15]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(S_AXI_WSTRB[2]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[23]));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(S_AXI_WSTRB[3]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[31]_i_2 
       (.I0(S_AXI_WREADY),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_AWVALID),
        .I3(S_AXI_WVALID),
        .O(slv_reg_wren__0));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(S_AXI_WSTRB[0]),
        .I2(p_0_in[0]),
        .I3(p_0_in[1]),
        .O(p_1_in[7]));
  FDRE \slv_reg3_reg[0] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[0]),
        .Q(slv_reg3[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[10]),
        .Q(slv_reg3[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[11]),
        .Q(slv_reg3[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[12]),
        .Q(slv_reg3[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[13]),
        .Q(slv_reg3[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[14]),
        .Q(slv_reg3[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[15]),
        .Q(slv_reg3[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[16]),
        .Q(slv_reg3[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[17]),
        .Q(slv_reg3[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[18]),
        .Q(slv_reg3[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[19]),
        .Q(slv_reg3[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[1]),
        .Q(slv_reg3[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[20]),
        .Q(slv_reg3[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[21]),
        .Q(slv_reg3[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[22]),
        .Q(slv_reg3[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[23]),
        .D(S_AXI_WDATA[23]),
        .Q(slv_reg3[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[24]),
        .Q(slv_reg3[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[25]),
        .Q(slv_reg3[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[26]),
        .Q(slv_reg3[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[27]),
        .Q(slv_reg3[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[28]),
        .Q(slv_reg3[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[29]),
        .Q(slv_reg3[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[2]),
        .Q(slv_reg3[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[30]),
        .Q(slv_reg3[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[31]),
        .D(S_AXI_WDATA[31]),
        .Q(slv_reg3[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[3]),
        .Q(slv_reg3[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[4]),
        .Q(slv_reg3[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[5]),
        .Q(slv_reg3[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[6]),
        .Q(slv_reg3[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[7]),
        .D(S_AXI_WDATA[7]),
        .Q(slv_reg3[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[8]),
        .Q(slv_reg3[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(S_AXI_ACLK),
        .CE(p_1_in[15]),
        .D(S_AXI_WDATA[9]),
        .Q(slv_reg3[9]),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(S_AXI_ARVALID),
        .I1(S_AXI_RVALID),
        .I2(S_AXI_ARREADY),
        .O(slv_reg_rden__0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_averageFFT_0_0,averageFFT_v3_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "averageFFT_v3_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (S_AXI_ACLK,
    S_AXI_ARESETN,
    S_AXI_AWADDR,
    S_AXI_AWPROT,
    S_AXI_AWVALID,
    S_AXI_AWREADY,
    S_AXI_WDATA,
    S_AXI_WSTRB,
    S_AXI_WVALID,
    S_AXI_WREADY,
    S_AXI_BRESP,
    S_AXI_BVALID,
    S_AXI_BREADY,
    S_AXI_ARADDR,
    S_AXI_ARPROT,
    S_AXI_ARVALID,
    S_AXI_ARREADY,
    S_AXI_RDATA,
    S_AXI_RRESP,
    S_AXI_RVALID,
    S_AXI_RREADY,
    s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    allowed_clk,
    azimut_0,
    m00_axis_aclk,
    clk_10MHz,
    m00_axis_aresetn,
    azimut8);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S_AXI_ACLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXI_ACLK, ASSOCIATED_RESET S_AXI_ARESETN, ASSOCIATED_BUSIF S_AXI, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input S_AXI_ACLK;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S_AXI_ARESETN RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXI_ARESETN, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1" *) input S_AXI_ARESETN;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI AWADDR" *) input [4:0]S_AXI_AWADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI AWPROT" *) input [2:0]S_AXI_AWPROT;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI AWVALID" *) input S_AXI_AWVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI AWREADY" *) output S_AXI_AWREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI WDATA" *) input [31:0]S_AXI_WDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI WSTRB" *) input [3:0]S_AXI_WSTRB;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI WVALID" *) input S_AXI_WVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI WREADY" *) output S_AXI_WREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI BRESP" *) output [1:0]S_AXI_BRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI BVALID" *) output S_AXI_BVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI BREADY" *) input S_AXI_BREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI ARADDR" *) input [4:0]S_AXI_ARADDR;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI ARPROT" *) input [2:0]S_AXI_ARPROT;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI ARVALID" *) input S_AXI_ARVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI ARREADY" *) output S_AXI_ARREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI RDATA" *) output [31:0]S_AXI_RDATA;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI RRESP" *) output [1:0]S_AXI_RRESP;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI RVALID" *) output S_AXI_RVALID;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S_AXI, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 1e+08, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input S_AXI_RREADY;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TDATA" *) input [63:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TSTRB" *) input [7:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input azimut_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF m00_axis:s00_axis, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1" *) input m00_axis_aresetn;
  output [15:0]azimut8;

  wire \<const0> ;
  wire \<const1> ;
  wire S_AXI_ACLK;
  wire [4:0]S_AXI_ARADDR;
  wire S_AXI_ARESETN;
  wire S_AXI_ARREADY;
  wire S_AXI_ARVALID;
  wire [4:0]S_AXI_AWADDR;
  wire S_AXI_AWREADY;
  wire S_AXI_AWVALID;
  wire S_AXI_BREADY;
  wire S_AXI_BVALID;
  wire [31:0]S_AXI_RDATA;
  wire S_AXI_RREADY;
  wire S_AXI_RVALID;
  wire [31:0]S_AXI_WDATA;
  wire S_AXI_WREADY;
  wire [3:0]S_AXI_WSTRB;
  wire S_AXI_WVALID;
  wire allowed_clk;
  wire [15:0]azimut8;
  wire azimut_0;
  wire clk_10MHz;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:11]\^m00_axis_tdata ;
  wire m00_axis_tlast;
  wire m00_axis_tready;
  wire m00_axis_tvalid;
  wire s00_axis_tlast;
  wire s00_axis_tvalid;

  assign S_AXI_BRESP[1] = \<const0> ;
  assign S_AXI_BRESP[0] = \<const0> ;
  assign S_AXI_RRESP[1] = \<const0> ;
  assign S_AXI_RRESP[0] = \<const0> ;
  assign m00_axis_tdata[31:16] = \^m00_axis_tdata [31:16];
  assign m00_axis_tdata[15] = \<const0> ;
  assign m00_axis_tdata[14] = \<const0> ;
  assign m00_axis_tdata[13] = \<const0> ;
  assign m00_axis_tdata[12] = \<const0> ;
  assign m00_axis_tdata[11] = \^m00_axis_tdata [11];
  assign m00_axis_tdata[10] = \<const0> ;
  assign m00_axis_tdata[9] = \<const0> ;
  assign m00_axis_tdata[8] = \<const0> ;
  assign m00_axis_tdata[7] = \<const0> ;
  assign m00_axis_tdata[6] = \<const0> ;
  assign m00_axis_tdata[5] = \<const0> ;
  assign m00_axis_tdata[4] = \<const0> ;
  assign m00_axis_tdata[3] = \<const0> ;
  assign m00_axis_tdata[2] = \<const0> ;
  assign m00_axis_tdata[1] = \<const0> ;
  assign m00_axis_tdata[0] = \<const0> ;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign s00_axis_tready = m00_axis_tready;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v3_0 inst
       (.S_AXI_ACLK(S_AXI_ACLK),
        .S_AXI_ARADDR(S_AXI_ARADDR[3:2]),
        .S_AXI_ARESETN(S_AXI_ARESETN),
        .S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_ARVALID(S_AXI_ARVALID),
        .S_AXI_AWADDR(S_AXI_AWADDR[3:2]),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_AWVALID(S_AXI_AWVALID),
        .S_AXI_BREADY(S_AXI_BREADY),
        .S_AXI_BVALID(S_AXI_BVALID),
        .S_AXI_RDATA(S_AXI_RDATA),
        .S_AXI_RREADY(S_AXI_RREADY),
        .S_AXI_RVALID(S_AXI_RVALID),
        .S_AXI_WDATA(S_AXI_WDATA),
        .S_AXI_WREADY(S_AXI_WREADY),
        .S_AXI_WSTRB(S_AXI_WSTRB),
        .S_AXI_WVALID(S_AXI_WVALID),
        .allowed_clk(allowed_clk),
        .azimut8(azimut8),
        .azimut_0(azimut_0),
        .clk_10MHz(clk_10MHz),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata({\^m00_axis_tdata [31:16],\^m00_axis_tdata [11]}),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid),
        .s00_axis_tlast(s00_axis_tlast),
        .s00_axis_tvalid(s00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

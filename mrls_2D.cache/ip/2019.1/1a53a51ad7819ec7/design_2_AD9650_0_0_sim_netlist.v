// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Nov 30 13:21:27 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_sim_netlist.v
// Design      : design_2_AD9650_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v1_0_S00_AXI
   (axi_wready_reg_0,
    axi_awready_reg_0,
    axi_arready_reg_0,
    s00_axi_bvalid,
    s00_axi_rvalid,
    S,
    \cnt_DCO_reg[15] ,
    \slv_reg3_reg[15]_0 ,
    m00_fft_axis_tlast_r0,
    \slv_reg3_reg[6]_0 ,
    DI,
    \slv_reg3_reg[14]_0 ,
    \slv_reg3_reg[14]_1 ,
    \cnt_DCO_reg[9] ,
    ADC_PDwN,
    Q,
    \slv_reg3_reg[3]_0 ,
    \slv_reg3_reg[7]_0 ,
    \slv_reg3_reg[11]_0 ,
    \slv_reg3_reg[15]_1 ,
    mux_fft_0,
    m01_fft_axis_tvalid_r_reg,
    s00_axi_rdata,
    s00_axi_aclk,
    cnt_DCO_reg,
    cnt_in_DCO__14,
    CO,
    s00_axi_aresetn,
    O,
    s00_axi_awvalid,
    s00_axi_wvalid,
    s00_axi_bready,
    s00_axi_arvalid,
    s00_axi_rready,
    m00_fft_axis_tvalid_r_reg,
    m00_fft_axis_tvalid_r_reg_0,
    m00_fft_axis_tvalid_r_reg_1,
    mux_fft,
    m00_fft_axis_tvalid,
    m01_fft_axis_tvalid,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wstrb);
  output axi_wready_reg_0;
  output axi_awready_reg_0;
  output axi_arready_reg_0;
  output s00_axi_bvalid;
  output s00_axi_rvalid;
  output [3:0]S;
  output [1:0]\cnt_DCO_reg[15] ;
  output [1:0]\slv_reg3_reg[15]_0 ;
  output m00_fft_axis_tlast_r0;
  output [3:0]\slv_reg3_reg[6]_0 ;
  output [3:0]DI;
  output [3:0]\slv_reg3_reg[14]_0 ;
  output [3:0]\slv_reg3_reg[14]_1 ;
  output [3:0]\cnt_DCO_reg[9] ;
  output ADC_PDwN;
  output [0:0]Q;
  output [3:0]\slv_reg3_reg[3]_0 ;
  output [3:0]\slv_reg3_reg[7]_0 ;
  output [3:0]\slv_reg3_reg[11]_0 ;
  output [3:0]\slv_reg3_reg[15]_1 ;
  output mux_fft_0;
  output m01_fft_axis_tvalid_r_reg;
  output [31:0]s00_axi_rdata;
  input s00_axi_aclk;
  input [15:0]cnt_DCO_reg;
  input cnt_in_DCO__14;
  input [0:0]CO;
  input s00_axi_aresetn;
  input [0:0]O;
  input s00_axi_awvalid;
  input s00_axi_wvalid;
  input s00_axi_bready;
  input s00_axi_arvalid;
  input s00_axi_rready;
  input m00_fft_axis_tvalid_r_reg;
  input m00_fft_axis_tvalid_r_reg_0;
  input [0:0]m00_fft_axis_tvalid_r_reg_1;
  input mux_fft;
  input m00_fft_axis_tvalid;
  input m01_fft_axis_tvalid;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input [3:0]s00_axi_wstrb;

  wire ADC_PDwN;
  wire [0:0]CO;
  wire [3:0]DI;
  wire [0:0]O;
  wire [0:0]Q;
  wire [3:0]S;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire \axi_araddr[2]_i_1_n_0 ;
  wire \axi_araddr[3]_i_1_n_0 ;
  wire \axi_araddr[4]_i_1_n_0 ;
  wire axi_arready0;
  wire axi_arready_reg_0;
  wire [4:2]axi_awaddr;
  wire \axi_awaddr[2]_i_1_n_0 ;
  wire \axi_awaddr[3]_i_1_n_0 ;
  wire \axi_awaddr[4]_i_1_n_0 ;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_awready_reg_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_1_n_0 ;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[10]_i_1_n_0 ;
  wire \axi_rdata[10]_i_2_n_0 ;
  wire \axi_rdata[11]_i_1_n_0 ;
  wire \axi_rdata[11]_i_2_n_0 ;
  wire \axi_rdata[12]_i_1_n_0 ;
  wire \axi_rdata[12]_i_2_n_0 ;
  wire \axi_rdata[13]_i_1_n_0 ;
  wire \axi_rdata[13]_i_2_n_0 ;
  wire \axi_rdata[14]_i_1_n_0 ;
  wire \axi_rdata[14]_i_2_n_0 ;
  wire \axi_rdata[15]_i_1_n_0 ;
  wire \axi_rdata[15]_i_2_n_0 ;
  wire \axi_rdata[16]_i_1_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_1_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_1_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_1_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_1_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[20]_i_1_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_1_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_1_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_1_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_1_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_1_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_1_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_1_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_1_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_1_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_1_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[30]_i_1_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_1_n_0 ;
  wire \axi_rdata[31]_i_2_n_0 ;
  wire \axi_rdata[3]_i_1_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[4]_i_1_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[5]_i_1_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[6]_i_1_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[7]_i_1_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[8]_i_1_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire \axi_rdata[9]_i_1_n_0 ;
  wire \axi_rdata[9]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire axi_wready_reg_0;
  wire cnt_DCO1_carry__0_i_3_n_2;
  wire cnt_DCO1_carry__0_i_3_n_3;
  wire cnt_DCO1_carry_i_5_n_0;
  wire cnt_DCO1_carry_i_5_n_1;
  wire cnt_DCO1_carry_i_5_n_2;
  wire cnt_DCO1_carry_i_5_n_3;
  wire cnt_DCO1_carry_i_6_n_0;
  wire cnt_DCO1_carry_i_6_n_1;
  wire cnt_DCO1_carry_i_6_n_2;
  wire cnt_DCO1_carry_i_6_n_3;
  wire cnt_DCO1_carry_i_7_n_0;
  wire cnt_DCO1_carry_i_7_n_1;
  wire cnt_DCO1_carry_i_7_n_2;
  wire cnt_DCO1_carry_i_7_n_3;
  wire [16:1]cnt_DCO2;
  wire \cnt_DCO[0]_i_3_n_0 ;
  wire \cnt_DCO[0]_i_4_n_0 ;
  wire \cnt_DCO[0]_i_5_n_0 ;
  wire \cnt_DCO[0]_i_6_n_0 ;
  wire \cnt_DCO[12]_i_2_n_0 ;
  wire \cnt_DCO[12]_i_3_n_0 ;
  wire \cnt_DCO[12]_i_4_n_0 ;
  wire \cnt_DCO[12]_i_5_n_0 ;
  wire \cnt_DCO[4]_i_2_n_0 ;
  wire \cnt_DCO[4]_i_3_n_0 ;
  wire \cnt_DCO[4]_i_4_n_0 ;
  wire \cnt_DCO[4]_i_5_n_0 ;
  wire \cnt_DCO[8]_i_2_n_0 ;
  wire \cnt_DCO[8]_i_3_n_0 ;
  wire \cnt_DCO[8]_i_4_n_0 ;
  wire \cnt_DCO[8]_i_5_n_0 ;
  wire [15:0]cnt_DCO_reg;
  wire \cnt_DCO_reg[0]_i_2_n_0 ;
  wire \cnt_DCO_reg[0]_i_2_n_1 ;
  wire \cnt_DCO_reg[0]_i_2_n_2 ;
  wire \cnt_DCO_reg[0]_i_2_n_3 ;
  wire \cnt_DCO_reg[12]_i_1_n_1 ;
  wire \cnt_DCO_reg[12]_i_1_n_2 ;
  wire \cnt_DCO_reg[12]_i_1_n_3 ;
  wire [1:0]\cnt_DCO_reg[15] ;
  wire \cnt_DCO_reg[4]_i_1_n_0 ;
  wire \cnt_DCO_reg[4]_i_1_n_1 ;
  wire \cnt_DCO_reg[4]_i_1_n_2 ;
  wire \cnt_DCO_reg[4]_i_1_n_3 ;
  wire \cnt_DCO_reg[8]_i_1_n_0 ;
  wire \cnt_DCO_reg[8]_i_1_n_1 ;
  wire \cnt_DCO_reg[8]_i_1_n_2 ;
  wire \cnt_DCO_reg[8]_i_1_n_3 ;
  wire [3:0]\cnt_DCO_reg[9] ;
  wire cnt_in_DCO__14;
  wire m00_fft_axis_tlast_r0;
  wire m00_fft_axis_tvalid;
  wire m00_fft_axis_tvalid_r_reg;
  wire m00_fft_axis_tvalid_r_reg_0;
  wire [0:0]m00_fft_axis_tvalid_r_reg_1;
  wire m01_fft_axis_tvalid;
  wire m01_fft_axis_tvalid_r_reg;
  wire mux_fft;
  wire mux_fft_0;
  wire [31:7]p_1_in;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [2:0]sel0;
  wire [31:0]slv_reg0;
  wire [31:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire [31:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[1]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg3[15]_i_1_n_0 ;
  wire \slv_reg3[23]_i_1_n_0 ;
  wire \slv_reg3[31]_i_1_n_0 ;
  wire \slv_reg3[7]_i_1_n_0 ;
  wire [3:0]\slv_reg3_reg[11]_0 ;
  wire [3:0]\slv_reg3_reg[14]_0 ;
  wire [3:0]\slv_reg3_reg[14]_1 ;
  wire [1:0]\slv_reg3_reg[15]_0 ;
  wire [3:0]\slv_reg3_reg[15]_1 ;
  wire [3:0]\slv_reg3_reg[3]_0 ;
  wire [3:0]\slv_reg3_reg[6]_0 ;
  wire [3:0]\slv_reg3_reg[7]_0 ;
  wire \slv_reg3_reg_n_0_[0] ;
  wire \slv_reg3_reg_n_0_[10] ;
  wire \slv_reg3_reg_n_0_[11] ;
  wire \slv_reg3_reg_n_0_[12] ;
  wire \slv_reg3_reg_n_0_[13] ;
  wire \slv_reg3_reg_n_0_[14] ;
  wire \slv_reg3_reg_n_0_[15] ;
  wire \slv_reg3_reg_n_0_[17] ;
  wire \slv_reg3_reg_n_0_[18] ;
  wire \slv_reg3_reg_n_0_[19] ;
  wire \slv_reg3_reg_n_0_[1] ;
  wire \slv_reg3_reg_n_0_[20] ;
  wire \slv_reg3_reg_n_0_[21] ;
  wire \slv_reg3_reg_n_0_[22] ;
  wire \slv_reg3_reg_n_0_[23] ;
  wire \slv_reg3_reg_n_0_[24] ;
  wire \slv_reg3_reg_n_0_[25] ;
  wire \slv_reg3_reg_n_0_[26] ;
  wire \slv_reg3_reg_n_0_[27] ;
  wire \slv_reg3_reg_n_0_[28] ;
  wire \slv_reg3_reg_n_0_[29] ;
  wire \slv_reg3_reg_n_0_[2] ;
  wire \slv_reg3_reg_n_0_[30] ;
  wire \slv_reg3_reg_n_0_[31] ;
  wire \slv_reg3_reg_n_0_[3] ;
  wire \slv_reg3_reg_n_0_[4] ;
  wire \slv_reg3_reg_n_0_[5] ;
  wire \slv_reg3_reg_n_0_[6] ;
  wire \slv_reg3_reg_n_0_[7] ;
  wire \slv_reg3_reg_n_0_[8] ;
  wire \slv_reg3_reg_n_0_[9] ;
  wire slv_reg_rden__0;
  wire slv_reg_wren__0;
  wire [2:2]NLW_cnt_DCO1_carry__0_i_3_CO_UNCONNECTED;
  wire [3:3]NLW_cnt_DCO1_carry__0_i_3_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_DCO_reg[12]_i_1_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'hF7FFC4CCC4CCC4CC)) 
    aw_en_i_1
       (.I0(s00_axi_awvalid),
        .I1(aw_en_reg_n_0),
        .I2(axi_awready_reg_0),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[2]_i_1 
       (.I0(s00_axi_araddr[0]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(sel0[0]),
        .O(\axi_araddr[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[3]_i_1 
       (.I0(s00_axi_araddr[1]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(sel0[1]),
        .O(\axi_araddr[3]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFB08)) 
    \axi_araddr[4]_i_1 
       (.I0(s00_axi_araddr[2]),
        .I1(s00_axi_arvalid),
        .I2(axi_arready_reg_0),
        .I3(sel0[2]),
        .O(\axi_araddr[4]_i_1_n_0 ));
  FDRE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[2]_i_1_n_0 ),
        .Q(sel0[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[3]_i_1_n_0 ),
        .Q(sel0[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_araddr[4]_i_1_n_0 ),
        .Q(sel0[2]),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(axi_arready_reg_0),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(axi_arready_reg_0),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[2]_i_1 
       (.I0(s00_axi_awaddr[0]),
        .I1(s00_axi_wvalid),
        .I2(axi_awready_reg_0),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_awvalid),
        .I5(axi_awaddr[2]),
        .O(\axi_awaddr[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[3]_i_1 
       (.I0(s00_axi_awaddr[1]),
        .I1(s00_axi_wvalid),
        .I2(axi_awready_reg_0),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_awvalid),
        .I5(axi_awaddr[3]),
        .O(\axi_awaddr[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFBFFFFFF08000000)) 
    \axi_awaddr[4]_i_1 
       (.I0(s00_axi_awaddr[2]),
        .I1(s00_axi_wvalid),
        .I2(axi_awready_reg_0),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_awvalid),
        .I5(axi_awaddr[4]),
        .O(\axi_awaddr[4]_i_1_n_0 ));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[2]_i_1_n_0 ),
        .Q(axi_awaddr[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[3]_i_1_n_0 ),
        .Q(axi_awaddr[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\axi_awaddr[4]_i_1_n_0 ),
        .Q(axi_awaddr[4]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h2000)) 
    axi_awready_i_2
       (.I0(s00_axi_wvalid),
        .I1(axi_awready_reg_0),
        .I2(aw_en_reg_n_0),
        .I3(s00_axi_awvalid),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(axi_awready_reg_0),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(axi_awready_reg_0),
        .I3(axi_wready_reg_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[0]_i_2 
       (.I0(\slv_reg3_reg_n_0_[0] ),
        .I1(slv_reg2[0]),
        .I2(sel0[1]),
        .I3(slv_reg1[0]),
        .I4(sel0[0]),
        .I5(slv_reg0[0]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[10]_i_1 
       (.I0(\axi_rdata[10]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[10]_i_2 
       (.I0(\slv_reg3_reg_n_0_[10] ),
        .I1(slv_reg2[10]),
        .I2(sel0[1]),
        .I3(slv_reg1[10]),
        .I4(sel0[0]),
        .I5(slv_reg0[10]),
        .O(\axi_rdata[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[11]_i_1 
       (.I0(\axi_rdata[11]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[11]_i_2 
       (.I0(\slv_reg3_reg_n_0_[11] ),
        .I1(slv_reg2[11]),
        .I2(sel0[1]),
        .I3(slv_reg1[11]),
        .I4(sel0[0]),
        .I5(slv_reg0[11]),
        .O(\axi_rdata[11]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[12]_i_1 
       (.I0(\axi_rdata[12]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[12]_i_2 
       (.I0(\slv_reg3_reg_n_0_[12] ),
        .I1(slv_reg2[12]),
        .I2(sel0[1]),
        .I3(slv_reg1[12]),
        .I4(sel0[0]),
        .I5(slv_reg0[12]),
        .O(\axi_rdata[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[13]_i_1 
       (.I0(\axi_rdata[13]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[13]_i_2 
       (.I0(\slv_reg3_reg_n_0_[13] ),
        .I1(slv_reg2[13]),
        .I2(sel0[1]),
        .I3(slv_reg1[13]),
        .I4(sel0[0]),
        .I5(slv_reg0[13]),
        .O(\axi_rdata[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[14]_i_1 
       (.I0(\axi_rdata[14]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[14]_i_2 
       (.I0(\slv_reg3_reg_n_0_[14] ),
        .I1(slv_reg2[14]),
        .I2(sel0[1]),
        .I3(slv_reg1[14]),
        .I4(sel0[0]),
        .I5(slv_reg0[14]),
        .O(\axi_rdata[14]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[15]_i_1 
       (.I0(\axi_rdata[15]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[15]_i_2 
       (.I0(\slv_reg3_reg_n_0_[15] ),
        .I1(slv_reg2[15]),
        .I2(sel0[1]),
        .I3(slv_reg1[15]),
        .I4(sel0[0]),
        .I5(slv_reg0[15]),
        .O(\axi_rdata[15]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[16]_i_2 
       (.I0(Q),
        .I1(slv_reg2[16]),
        .I2(sel0[1]),
        .I3(slv_reg1[16]),
        .I4(sel0[0]),
        .I5(slv_reg0[16]),
        .O(\axi_rdata[16]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[17]_i_2 
       (.I0(\slv_reg3_reg_n_0_[17] ),
        .I1(slv_reg2[17]),
        .I2(sel0[1]),
        .I3(slv_reg1[17]),
        .I4(sel0[0]),
        .I5(slv_reg0[17]),
        .O(\axi_rdata[17]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[18]_i_2 
       (.I0(\slv_reg3_reg_n_0_[18] ),
        .I1(slv_reg2[18]),
        .I2(sel0[1]),
        .I3(slv_reg1[18]),
        .I4(sel0[0]),
        .I5(slv_reg0[18]),
        .O(\axi_rdata[18]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[19]_i_2 
       (.I0(\slv_reg3_reg_n_0_[19] ),
        .I1(slv_reg2[19]),
        .I2(sel0[1]),
        .I3(slv_reg1[19]),
        .I4(sel0[0]),
        .I5(slv_reg0[19]),
        .O(\axi_rdata[19]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[1]_i_2 
       (.I0(\slv_reg3_reg_n_0_[1] ),
        .I1(ADC_PDwN),
        .I2(sel0[1]),
        .I3(slv_reg1[1]),
        .I4(sel0[0]),
        .I5(slv_reg0[1]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[20]_i_2 
       (.I0(\slv_reg3_reg_n_0_[20] ),
        .I1(slv_reg2[20]),
        .I2(sel0[1]),
        .I3(slv_reg1[20]),
        .I4(sel0[0]),
        .I5(slv_reg0[20]),
        .O(\axi_rdata[20]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[21]_i_2 
       (.I0(\slv_reg3_reg_n_0_[21] ),
        .I1(slv_reg2[21]),
        .I2(sel0[1]),
        .I3(slv_reg1[21]),
        .I4(sel0[0]),
        .I5(slv_reg0[21]),
        .O(\axi_rdata[21]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[22]_i_2 
       (.I0(\slv_reg3_reg_n_0_[22] ),
        .I1(slv_reg2[22]),
        .I2(sel0[1]),
        .I3(slv_reg1[22]),
        .I4(sel0[0]),
        .I5(slv_reg0[22]),
        .O(\axi_rdata[22]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[23]_i_2 
       (.I0(\slv_reg3_reg_n_0_[23] ),
        .I1(slv_reg2[23]),
        .I2(sel0[1]),
        .I3(slv_reg1[23]),
        .I4(sel0[0]),
        .I5(slv_reg0[23]),
        .O(\axi_rdata[23]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[24]_i_2 
       (.I0(\slv_reg3_reg_n_0_[24] ),
        .I1(slv_reg2[24]),
        .I2(sel0[1]),
        .I3(slv_reg1[24]),
        .I4(sel0[0]),
        .I5(slv_reg0[24]),
        .O(\axi_rdata[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[25]_i_2 
       (.I0(\slv_reg3_reg_n_0_[25] ),
        .I1(slv_reg2[25]),
        .I2(sel0[1]),
        .I3(slv_reg1[25]),
        .I4(sel0[0]),
        .I5(slv_reg0[25]),
        .O(\axi_rdata[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[26]_i_2 
       (.I0(\slv_reg3_reg_n_0_[26] ),
        .I1(slv_reg2[26]),
        .I2(sel0[1]),
        .I3(slv_reg1[26]),
        .I4(sel0[0]),
        .I5(slv_reg0[26]),
        .O(\axi_rdata[26]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[27]_i_2 
       (.I0(\slv_reg3_reg_n_0_[27] ),
        .I1(slv_reg2[27]),
        .I2(sel0[1]),
        .I3(slv_reg1[27]),
        .I4(sel0[0]),
        .I5(slv_reg0[27]),
        .O(\axi_rdata[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[28]_i_2 
       (.I0(\slv_reg3_reg_n_0_[28] ),
        .I1(slv_reg2[28]),
        .I2(sel0[1]),
        .I3(slv_reg1[28]),
        .I4(sel0[0]),
        .I5(slv_reg0[28]),
        .O(\axi_rdata[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[29]_i_2 
       (.I0(\slv_reg3_reg_n_0_[29] ),
        .I1(slv_reg2[29]),
        .I2(sel0[1]),
        .I3(slv_reg1[29]),
        .I4(sel0[0]),
        .I5(slv_reg0[29]),
        .O(\axi_rdata[29]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[2]_i_2 
       (.I0(\slv_reg3_reg_n_0_[2] ),
        .I1(slv_reg2[2]),
        .I2(sel0[1]),
        .I3(slv_reg1[2]),
        .I4(sel0[0]),
        .I5(slv_reg0[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[30]_i_2 
       (.I0(\slv_reg3_reg_n_0_[30] ),
        .I1(slv_reg2[30]),
        .I2(sel0[1]),
        .I3(slv_reg1[30]),
        .I4(sel0[0]),
        .I5(slv_reg0[30]),
        .O(\axi_rdata[30]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[31]_i_1 
       (.I0(\axi_rdata[31]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[31]_i_2 
       (.I0(\slv_reg3_reg_n_0_[31] ),
        .I1(slv_reg2[31]),
        .I2(sel0[1]),
        .I3(slv_reg1[31]),
        .I4(sel0[0]),
        .I5(slv_reg0[31]),
        .O(\axi_rdata[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[3]_i_2 
       (.I0(\slv_reg3_reg_n_0_[3] ),
        .I1(slv_reg2[3]),
        .I2(sel0[1]),
        .I3(slv_reg1[3]),
        .I4(sel0[0]),
        .I5(slv_reg0[3]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[4]_i_2 
       (.I0(\slv_reg3_reg_n_0_[4] ),
        .I1(slv_reg2[4]),
        .I2(sel0[1]),
        .I3(slv_reg1[4]),
        .I4(sel0[0]),
        .I5(slv_reg0[4]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[5]_i_2 
       (.I0(\slv_reg3_reg_n_0_[5] ),
        .I1(slv_reg2[5]),
        .I2(sel0[1]),
        .I3(slv_reg1[5]),
        .I4(sel0[0]),
        .I5(slv_reg0[5]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[6]_i_2 
       (.I0(\slv_reg3_reg_n_0_[6] ),
        .I1(slv_reg2[6]),
        .I2(sel0[1]),
        .I3(slv_reg1[6]),
        .I4(sel0[0]),
        .I5(slv_reg0[6]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[7]_i_1 
       (.I0(\axi_rdata[7]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[7]_i_2 
       (.I0(\slv_reg3_reg_n_0_[7] ),
        .I1(slv_reg2[7]),
        .I2(sel0[1]),
        .I3(slv_reg1[7]),
        .I4(sel0[0]),
        .I5(slv_reg0[7]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[8]_i_2 
       (.I0(\slv_reg3_reg_n_0_[8] ),
        .I1(slv_reg2[8]),
        .I2(sel0[1]),
        .I3(slv_reg1[8]),
        .I4(sel0[0]),
        .I5(slv_reg0[8]),
        .O(\axi_rdata[8]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \axi_rdata[9]_i_1 
       (.I0(\axi_rdata[9]_i_2_n_0 ),
        .I1(sel0[2]),
        .O(\axi_rdata[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \axi_rdata[9]_i_2 
       (.I0(\slv_reg3_reg_n_0_[9] ),
        .I1(slv_reg2[9]),
        .I2(sel0[1]),
        .I3(slv_reg1[9]),
        .I4(sel0[0]),
        .I5(slv_reg0[9]),
        .O(\axi_rdata[9]_i_2_n_0 ));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[0]_i_1_n_0 ),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[10]_i_1_n_0 ),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[11]_i_1_n_0 ),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[12]_i_1_n_0 ),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[13]_i_1_n_0 ),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[14]_i_1_n_0 ),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[15]_i_1_n_0 ),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[16]_i_1_n_0 ),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[17]_i_1_n_0 ),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[18]_i_1_n_0 ),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[19]_i_1_n_0 ),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[1]_i_1_n_0 ),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[20]_i_1_n_0 ),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[21]_i_1_n_0 ),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[22]_i_1_n_0 ),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[23]_i_1_n_0 ),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[24]_i_1_n_0 ),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[25]_i_1_n_0 ),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[26]_i_1_n_0 ),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[27]_i_1_n_0 ),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[28]_i_1_n_0 ),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[29]_i_1_n_0 ),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[2]_i_1_n_0 ),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[30]_i_1_n_0 ),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[31]_i_1_n_0 ),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[3]_i_1_n_0 ),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[4]_i_1_n_0 ),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[5]_i_1_n_0 ),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[6]_i_1_n_0 ),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[7]_i_1_n_0 ),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[8]_i_1_n_0 ),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden__0),
        .D(\axi_rdata[9]_i_1_n_0 ),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(axi_arready_reg_0),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0800)) 
    axi_wready_i_1
       (.I0(s00_axi_awvalid),
        .I1(s00_axi_wvalid),
        .I2(axi_wready_reg_0),
        .I3(aw_en_reg_n_0),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(axi_wready_reg_0),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h09)) 
    cnt_DCO1_carry__0_i_1
       (.I0(cnt_DCO_reg[15]),
        .I1(cnt_DCO2[15]),
        .I2(cnt_DCO2[16]),
        .O(\cnt_DCO_reg[15] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_DCO1_carry__0_i_2
       (.I0(cnt_DCO_reg[12]),
        .I1(cnt_DCO2[12]),
        .I2(cnt_DCO2[14]),
        .I3(cnt_DCO_reg[14]),
        .I4(cnt_DCO2[13]),
        .I5(cnt_DCO_reg[13]),
        .O(\cnt_DCO_reg[15] [0]));
  CARRY4 cnt_DCO1_carry__0_i_3
       (.CI(cnt_DCO1_carry_i_5_n_0),
        .CO({cnt_DCO2[16],NLW_cnt_DCO1_carry__0_i_3_CO_UNCONNECTED[2],cnt_DCO1_carry__0_i_3_n_2,cnt_DCO1_carry__0_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_cnt_DCO1_carry__0_i_3_O_UNCONNECTED[3],cnt_DCO2[15:13]}),
        .S({1'b1,\slv_reg3_reg_n_0_[15] ,\slv_reg3_reg_n_0_[14] ,\slv_reg3_reg_n_0_[13] }));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_DCO1_carry_i_1
       (.I0(cnt_DCO_reg[9]),
        .I1(cnt_DCO2[9]),
        .I2(cnt_DCO2[11]),
        .I3(cnt_DCO_reg[11]),
        .I4(cnt_DCO2[10]),
        .I5(cnt_DCO_reg[10]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_DCO1_carry_i_2
       (.I0(cnt_DCO_reg[6]),
        .I1(cnt_DCO2[6]),
        .I2(cnt_DCO2[8]),
        .I3(cnt_DCO_reg[8]),
        .I4(cnt_DCO2[7]),
        .I5(cnt_DCO_reg[7]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    cnt_DCO1_carry_i_3
       (.I0(cnt_DCO_reg[3]),
        .I1(cnt_DCO2[3]),
        .I2(cnt_DCO2[5]),
        .I3(cnt_DCO_reg[5]),
        .I4(cnt_DCO2[4]),
        .I5(cnt_DCO_reg[4]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h0000900990090000)) 
    cnt_DCO1_carry_i_4
       (.I0(cnt_DCO2[2]),
        .I1(cnt_DCO_reg[2]),
        .I2(cnt_DCO2[1]),
        .I3(cnt_DCO_reg[1]),
        .I4(\slv_reg3_reg_n_0_[0] ),
        .I5(cnt_DCO_reg[0]),
        .O(S[0]));
  CARRY4 cnt_DCO1_carry_i_5
       (.CI(cnt_DCO1_carry_i_6_n_0),
        .CO({cnt_DCO1_carry_i_5_n_0,cnt_DCO1_carry_i_5_n_1,cnt_DCO1_carry_i_5_n_2,cnt_DCO1_carry_i_5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt_DCO2[12:9]),
        .S({\slv_reg3_reg_n_0_[12] ,\slv_reg3_reg_n_0_[11] ,\slv_reg3_reg_n_0_[10] ,\slv_reg3_reg_n_0_[9] }));
  CARRY4 cnt_DCO1_carry_i_6
       (.CI(cnt_DCO1_carry_i_7_n_0),
        .CO({cnt_DCO1_carry_i_6_n_0,cnt_DCO1_carry_i_6_n_1,cnt_DCO1_carry_i_6_n_2,cnt_DCO1_carry_i_6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt_DCO2[8:5]),
        .S({\slv_reg3_reg_n_0_[8] ,\slv_reg3_reg_n_0_[7] ,\slv_reg3_reg_n_0_[6] ,\slv_reg3_reg_n_0_[5] }));
  CARRY4 cnt_DCO1_carry_i_7
       (.CI(1'b0),
        .CO({cnt_DCO1_carry_i_7_n_0,cnt_DCO1_carry_i_7_n_1,cnt_DCO1_carry_i_7_n_2,cnt_DCO1_carry_i_7_n_3}),
        .CYINIT(\slv_reg3_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(cnt_DCO2[4:1]),
        .S({\slv_reg3_reg_n_0_[4] ,\slv_reg3_reg_n_0_[3] ,\slv_reg3_reg_n_0_[2] ,\slv_reg3_reg_n_0_[1] }));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[0]_i_3 
       (.I0(\slv_reg3_reg_n_0_[3] ),
        .I1(O),
        .I2(cnt_DCO_reg[3]),
        .O(\cnt_DCO[0]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[0]_i_4 
       (.I0(\slv_reg3_reg_n_0_[2] ),
        .I1(O),
        .I2(cnt_DCO_reg[2]),
        .O(\cnt_DCO[0]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[0]_i_5 
       (.I0(\slv_reg3_reg_n_0_[1] ),
        .I1(O),
        .I2(cnt_DCO_reg[1]),
        .O(\cnt_DCO[0]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h1D)) 
    \cnt_DCO[0]_i_6 
       (.I0(cnt_DCO_reg[0]),
        .I1(O),
        .I2(\slv_reg3_reg_n_0_[0] ),
        .O(\cnt_DCO[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[12]_i_2 
       (.I0(\slv_reg3_reg_n_0_[15] ),
        .I1(O),
        .I2(cnt_DCO_reg[15]),
        .O(\cnt_DCO[12]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[12]_i_3 
       (.I0(\slv_reg3_reg_n_0_[14] ),
        .I1(O),
        .I2(cnt_DCO_reg[14]),
        .O(\cnt_DCO[12]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[12]_i_4 
       (.I0(\slv_reg3_reg_n_0_[13] ),
        .I1(O),
        .I2(cnt_DCO_reg[13]),
        .O(\cnt_DCO[12]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[12]_i_5 
       (.I0(\slv_reg3_reg_n_0_[12] ),
        .I1(O),
        .I2(cnt_DCO_reg[12]),
        .O(\cnt_DCO[12]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[4]_i_2 
       (.I0(\slv_reg3_reg_n_0_[7] ),
        .I1(O),
        .I2(cnt_DCO_reg[7]),
        .O(\cnt_DCO[4]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[4]_i_3 
       (.I0(\slv_reg3_reg_n_0_[6] ),
        .I1(O),
        .I2(cnt_DCO_reg[6]),
        .O(\cnt_DCO[4]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[4]_i_4 
       (.I0(\slv_reg3_reg_n_0_[5] ),
        .I1(O),
        .I2(cnt_DCO_reg[5]),
        .O(\cnt_DCO[4]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[4]_i_5 
       (.I0(\slv_reg3_reg_n_0_[4] ),
        .I1(O),
        .I2(cnt_DCO_reg[4]),
        .O(\cnt_DCO[4]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[8]_i_2 
       (.I0(\slv_reg3_reg_n_0_[11] ),
        .I1(O),
        .I2(cnt_DCO_reg[11]),
        .O(\cnt_DCO[8]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[8]_i_3 
       (.I0(\slv_reg3_reg_n_0_[10] ),
        .I1(O),
        .I2(cnt_DCO_reg[10]),
        .O(\cnt_DCO[8]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[8]_i_4 
       (.I0(\slv_reg3_reg_n_0_[9] ),
        .I1(O),
        .I2(cnt_DCO_reg[9]),
        .O(\cnt_DCO[8]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \cnt_DCO[8]_i_5 
       (.I0(\slv_reg3_reg_n_0_[8] ),
        .I1(O),
        .I2(cnt_DCO_reg[8]),
        .O(\cnt_DCO[8]_i_5_n_0 ));
  CARRY4 \cnt_DCO_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_DCO_reg[0]_i_2_n_0 ,\cnt_DCO_reg[0]_i_2_n_1 ,\cnt_DCO_reg[0]_i_2_n_2 ,\cnt_DCO_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O(\slv_reg3_reg[3]_0 ),
        .S({\cnt_DCO[0]_i_3_n_0 ,\cnt_DCO[0]_i_4_n_0 ,\cnt_DCO[0]_i_5_n_0 ,\cnt_DCO[0]_i_6_n_0 }));
  CARRY4 \cnt_DCO_reg[12]_i_1 
       (.CI(\cnt_DCO_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_DCO_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_DCO_reg[12]_i_1_n_1 ,\cnt_DCO_reg[12]_i_1_n_2 ,\cnt_DCO_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\slv_reg3_reg[15]_1 ),
        .S({\cnt_DCO[12]_i_2_n_0 ,\cnt_DCO[12]_i_3_n_0 ,\cnt_DCO[12]_i_4_n_0 ,\cnt_DCO[12]_i_5_n_0 }));
  CARRY4 \cnt_DCO_reg[4]_i_1 
       (.CI(\cnt_DCO_reg[0]_i_2_n_0 ),
        .CO({\cnt_DCO_reg[4]_i_1_n_0 ,\cnt_DCO_reg[4]_i_1_n_1 ,\cnt_DCO_reg[4]_i_1_n_2 ,\cnt_DCO_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\slv_reg3_reg[7]_0 ),
        .S({\cnt_DCO[4]_i_2_n_0 ,\cnt_DCO[4]_i_3_n_0 ,\cnt_DCO[4]_i_4_n_0 ,\cnt_DCO[4]_i_5_n_0 }));
  CARRY4 \cnt_DCO_reg[8]_i_1 
       (.CI(\cnt_DCO_reg[4]_i_1_n_0 ),
        .CO({\cnt_DCO_reg[8]_i_1_n_0 ,\cnt_DCO_reg[8]_i_1_n_1 ,\cnt_DCO_reg[8]_i_1_n_2 ,\cnt_DCO_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\slv_reg3_reg[11]_0 ),
        .S({\cnt_DCO[8]_i_2_n_0 ,\cnt_DCO[8]_i_3_n_0 ,\cnt_DCO[8]_i_4_n_0 ,\cnt_DCO[8]_i_5_n_0 }));
  LUT2 #(
    .INIT(4'h9)) 
    m00_fft_axis_tlast_r1_carry__0_i_1
       (.I0(\slv_reg3_reg_n_0_[15] ),
        .I1(cnt_DCO_reg[15]),
        .O(\slv_reg3_reg[15]_0 [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_fft_axis_tlast_r1_carry__0_i_2
       (.I0(cnt_DCO_reg[12]),
        .I1(\slv_reg3_reg_n_0_[12] ),
        .I2(\slv_reg3_reg_n_0_[14] ),
        .I3(cnt_DCO_reg[14]),
        .I4(\slv_reg3_reg_n_0_[13] ),
        .I5(cnt_DCO_reg[13]),
        .O(\slv_reg3_reg[15]_0 [0]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_fft_axis_tlast_r1_carry_i_1
       (.I0(cnt_DCO_reg[9]),
        .I1(\slv_reg3_reg_n_0_[9] ),
        .I2(\slv_reg3_reg_n_0_[11] ),
        .I3(cnt_DCO_reg[11]),
        .I4(\slv_reg3_reg_n_0_[10] ),
        .I5(cnt_DCO_reg[10]),
        .O(\cnt_DCO_reg[9] [3]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_fft_axis_tlast_r1_carry_i_2
       (.I0(cnt_DCO_reg[6]),
        .I1(\slv_reg3_reg_n_0_[6] ),
        .I2(\slv_reg3_reg_n_0_[8] ),
        .I3(cnt_DCO_reg[8]),
        .I4(\slv_reg3_reg_n_0_[7] ),
        .I5(cnt_DCO_reg[7]),
        .O(\cnt_DCO_reg[9] [2]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_fft_axis_tlast_r1_carry_i_3
       (.I0(cnt_DCO_reg[3]),
        .I1(\slv_reg3_reg_n_0_[3] ),
        .I2(\slv_reg3_reg_n_0_[5] ),
        .I3(cnt_DCO_reg[5]),
        .I4(\slv_reg3_reg_n_0_[4] ),
        .I5(cnt_DCO_reg[4]),
        .O(\cnt_DCO_reg[9] [1]));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    m00_fft_axis_tlast_r1_carry_i_4
       (.I0(cnt_DCO_reg[0]),
        .I1(\slv_reg3_reg_n_0_[0] ),
        .I2(\slv_reg3_reg_n_0_[2] ),
        .I3(cnt_DCO_reg[2]),
        .I4(\slv_reg3_reg_n_0_[1] ),
        .I5(cnt_DCO_reg[1]),
        .O(\cnt_DCO_reg[9] [0]));
  LUT2 #(
    .INIT(4'h8)) 
    m00_fft_axis_tlast_r_i_1
       (.I0(cnt_in_DCO__14),
        .I1(CO),
        .O(m00_fft_axis_tlast_r0));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry__0_i_1
       (.I0(\slv_reg3_reg_n_0_[14] ),
        .I1(cnt_DCO_reg[14]),
        .I2(cnt_DCO_reg[15]),
        .I3(\slv_reg3_reg_n_0_[15] ),
        .O(\slv_reg3_reg[14]_1 [3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry__0_i_2
       (.I0(\slv_reg3_reg_n_0_[12] ),
        .I1(cnt_DCO_reg[12]),
        .I2(cnt_DCO_reg[13]),
        .I3(\slv_reg3_reg_n_0_[13] ),
        .O(\slv_reg3_reg[14]_1 [2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry__0_i_3
       (.I0(\slv_reg3_reg_n_0_[10] ),
        .I1(cnt_DCO_reg[10]),
        .I2(cnt_DCO_reg[11]),
        .I3(\slv_reg3_reg_n_0_[11] ),
        .O(\slv_reg3_reg[14]_1 [1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry__0_i_4
       (.I0(\slv_reg3_reg_n_0_[8] ),
        .I1(cnt_DCO_reg[8]),
        .I2(cnt_DCO_reg[9]),
        .I3(\slv_reg3_reg_n_0_[9] ),
        .O(\slv_reg3_reg[14]_1 [0]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry__0_i_5
       (.I0(\slv_reg3_reg_n_0_[14] ),
        .I1(cnt_DCO_reg[14]),
        .I2(\slv_reg3_reg_n_0_[15] ),
        .I3(cnt_DCO_reg[15]),
        .O(\slv_reg3_reg[14]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry__0_i_6
       (.I0(\slv_reg3_reg_n_0_[12] ),
        .I1(cnt_DCO_reg[12]),
        .I2(\slv_reg3_reg_n_0_[13] ),
        .I3(cnt_DCO_reg[13]),
        .O(\slv_reg3_reg[14]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry__0_i_7
       (.I0(\slv_reg3_reg_n_0_[10] ),
        .I1(cnt_DCO_reg[10]),
        .I2(\slv_reg3_reg_n_0_[11] ),
        .I3(cnt_DCO_reg[11]),
        .O(\slv_reg3_reg[14]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry__0_i_8
       (.I0(\slv_reg3_reg_n_0_[8] ),
        .I1(cnt_DCO_reg[8]),
        .I2(\slv_reg3_reg_n_0_[9] ),
        .I3(cnt_DCO_reg[9]),
        .O(\slv_reg3_reg[14]_0 [0]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry_i_1
       (.I0(\slv_reg3_reg_n_0_[6] ),
        .I1(cnt_DCO_reg[6]),
        .I2(cnt_DCO_reg[7]),
        .I3(\slv_reg3_reg_n_0_[7] ),
        .O(DI[3]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry_i_2
       (.I0(\slv_reg3_reg_n_0_[4] ),
        .I1(cnt_DCO_reg[4]),
        .I2(cnt_DCO_reg[5]),
        .I3(\slv_reg3_reg_n_0_[5] ),
        .O(DI[2]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry_i_3
       (.I0(\slv_reg3_reg_n_0_[2] ),
        .I1(cnt_DCO_reg[2]),
        .I2(cnt_DCO_reg[3]),
        .I3(\slv_reg3_reg_n_0_[3] ),
        .O(DI[1]));
  LUT4 #(
    .INIT(16'h2F02)) 
    m00_fft_axis_tvalid_r1_carry_i_4
       (.I0(\slv_reg3_reg_n_0_[0] ),
        .I1(cnt_DCO_reg[0]),
        .I2(cnt_DCO_reg[1]),
        .I3(\slv_reg3_reg_n_0_[1] ),
        .O(DI[0]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry_i_5
       (.I0(\slv_reg3_reg_n_0_[6] ),
        .I1(cnt_DCO_reg[6]),
        .I2(\slv_reg3_reg_n_0_[7] ),
        .I3(cnt_DCO_reg[7]),
        .O(\slv_reg3_reg[6]_0 [3]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry_i_6
       (.I0(\slv_reg3_reg_n_0_[4] ),
        .I1(cnt_DCO_reg[4]),
        .I2(\slv_reg3_reg_n_0_[5] ),
        .I3(cnt_DCO_reg[5]),
        .O(\slv_reg3_reg[6]_0 [2]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry_i_7
       (.I0(\slv_reg3_reg_n_0_[2] ),
        .I1(cnt_DCO_reg[2]),
        .I2(\slv_reg3_reg_n_0_[3] ),
        .I3(cnt_DCO_reg[3]),
        .O(\slv_reg3_reg[6]_0 [1]));
  LUT4 #(
    .INIT(16'h9009)) 
    m00_fft_axis_tvalid_r1_carry_i_8
       (.I0(\slv_reg3_reg_n_0_[0] ),
        .I1(cnt_DCO_reg[0]),
        .I2(\slv_reg3_reg_n_0_[1] ),
        .I3(cnt_DCO_reg[1]),
        .O(\slv_reg3_reg[6]_0 [0]));
  LUT6 #(
    .INIT(64'hA800FFFFA8000000)) 
    m00_fft_axis_tvalid_r_i_1
       (.I0(cnt_in_DCO__14),
        .I1(m00_fft_axis_tvalid_r_reg),
        .I2(m00_fft_axis_tvalid_r_reg_0),
        .I3(m00_fft_axis_tvalid_r_reg_1),
        .I4(mux_fft),
        .I5(m00_fft_axis_tvalid),
        .O(mux_fft_0));
  LUT6 #(
    .INIT(64'hB8B8B88888888888)) 
    m01_fft_axis_tvalid_r_i_1
       (.I0(m01_fft_axis_tvalid),
        .I1(mux_fft),
        .I2(cnt_in_DCO__14),
        .I3(m00_fft_axis_tvalid_r_reg),
        .I4(m00_fft_axis_tvalid_r_reg_0),
        .I5(m00_fft_axis_tvalid_r_reg_1),
        .O(m01_fft_axis_tvalid_r_reg));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[1]),
        .O(p_1_in[15]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[3]),
        .I2(axi_awaddr[2]),
        .I3(axi_awaddr[4]),
        .I4(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg0[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[10] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg0[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[11] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg0[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[12] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg0[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[13] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg0[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[14] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg0[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[15] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg0[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg0[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg0[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg0[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg0[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg0[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg0[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg0[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg0[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg0[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg0[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg0[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg0[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg0[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg0[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg0[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg0[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg0[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg0[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg0[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg0[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg0[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg0[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg0[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[8] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg0[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[9] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[15]),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg0[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[2]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg1[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg1[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg1[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg1[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg1[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg1[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg1[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg1[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg1[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg1[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg1[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg1[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg1[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg1[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg1[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[1]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[1]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[0]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg2[1]_i_2 
       (.I0(axi_wready_reg_0),
        .I1(axi_awready_reg_0),
        .I2(s00_axi_awvalid),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[2]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[2]),
        .I3(s00_axi_wstrb[3]),
        .I4(axi_awaddr[3]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg2[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg2[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg2[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg2[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg2[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg2[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg2[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg2[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg2[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg2[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(ADC_PDwN),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg2[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg2[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg2[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg2[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg2[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg2[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg2[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg2[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg2[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg2[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg2[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg2[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[1]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg2[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg2[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[15]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg3[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[23]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg3[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[31]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg3[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h20000000)) 
    \slv_reg3[7]_i_1 
       (.I0(slv_reg_wren__0),
        .I1(axi_awaddr[4]),
        .I2(axi_awaddr[3]),
        .I3(axi_awaddr[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg3[7]_i_1_n_0 ));
  FDRE \slv_reg3_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg3_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg3_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg3_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg3_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg3_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg3_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg3_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(Q),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg3_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg3_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg3_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(\slv_reg3_reg_n_0_[1] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg3_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg3_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg3_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg3_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg3_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg3_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg3_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg3_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg3_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg3_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(\slv_reg3_reg_n_0_[2] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg3_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg3_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(\slv_reg3_reg_n_0_[3] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(\slv_reg3_reg_n_0_[4] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(\slv_reg3_reg_n_0_[5] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(\slv_reg3_reg_n_0_[6] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(\slv_reg3_reg_n_0_[7] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg3_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg3_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg3[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg3_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT3 #(
    .INIT(8'h20)) 
    slv_reg_rden
       (.I0(s00_axi_arvalid),
        .I1(s00_axi_rvalid),
        .I2(axi_arready_reg_0),
        .O(slv_reg_rden__0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0
   (s00_axi_wready,
    s00_axi_awready,
    s00_axi_arready,
    ADC_PDwN,
    s00_axi_rdata,
    m00_fft_axis_tlast,
    SYNC,
    s00_axi_rvalid,
    m01_fft_axis_tdata,
    s00_axi_bvalid,
    m00_fft_axis_tvalid,
    m01_fft_axis_tvalid,
    s00_axi_aclk,
    s00_axi_awaddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_arvalid,
    clk_10MHz,
    s00_axi_wstrb,
    dco_or_dcoa,
    s00_axi_aresetn,
    allowed_clk,
    DATA_INA,
    mux_fft,
    DATA_INB,
    s00_axi_bready,
    s00_axi_rready);
  output s00_axi_wready;
  output s00_axi_awready;
  output s00_axi_arready;
  output ADC_PDwN;
  output [31:0]s00_axi_rdata;
  output m00_fft_axis_tlast;
  output SYNC;
  output s00_axi_rvalid;
  output [31:0]m01_fft_axis_tdata;
  output s00_axi_bvalid;
  output m00_fft_axis_tvalid;
  output m01_fft_axis_tvalid;
  input s00_axi_aclk;
  input [2:0]s00_axi_awaddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_arvalid;
  input clk_10MHz;
  input [3:0]s00_axi_wstrb;
  input dco_or_dcoa;
  input s00_axi_aresetn;
  input allowed_clk;
  input [15:0]DATA_INA;
  input mux_fft;
  input [15:0]DATA_INB;
  input s00_axi_bready;
  input s00_axi_rready;

  wire AD9650_v1_0_S00_AXI_inst_n_10;
  wire AD9650_v1_0_S00_AXI_inst_n_11;
  wire AD9650_v1_0_S00_AXI_inst_n_12;
  wire AD9650_v1_0_S00_AXI_inst_n_14;
  wire AD9650_v1_0_S00_AXI_inst_n_15;
  wire AD9650_v1_0_S00_AXI_inst_n_16;
  wire AD9650_v1_0_S00_AXI_inst_n_17;
  wire AD9650_v1_0_S00_AXI_inst_n_18;
  wire AD9650_v1_0_S00_AXI_inst_n_19;
  wire AD9650_v1_0_S00_AXI_inst_n_20;
  wire AD9650_v1_0_S00_AXI_inst_n_21;
  wire AD9650_v1_0_S00_AXI_inst_n_22;
  wire AD9650_v1_0_S00_AXI_inst_n_23;
  wire AD9650_v1_0_S00_AXI_inst_n_24;
  wire AD9650_v1_0_S00_AXI_inst_n_25;
  wire AD9650_v1_0_S00_AXI_inst_n_26;
  wire AD9650_v1_0_S00_AXI_inst_n_27;
  wire AD9650_v1_0_S00_AXI_inst_n_28;
  wire AD9650_v1_0_S00_AXI_inst_n_29;
  wire AD9650_v1_0_S00_AXI_inst_n_30;
  wire AD9650_v1_0_S00_AXI_inst_n_31;
  wire AD9650_v1_0_S00_AXI_inst_n_32;
  wire AD9650_v1_0_S00_AXI_inst_n_33;
  wire AD9650_v1_0_S00_AXI_inst_n_36;
  wire AD9650_v1_0_S00_AXI_inst_n_37;
  wire AD9650_v1_0_S00_AXI_inst_n_38;
  wire AD9650_v1_0_S00_AXI_inst_n_39;
  wire AD9650_v1_0_S00_AXI_inst_n_40;
  wire AD9650_v1_0_S00_AXI_inst_n_41;
  wire AD9650_v1_0_S00_AXI_inst_n_42;
  wire AD9650_v1_0_S00_AXI_inst_n_43;
  wire AD9650_v1_0_S00_AXI_inst_n_44;
  wire AD9650_v1_0_S00_AXI_inst_n_45;
  wire AD9650_v1_0_S00_AXI_inst_n_46;
  wire AD9650_v1_0_S00_AXI_inst_n_47;
  wire AD9650_v1_0_S00_AXI_inst_n_48;
  wire AD9650_v1_0_S00_AXI_inst_n_49;
  wire AD9650_v1_0_S00_AXI_inst_n_5;
  wire AD9650_v1_0_S00_AXI_inst_n_50;
  wire AD9650_v1_0_S00_AXI_inst_n_51;
  wire AD9650_v1_0_S00_AXI_inst_n_52;
  wire AD9650_v1_0_S00_AXI_inst_n_53;
  wire AD9650_v1_0_S00_AXI_inst_n_6;
  wire AD9650_v1_0_S00_AXI_inst_n_7;
  wire AD9650_v1_0_S00_AXI_inst_n_8;
  wire AD9650_v1_0_S00_AXI_inst_n_9;
  wire ADC_PDwN;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire SYNC;
  wire SYNC_signal;
  wire allowed_clk;
  wire clear;
  wire clk_10MHz;
  wire cnt_DCO1;
  wire cnt_DCO1_carry__0_n_3;
  wire cnt_DCO1_carry_n_0;
  wire cnt_DCO1_carry_n_1;
  wire cnt_DCO1_carry_n_2;
  wire cnt_DCO1_carry_n_3;
  wire [15:0]cnt_DCO_reg;
  wire \cnt_in_DCO[0]_i_1_n_0 ;
  wire \cnt_in_DCO[0]_i_3_n_0 ;
  wire cnt_in_DCO__14;
  wire [15:0]cnt_in_DCO_reg;
  wire \cnt_in_DCO_reg[0]_i_2_n_0 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_1 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_2 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_3 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_4 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_5 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_6 ;
  wire \cnt_in_DCO_reg[0]_i_2_n_7 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[12]_i_1_n_7 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_0 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[4]_i_1_n_7 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_0 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_1 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_2 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_3 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_4 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_5 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_6 ;
  wire \cnt_in_DCO_reg[8]_i_1_n_7 ;
  wire dco_or_dcoa;
  wire load;
  wire m00_fft_axis_tlast;
  wire m00_fft_axis_tlast_r0;
  wire m00_fft_axis_tlast_r1;
  wire m00_fft_axis_tlast_r1_carry__0_n_3;
  wire m00_fft_axis_tlast_r1_carry_n_0;
  wire m00_fft_axis_tlast_r1_carry_n_1;
  wire m00_fft_axis_tlast_r1_carry_n_2;
  wire m00_fft_axis_tlast_r1_carry_n_3;
  wire m00_fft_axis_tvalid;
  wire m00_fft_axis_tvalid_r1;
  wire m00_fft_axis_tvalid_r1_carry__0_n_1;
  wire m00_fft_axis_tvalid_r1_carry__0_n_2;
  wire m00_fft_axis_tvalid_r1_carry__0_n_3;
  wire m00_fft_axis_tvalid_r1_carry_n_0;
  wire m00_fft_axis_tvalid_r1_carry_n_1;
  wire m00_fft_axis_tvalid_r1_carry_n_2;
  wire m00_fft_axis_tvalid_r1_carry_n_3;
  wire m00_fft_axis_tvalid_r_i_10_n_0;
  wire m00_fft_axis_tvalid_r_i_3_n_0;
  wire m00_fft_axis_tvalid_r_i_4_n_0;
  wire m00_fft_axis_tvalid_r_i_5_n_0;
  wire m00_fft_axis_tvalid_r_i_6_n_0;
  wire m00_fft_axis_tvalid_r_i_7_n_0;
  wire m00_fft_axis_tvalid_r_i_8_n_0;
  wire m00_fft_axis_tvalid_r_i_9_n_0;
  wire [31:0]m01_fft_axis_tdata;
  wire m01_fft_axis_tvalid;
  wire mux_fft;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire [3:0]NLW_cnt_DCO1_carry_O_UNCONNECTED;
  wire [3:2]NLW_cnt_DCO1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_cnt_DCO1_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_m00_fft_axis_tlast_r1_carry_O_UNCONNECTED;
  wire [3:2]NLW_m00_fft_axis_tlast_r1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_m00_fft_axis_tlast_r1_carry__0_O_UNCONNECTED;
  wire [3:0]NLW_m00_fft_axis_tvalid_r1_carry_O_UNCONNECTED;
  wire [3:0]NLW_m00_fft_axis_tvalid_r1_carry__0_O_UNCONNECTED;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v1_0_S00_AXI AD9650_v1_0_S00_AXI_inst
       (.ADC_PDwN(ADC_PDwN),
        .CO(m00_fft_axis_tlast_r1),
        .DI({AD9650_v1_0_S00_AXI_inst_n_18,AD9650_v1_0_S00_AXI_inst_n_19,AD9650_v1_0_S00_AXI_inst_n_20,AD9650_v1_0_S00_AXI_inst_n_21}),
        .O(load),
        .Q(SYNC_signal),
        .S({AD9650_v1_0_S00_AXI_inst_n_5,AD9650_v1_0_S00_AXI_inst_n_6,AD9650_v1_0_S00_AXI_inst_n_7,AD9650_v1_0_S00_AXI_inst_n_8}),
        .axi_arready_reg_0(s00_axi_arready),
        .axi_awready_reg_0(s00_axi_awready),
        .axi_wready_reg_0(s00_axi_wready),
        .cnt_DCO_reg(cnt_DCO_reg),
        .\cnt_DCO_reg[15] ({AD9650_v1_0_S00_AXI_inst_n_9,AD9650_v1_0_S00_AXI_inst_n_10}),
        .\cnt_DCO_reg[9] ({AD9650_v1_0_S00_AXI_inst_n_30,AD9650_v1_0_S00_AXI_inst_n_31,AD9650_v1_0_S00_AXI_inst_n_32,AD9650_v1_0_S00_AXI_inst_n_33}),
        .cnt_in_DCO__14(cnt_in_DCO__14),
        .m00_fft_axis_tlast_r0(m00_fft_axis_tlast_r0),
        .m00_fft_axis_tvalid(m00_fft_axis_tvalid),
        .m00_fft_axis_tvalid_r_reg(m00_fft_axis_tvalid_r_i_3_n_0),
        .m00_fft_axis_tvalid_r_reg_0(m00_fft_axis_tvalid_r_i_4_n_0),
        .m00_fft_axis_tvalid_r_reg_1(m00_fft_axis_tvalid_r1),
        .m01_fft_axis_tvalid(m01_fft_axis_tvalid),
        .m01_fft_axis_tvalid_r_reg(AD9650_v1_0_S00_AXI_inst_n_53),
        .mux_fft(mux_fft),
        .mux_fft_0(AD9650_v1_0_S00_AXI_inst_n_52),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .\slv_reg3_reg[11]_0 ({AD9650_v1_0_S00_AXI_inst_n_44,AD9650_v1_0_S00_AXI_inst_n_45,AD9650_v1_0_S00_AXI_inst_n_46,AD9650_v1_0_S00_AXI_inst_n_47}),
        .\slv_reg3_reg[14]_0 ({AD9650_v1_0_S00_AXI_inst_n_22,AD9650_v1_0_S00_AXI_inst_n_23,AD9650_v1_0_S00_AXI_inst_n_24,AD9650_v1_0_S00_AXI_inst_n_25}),
        .\slv_reg3_reg[14]_1 ({AD9650_v1_0_S00_AXI_inst_n_26,AD9650_v1_0_S00_AXI_inst_n_27,AD9650_v1_0_S00_AXI_inst_n_28,AD9650_v1_0_S00_AXI_inst_n_29}),
        .\slv_reg3_reg[15]_0 ({AD9650_v1_0_S00_AXI_inst_n_11,AD9650_v1_0_S00_AXI_inst_n_12}),
        .\slv_reg3_reg[15]_1 ({AD9650_v1_0_S00_AXI_inst_n_48,AD9650_v1_0_S00_AXI_inst_n_49,AD9650_v1_0_S00_AXI_inst_n_50,AD9650_v1_0_S00_AXI_inst_n_51}),
        .\slv_reg3_reg[3]_0 ({AD9650_v1_0_S00_AXI_inst_n_36,AD9650_v1_0_S00_AXI_inst_n_37,AD9650_v1_0_S00_AXI_inst_n_38,AD9650_v1_0_S00_AXI_inst_n_39}),
        .\slv_reg3_reg[6]_0 ({AD9650_v1_0_S00_AXI_inst_n_14,AD9650_v1_0_S00_AXI_inst_n_15,AD9650_v1_0_S00_AXI_inst_n_16,AD9650_v1_0_S00_AXI_inst_n_17}),
        .\slv_reg3_reg[7]_0 ({AD9650_v1_0_S00_AXI_inst_n_40,AD9650_v1_0_S00_AXI_inst_n_41,AD9650_v1_0_S00_AXI_inst_n_42,AD9650_v1_0_S00_AXI_inst_n_43}));
  FDRE SYNC_reg_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(SYNC_signal),
        .Q(SYNC),
        .R(1'b0));
  CARRY4 cnt_DCO1_carry
       (.CI(1'b0),
        .CO({cnt_DCO1_carry_n_0,cnt_DCO1_carry_n_1,cnt_DCO1_carry_n_2,cnt_DCO1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b1,1'b1,1'b1,1'b1}),
        .O(NLW_cnt_DCO1_carry_O_UNCONNECTED[3:0]),
        .S({AD9650_v1_0_S00_AXI_inst_n_5,AD9650_v1_0_S00_AXI_inst_n_6,AD9650_v1_0_S00_AXI_inst_n_7,AD9650_v1_0_S00_AXI_inst_n_8}));
  CARRY4 cnt_DCO1_carry__0
       (.CI(cnt_DCO1_carry_n_0),
        .CO({NLW_cnt_DCO1_carry__0_CO_UNCONNECTED[3:2],cnt_DCO1,cnt_DCO1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b1,1'b1}),
        .O({NLW_cnt_DCO1_carry__0_O_UNCONNECTED[3],load,NLW_cnt_DCO1_carry__0_O_UNCONNECTED[1:0]}),
        .S({1'b0,1'b1,AD9650_v1_0_S00_AXI_inst_n_9,AD9650_v1_0_S00_AXI_inst_n_10}));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_DCO[0]_i_1 
       (.I0(allowed_clk),
        .O(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[0] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_39),
        .Q(cnt_DCO_reg[0]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[10] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_45),
        .Q(cnt_DCO_reg[10]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[11] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_44),
        .Q(cnt_DCO_reg[11]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[12] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_51),
        .Q(cnt_DCO_reg[12]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[13] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_50),
        .Q(cnt_DCO_reg[13]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[14] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_49),
        .Q(cnt_DCO_reg[14]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[15] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_48),
        .Q(cnt_DCO_reg[15]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[1] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_38),
        .Q(cnt_DCO_reg[1]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[2] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_37),
        .Q(cnt_DCO_reg[2]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[3] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_36),
        .Q(cnt_DCO_reg[3]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[4] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_43),
        .Q(cnt_DCO_reg[4]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[5] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_42),
        .Q(cnt_DCO_reg[5]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[6] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_41),
        .Q(cnt_DCO_reg[6]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[7] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_40),
        .Q(cnt_DCO_reg[7]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[8] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_47),
        .Q(cnt_DCO_reg[8]),
        .R(clear));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \cnt_DCO_reg[9] 
       (.C(dco_or_dcoa),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_46),
        .Q(cnt_DCO_reg[9]),
        .R(clear));
  LUT3 #(
    .INIT(8'h7F)) 
    \cnt_in_DCO[0]_i_1 
       (.I0(dco_or_dcoa),
        .I1(allowed_clk),
        .I2(s00_axi_aresetn),
        .O(\cnt_in_DCO[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_in_DCO[0]_i_3 
       (.I0(cnt_in_DCO_reg[0]),
        .O(\cnt_in_DCO[0]_i_3_n_0 ));
  FDRE \cnt_in_DCO_reg[0] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_7 ),
        .Q(cnt_in_DCO_reg[0]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_in_DCO_reg[0]_i_2_n_0 ,\cnt_in_DCO_reg[0]_i_2_n_1 ,\cnt_in_DCO_reg[0]_i_2_n_2 ,\cnt_in_DCO_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_in_DCO_reg[0]_i_2_n_4 ,\cnt_in_DCO_reg[0]_i_2_n_5 ,\cnt_in_DCO_reg[0]_i_2_n_6 ,\cnt_in_DCO_reg[0]_i_2_n_7 }),
        .S({cnt_in_DCO_reg[3:1],\cnt_in_DCO[0]_i_3_n_0 }));
  FDRE \cnt_in_DCO_reg[10] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[10]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[11] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[11]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[12] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[12]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[12]_i_1 
       (.CI(\cnt_in_DCO_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_in_DCO_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_in_DCO_reg[12]_i_1_n_1 ,\cnt_in_DCO_reg[12]_i_1_n_2 ,\cnt_in_DCO_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[12]_i_1_n_4 ,\cnt_in_DCO_reg[12]_i_1_n_5 ,\cnt_in_DCO_reg[12]_i_1_n_6 ,\cnt_in_DCO_reg[12]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[15:12]));
  FDRE \cnt_in_DCO_reg[13] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[13]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[14] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[14]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[15] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[12]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[15]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[1] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_6 ),
        .Q(cnt_in_DCO_reg[1]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[2] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_5 ),
        .Q(cnt_in_DCO_reg[2]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[3] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[0]_i_2_n_4 ),
        .Q(cnt_in_DCO_reg[3]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[4] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[4]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[4]_i_1 
       (.CI(\cnt_in_DCO_reg[0]_i_2_n_0 ),
        .CO({\cnt_in_DCO_reg[4]_i_1_n_0 ,\cnt_in_DCO_reg[4]_i_1_n_1 ,\cnt_in_DCO_reg[4]_i_1_n_2 ,\cnt_in_DCO_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[4]_i_1_n_4 ,\cnt_in_DCO_reg[4]_i_1_n_5 ,\cnt_in_DCO_reg[4]_i_1_n_6 ,\cnt_in_DCO_reg[4]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[7:4]));
  FDRE \cnt_in_DCO_reg[5] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[5]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[6] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_5 ),
        .Q(cnt_in_DCO_reg[6]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[7] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[4]_i_1_n_4 ),
        .Q(cnt_in_DCO_reg[7]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  FDRE \cnt_in_DCO_reg[8] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_7 ),
        .Q(cnt_in_DCO_reg[8]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 \cnt_in_DCO_reg[8]_i_1 
       (.CI(\cnt_in_DCO_reg[4]_i_1_n_0 ),
        .CO({\cnt_in_DCO_reg[8]_i_1_n_0 ,\cnt_in_DCO_reg[8]_i_1_n_1 ,\cnt_in_DCO_reg[8]_i_1_n_2 ,\cnt_in_DCO_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_in_DCO_reg[8]_i_1_n_4 ,\cnt_in_DCO_reg[8]_i_1_n_5 ,\cnt_in_DCO_reg[8]_i_1_n_6 ,\cnt_in_DCO_reg[8]_i_1_n_7 }),
        .S(cnt_in_DCO_reg[11:8]));
  FDRE \cnt_in_DCO_reg[9] 
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(\cnt_in_DCO_reg[8]_i_1_n_6 ),
        .Q(cnt_in_DCO_reg[9]),
        .R(\cnt_in_DCO[0]_i_1_n_0 ));
  CARRY4 m00_fft_axis_tlast_r1_carry
       (.CI(1'b0),
        .CO({m00_fft_axis_tlast_r1_carry_n_0,m00_fft_axis_tlast_r1_carry_n_1,m00_fft_axis_tlast_r1_carry_n_2,m00_fft_axis_tlast_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_fft_axis_tlast_r1_carry_O_UNCONNECTED[3:0]),
        .S({AD9650_v1_0_S00_AXI_inst_n_30,AD9650_v1_0_S00_AXI_inst_n_31,AD9650_v1_0_S00_AXI_inst_n_32,AD9650_v1_0_S00_AXI_inst_n_33}));
  CARRY4 m00_fft_axis_tlast_r1_carry__0
       (.CI(m00_fft_axis_tlast_r1_carry_n_0),
        .CO({NLW_m00_fft_axis_tlast_r1_carry__0_CO_UNCONNECTED[3:2],m00_fft_axis_tlast_r1,m00_fft_axis_tlast_r1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_m00_fft_axis_tlast_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,AD9650_v1_0_S00_AXI_inst_n_11,AD9650_v1_0_S00_AXI_inst_n_12}));
  FDRE m00_fft_axis_tlast_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(m00_fft_axis_tlast_r0),
        .Q(m00_fft_axis_tlast),
        .R(1'b0));
  CARRY4 m00_fft_axis_tvalid_r1_carry
       (.CI(1'b0),
        .CO({m00_fft_axis_tvalid_r1_carry_n_0,m00_fft_axis_tvalid_r1_carry_n_1,m00_fft_axis_tvalid_r1_carry_n_2,m00_fft_axis_tvalid_r1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({AD9650_v1_0_S00_AXI_inst_n_18,AD9650_v1_0_S00_AXI_inst_n_19,AD9650_v1_0_S00_AXI_inst_n_20,AD9650_v1_0_S00_AXI_inst_n_21}),
        .O(NLW_m00_fft_axis_tvalid_r1_carry_O_UNCONNECTED[3:0]),
        .S({AD9650_v1_0_S00_AXI_inst_n_14,AD9650_v1_0_S00_AXI_inst_n_15,AD9650_v1_0_S00_AXI_inst_n_16,AD9650_v1_0_S00_AXI_inst_n_17}));
  CARRY4 m00_fft_axis_tvalid_r1_carry__0
       (.CI(m00_fft_axis_tvalid_r1_carry_n_0),
        .CO({m00_fft_axis_tvalid_r1,m00_fft_axis_tvalid_r1_carry__0_n_1,m00_fft_axis_tvalid_r1_carry__0_n_2,m00_fft_axis_tvalid_r1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({AD9650_v1_0_S00_AXI_inst_n_26,AD9650_v1_0_S00_AXI_inst_n_27,AD9650_v1_0_S00_AXI_inst_n_28,AD9650_v1_0_S00_AXI_inst_n_29}),
        .O(NLW_m00_fft_axis_tvalid_r1_carry__0_O_UNCONNECTED[3:0]),
        .S({AD9650_v1_0_S00_AXI_inst_n_22,AD9650_v1_0_S00_AXI_inst_n_23,AD9650_v1_0_S00_AXI_inst_n_24,AD9650_v1_0_S00_AXI_inst_n_25}));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_10
       (.I0(cnt_DCO_reg[14]),
        .I1(cnt_DCO_reg[13]),
        .I2(cnt_DCO_reg[0]),
        .I3(cnt_DCO_reg[15]),
        .O(m00_fft_axis_tvalid_r_i_10_n_0));
  LUT4 #(
    .INIT(16'h0001)) 
    m00_fft_axis_tvalid_r_i_2
       (.I0(m00_fft_axis_tvalid_r_i_5_n_0),
        .I1(m00_fft_axis_tvalid_r_i_6_n_0),
        .I2(m00_fft_axis_tvalid_r_i_7_n_0),
        .I3(m00_fft_axis_tvalid_r_i_8_n_0),
        .O(cnt_in_DCO__14));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_3
       (.I0(cnt_DCO_reg[3]),
        .I1(cnt_DCO_reg[4]),
        .I2(cnt_DCO_reg[1]),
        .I3(cnt_DCO_reg[2]),
        .I4(m00_fft_axis_tvalid_r_i_9_n_0),
        .O(m00_fft_axis_tvalid_r_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    m00_fft_axis_tvalid_r_i_4
       (.I0(cnt_DCO_reg[11]),
        .I1(cnt_DCO_reg[12]),
        .I2(cnt_DCO_reg[9]),
        .I3(cnt_DCO_reg[10]),
        .I4(m00_fft_axis_tvalid_r_i_10_n_0),
        .O(m00_fft_axis_tvalid_r_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_5
       (.I0(cnt_in_DCO_reg[10]),
        .I1(cnt_in_DCO_reg[11]),
        .I2(cnt_in_DCO_reg[8]),
        .I3(cnt_in_DCO_reg[9]),
        .O(m00_fft_axis_tvalid_r_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_6
       (.I0(cnt_in_DCO_reg[15]),
        .I1(cnt_in_DCO_reg[14]),
        .I2(cnt_in_DCO_reg[12]),
        .I3(cnt_in_DCO_reg[13]),
        .O(m00_fft_axis_tvalid_r_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFEF)) 
    m00_fft_axis_tvalid_r_i_7
       (.I0(cnt_in_DCO_reg[2]),
        .I1(cnt_in_DCO_reg[3]),
        .I2(cnt_in_DCO_reg[0]),
        .I3(cnt_in_DCO_reg[1]),
        .O(m00_fft_axis_tvalid_r_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_8
       (.I0(cnt_in_DCO_reg[6]),
        .I1(cnt_in_DCO_reg[7]),
        .I2(cnt_in_DCO_reg[4]),
        .I3(cnt_in_DCO_reg[5]),
        .O(m00_fft_axis_tvalid_r_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    m00_fft_axis_tvalid_r_i_9
       (.I0(cnt_DCO_reg[6]),
        .I1(cnt_DCO_reg[5]),
        .I2(cnt_DCO_reg[8]),
        .I3(cnt_DCO_reg[7]),
        .O(m00_fft_axis_tvalid_r_i_9_n_0));
  FDRE m00_fft_axis_tvalid_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_52),
        .Q(m00_fft_axis_tvalid),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[0]_INST_0 
       (.I0(DATA_INA[0]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[10]_INST_0 
       (.I0(DATA_INA[10]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[11]_INST_0 
       (.I0(DATA_INA[11]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[12]_INST_0 
       (.I0(DATA_INA[12]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[13]_INST_0 
       (.I0(DATA_INA[13]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[14]_INST_0 
       (.I0(DATA_INA[14]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[15]_INST_0 
       (.I0(DATA_INA[15]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[16]_INST_0 
       (.I0(DATA_INB[0]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[17]_INST_0 
       (.I0(DATA_INB[1]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[18]_INST_0 
       (.I0(DATA_INB[2]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[19]_INST_0 
       (.I0(DATA_INB[3]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[1]_INST_0 
       (.I0(DATA_INA[1]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[20]_INST_0 
       (.I0(DATA_INB[4]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[21]_INST_0 
       (.I0(DATA_INB[5]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[22]_INST_0 
       (.I0(DATA_INB[6]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[23]_INST_0 
       (.I0(DATA_INB[7]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[24]_INST_0 
       (.I0(DATA_INB[8]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[25]_INST_0 
       (.I0(DATA_INB[9]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[26]_INST_0 
       (.I0(DATA_INB[10]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[27]_INST_0 
       (.I0(DATA_INB[11]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[28]_INST_0 
       (.I0(DATA_INB[12]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[29]_INST_0 
       (.I0(DATA_INB[13]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[2]_INST_0 
       (.I0(DATA_INA[2]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[30]_INST_0 
       (.I0(DATA_INB[14]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[31]_INST_0 
       (.I0(DATA_INB[15]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[3]_INST_0 
       (.I0(DATA_INA[3]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[4]_INST_0 
       (.I0(DATA_INA[4]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[5]_INST_0 
       (.I0(DATA_INA[5]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[6]_INST_0 
       (.I0(DATA_INA[6]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[7]_INST_0 
       (.I0(DATA_INA[7]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[8]_INST_0 
       (.I0(DATA_INA[8]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[9]_INST_0 
       (.I0(DATA_INA[9]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[9]));
  FDRE m01_fft_axis_tvalid_r_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(AD9650_v1_0_S00_AXI_inst_n_53),
        .Q(m01_fft_axis_tvalid),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_2_AD9650_0_0,AD9650_v2_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "AD9650_v2_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_10MHz,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    m00_fft_axis_tvalid,
    m00_fft_axis_tdata,
    m00_fft_axis_tstrb,
    m00_fft_axis_tlast,
    m00_fft_axis_tready,
    m01_fft_axis_tvalid,
    m01_fft_axis_tdata,
    m01_fft_axis_tstrb,
    m01_fft_axis_tlast,
    m01_fft_axis_tready,
    dco_or_dcoa,
    dco_or_dcob,
    dco_or_ora,
    dco_or_orb,
    ADC_PDwN,
    SYNC,
    DATA_INA,
    DATA_INB,
    allowed_clk,
    mux_fft,
    s00_axi_aclk,
    s00_axi_aresetn,
    DCO);
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) input [5:0]s00_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [5:0]s00_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 4, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 100000000, ID_WIDTH 0, ADDR_WIDTH 6, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s00_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID" *) output m00_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA" *) output [31:0]m00_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB" *) output [3:0]m00_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST" *) output m00_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID" *) output m01_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA" *) output [31:0]m01_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB" *) output [3:0]m01_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST" *) output m01_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m01_fft_axis_tready;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcoa" *) input dco_or_dcoa;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcob" *) input dco_or_dcob;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR ora" *) input dco_or_ora;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR orb" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true" *) input dco_or_orb;
  output ADC_PDwN;
  output SYNC;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input mux_fft;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m01_fft_axis:m00_fft_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output DCO;

  wire \<const0> ;
  wire \<const1> ;
  wire ADC_PDwN;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire SYNC;
  wire allowed_clk;
  wire clk_10MHz;
  wire dco_or_dcoa;
  wire [31:0]m00_fft_axis_tdata;
  wire m00_fft_axis_tlast;
  wire m00_fft_axis_tvalid;
  wire [31:0]m01_fft_axis_tdata;
  wire m01_fft_axis_tvalid;
  wire mux_fft;
  wire s00_axi_aclk;
  wire [5:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [5:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;

  assign DCO = dco_or_dcoa;
  assign m00_fft_axis_tstrb[3] = \<const1> ;
  assign m00_fft_axis_tstrb[2] = \<const1> ;
  assign m00_fft_axis_tstrb[1] = \<const1> ;
  assign m00_fft_axis_tstrb[0] = \<const1> ;
  assign m01_fft_axis_tstrb[3] = \<const1> ;
  assign m01_fft_axis_tstrb[2] = \<const1> ;
  assign m01_fft_axis_tstrb[1] = \<const1> ;
  assign m01_fft_axis_tstrb[0] = \<const1> ;
  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 inst
       (.ADC_PDwN(ADC_PDwN),
        .DATA_INA(DATA_INA),
        .DATA_INB(DATA_INB),
        .SYNC(SYNC),
        .allowed_clk(allowed_clk),
        .clk_10MHz(clk_10MHz),
        .dco_or_dcoa(dco_or_dcoa),
        .m00_fft_axis_tlast(m00_fft_axis_tlast),
        .m00_fft_axis_tvalid(m00_fft_axis_tvalid),
        .m01_fft_axis_tdata(m01_fft_axis_tdata),
        .m01_fft_axis_tvalid(m01_fft_axis_tvalid),
        .mux_fft(mux_fft),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arready(s00_axi_arready),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awready(s00_axi_awready),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wready(s00_axi_wready),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[0]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[0]),
        .O(m00_fft_axis_tdata[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[10]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[10]),
        .O(m00_fft_axis_tdata[10]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[11]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[11]),
        .O(m00_fft_axis_tdata[11]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[12]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[12]),
        .O(m00_fft_axis_tdata[12]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[13]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[13]),
        .O(m00_fft_axis_tdata[13]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[14]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[14]),
        .O(m00_fft_axis_tdata[14]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[15]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[15]),
        .O(m00_fft_axis_tdata[15]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[16]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[0]),
        .O(m00_fft_axis_tdata[16]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[17]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[1]),
        .O(m00_fft_axis_tdata[17]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[18]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[2]),
        .O(m00_fft_axis_tdata[18]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[19]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[3]),
        .O(m00_fft_axis_tdata[19]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[1]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[1]),
        .O(m00_fft_axis_tdata[1]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[20]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[4]),
        .O(m00_fft_axis_tdata[20]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[21]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[5]),
        .O(m00_fft_axis_tdata[21]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[22]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[6]),
        .O(m00_fft_axis_tdata[22]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[23]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[7]),
        .O(m00_fft_axis_tdata[23]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[24]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[8]),
        .O(m00_fft_axis_tdata[24]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[25]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[9]),
        .O(m00_fft_axis_tdata[25]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[26]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[10]),
        .O(m00_fft_axis_tdata[26]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[27]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[11]),
        .O(m00_fft_axis_tdata[27]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[28]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[12]),
        .O(m00_fft_axis_tdata[28]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[29]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[13]),
        .O(m00_fft_axis_tdata[29]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[2]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[2]),
        .O(m00_fft_axis_tdata[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[30]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[14]),
        .O(m00_fft_axis_tdata[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[31]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[15]),
        .O(m00_fft_axis_tdata[31]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[3]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[3]),
        .O(m00_fft_axis_tdata[3]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[4]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[4]),
        .O(m00_fft_axis_tdata[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[5]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[5]),
        .O(m00_fft_axis_tdata[5]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[6]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[6]),
        .O(m00_fft_axis_tdata[6]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[7]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[7]),
        .O(m00_fft_axis_tdata[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[8]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[8]),
        .O(m00_fft_axis_tdata[8]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[9]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[9]),
        .O(m00_fft_axis_tdata[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

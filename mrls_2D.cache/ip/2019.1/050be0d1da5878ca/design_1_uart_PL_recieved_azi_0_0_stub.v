// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Sat Dec  4 14:52:47 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_uart_PL_recieved_azi_0_0_stub.v
// Design      : design_1_uart_PL_recieved_azi_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "uart_PL_recieved_azimut_v1_0,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(i_Clock, i_Tx_DV, i_Tx_Byte, o_Tx_Active, 
  o_Tx_Serial, o_Tx_Done, i_Rx_Serial, o_Rx_DV, o_Rx_Byte)
/* synthesis syn_black_box black_box_pad_pin="i_Clock,i_Tx_DV,i_Tx_Byte[7:0],o_Tx_Active,o_Tx_Serial,o_Tx_Done,i_Rx_Serial,o_Rx_DV,o_Rx_Byte[7:0]" */;
  input i_Clock;
  input i_Tx_DV;
  input [7:0]i_Tx_Byte;
  output o_Tx_Active;
  output o_Tx_Serial;
  output o_Tx_Done;
  input i_Rx_Serial;
  output o_Rx_DV;
  output [7:0]o_Rx_Byte;
endmodule

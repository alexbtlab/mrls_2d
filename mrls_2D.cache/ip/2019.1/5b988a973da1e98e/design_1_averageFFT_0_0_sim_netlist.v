// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Wed Dec  8 13:32:48 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.v
// Design      : design_1_averageFFT_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0
   (reset_cnt_trig_ila,
    \azimut_r_reg[2]_0 ,
    \azimut_r_reg[0]_0 ,
    \azimut_r_reg[1]_0 ,
    azimut8,
    interrupt_frame,
    allowed_clk,
    m00_axis_aresetn,
    clk_10MHz,
    m00_axis_aclk,
    azimut_0);
  output reset_cnt_trig_ila;
  output \azimut_r_reg[2]_0 ;
  output \azimut_r_reg[0]_0 ;
  output \azimut_r_reg[1]_0 ;
  output [15:0]azimut8;
  output interrupt_frame;
  input allowed_clk;
  input m00_axis_aresetn;
  input clk_10MHz;
  input m00_axis_aclk;
  input azimut_0;

  wire allowed_clk;
  wire [15:0]azimut8;
  wire azimut8_r;
  wire \azimut8_r[15]_i_1_n_0 ;
  wire \azimut8_r[15]_i_4_n_0 ;
  wire \azimut8_r[15]_i_5_n_0 ;
  wire \azimut8_r[15]_i_6_n_0 ;
  wire \azimut8_r[3]_i_2_n_0 ;
  wire \azimut8_r_reg[11]_i_1_n_0 ;
  wire \azimut8_r_reg[11]_i_1_n_1 ;
  wire \azimut8_r_reg[11]_i_1_n_2 ;
  wire \azimut8_r_reg[11]_i_1_n_3 ;
  wire \azimut8_r_reg[11]_i_1_n_4 ;
  wire \azimut8_r_reg[11]_i_1_n_5 ;
  wire \azimut8_r_reg[11]_i_1_n_6 ;
  wire \azimut8_r_reg[11]_i_1_n_7 ;
  wire \azimut8_r_reg[15]_i_3_n_1 ;
  wire \azimut8_r_reg[15]_i_3_n_2 ;
  wire \azimut8_r_reg[15]_i_3_n_3 ;
  wire \azimut8_r_reg[15]_i_3_n_4 ;
  wire \azimut8_r_reg[15]_i_3_n_5 ;
  wire \azimut8_r_reg[15]_i_3_n_6 ;
  wire \azimut8_r_reg[15]_i_3_n_7 ;
  wire \azimut8_r_reg[3]_i_1_n_0 ;
  wire \azimut8_r_reg[3]_i_1_n_1 ;
  wire \azimut8_r_reg[3]_i_1_n_2 ;
  wire \azimut8_r_reg[3]_i_1_n_3 ;
  wire \azimut8_r_reg[3]_i_1_n_4 ;
  wire \azimut8_r_reg[3]_i_1_n_5 ;
  wire \azimut8_r_reg[3]_i_1_n_6 ;
  wire \azimut8_r_reg[3]_i_1_n_7 ;
  wire \azimut8_r_reg[7]_i_1_n_0 ;
  wire \azimut8_r_reg[7]_i_1_n_1 ;
  wire \azimut8_r_reg[7]_i_1_n_2 ;
  wire \azimut8_r_reg[7]_i_1_n_3 ;
  wire \azimut8_r_reg[7]_i_1_n_4 ;
  wire \azimut8_r_reg[7]_i_1_n_5 ;
  wire \azimut8_r_reg[7]_i_1_n_6 ;
  wire \azimut8_r_reg[7]_i_1_n_7 ;
  wire azimut_0;
  wire azimut_0_prev;
  wire azimut_r;
  wire \azimut_r[0]_i_1_n_0 ;
  wire \azimut_r[1]_i_1_n_0 ;
  wire \azimut_r[2]_i_1_n_0 ;
  wire \azimut_r_reg[0]_0 ;
  wire \azimut_r_reg[1]_0 ;
  wire \azimut_r_reg[2]_0 ;
  wire clear;
  wire clk_10MHz;
  wire \cnt100[0]_i_3_n_0 ;
  wire [31:0]cnt100_reg;
  wire \cnt100_reg[0]_i_2_n_0 ;
  wire \cnt100_reg[0]_i_2_n_1 ;
  wire \cnt100_reg[0]_i_2_n_2 ;
  wire \cnt100_reg[0]_i_2_n_3 ;
  wire \cnt100_reg[0]_i_2_n_4 ;
  wire \cnt100_reg[0]_i_2_n_5 ;
  wire \cnt100_reg[0]_i_2_n_6 ;
  wire \cnt100_reg[0]_i_2_n_7 ;
  wire \cnt100_reg[12]_i_1_n_0 ;
  wire \cnt100_reg[12]_i_1_n_1 ;
  wire \cnt100_reg[12]_i_1_n_2 ;
  wire \cnt100_reg[12]_i_1_n_3 ;
  wire \cnt100_reg[12]_i_1_n_4 ;
  wire \cnt100_reg[12]_i_1_n_5 ;
  wire \cnt100_reg[12]_i_1_n_6 ;
  wire \cnt100_reg[12]_i_1_n_7 ;
  wire \cnt100_reg[16]_i_1_n_0 ;
  wire \cnt100_reg[16]_i_1_n_1 ;
  wire \cnt100_reg[16]_i_1_n_2 ;
  wire \cnt100_reg[16]_i_1_n_3 ;
  wire \cnt100_reg[16]_i_1_n_4 ;
  wire \cnt100_reg[16]_i_1_n_5 ;
  wire \cnt100_reg[16]_i_1_n_6 ;
  wire \cnt100_reg[16]_i_1_n_7 ;
  wire \cnt100_reg[20]_i_1_n_0 ;
  wire \cnt100_reg[20]_i_1_n_1 ;
  wire \cnt100_reg[20]_i_1_n_2 ;
  wire \cnt100_reg[20]_i_1_n_3 ;
  wire \cnt100_reg[20]_i_1_n_4 ;
  wire \cnt100_reg[20]_i_1_n_5 ;
  wire \cnt100_reg[20]_i_1_n_6 ;
  wire \cnt100_reg[20]_i_1_n_7 ;
  wire \cnt100_reg[24]_i_1_n_0 ;
  wire \cnt100_reg[24]_i_1_n_1 ;
  wire \cnt100_reg[24]_i_1_n_2 ;
  wire \cnt100_reg[24]_i_1_n_3 ;
  wire \cnt100_reg[24]_i_1_n_4 ;
  wire \cnt100_reg[24]_i_1_n_5 ;
  wire \cnt100_reg[24]_i_1_n_6 ;
  wire \cnt100_reg[24]_i_1_n_7 ;
  wire \cnt100_reg[28]_i_1_n_1 ;
  wire \cnt100_reg[28]_i_1_n_2 ;
  wire \cnt100_reg[28]_i_1_n_3 ;
  wire \cnt100_reg[28]_i_1_n_4 ;
  wire \cnt100_reg[28]_i_1_n_5 ;
  wire \cnt100_reg[28]_i_1_n_6 ;
  wire \cnt100_reg[28]_i_1_n_7 ;
  wire \cnt100_reg[4]_i_1_n_0 ;
  wire \cnt100_reg[4]_i_1_n_1 ;
  wire \cnt100_reg[4]_i_1_n_2 ;
  wire \cnt100_reg[4]_i_1_n_3 ;
  wire \cnt100_reg[4]_i_1_n_4 ;
  wire \cnt100_reg[4]_i_1_n_5 ;
  wire \cnt100_reg[4]_i_1_n_6 ;
  wire \cnt100_reg[4]_i_1_n_7 ;
  wire \cnt100_reg[8]_i_1_n_0 ;
  wire \cnt100_reg[8]_i_1_n_1 ;
  wire \cnt100_reg[8]_i_1_n_2 ;
  wire \cnt100_reg[8]_i_1_n_3 ;
  wire \cnt100_reg[8]_i_1_n_4 ;
  wire \cnt100_reg[8]_i_1_n_5 ;
  wire \cnt100_reg[8]_i_1_n_6 ;
  wire \cnt100_reg[8]_i_1_n_7 ;
  wire \cnt_low_allowed_clk[0]_i_1_n_0 ;
  wire \cnt_low_allowed_clk[0]_i_3_n_0 ;
  wire [15:0]cnt_low_allowed_clk_reg;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_0 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_1 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_2 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_3 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_4 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_5 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_6 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_7 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_1 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_2 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_3 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_4 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_5 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_6 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_7 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_0 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_1 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_2 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_3 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_4 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_5 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_6 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_7 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_0 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_1 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_2 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_3 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_4 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_5 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_6 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_7 ;
  wire interrupt_frame;
  wire interrupt_frame_i_10_n_0;
  wire interrupt_frame_i_11_n_0;
  wire interrupt_frame_i_12_n_0;
  wire interrupt_frame_i_13_n_0;
  wire interrupt_frame_i_14_n_0;
  wire interrupt_frame_i_15_n_0;
  wire interrupt_frame_i_16_n_0;
  wire interrupt_frame_i_1_n_0;
  wire interrupt_frame_i_2_n_0;
  wire interrupt_frame_i_3_n_0;
  wire interrupt_frame_i_4_n_0;
  wire interrupt_frame_i_5_n_0;
  wire interrupt_frame_i_6_n_0;
  wire interrupt_frame_i_7_n_0;
  wire interrupt_frame_i_8_n_0;
  wire interrupt_frame_i_9_n_0;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire reset_cnt_trig0;
  wire reset_cnt_trig_ila;
  wire [3:3]\NLW_azimut8_r_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED ;

  LUT2 #(
    .INIT(4'hB)) 
    \azimut8_r[15]_i_1 
       (.I0(reset_cnt_trig_ila),
        .I1(m00_axis_aresetn),
        .O(\azimut8_r[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \azimut8_r[15]_i_2 
       (.I0(\azimut8_r[15]_i_4_n_0 ),
        .I1(\azimut8_r[15]_i_5_n_0 ),
        .I2(\azimut_r_reg[2]_0 ),
        .I3(\azimut_r_reg[0]_0 ),
        .I4(\azimut_r_reg[1]_0 ),
        .I5(\azimut8_r[15]_i_6_n_0 ),
        .O(azimut8_r));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \azimut8_r[15]_i_4 
       (.I0(cnt_low_allowed_clk_reg[6]),
        .I1(cnt_low_allowed_clk_reg[7]),
        .I2(cnt_low_allowed_clk_reg[4]),
        .I3(cnt_low_allowed_clk_reg[5]),
        .I4(cnt_low_allowed_clk_reg[9]),
        .I5(cnt_low_allowed_clk_reg[8]),
        .O(\azimut8_r[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \azimut8_r[15]_i_5 
       (.I0(cnt_low_allowed_clk_reg[0]),
        .I1(cnt_low_allowed_clk_reg[1]),
        .I2(cnt_low_allowed_clk_reg[2]),
        .I3(cnt_low_allowed_clk_reg[3]),
        .O(\azimut8_r[15]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \azimut8_r[15]_i_6 
       (.I0(cnt_low_allowed_clk_reg[12]),
        .I1(cnt_low_allowed_clk_reg[13]),
        .I2(cnt_low_allowed_clk_reg[10]),
        .I3(cnt_low_allowed_clk_reg[11]),
        .I4(cnt_low_allowed_clk_reg[15]),
        .I5(cnt_low_allowed_clk_reg[14]),
        .O(\azimut8_r[15]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \azimut8_r[3]_i_2 
       (.I0(azimut8[0]),
        .O(\azimut8_r[3]_i_2_n_0 ));
  FDRE \azimut8_r_reg[0] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[3]_i_1_n_7 ),
        .Q(azimut8[0]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[10] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[11]_i_1_n_5 ),
        .Q(azimut8[10]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[11] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[11]_i_1_n_4 ),
        .Q(azimut8[11]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  CARRY4 \azimut8_r_reg[11]_i_1 
       (.CI(\azimut8_r_reg[7]_i_1_n_0 ),
        .CO({\azimut8_r_reg[11]_i_1_n_0 ,\azimut8_r_reg[11]_i_1_n_1 ,\azimut8_r_reg[11]_i_1_n_2 ,\azimut8_r_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\azimut8_r_reg[11]_i_1_n_4 ,\azimut8_r_reg[11]_i_1_n_5 ,\azimut8_r_reg[11]_i_1_n_6 ,\azimut8_r_reg[11]_i_1_n_7 }),
        .S(azimut8[11:8]));
  FDRE \azimut8_r_reg[12] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[15]_i_3_n_7 ),
        .Q(azimut8[12]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[13] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[15]_i_3_n_6 ),
        .Q(azimut8[13]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[14] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[15]_i_3_n_5 ),
        .Q(azimut8[14]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[15] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[15]_i_3_n_4 ),
        .Q(azimut8[15]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  CARRY4 \azimut8_r_reg[15]_i_3 
       (.CI(\azimut8_r_reg[11]_i_1_n_0 ),
        .CO({\NLW_azimut8_r_reg[15]_i_3_CO_UNCONNECTED [3],\azimut8_r_reg[15]_i_3_n_1 ,\azimut8_r_reg[15]_i_3_n_2 ,\azimut8_r_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\azimut8_r_reg[15]_i_3_n_4 ,\azimut8_r_reg[15]_i_3_n_5 ,\azimut8_r_reg[15]_i_3_n_6 ,\azimut8_r_reg[15]_i_3_n_7 }),
        .S(azimut8[15:12]));
  FDRE \azimut8_r_reg[1] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[3]_i_1_n_6 ),
        .Q(azimut8[1]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[2] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[3]_i_1_n_5 ),
        .Q(azimut8[2]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[3] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[3]_i_1_n_4 ),
        .Q(azimut8[3]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  CARRY4 \azimut8_r_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\azimut8_r_reg[3]_i_1_n_0 ,\azimut8_r_reg[3]_i_1_n_1 ,\azimut8_r_reg[3]_i_1_n_2 ,\azimut8_r_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\azimut8_r_reg[3]_i_1_n_4 ,\azimut8_r_reg[3]_i_1_n_5 ,\azimut8_r_reg[3]_i_1_n_6 ,\azimut8_r_reg[3]_i_1_n_7 }),
        .S({azimut8[3:1],\azimut8_r[3]_i_2_n_0 }));
  FDRE \azimut8_r_reg[4] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[7]_i_1_n_7 ),
        .Q(azimut8[4]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[5] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[7]_i_1_n_6 ),
        .Q(azimut8[5]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[6] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[7]_i_1_n_5 ),
        .Q(azimut8[6]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[7] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[7]_i_1_n_4 ),
        .Q(azimut8[7]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  CARRY4 \azimut8_r_reg[7]_i_1 
       (.CI(\azimut8_r_reg[3]_i_1_n_0 ),
        .CO({\azimut8_r_reg[7]_i_1_n_0 ,\azimut8_r_reg[7]_i_1_n_1 ,\azimut8_r_reg[7]_i_1_n_2 ,\azimut8_r_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\azimut8_r_reg[7]_i_1_n_4 ,\azimut8_r_reg[7]_i_1_n_5 ,\azimut8_r_reg[7]_i_1_n_6 ,\azimut8_r_reg[7]_i_1_n_7 }),
        .S(azimut8[7:4]));
  FDRE \azimut8_r_reg[8] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[11]_i_1_n_7 ),
        .Q(azimut8[8]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE \azimut8_r_reg[9] 
       (.C(clk_10MHz),
        .CE(azimut8_r),
        .D(\azimut8_r_reg[11]_i_1_n_6 ),
        .Q(azimut8[9]),
        .R(\azimut8_r[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    azimut_0_prev_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(azimut_0),
        .Q(azimut_0_prev),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0060)) 
    \azimut_r[0]_i_1 
       (.I0(\azimut_r_reg[0]_0 ),
        .I1(azimut_r),
        .I2(m00_axis_aresetn),
        .I3(reset_cnt_trig_ila),
        .O(\azimut_r[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00006A00)) 
    \azimut_r[1]_i_1 
       (.I0(\azimut_r_reg[1]_0 ),
        .I1(azimut_r),
        .I2(\azimut_r_reg[0]_0 ),
        .I3(m00_axis_aresetn),
        .I4(reset_cnt_trig_ila),
        .O(\azimut_r[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000006AAA0000)) 
    \azimut_r[2]_i_1 
       (.I0(\azimut_r_reg[2]_0 ),
        .I1(azimut_r),
        .I2(\azimut_r_reg[0]_0 ),
        .I3(\azimut_r_reg[1]_0 ),
        .I4(m00_axis_aresetn),
        .I5(reset_cnt_trig_ila),
        .O(\azimut_r[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0020000000000000)) 
    \azimut_r[2]_i_2 
       (.I0(\azimut8_r[15]_i_6_n_0 ),
        .I1(cnt_low_allowed_clk_reg[0]),
        .I2(cnt_low_allowed_clk_reg[1]),
        .I3(cnt_low_allowed_clk_reg[2]),
        .I4(cnt_low_allowed_clk_reg[3]),
        .I5(\azimut8_r[15]_i_4_n_0 ),
        .O(azimut_r));
  FDRE \azimut_r_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\azimut_r[0]_i_1_n_0 ),
        .Q(\azimut_r_reg[0]_0 ),
        .R(1'b0));
  FDRE \azimut_r_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\azimut_r[1]_i_1_n_0 ),
        .Q(\azimut_r_reg[1]_0 ),
        .R(1'b0));
  FDRE \azimut_r_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\azimut_r[2]_i_1_n_0 ),
        .Q(\azimut_r_reg[2]_0 ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_1 
       (.I0(allowed_clk),
        .O(clear));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt100[0]_i_3 
       (.I0(cnt100_reg[0]),
        .O(\cnt100[0]_i_3_n_0 ));
  FDRE \cnt100_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_7 ),
        .Q(cnt100_reg[0]),
        .R(clear));
  CARRY4 \cnt100_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt100_reg[0]_i_2_n_0 ,\cnt100_reg[0]_i_2_n_1 ,\cnt100_reg[0]_i_2_n_2 ,\cnt100_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt100_reg[0]_i_2_n_4 ,\cnt100_reg[0]_i_2_n_5 ,\cnt100_reg[0]_i_2_n_6 ,\cnt100_reg[0]_i_2_n_7 }),
        .S({cnt100_reg[3:1],\cnt100[0]_i_3_n_0 }));
  FDRE \cnt100_reg[10] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_5 ),
        .Q(cnt100_reg[10]),
        .R(clear));
  FDRE \cnt100_reg[11] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_4 ),
        .Q(cnt100_reg[11]),
        .R(clear));
  FDRE \cnt100_reg[12] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_7 ),
        .Q(cnt100_reg[12]),
        .R(clear));
  CARRY4 \cnt100_reg[12]_i_1 
       (.CI(\cnt100_reg[8]_i_1_n_0 ),
        .CO({\cnt100_reg[12]_i_1_n_0 ,\cnt100_reg[12]_i_1_n_1 ,\cnt100_reg[12]_i_1_n_2 ,\cnt100_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[12]_i_1_n_4 ,\cnt100_reg[12]_i_1_n_5 ,\cnt100_reg[12]_i_1_n_6 ,\cnt100_reg[12]_i_1_n_7 }),
        .S(cnt100_reg[15:12]));
  FDRE \cnt100_reg[13] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_6 ),
        .Q(cnt100_reg[13]),
        .R(clear));
  FDRE \cnt100_reg[14] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_5 ),
        .Q(cnt100_reg[14]),
        .R(clear));
  FDRE \cnt100_reg[15] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[12]_i_1_n_4 ),
        .Q(cnt100_reg[15]),
        .R(clear));
  FDRE \cnt100_reg[16] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_7 ),
        .Q(cnt100_reg[16]),
        .R(clear));
  CARRY4 \cnt100_reg[16]_i_1 
       (.CI(\cnt100_reg[12]_i_1_n_0 ),
        .CO({\cnt100_reg[16]_i_1_n_0 ,\cnt100_reg[16]_i_1_n_1 ,\cnt100_reg[16]_i_1_n_2 ,\cnt100_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[16]_i_1_n_4 ,\cnt100_reg[16]_i_1_n_5 ,\cnt100_reg[16]_i_1_n_6 ,\cnt100_reg[16]_i_1_n_7 }),
        .S(cnt100_reg[19:16]));
  FDRE \cnt100_reg[17] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_6 ),
        .Q(cnt100_reg[17]),
        .R(clear));
  FDRE \cnt100_reg[18] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_5 ),
        .Q(cnt100_reg[18]),
        .R(clear));
  FDRE \cnt100_reg[19] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[16]_i_1_n_4 ),
        .Q(cnt100_reg[19]),
        .R(clear));
  FDRE \cnt100_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_6 ),
        .Q(cnt100_reg[1]),
        .R(clear));
  FDRE \cnt100_reg[20] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_7 ),
        .Q(cnt100_reg[20]),
        .R(clear));
  CARRY4 \cnt100_reg[20]_i_1 
       (.CI(\cnt100_reg[16]_i_1_n_0 ),
        .CO({\cnt100_reg[20]_i_1_n_0 ,\cnt100_reg[20]_i_1_n_1 ,\cnt100_reg[20]_i_1_n_2 ,\cnt100_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[20]_i_1_n_4 ,\cnt100_reg[20]_i_1_n_5 ,\cnt100_reg[20]_i_1_n_6 ,\cnt100_reg[20]_i_1_n_7 }),
        .S(cnt100_reg[23:20]));
  FDRE \cnt100_reg[21] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_6 ),
        .Q(cnt100_reg[21]),
        .R(clear));
  FDRE \cnt100_reg[22] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_5 ),
        .Q(cnt100_reg[22]),
        .R(clear));
  FDRE \cnt100_reg[23] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[20]_i_1_n_4 ),
        .Q(cnt100_reg[23]),
        .R(clear));
  FDRE \cnt100_reg[24] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_7 ),
        .Q(cnt100_reg[24]),
        .R(clear));
  CARRY4 \cnt100_reg[24]_i_1 
       (.CI(\cnt100_reg[20]_i_1_n_0 ),
        .CO({\cnt100_reg[24]_i_1_n_0 ,\cnt100_reg[24]_i_1_n_1 ,\cnt100_reg[24]_i_1_n_2 ,\cnt100_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[24]_i_1_n_4 ,\cnt100_reg[24]_i_1_n_5 ,\cnt100_reg[24]_i_1_n_6 ,\cnt100_reg[24]_i_1_n_7 }),
        .S(cnt100_reg[27:24]));
  FDRE \cnt100_reg[25] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_6 ),
        .Q(cnt100_reg[25]),
        .R(clear));
  FDRE \cnt100_reg[26] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_5 ),
        .Q(cnt100_reg[26]),
        .R(clear));
  FDRE \cnt100_reg[27] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[24]_i_1_n_4 ),
        .Q(cnt100_reg[27]),
        .R(clear));
  FDRE \cnt100_reg[28] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_7 ),
        .Q(cnt100_reg[28]),
        .R(clear));
  CARRY4 \cnt100_reg[28]_i_1 
       (.CI(\cnt100_reg[24]_i_1_n_0 ),
        .CO({\NLW_cnt100_reg[28]_i_1_CO_UNCONNECTED [3],\cnt100_reg[28]_i_1_n_1 ,\cnt100_reg[28]_i_1_n_2 ,\cnt100_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[28]_i_1_n_4 ,\cnt100_reg[28]_i_1_n_5 ,\cnt100_reg[28]_i_1_n_6 ,\cnt100_reg[28]_i_1_n_7 }),
        .S(cnt100_reg[31:28]));
  FDRE \cnt100_reg[29] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_6 ),
        .Q(cnt100_reg[29]),
        .R(clear));
  FDRE \cnt100_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_5 ),
        .Q(cnt100_reg[2]),
        .R(clear));
  FDRE \cnt100_reg[30] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_5 ),
        .Q(cnt100_reg[30]),
        .R(clear));
  FDRE \cnt100_reg[31] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[28]_i_1_n_4 ),
        .Q(cnt100_reg[31]),
        .R(clear));
  FDRE \cnt100_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[0]_i_2_n_4 ),
        .Q(cnt100_reg[3]),
        .R(clear));
  FDRE \cnt100_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_7 ),
        .Q(cnt100_reg[4]),
        .R(clear));
  CARRY4 \cnt100_reg[4]_i_1 
       (.CI(\cnt100_reg[0]_i_2_n_0 ),
        .CO({\cnt100_reg[4]_i_1_n_0 ,\cnt100_reg[4]_i_1_n_1 ,\cnt100_reg[4]_i_1_n_2 ,\cnt100_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[4]_i_1_n_4 ,\cnt100_reg[4]_i_1_n_5 ,\cnt100_reg[4]_i_1_n_6 ,\cnt100_reg[4]_i_1_n_7 }),
        .S(cnt100_reg[7:4]));
  FDRE \cnt100_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_6 ),
        .Q(cnt100_reg[5]),
        .R(clear));
  FDRE \cnt100_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_5 ),
        .Q(cnt100_reg[6]),
        .R(clear));
  FDRE \cnt100_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[4]_i_1_n_4 ),
        .Q(cnt100_reg[7]),
        .R(clear));
  FDRE \cnt100_reg[8] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_7 ),
        .Q(cnt100_reg[8]),
        .R(clear));
  CARRY4 \cnt100_reg[8]_i_1 
       (.CI(\cnt100_reg[4]_i_1_n_0 ),
        .CO({\cnt100_reg[8]_i_1_n_0 ,\cnt100_reg[8]_i_1_n_1 ,\cnt100_reg[8]_i_1_n_2 ,\cnt100_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt100_reg[8]_i_1_n_4 ,\cnt100_reg[8]_i_1_n_5 ,\cnt100_reg[8]_i_1_n_6 ,\cnt100_reg[8]_i_1_n_7 }),
        .S(cnt100_reg[11:8]));
  FDRE \cnt100_reg[9] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(\cnt100_reg[8]_i_1_n_6 ),
        .Q(cnt100_reg[9]),
        .R(clear));
  LUT2 #(
    .INIT(4'hB)) 
    \cnt_low_allowed_clk[0]_i_1 
       (.I0(allowed_clk),
        .I1(m00_axis_aresetn),
        .O(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_low_allowed_clk[0]_i_3 
       (.I0(cnt_low_allowed_clk_reg[0]),
        .O(\cnt_low_allowed_clk[0]_i_3_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_7 ),
        .Q(cnt_low_allowed_clk_reg[0]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_low_allowed_clk_reg[0]_i_2_n_0 ,\cnt_low_allowed_clk_reg[0]_i_2_n_1 ,\cnt_low_allowed_clk_reg[0]_i_2_n_2 ,\cnt_low_allowed_clk_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_low_allowed_clk_reg[0]_i_2_n_4 ,\cnt_low_allowed_clk_reg[0]_i_2_n_5 ,\cnt_low_allowed_clk_reg[0]_i_2_n_6 ,\cnt_low_allowed_clk_reg[0]_i_2_n_7 }),
        .S({cnt_low_allowed_clk_reg[3:1],\cnt_low_allowed_clk[0]_i_3_n_0 }));
  FDRE \cnt_low_allowed_clk_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_5 ),
        .Q(cnt_low_allowed_clk_reg[10]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_4 ),
        .Q(cnt_low_allowed_clk_reg[11]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_7 ),
        .Q(cnt_low_allowed_clk_reg[12]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[12]_i_1 
       (.CI(\cnt_low_allowed_clk_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_low_allowed_clk_reg[12]_i_1_n_1 ,\cnt_low_allowed_clk_reg[12]_i_1_n_2 ,\cnt_low_allowed_clk_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_low_allowed_clk_reg[12]_i_1_n_4 ,\cnt_low_allowed_clk_reg[12]_i_1_n_5 ,\cnt_low_allowed_clk_reg[12]_i_1_n_6 ,\cnt_low_allowed_clk_reg[12]_i_1_n_7 }),
        .S(cnt_low_allowed_clk_reg[15:12]));
  FDRE \cnt_low_allowed_clk_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_6 ),
        .Q(cnt_low_allowed_clk_reg[13]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_5 ),
        .Q(cnt_low_allowed_clk_reg[14]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_4 ),
        .Q(cnt_low_allowed_clk_reg[15]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_6 ),
        .Q(cnt_low_allowed_clk_reg[1]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_5 ),
        .Q(cnt_low_allowed_clk_reg[2]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_4 ),
        .Q(cnt_low_allowed_clk_reg[3]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_7 ),
        .Q(cnt_low_allowed_clk_reg[4]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[4]_i_1 
       (.CI(\cnt_low_allowed_clk_reg[0]_i_2_n_0 ),
        .CO({\cnt_low_allowed_clk_reg[4]_i_1_n_0 ,\cnt_low_allowed_clk_reg[4]_i_1_n_1 ,\cnt_low_allowed_clk_reg[4]_i_1_n_2 ,\cnt_low_allowed_clk_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_low_allowed_clk_reg[4]_i_1_n_4 ,\cnt_low_allowed_clk_reg[4]_i_1_n_5 ,\cnt_low_allowed_clk_reg[4]_i_1_n_6 ,\cnt_low_allowed_clk_reg[4]_i_1_n_7 }),
        .S(cnt_low_allowed_clk_reg[7:4]));
  FDRE \cnt_low_allowed_clk_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_6 ),
        .Q(cnt_low_allowed_clk_reg[5]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_5 ),
        .Q(cnt_low_allowed_clk_reg[6]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_4 ),
        .Q(cnt_low_allowed_clk_reg[7]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_7 ),
        .Q(cnt_low_allowed_clk_reg[8]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[8]_i_1 
       (.CI(\cnt_low_allowed_clk_reg[4]_i_1_n_0 ),
        .CO({\cnt_low_allowed_clk_reg[8]_i_1_n_0 ,\cnt_low_allowed_clk_reg[8]_i_1_n_1 ,\cnt_low_allowed_clk_reg[8]_i_1_n_2 ,\cnt_low_allowed_clk_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_low_allowed_clk_reg[8]_i_1_n_4 ,\cnt_low_allowed_clk_reg[8]_i_1_n_5 ,\cnt_low_allowed_clk_reg[8]_i_1_n_6 ,\cnt_low_allowed_clk_reg[8]_i_1_n_7 }),
        .S(cnt_low_allowed_clk_reg[11:8]));
  FDRE \cnt_low_allowed_clk_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_6 ),
        .Q(cnt_low_allowed_clk_reg[9]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFF0D0D0D0)) 
    interrupt_frame_i_1
       (.I0(cnt100_reg[8]),
        .I1(interrupt_frame_i_2_n_0),
        .I2(interrupt_frame_i_3_n_0),
        .I3(interrupt_frame_i_4_n_0),
        .I4(cnt100_reg[7]),
        .I5(interrupt_frame_i_5_n_0),
        .O(interrupt_frame_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFDF)) 
    interrupt_frame_i_10
       (.I0(cnt100_reg[10]),
        .I1(cnt100_reg[11]),
        .I2(cnt100_reg[12]),
        .I3(cnt100_reg[13]),
        .O(interrupt_frame_i_10_n_0));
  LUT6 #(
    .INIT(64'hFDFFFFFFFFFFFFFF)) 
    interrupt_frame_i_11
       (.I0(cnt100_reg[9]),
        .I1(cnt100_reg[30]),
        .I2(cnt100_reg[31]),
        .I3(\azimut_r_reg[2]_0 ),
        .I4(\azimut_r_reg[0]_0 ),
        .I5(\azimut_r_reg[1]_0 ),
        .O(interrupt_frame_i_11_n_0));
  LUT6 #(
    .INIT(64'h00CF00000000AA00)) 
    interrupt_frame_i_12
       (.I0(interrupt_frame_i_14_n_0),
        .I1(cnt100_reg[5]),
        .I2(interrupt_frame_i_15_n_0),
        .I3(cnt100_reg[8]),
        .I4(cnt100_reg[7]),
        .I5(cnt100_reg[6]),
        .O(interrupt_frame_i_12_n_0));
  LUT6 #(
    .INIT(64'hFEFF7F7F00FF00FF)) 
    interrupt_frame_i_13
       (.I0(cnt100_reg[4]),
        .I1(cnt100_reg[3]),
        .I2(cnt100_reg[2]),
        .I3(cnt100_reg[7]),
        .I4(interrupt_frame_i_16_n_0),
        .I5(cnt100_reg[5]),
        .O(interrupt_frame_i_13_n_0));
  LUT6 #(
    .INIT(64'h57FFFFFFFFFFFFFF)) 
    interrupt_frame_i_14
       (.I0(cnt100_reg[5]),
        .I1(cnt100_reg[0]),
        .I2(cnt100_reg[1]),
        .I3(cnt100_reg[2]),
        .I4(cnt100_reg[3]),
        .I5(cnt100_reg[4]),
        .O(interrupt_frame_i_14_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    interrupt_frame_i_15
       (.I0(cnt100_reg[3]),
        .I1(cnt100_reg[4]),
        .O(interrupt_frame_i_15_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h1)) 
    interrupt_frame_i_16
       (.I0(cnt100_reg[0]),
        .I1(cnt100_reg[1]),
        .O(interrupt_frame_i_16_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    interrupt_frame_i_2
       (.I0(interrupt_frame_i_6_n_0),
        .I1(interrupt_frame_i_7_n_0),
        .I2(interrupt_frame_i_8_n_0),
        .I3(interrupt_frame_i_9_n_0),
        .I4(interrupt_frame_i_10_n_0),
        .I5(interrupt_frame_i_11_n_0),
        .O(interrupt_frame_i_2_n_0));
  LUT4 #(
    .INIT(16'h88A8)) 
    interrupt_frame_i_3
       (.I0(m00_axis_aresetn),
        .I1(interrupt_frame),
        .I2(interrupt_frame_i_12_n_0),
        .I3(interrupt_frame_i_2_n_0),
        .O(interrupt_frame_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFE0)) 
    interrupt_frame_i_4
       (.I0(cnt100_reg[1]),
        .I1(cnt100_reg[0]),
        .I2(cnt100_reg[5]),
        .I3(cnt100_reg[6]),
        .O(interrupt_frame_i_4_n_0));
  LUT6 #(
    .INIT(64'h4040444000000000)) 
    interrupt_frame_i_5
       (.I0(cnt100_reg[6]),
        .I1(m00_axis_aresetn),
        .I2(interrupt_frame),
        .I3(interrupt_frame_i_12_n_0),
        .I4(interrupt_frame_i_2_n_0),
        .I5(interrupt_frame_i_13_n_0),
        .O(interrupt_frame_i_5_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_6
       (.I0(cnt100_reg[23]),
        .I1(cnt100_reg[22]),
        .I2(cnt100_reg[25]),
        .I3(cnt100_reg[24]),
        .O(interrupt_frame_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_7
       (.I0(cnt100_reg[27]),
        .I1(cnt100_reg[26]),
        .I2(cnt100_reg[29]),
        .I3(cnt100_reg[28]),
        .O(interrupt_frame_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFF7)) 
    interrupt_frame_i_8
       (.I0(cnt100_reg[15]),
        .I1(cnt100_reg[14]),
        .I2(cnt100_reg[17]),
        .I3(cnt100_reg[16]),
        .O(interrupt_frame_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    interrupt_frame_i_9
       (.I0(cnt100_reg[19]),
        .I1(cnt100_reg[18]),
        .I2(cnt100_reg[21]),
        .I3(cnt100_reg[20]),
        .O(interrupt_frame_i_9_n_0));
  FDRE interrupt_frame_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(interrupt_frame_i_1_n_0),
        .Q(interrupt_frame),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    reset_cnt_trig_i_1
       (.I0(azimut_0),
        .I1(azimut_0_prev),
        .O(reset_cnt_trig0));
  FDRE #(
    .INIT(1'b0)) 
    reset_cnt_trig_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(reset_cnt_trig0),
        .Q(reset_cnt_trig_ila),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_averageFFT_0_0,averageFFT_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "averageFFT_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    allowed_clk,
    azimut_0,
    m00_axis_aresetn,
    azimut_ila,
    low_azimut_ila,
    m00_axis_aclk,
    clk_10MHz,
    interrupt_frame,
    reset_cnt_trig_ila,
    azimut,
    azimut8);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TDATA" *) input [63:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TSTRB" *) input [7:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input azimut_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1" *) input m00_axis_aresetn;
  output [15:0]azimut_ila;
  output [15:0]low_azimut_ila;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF s00_axis:m00_axis, FREQ_HZ 1e+08, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 interrupt_frame INTERRUPT" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME interrupt_frame, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output interrupt_frame;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset_cnt_trig_ila RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset_cnt_trig_ila, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output reset_cnt_trig_ila;
  output [31:0]azimut;
  output [31:0]azimut8;

  wire \<const0> ;
  wire \<const1> ;
  wire allowed_clk;
  wire [2:0]\^azimut ;
  wire [15:0]\^azimut8 ;
  wire azimut_0;
  wire clk_10MHz;
  wire interrupt_frame;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire m00_axis_tready;
  wire reset_cnt_trig_ila;

  assign azimut[31] = \<const0> ;
  assign azimut[30] = \<const0> ;
  assign azimut[29] = \<const0> ;
  assign azimut[28] = \<const0> ;
  assign azimut[27] = \<const0> ;
  assign azimut[26] = \<const0> ;
  assign azimut[25] = \<const0> ;
  assign azimut[24] = \<const0> ;
  assign azimut[23] = \<const0> ;
  assign azimut[22] = \<const0> ;
  assign azimut[21] = \<const0> ;
  assign azimut[20] = \<const0> ;
  assign azimut[19] = \<const0> ;
  assign azimut[18] = \<const0> ;
  assign azimut[17] = \<const0> ;
  assign azimut[16] = \<const0> ;
  assign azimut[15] = \<const0> ;
  assign azimut[14] = \<const0> ;
  assign azimut[13] = \<const0> ;
  assign azimut[12] = \<const0> ;
  assign azimut[11] = \<const0> ;
  assign azimut[10] = \<const0> ;
  assign azimut[9] = \<const0> ;
  assign azimut[8] = \<const0> ;
  assign azimut[7] = \<const0> ;
  assign azimut[6] = \<const0> ;
  assign azimut[5] = \<const0> ;
  assign azimut[4] = \<const0> ;
  assign azimut[3] = \<const0> ;
  assign azimut[2:0] = \^azimut [2:0];
  assign azimut8[31] = \<const0> ;
  assign azimut8[30] = \<const0> ;
  assign azimut8[29] = \<const0> ;
  assign azimut8[28] = \<const0> ;
  assign azimut8[27] = \<const0> ;
  assign azimut8[26] = \<const0> ;
  assign azimut8[25] = \<const0> ;
  assign azimut8[24] = \<const0> ;
  assign azimut8[23] = \<const0> ;
  assign azimut8[22] = \<const0> ;
  assign azimut8[21] = \<const0> ;
  assign azimut8[20] = \<const0> ;
  assign azimut8[19] = \<const0> ;
  assign azimut8[18] = \<const0> ;
  assign azimut8[17] = \<const0> ;
  assign azimut8[16] = \<const0> ;
  assign azimut8[15:0] = \^azimut8 [15:0];
  assign azimut_ila[15] = \<const0> ;
  assign azimut_ila[14] = \<const0> ;
  assign azimut_ila[13] = \<const0> ;
  assign azimut_ila[12] = \<const0> ;
  assign azimut_ila[11] = \<const0> ;
  assign azimut_ila[10] = \<const0> ;
  assign azimut_ila[9] = \<const0> ;
  assign azimut_ila[8] = \<const0> ;
  assign azimut_ila[7] = \<const0> ;
  assign azimut_ila[6] = \<const0> ;
  assign azimut_ila[5] = \<const0> ;
  assign azimut_ila[4] = \<const0> ;
  assign azimut_ila[3] = \<const0> ;
  assign azimut_ila[2] = \<const0> ;
  assign azimut_ila[1] = \<const0> ;
  assign azimut_ila[0] = \<const0> ;
  assign m00_axis_tdata[31] = \<const0> ;
  assign m00_axis_tdata[30] = \<const0> ;
  assign m00_axis_tdata[29] = \<const0> ;
  assign m00_axis_tdata[28] = \<const0> ;
  assign m00_axis_tdata[27] = \<const0> ;
  assign m00_axis_tdata[26] = \<const0> ;
  assign m00_axis_tdata[25] = \<const0> ;
  assign m00_axis_tdata[24] = \<const0> ;
  assign m00_axis_tdata[23] = \<const0> ;
  assign m00_axis_tdata[22] = \<const0> ;
  assign m00_axis_tdata[21] = \<const0> ;
  assign m00_axis_tdata[20] = \<const0> ;
  assign m00_axis_tdata[19] = \<const0> ;
  assign m00_axis_tdata[18] = \<const0> ;
  assign m00_axis_tdata[17] = \<const0> ;
  assign m00_axis_tdata[16] = \<const0> ;
  assign m00_axis_tdata[15] = \<const0> ;
  assign m00_axis_tdata[14] = \<const0> ;
  assign m00_axis_tdata[13] = \<const0> ;
  assign m00_axis_tdata[12] = \<const0> ;
  assign m00_axis_tdata[11] = \<const0> ;
  assign m00_axis_tdata[10] = \<const0> ;
  assign m00_axis_tdata[9] = \<const0> ;
  assign m00_axis_tdata[8] = \<const0> ;
  assign m00_axis_tdata[7] = \<const0> ;
  assign m00_axis_tdata[6] = \<const0> ;
  assign m00_axis_tdata[5] = \<const0> ;
  assign m00_axis_tdata[4] = \<const0> ;
  assign m00_axis_tdata[3] = \<const0> ;
  assign m00_axis_tdata[2] = \<const0> ;
  assign m00_axis_tdata[1] = \<const0> ;
  assign m00_axis_tdata[0] = \<const0> ;
  assign m00_axis_tlast = \<const0> ;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign m00_axis_tvalid = \<const0> ;
  assign s00_axis_tready = m00_axis_tready;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 inst
       (.allowed_clk(allowed_clk),
        .azimut8(\^azimut8 ),
        .azimut_0(azimut_0),
        .\azimut_r_reg[0]_0 (\^azimut [0]),
        .\azimut_r_reg[1]_0 (\^azimut [1]),
        .\azimut_r_reg[2]_0 (\^azimut [2]),
        .clk_10MHz(clk_10MHz),
        .interrupt_frame(interrupt_frame),
        .m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .reset_cnt_trig_ila(reset_cnt_trig_ila));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Tue Nov 30 13:07:39 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_2_AD9650_0_0_sim_netlist.v
// Design      : design_2_AD9650_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0
   (m01_fft_axis_tdata,
    DATA_INA,
    mux_fft,
    DATA_INB);
  output [31:0]m01_fft_axis_tdata;
  input [15:0]DATA_INA;
  input mux_fft;
  input [15:0]DATA_INB;

  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire [31:0]m01_fft_axis_tdata;
  wire mux_fft;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[0]_INST_0 
       (.I0(DATA_INA[0]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[0]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[10]_INST_0 
       (.I0(DATA_INA[10]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[10]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[11]_INST_0 
       (.I0(DATA_INA[11]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[11]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[12]_INST_0 
       (.I0(DATA_INA[12]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[12]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[13]_INST_0 
       (.I0(DATA_INA[13]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[13]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[14]_INST_0 
       (.I0(DATA_INA[14]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[14]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[15]_INST_0 
       (.I0(DATA_INA[15]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[15]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[16]_INST_0 
       (.I0(DATA_INB[0]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[16]));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[17]_INST_0 
       (.I0(DATA_INB[1]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[17]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[18]_INST_0 
       (.I0(DATA_INB[2]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[18]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[19]_INST_0 
       (.I0(DATA_INB[3]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[19]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[1]_INST_0 
       (.I0(DATA_INA[1]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[1]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[20]_INST_0 
       (.I0(DATA_INB[4]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[20]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[21]_INST_0 
       (.I0(DATA_INB[5]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[21]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[22]_INST_0 
       (.I0(DATA_INB[6]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[22]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[23]_INST_0 
       (.I0(DATA_INB[7]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[23]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[24]_INST_0 
       (.I0(DATA_INB[8]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[24]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[25]_INST_0 
       (.I0(DATA_INB[9]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[25]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[26]_INST_0 
       (.I0(DATA_INB[10]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[26]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[27]_INST_0 
       (.I0(DATA_INB[11]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[27]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[28]_INST_0 
       (.I0(DATA_INB[12]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[28]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[29]_INST_0 
       (.I0(DATA_INB[13]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[29]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[2]_INST_0 
       (.I0(DATA_INA[2]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[2]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[30]_INST_0 
       (.I0(DATA_INB[14]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[30]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[31]_INST_0 
       (.I0(DATA_INB[15]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[31]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[3]_INST_0 
       (.I0(DATA_INA[3]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[4]_INST_0 
       (.I0(DATA_INA[4]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[4]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[5]_INST_0 
       (.I0(DATA_INA[5]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[5]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[6]_INST_0 
       (.I0(DATA_INA[6]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[6]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[7]_INST_0 
       (.I0(DATA_INA[7]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[7]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[8]_INST_0 
       (.I0(DATA_INA[8]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[8]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \m01_fft_axis_tdata[9]_INST_0 
       (.I0(DATA_INA[9]),
        .I1(mux_fft),
        .O(m01_fft_axis_tdata[9]));
endmodule

(* CHECK_LICENSE_TYPE = "design_2_AD9650_0_0,AD9650_v2_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "AD9650_v2_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_10MHz,
    m00_fft_axis_tvalid,
    m00_fft_axis_tdata,
    m00_fft_axis_tstrb,
    m00_fft_axis_tlast,
    m00_fft_axis_tready,
    m01_fft_axis_tvalid,
    m01_fft_axis_tdata,
    m01_fft_axis_tstrb,
    m01_fft_axis_tlast,
    m01_fft_axis_tready,
    dco_or_dcoa,
    dco_or_dcob,
    dco_or_ora,
    dco_or_orb,
    ADC_PDwN,
    SYNC,
    DATA_INA,
    DATA_INB,
    allowed_clk,
    mux_fft,
    s00_axi_aclk,
    s00_axi_aresetn,
    DCO);
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TVALID" *) output m00_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TDATA" *) output [31:0]m00_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TSTRB" *) output [3:0]m00_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TLAST" *) output m00_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_fft_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TVALID" *) output m01_fft_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TDATA" *) output [31:0]m01_fft_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TSTRB" *) output [3:0]m01_fft_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TLAST" *) output m01_fft_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m01_fft_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m01_fft_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, LAYERED_METADATA undef, INSERT_VIP 0" *) input m01_fft_axis_tready;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcoa" *) input dco_or_dcoa;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR dcob" *) input dco_or_dcob;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR ora" *) input dco_or_ora;
  (* X_INTERFACE_INFO = "user.org:interface:dco_or:1.0 DCO_OR orb" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME DCO_OR, SV_INTERFACE true" *) input dco_or_orb;
  output ADC_PDwN;
  output SYNC;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_allowed_clk_0, INSERT_VIP 0" *) input allowed_clk;
  input mux_fft;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI:m01_fft_axis:m00_fft_axis, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_2_m00_axis_aclk, INSERT_VIP 0" *) input s00_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output DCO;

  wire \<const0> ;
  wire \<const1> ;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire dco_or_dcoa;
  wire [31:0]m00_fft_axis_tdata;
  wire [31:0]m01_fft_axis_tdata;
  wire mux_fft;

  assign DCO = dco_or_dcoa;
  assign SYNC = \<const0> ;
  assign m00_fft_axis_tstrb[3] = \<const1> ;
  assign m00_fft_axis_tstrb[2] = \<const1> ;
  assign m00_fft_axis_tstrb[1] = \<const1> ;
  assign m00_fft_axis_tstrb[0] = \<const1> ;
  assign m01_fft_axis_tstrb[3] = \<const1> ;
  assign m01_fft_axis_tstrb[2] = \<const1> ;
  assign m01_fft_axis_tstrb[1] = \<const1> ;
  assign m01_fft_axis_tstrb[0] = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_AD9650_v2_0 inst
       (.DATA_INA(DATA_INA),
        .DATA_INB(DATA_INB),
        .m01_fft_axis_tdata(m01_fft_axis_tdata),
        .mux_fft(mux_fft));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[0]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[0]),
        .O(m00_fft_axis_tdata[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[10]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[10]),
        .O(m00_fft_axis_tdata[10]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[11]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[11]),
        .O(m00_fft_axis_tdata[11]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[12]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[12]),
        .O(m00_fft_axis_tdata[12]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[13]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[13]),
        .O(m00_fft_axis_tdata[13]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[14]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[14]),
        .O(m00_fft_axis_tdata[14]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[15]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[15]),
        .O(m00_fft_axis_tdata[15]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[16]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[0]),
        .O(m00_fft_axis_tdata[16]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[17]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[1]),
        .O(m00_fft_axis_tdata[17]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[18]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[2]),
        .O(m00_fft_axis_tdata[18]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[19]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[3]),
        .O(m00_fft_axis_tdata[19]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[1]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[1]),
        .O(m00_fft_axis_tdata[1]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[20]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[4]),
        .O(m00_fft_axis_tdata[20]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[21]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[5]),
        .O(m00_fft_axis_tdata[21]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[22]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[6]),
        .O(m00_fft_axis_tdata[22]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[23]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[7]),
        .O(m00_fft_axis_tdata[23]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[24]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[8]),
        .O(m00_fft_axis_tdata[24]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[25]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[9]),
        .O(m00_fft_axis_tdata[25]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[26]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[10]),
        .O(m00_fft_axis_tdata[26]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[27]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[11]),
        .O(m00_fft_axis_tdata[27]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[28]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[12]),
        .O(m00_fft_axis_tdata[28]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[29]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[13]),
        .O(m00_fft_axis_tdata[29]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[2]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[2]),
        .O(m00_fft_axis_tdata[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[30]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[14]),
        .O(m00_fft_axis_tdata[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[31]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INB[15]),
        .O(m00_fft_axis_tdata[31]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[3]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[3]),
        .O(m00_fft_axis_tdata[3]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[4]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[4]),
        .O(m00_fft_axis_tdata[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[5]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[5]),
        .O(m00_fft_axis_tdata[5]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[6]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[6]),
        .O(m00_fft_axis_tdata[6]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[7]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[7]),
        .O(m00_fft_axis_tdata[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[8]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[8]),
        .O(m00_fft_axis_tdata[8]));
  LUT2 #(
    .INIT(4'h8)) 
    \m00_fft_axis_tdata[9]_INST_0 
       (.I0(mux_fft),
        .I1(DATA_INA[9]),
        .O(m00_fft_axis_tdata[9]));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

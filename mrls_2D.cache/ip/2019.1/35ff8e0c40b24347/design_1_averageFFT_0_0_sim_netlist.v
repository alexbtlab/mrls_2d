// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Wed Dec  8 12:48:09 2021
// Host        : mlpc2 running 64-bit Ubuntu 18.04.5 LTS
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_averageFFT_0_0_sim_netlist.v
// Design      : design_1_averageFFT_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0
   (reset_cnt_trig_ila,
    azimut,
    allowed_clk,
    m00_axis_aresetn,
    clk_10MHz,
    azimut_0);
  output reset_cnt_trig_ila;
  output [1:0]azimut;
  input allowed_clk;
  input m00_axis_aresetn;
  input clk_10MHz;
  input azimut_0;

  wire allowed_clk;
  wire [1:0]azimut;
  wire azimut_0;
  wire azimut_0_prev;
  wire \azimut_r[0]_i_1_n_0 ;
  wire \azimut_r[1]_i_1_n_0 ;
  wire \azimut_r[1]_i_2_n_0 ;
  wire \azimut_r[1]_i_3_n_0 ;
  wire \azimut_r[1]_i_4_n_0 ;
  wire \azimut_r[1]_i_5_n_0 ;
  wire clk_10MHz;
  wire \cnt_low_allowed_clk[0]_i_1_n_0 ;
  wire \cnt_low_allowed_clk[0]_i_3_n_0 ;
  wire [15:0]cnt_low_allowed_clk_reg;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_0 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_1 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_2 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_3 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_4 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_5 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_6 ;
  wire \cnt_low_allowed_clk_reg[0]_i_2_n_7 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_1 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_2 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_3 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_4 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_5 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_6 ;
  wire \cnt_low_allowed_clk_reg[12]_i_1_n_7 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_0 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_1 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_2 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_3 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_4 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_5 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_6 ;
  wire \cnt_low_allowed_clk_reg[4]_i_1_n_7 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_0 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_1 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_2 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_3 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_4 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_5 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_6 ;
  wire \cnt_low_allowed_clk_reg[8]_i_1_n_7 ;
  wire m00_axis_aresetn;
  wire reset_cnt_trig0;
  wire reset_cnt_trig_ila;
  wire [3:3]\NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED ;

  FDRE #(
    .INIT(1'b0)) 
    azimut_0_prev_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(azimut_0),
        .Q(azimut_0_prev),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h000000006AAA0000)) 
    \azimut_r[0]_i_1 
       (.I0(azimut[0]),
        .I1(\azimut_r[1]_i_4_n_0 ),
        .I2(\azimut_r[1]_i_3_n_0 ),
        .I3(\azimut_r[1]_i_2_n_0 ),
        .I4(m00_axis_aresetn),
        .I5(reset_cnt_trig_ila),
        .O(\azimut_r[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h000000006AAAAAAA)) 
    \azimut_r[1]_i_1 
       (.I0(azimut[1]),
        .I1(\azimut_r[1]_i_2_n_0 ),
        .I2(\azimut_r[1]_i_3_n_0 ),
        .I3(\azimut_r[1]_i_4_n_0 ),
        .I4(azimut[0]),
        .I5(\azimut_r[1]_i_5_n_0 ),
        .O(\azimut_r[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \azimut_r[1]_i_2 
       (.I0(cnt_low_allowed_clk_reg[6]),
        .I1(cnt_low_allowed_clk_reg[7]),
        .I2(cnt_low_allowed_clk_reg[4]),
        .I3(cnt_low_allowed_clk_reg[5]),
        .I4(cnt_low_allowed_clk_reg[9]),
        .I5(cnt_low_allowed_clk_reg[8]),
        .O(\azimut_r[1]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0400)) 
    \azimut_r[1]_i_3 
       (.I0(cnt_low_allowed_clk_reg[0]),
        .I1(cnt_low_allowed_clk_reg[1]),
        .I2(cnt_low_allowed_clk_reg[2]),
        .I3(cnt_low_allowed_clk_reg[3]),
        .O(\azimut_r[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \azimut_r[1]_i_4 
       (.I0(cnt_low_allowed_clk_reg[12]),
        .I1(cnt_low_allowed_clk_reg[13]),
        .I2(cnt_low_allowed_clk_reg[10]),
        .I3(cnt_low_allowed_clk_reg[11]),
        .I4(cnt_low_allowed_clk_reg[15]),
        .I5(cnt_low_allowed_clk_reg[14]),
        .O(\azimut_r[1]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \azimut_r[1]_i_5 
       (.I0(reset_cnt_trig_ila),
        .I1(m00_axis_aresetn),
        .O(\azimut_r[1]_i_5_n_0 ));
  FDRE \azimut_r_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\azimut_r[0]_i_1_n_0 ),
        .Q(azimut[0]),
        .R(1'b0));
  FDRE \azimut_r_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\azimut_r[1]_i_1_n_0 ),
        .Q(azimut[1]),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hB)) 
    \cnt_low_allowed_clk[0]_i_1 
       (.I0(allowed_clk),
        .I1(m00_axis_aresetn),
        .O(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_low_allowed_clk[0]_i_3 
       (.I0(cnt_low_allowed_clk_reg[0]),
        .O(\cnt_low_allowed_clk[0]_i_3_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[0] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_7 ),
        .Q(cnt_low_allowed_clk_reg[0]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_low_allowed_clk_reg[0]_i_2_n_0 ,\cnt_low_allowed_clk_reg[0]_i_2_n_1 ,\cnt_low_allowed_clk_reg[0]_i_2_n_2 ,\cnt_low_allowed_clk_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_low_allowed_clk_reg[0]_i_2_n_4 ,\cnt_low_allowed_clk_reg[0]_i_2_n_5 ,\cnt_low_allowed_clk_reg[0]_i_2_n_6 ,\cnt_low_allowed_clk_reg[0]_i_2_n_7 }),
        .S({cnt_low_allowed_clk_reg[3:1],\cnt_low_allowed_clk[0]_i_3_n_0 }));
  FDRE \cnt_low_allowed_clk_reg[10] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_5 ),
        .Q(cnt_low_allowed_clk_reg[10]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[11] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_4 ),
        .Q(cnt_low_allowed_clk_reg[11]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[12] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_7 ),
        .Q(cnt_low_allowed_clk_reg[12]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[12]_i_1 
       (.CI(\cnt_low_allowed_clk_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_low_allowed_clk_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_low_allowed_clk_reg[12]_i_1_n_1 ,\cnt_low_allowed_clk_reg[12]_i_1_n_2 ,\cnt_low_allowed_clk_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_low_allowed_clk_reg[12]_i_1_n_4 ,\cnt_low_allowed_clk_reg[12]_i_1_n_5 ,\cnt_low_allowed_clk_reg[12]_i_1_n_6 ,\cnt_low_allowed_clk_reg[12]_i_1_n_7 }),
        .S(cnt_low_allowed_clk_reg[15:12]));
  FDRE \cnt_low_allowed_clk_reg[13] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_6 ),
        .Q(cnt_low_allowed_clk_reg[13]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[14] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_5 ),
        .Q(cnt_low_allowed_clk_reg[14]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[15] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[12]_i_1_n_4 ),
        .Q(cnt_low_allowed_clk_reg[15]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[1] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_6 ),
        .Q(cnt_low_allowed_clk_reg[1]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[2] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_5 ),
        .Q(cnt_low_allowed_clk_reg[2]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[3] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[0]_i_2_n_4 ),
        .Q(cnt_low_allowed_clk_reg[3]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[4] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_7 ),
        .Q(cnt_low_allowed_clk_reg[4]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[4]_i_1 
       (.CI(\cnt_low_allowed_clk_reg[0]_i_2_n_0 ),
        .CO({\cnt_low_allowed_clk_reg[4]_i_1_n_0 ,\cnt_low_allowed_clk_reg[4]_i_1_n_1 ,\cnt_low_allowed_clk_reg[4]_i_1_n_2 ,\cnt_low_allowed_clk_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_low_allowed_clk_reg[4]_i_1_n_4 ,\cnt_low_allowed_clk_reg[4]_i_1_n_5 ,\cnt_low_allowed_clk_reg[4]_i_1_n_6 ,\cnt_low_allowed_clk_reg[4]_i_1_n_7 }),
        .S(cnt_low_allowed_clk_reg[7:4]));
  FDRE \cnt_low_allowed_clk_reg[5] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_6 ),
        .Q(cnt_low_allowed_clk_reg[5]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[6] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_5 ),
        .Q(cnt_low_allowed_clk_reg[6]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[7] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[4]_i_1_n_4 ),
        .Q(cnt_low_allowed_clk_reg[7]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  FDRE \cnt_low_allowed_clk_reg[8] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_7 ),
        .Q(cnt_low_allowed_clk_reg[8]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  CARRY4 \cnt_low_allowed_clk_reg[8]_i_1 
       (.CI(\cnt_low_allowed_clk_reg[4]_i_1_n_0 ),
        .CO({\cnt_low_allowed_clk_reg[8]_i_1_n_0 ,\cnt_low_allowed_clk_reg[8]_i_1_n_1 ,\cnt_low_allowed_clk_reg[8]_i_1_n_2 ,\cnt_low_allowed_clk_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_low_allowed_clk_reg[8]_i_1_n_4 ,\cnt_low_allowed_clk_reg[8]_i_1_n_5 ,\cnt_low_allowed_clk_reg[8]_i_1_n_6 ,\cnt_low_allowed_clk_reg[8]_i_1_n_7 }),
        .S(cnt_low_allowed_clk_reg[11:8]));
  FDRE \cnt_low_allowed_clk_reg[9] 
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(\cnt_low_allowed_clk_reg[8]_i_1_n_6 ),
        .Q(cnt_low_allowed_clk_reg[9]),
        .R(\cnt_low_allowed_clk[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    reset_cnt_trig_i_1
       (.I0(azimut_0),
        .I1(azimut_0_prev),
        .O(reset_cnt_trig0));
  FDRE #(
    .INIT(1'b0)) 
    reset_cnt_trig_reg
       (.C(clk_10MHz),
        .CE(1'b1),
        .D(reset_cnt_trig0),
        .Q(reset_cnt_trig_ila),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_averageFFT_0_0,averageFFT_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "averageFFT_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (s00_axis_tready,
    s00_axis_tdata,
    s00_axis_tstrb,
    s00_axis_tlast,
    s00_axis_tvalid,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tready,
    allowed_clk,
    azimut_0,
    m00_axis_aresetn,
    azimut_ila,
    low_azimut_ila,
    m00_axis_aclk,
    clk_10MHz,
    interrupt_frame,
    reset_cnt_trig_ila,
    allowed_clk_prev_ila,
    azimut,
    azimut8);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TREADY" *) output s00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TDATA" *) input [63:0]s00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TSTRB" *) input [7:0]s00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TLAST" *) input s00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s00_axis TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axis, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input s00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m00_axis TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 allowed_clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME allowed_clk, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input allowed_clk;
  input azimut_0;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0, PortWidth 1" *) input m00_axis_aresetn;
  output [15:0]azimut_ila;
  output [15:0]low_azimut_ila;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_RESET m00_axis_aresetn, ASSOCIATED_BUSIF s00_axis:m00_axis, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_processing_system7_0_0_FCLK_CLK0, INSERT_VIP 0" *) input m00_axis_aclk;
  input clk_10MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:interrupt:1.0 interrupt_frame INTERRUPT" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME interrupt_frame, SENSITIVITY LEVEL_HIGH, PortWidth 1" *) output interrupt_frame;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 reset_cnt_trig_ila RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME reset_cnt_trig_ila, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) output reset_cnt_trig_ila;
  output allowed_clk_prev_ila;
  output [31:0]azimut;
  output [31:0]azimut8;

  wire \<const0> ;
  wire \<const1> ;
  wire allowed_clk;
  wire [1:0]\^azimut ;
  wire azimut_0;
  wire clk_10MHz;
  wire m00_axis_aresetn;
  wire m00_axis_tready;
  wire reset_cnt_trig_ila;

  assign azimut[31] = \<const0> ;
  assign azimut[30] = \<const0> ;
  assign azimut[29] = \<const0> ;
  assign azimut[28] = \<const0> ;
  assign azimut[27] = \<const0> ;
  assign azimut[26] = \<const0> ;
  assign azimut[25] = \<const0> ;
  assign azimut[24] = \<const0> ;
  assign azimut[23] = \<const0> ;
  assign azimut[22] = \<const0> ;
  assign azimut[21] = \<const0> ;
  assign azimut[20] = \<const0> ;
  assign azimut[19] = \<const0> ;
  assign azimut[18] = \<const0> ;
  assign azimut[17] = \<const0> ;
  assign azimut[16] = \<const0> ;
  assign azimut[15] = \<const0> ;
  assign azimut[14] = \<const0> ;
  assign azimut[13] = \<const0> ;
  assign azimut[12] = \<const0> ;
  assign azimut[11] = \<const0> ;
  assign azimut[10] = \<const0> ;
  assign azimut[9] = \<const0> ;
  assign azimut[8] = \<const0> ;
  assign azimut[7] = \<const0> ;
  assign azimut[6] = \<const0> ;
  assign azimut[5] = \<const0> ;
  assign azimut[4] = \<const0> ;
  assign azimut[3] = \<const0> ;
  assign azimut[2] = \<const0> ;
  assign azimut[1:0] = \^azimut [1:0];
  assign azimut8[31] = \<const0> ;
  assign azimut8[30] = \<const0> ;
  assign azimut8[29] = \<const0> ;
  assign azimut8[28] = \<const0> ;
  assign azimut8[27] = \<const0> ;
  assign azimut8[26] = \<const0> ;
  assign azimut8[25] = \<const0> ;
  assign azimut8[24] = \<const0> ;
  assign azimut8[23] = \<const0> ;
  assign azimut8[22] = \<const0> ;
  assign azimut8[21] = \<const0> ;
  assign azimut8[20] = \<const0> ;
  assign azimut8[19] = \<const0> ;
  assign azimut8[18] = \<const0> ;
  assign azimut8[17] = \<const0> ;
  assign azimut8[16] = \<const0> ;
  assign azimut8[15] = \<const0> ;
  assign azimut8[14] = \<const0> ;
  assign azimut8[13] = \<const0> ;
  assign azimut8[12] = \<const0> ;
  assign azimut8[11] = \<const0> ;
  assign azimut8[10] = \<const0> ;
  assign azimut8[9] = \<const0> ;
  assign azimut8[8] = \<const0> ;
  assign azimut8[7] = \<const0> ;
  assign azimut8[6] = \<const0> ;
  assign azimut8[5] = \<const0> ;
  assign azimut8[4] = \<const0> ;
  assign azimut8[3] = \<const0> ;
  assign azimut8[2] = \<const0> ;
  assign azimut8[1] = \<const0> ;
  assign azimut8[0] = \<const0> ;
  assign azimut_ila[15] = \<const0> ;
  assign azimut_ila[14] = \<const0> ;
  assign azimut_ila[13] = \<const0> ;
  assign azimut_ila[12] = \<const0> ;
  assign azimut_ila[11] = \<const0> ;
  assign azimut_ila[10] = \<const0> ;
  assign azimut_ila[9] = \<const0> ;
  assign azimut_ila[8] = \<const0> ;
  assign azimut_ila[7] = \<const0> ;
  assign azimut_ila[6] = \<const0> ;
  assign azimut_ila[5] = \<const0> ;
  assign azimut_ila[4] = \<const0> ;
  assign azimut_ila[3] = \<const0> ;
  assign azimut_ila[2] = \<const0> ;
  assign azimut_ila[1] = \<const0> ;
  assign azimut_ila[0] = \<const0> ;
  assign interrupt_frame = \<const0> ;
  assign m00_axis_tdata[31] = \<const0> ;
  assign m00_axis_tdata[30] = \<const0> ;
  assign m00_axis_tdata[29] = \<const0> ;
  assign m00_axis_tdata[28] = \<const0> ;
  assign m00_axis_tdata[27] = \<const0> ;
  assign m00_axis_tdata[26] = \<const0> ;
  assign m00_axis_tdata[25] = \<const0> ;
  assign m00_axis_tdata[24] = \<const0> ;
  assign m00_axis_tdata[23] = \<const0> ;
  assign m00_axis_tdata[22] = \<const0> ;
  assign m00_axis_tdata[21] = \<const0> ;
  assign m00_axis_tdata[20] = \<const0> ;
  assign m00_axis_tdata[19] = \<const0> ;
  assign m00_axis_tdata[18] = \<const0> ;
  assign m00_axis_tdata[17] = \<const0> ;
  assign m00_axis_tdata[16] = \<const0> ;
  assign m00_axis_tdata[15] = \<const0> ;
  assign m00_axis_tdata[14] = \<const0> ;
  assign m00_axis_tdata[13] = \<const0> ;
  assign m00_axis_tdata[12] = \<const0> ;
  assign m00_axis_tdata[11] = \<const0> ;
  assign m00_axis_tdata[10] = \<const0> ;
  assign m00_axis_tdata[9] = \<const0> ;
  assign m00_axis_tdata[8] = \<const0> ;
  assign m00_axis_tdata[7] = \<const0> ;
  assign m00_axis_tdata[6] = \<const0> ;
  assign m00_axis_tdata[5] = \<const0> ;
  assign m00_axis_tdata[4] = \<const0> ;
  assign m00_axis_tdata[3] = \<const0> ;
  assign m00_axis_tdata[2] = \<const0> ;
  assign m00_axis_tdata[1] = \<const0> ;
  assign m00_axis_tdata[0] = \<const0> ;
  assign m00_axis_tlast = \<const0> ;
  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  assign m00_axis_tvalid = \<const0> ;
  assign s00_axis_tready = m00_axis_tready;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_averageFFT_v1_0 inst
       (.allowed_clk(allowed_clk),
        .azimut(\^azimut ),
        .azimut_0(azimut_0),
        .clk_10MHz(clk_10MHz),
        .m00_axis_aresetn(m00_axis_aresetn),
        .reset_cnt_trig_ila(reset_cnt_trig_ila));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

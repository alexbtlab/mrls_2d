
//module ADCsyncData(
//    input clk,
//    input reset,
//    input d_in,
//    output reg q_out
//    );
    
//    always @ (posedge clk)
//        begin
//            if (reset == 1'b1)
//                begin
//                    q_out <= 1'b0;
//                end
//            else 
//                begin
//                    q_out <= d_in;
//                end
//        end
//endmodule

`timescale 1 ns / 1 ps

	module ADCsyncData 
	(
		// Users to add ports here

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
//		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
//		input wire [2 : 0] s00_axi_awprot,
//		input wire  s00_axi_awvalid,
//		output wire  s00_axi_awready,
//		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
//		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
//		input wire  s00_axi_wvalid,
//		output wire  s00_axi_wready,
//		output wire [1 : 0] s00_axi_bresp,
//		output wire  s00_axi_bvalid,
//		input wire  s00_axi_bready,
//		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
//		input wire [2 : 0] s00_axi_arprot,
//		input wire  s00_axi_arvalid,
//		output wire  s00_axi_arready,
//		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
//		output wire [1 : 0] s00_axi_rresp,
//		output wire  s00_axi_rvalid,
//		input wire  s00_axi_rready,
		
		input wire  [15:0] DATA_CH1_in,
		input wire  [15:0] DATA_CH2_in,
		input wire  clkDCO_10MHz,
		input wire  clk_10MHz,
		input wire  clk_100MHz,
		
		output wire  [15:0] DATA_CH1_sync,
		output wire  [15:0] DATA_CH2_sync
//		output wire  clkDCO_10MHz,
//		output wire  clk_10MHz,
//		output wire  clk_10M0Hz
		
		
	);
// Instantiation of Axi Bus Interface S00_AXI
//	syncADCData_v1_0_S00_AXI # ( 
//		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
//		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
//	) syncADCData_v1_0_S00_AXI_inst (
//		.S_AXI_ACLK(s00_axi_aclk),
//		.S_AXI_ARESETN(s00_axi_aresetn),
//		.S_AXI_AWADDR(s00_axi_awaddr),
//		.S_AXI_AWPROT(s00_axi_awprot),
//		.S_AXI_AWVALID(s00_axi_awvalid),
//		.S_AXI_AWREADY(s00_axi_awready),
//		.S_AXI_WDATA(s00_axi_wdata),
//		.S_AXI_WSTRB(s00_axi_wstrb),
//		.S_AXI_WVALID(s00_axi_wvalid),
//		.S_AXI_WREADY(s00_axi_wready),
//		.S_AXI_BRESP(s00_axi_bresp),
//		.S_AXI_BVALID(s00_axi_bvalid),
//		.S_AXI_BREADY(s00_axi_bready),
//		.S_AXI_ARADDR(s00_axi_araddr),
//		.S_AXI_ARPROT(s00_axi_arprot),
//		.S_AXI_ARVALID(s00_axi_arvalid),
//		.S_AXI_ARREADY(s00_axi_arready),
//		.S_AXI_RDATA(s00_axi_rdata),
//		.S_AXI_RRESP(s00_axi_rresp),
//		.S_AXI_RVALID(s00_axi_rvalid),
//		.S_AXI_RREADY(s00_axi_rready)
//	);

reg [ 15:0 ] data_CH1_sync;
reg [ 15:0 ] data_CH2_sync;

assign DATA_CH1_sync = data_CH1_sync;
assign DATA_CH2_sync = data_CH2_sync;

reg [31:0] cntDCO;
reg [15:0] data_CH1_tmp;
reg [15:0] data_CH2_tmp;
/*----------------------------------------------------------------------------*/
always @ ( posedge s00_axi_aclk ) begin
       if ( s00_axi_aresetn ) begin
               if ( clkDCO_10MHz ) begin
                   if ( cntDCO != 1000)
                        cntDCO <= cntDCO + 1;
                   else
                        cntDCO <= 0;
               end
               else begin
                   cntDCO <= 0;
               end 
               if ( cntDCO == 2) begin
                   data_CH1_tmp <= DATA_CH1_in; 
                   data_CH2_tmp <= DATA_CH2_in;
               end
               else begin
                   data_CH1_tmp <= data_CH1_tmp; 
                   data_CH2_tmp <= data_CH2_tmp;              
               end
       end
       begin
           cntDCO <= 0;
           data_CH1_tmp  <= 0;
           data_CH2_tmp  <= 0;
       end 
end
///*----------------------------------------------------------------------------*/	
//always @ ( posedge s00_axi_aclk ) begin
//       if ( s00_axi_aresetn ) begin
//           data_CH1_sync <= data_CH1_tmp;
//           data_CH2_sync <= data_CH2_tmp;
//       end
//       begin
//           data_CH1_sync <= 0;
//           data_CH2_sync <= 0;
//       end
//end
endmodule

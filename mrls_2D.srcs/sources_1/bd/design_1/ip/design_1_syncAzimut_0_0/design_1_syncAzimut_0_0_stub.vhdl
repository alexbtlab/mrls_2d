-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Feb 26 18:39:47 2022
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               C:/project/mrls_2d/mrls_2D.srcs/sources_1/bd/design_1/ip/design_1_syncAzimut_0_0/design_1_syncAzimut_0_0_stub.vhdl
-- Design      : design_1_syncAzimut_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity design_1_syncAzimut_0_0 is
  Port ( 
    sysclk_100 : in STD_LOGIC;
    reset : in STD_LOGIC;
    async_azimut : in STD_LOGIC;
    sync_azimut : out STD_LOGIC
  );

end design_1_syncAzimut_0_0;

architecture stub of design_1_syncAzimut_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "sysclk_100,reset,async_azimut,sync_azimut";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "syncAzimut_v1_0,Vivado 2019.1";
begin
end;

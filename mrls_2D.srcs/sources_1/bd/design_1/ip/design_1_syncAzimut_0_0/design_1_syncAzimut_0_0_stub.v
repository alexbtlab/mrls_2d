// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Feb 26 18:39:47 2022
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/project/mrls_2d/mrls_2D.srcs/sources_1/bd/design_1/ip/design_1_syncAzimut_0_0/design_1_syncAzimut_0_0_stub.v
// Design      : design_1_syncAzimut_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "syncAzimut_v1_0,Vivado 2019.1" *)
module design_1_syncAzimut_0_0(sysclk_100, reset, async_azimut, sync_azimut)
/* synthesis syn_black_box black_box_pad_pin="sysclk_100,reset,async_azimut,sync_azimut" */;
  input sysclk_100;
  input reset;
  input async_azimut;
  output sync_azimut;
endmodule

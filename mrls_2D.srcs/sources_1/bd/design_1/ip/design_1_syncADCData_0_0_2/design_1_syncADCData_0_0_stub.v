// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Feb 26 18:39:47 2022
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/project/mrls_2d/mrls_2D.srcs/sources_1/bd/design_1/ip/design_1_syncADCData_0_0_2/design_1_syncADCData_0_0_stub.v
// Design      : design_1_syncADCData_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "syncADCData_v1_0,Vivado 2019.1" *)
module design_1_syncADCData_0_0(dco_or_dcoa, dco_or_dcob, dco_or_ora, 
  dco_or_orb, reset, DATA_CH1_out, DATA_CH2_out, clk_10MHz, clk_100MHz, DATA_CH1_in, DATA_CH2_in)
/* synthesis syn_black_box black_box_pad_pin="dco_or_dcoa,dco_or_dcob,dco_or_ora,dco_or_orb,reset,DATA_CH1_out[15:0],DATA_CH2_out[15:0],clk_10MHz,clk_100MHz,DATA_CH1_in[15:0],DATA_CH2_in[15:0]" */;
  input dco_or_dcoa;
  input dco_or_dcob;
  input dco_or_ora;
  input dco_or_orb;
  input reset;
  output [15:0]DATA_CH1_out;
  output [15:0]DATA_CH2_out;
  input clk_10MHz;
  input clk_100MHz;
  input [15:0]DATA_CH1_in;
  input [15:0]DATA_CH2_in;
endmodule

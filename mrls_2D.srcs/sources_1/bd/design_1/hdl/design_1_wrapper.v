//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Sat Feb 26 20:47:04 2022
//Host        : zl-04 running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (ADC_PDwN,
    ADC_SPI_cs,
    ADC_SPI_sck,
    ADC_SPI_sdio,
    ATTEN,
    DATA_INA,
    DATA_INB,
    DCO_OR_dcoa,
    DCO_OR_dcob,
    DCO_OR_ora,
    DCO_OR_orb,
    DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    IIC_scl_io,
    IIC_sda_io,
    IO_5,
    IO_7,
    LO_AMP_EN_FPGA,
    PAMP_EN,
    PLL_POW_EN,
    PLL_TRIG,
    RX_AMP_amp0,
    RX_AMP_amp1,
    RX_AMP_amp2,
    SPI_PLL_cen,
    SPI_PLL_ld_sdo,
    SPI_PLL_mosi,
    SPI_PLL_sck,
    SPI_PLL_sen,
    SW_1_sw1_if1_ctrl,
    SW_1_sw1_if2_ctrl,
    SW_2_sw2_if1_ctrl,
    SW_2_sw2_if2_ctrl,
    SW_3_sw3_if1_ctrl,
    SW_3_sw3_if2_ctrl,
    SW_BASE_sw_out_if1_a0,
    SW_BASE_sw_out_if1_a1,
    SW_BASE_sw_out_if2_a0,
    SW_BASE_sw_out_if2_a1,
    SYNC,
    UART_0_rxd,
    UART_0_txd,
    azimut_0);
  output ADC_PDwN;
  output ADC_SPI_cs;
  output ADC_SPI_sck;
  inout ADC_SPI_sdio;
  output [5:0]ATTEN;
  input [15:0]DATA_INA;
  input [15:0]DATA_INB;
  input DCO_OR_dcoa;
  input DCO_OR_dcob;
  input DCO_OR_ora;
  input DCO_OR_orb;
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  inout IIC_scl_io;
  inout IIC_sda_io;
  input IO_5;
  output IO_7;
  output [0:0]LO_AMP_EN_FPGA;
  output PAMP_EN;
  output PLL_POW_EN;
  output PLL_TRIG;
  output RX_AMP_amp0;
  output RX_AMP_amp1;
  output RX_AMP_amp2;
  output SPI_PLL_cen;
  input SPI_PLL_ld_sdo;
  output SPI_PLL_mosi;
  output SPI_PLL_sck;
  output SPI_PLL_sen;
  output SW_1_sw1_if1_ctrl;
  output SW_1_sw1_if2_ctrl;
  output SW_2_sw2_if1_ctrl;
  output SW_2_sw2_if2_ctrl;
  output SW_3_sw3_if1_ctrl;
  output SW_3_sw3_if2_ctrl;
  output SW_BASE_sw_out_if1_a0;
  output SW_BASE_sw_out_if1_a1;
  output SW_BASE_sw_out_if2_a0;
  output SW_BASE_sw_out_if2_a1;
  output SYNC;
  input UART_0_rxd;
  output UART_0_txd;
  input azimut_0;

  wire ADC_PDwN;
  wire ADC_SPI_cs;
  wire ADC_SPI_sck;
  wire ADC_SPI_sdio;
  wire [5:0]ATTEN;
  wire [15:0]DATA_INA;
  wire [15:0]DATA_INB;
  wire DCO_OR_dcoa;
  wire DCO_OR_dcob;
  wire DCO_OR_ora;
  wire DCO_OR_orb;
  wire [14:0]DDR_addr;
  wire [2:0]DDR_ba;
  wire DDR_cas_n;
  wire DDR_ck_n;
  wire DDR_ck_p;
  wire DDR_cke;
  wire DDR_cs_n;
  wire [3:0]DDR_dm;
  wire [31:0]DDR_dq;
  wire [3:0]DDR_dqs_n;
  wire [3:0]DDR_dqs_p;
  wire DDR_odt;
  wire DDR_ras_n;
  wire DDR_reset_n;
  wire DDR_we_n;
  wire FIXED_IO_ddr_vrn;
  wire FIXED_IO_ddr_vrp;
  wire [53:0]FIXED_IO_mio;
  wire FIXED_IO_ps_clk;
  wire FIXED_IO_ps_porb;
  wire FIXED_IO_ps_srstb;
  wire IIC_scl_i;
  wire IIC_scl_io;
  wire IIC_scl_o;
  wire IIC_scl_t;
  wire IIC_sda_i;
  wire IIC_sda_io;
  wire IIC_sda_o;
  wire IIC_sda_t;
  wire IO_5;
  wire IO_7;
  wire [0:0]LO_AMP_EN_FPGA;
  wire PAMP_EN;
  wire PLL_POW_EN;
  wire PLL_TRIG;
  wire RX_AMP_amp0;
  wire RX_AMP_amp1;
  wire RX_AMP_amp2;
  wire SPI_PLL_cen;
  wire SPI_PLL_ld_sdo;
  wire SPI_PLL_mosi;
  wire SPI_PLL_sck;
  wire SPI_PLL_sen;
  wire SW_1_sw1_if1_ctrl;
  wire SW_1_sw1_if2_ctrl;
  wire SW_2_sw2_if1_ctrl;
  wire SW_2_sw2_if2_ctrl;
  wire SW_3_sw3_if1_ctrl;
  wire SW_3_sw3_if2_ctrl;
  wire SW_BASE_sw_out_if1_a0;
  wire SW_BASE_sw_out_if1_a1;
  wire SW_BASE_sw_out_if2_a0;
  wire SW_BASE_sw_out_if2_a1;
  wire SYNC;
  wire UART_0_rxd;
  wire UART_0_txd;
  wire azimut_0;

  IOBUF IIC_scl_iobuf
       (.I(IIC_scl_o),
        .IO(IIC_scl_io),
        .O(IIC_scl_i),
        .T(IIC_scl_t));
  IOBUF IIC_sda_iobuf
       (.I(IIC_sda_o),
        .IO(IIC_sda_io),
        .O(IIC_sda_i),
        .T(IIC_sda_t));
  design_1 design_1_i
       (.ADC_PDwN(ADC_PDwN),
        .ADC_SPI_cs(ADC_SPI_cs),
        .ADC_SPI_sck(ADC_SPI_sck),
        .ADC_SPI_sdio(ADC_SPI_sdio),
        .ATTEN(ATTEN),
        .DATA_INA(DATA_INA),
        .DATA_INB(DATA_INB),
        .DCO_OR_dcoa(DCO_OR_dcoa),
        .DCO_OR_dcob(DCO_OR_dcob),
        .DCO_OR_ora(DCO_OR_ora),
        .DCO_OR_orb(DCO_OR_orb),
        .DDR_addr(DDR_addr),
        .DDR_ba(DDR_ba),
        .DDR_cas_n(DDR_cas_n),
        .DDR_ck_n(DDR_ck_n),
        .DDR_ck_p(DDR_ck_p),
        .DDR_cke(DDR_cke),
        .DDR_cs_n(DDR_cs_n),
        .DDR_dm(DDR_dm),
        .DDR_dq(DDR_dq),
        .DDR_dqs_n(DDR_dqs_n),
        .DDR_dqs_p(DDR_dqs_p),
        .DDR_odt(DDR_odt),
        .DDR_ras_n(DDR_ras_n),
        .DDR_reset_n(DDR_reset_n),
        .DDR_we_n(DDR_we_n),
        .FIXED_IO_ddr_vrn(FIXED_IO_ddr_vrn),
        .FIXED_IO_ddr_vrp(FIXED_IO_ddr_vrp),
        .FIXED_IO_mio(FIXED_IO_mio),
        .FIXED_IO_ps_clk(FIXED_IO_ps_clk),
        .FIXED_IO_ps_porb(FIXED_IO_ps_porb),
        .FIXED_IO_ps_srstb(FIXED_IO_ps_srstb),
        .IIC_scl_i(IIC_scl_i),
        .IIC_scl_o(IIC_scl_o),
        .IIC_scl_t(IIC_scl_t),
        .IIC_sda_i(IIC_sda_i),
        .IIC_sda_o(IIC_sda_o),
        .IIC_sda_t(IIC_sda_t),
        .IO_5(IO_5),
        .IO_7(IO_7),
        .LO_AMP_EN_FPGA(LO_AMP_EN_FPGA),
        .PAMP_EN(PAMP_EN),
        .PLL_POW_EN(PLL_POW_EN),
        .PLL_TRIG(PLL_TRIG),
        .RX_AMP_amp0(RX_AMP_amp0),
        .RX_AMP_amp1(RX_AMP_amp1),
        .RX_AMP_amp2(RX_AMP_amp2),
        .SPI_PLL_cen(SPI_PLL_cen),
        .SPI_PLL_ld_sdo(SPI_PLL_ld_sdo),
        .SPI_PLL_mosi(SPI_PLL_mosi),
        .SPI_PLL_sck(SPI_PLL_sck),
        .SPI_PLL_sen(SPI_PLL_sen),
        .SW_1_sw1_if1_ctrl(SW_1_sw1_if1_ctrl),
        .SW_1_sw1_if2_ctrl(SW_1_sw1_if2_ctrl),
        .SW_2_sw2_if1_ctrl(SW_2_sw2_if1_ctrl),
        .SW_2_sw2_if2_ctrl(SW_2_sw2_if2_ctrl),
        .SW_3_sw3_if1_ctrl(SW_3_sw3_if1_ctrl),
        .SW_3_sw3_if2_ctrl(SW_3_sw3_if2_ctrl),
        .SW_BASE_sw_out_if1_a0(SW_BASE_sw_out_if1_a0),
        .SW_BASE_sw_out_if1_a1(SW_BASE_sw_out_if1_a1),
        .SW_BASE_sw_out_if2_a0(SW_BASE_sw_out_if2_a0),
        .SW_BASE_sw_out_if2_a1(SW_BASE_sw_out_if2_a1),
        .SYNC(SYNC),
        .UART_0_rxd(UART_0_rxd),
        .UART_0_txd(UART_0_txd),
        .azimut_0(azimut_0));
endmodule

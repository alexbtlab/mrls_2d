//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
//Date        : Fri Feb  4 16:15:42 2022
//Host        : alexbtlab-System-Product-Name running 64-bit Ubuntu 20.04.3 LTS
//Command     : generate_target design_2_wrapper.bd
//Design      : design_2_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_2_wrapper
   (aclk_10MHz,
    allowed_clk,
    azimut_0,
    clk_10MHz,
    ext_load_sweep_val,
    fft_data_out,
    fft_tlast,
    fft_tvalid,
    fifo_in_tdata,
    frameSize,
    m00_axis_aclk,
    mux_fft,
    s00_axis_aresetn,
    sweep_val_ext);
  input aclk_10MHz;
  input allowed_clk;
  input azimut_0;
  input clk_10MHz;
  input ext_load_sweep_val;
  output [31:0]fft_data_out;
  output fft_tlast;
  output fft_tvalid;
  input [31:0]fifo_in_tdata;
  input [15:0]frameSize;
  input m00_axis_aclk;
  input mux_fft;
  input s00_axis_aresetn;
  input [15:0]sweep_val_ext;

  wire aclk_10MHz;
  wire allowed_clk;
  wire azimut_0;
  wire clk_10MHz;
  wire ext_load_sweep_val;
  wire [31:0]fft_data_out;
  wire fft_tlast;
  wire fft_tvalid;
  wire [31:0]fifo_in_tdata;
  wire [15:0]frameSize;
  wire m00_axis_aclk;
  wire mux_fft;
  wire s00_axis_aresetn;
  wire [15:0]sweep_val_ext;

  design_2 design_2_i
       (.aclk_10MHz(aclk_10MHz),
        .allowed_clk(allowed_clk),
        .azimut_0(azimut_0),
        .clk_10MHz(clk_10MHz),
        .ext_load_sweep_val(ext_load_sweep_val),
        .fft_data_out(fft_data_out),
        .fft_tlast(fft_tlast),
        .fft_tvalid(fft_tvalid),
        .fifo_in_tdata(fifo_in_tdata),
        .frameSize(frameSize),
        .m00_axis_aclk(m00_axis_aclk),
        .mux_fft(mux_fft),
        .s00_axis_aresetn(s00_axis_aresetn),
        .sweep_val_ext(sweep_val_ext));
endmodule
